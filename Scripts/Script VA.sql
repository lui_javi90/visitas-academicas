-- va = Visitas Academicas
--------------------------------Script Visitas Academicas--------------------------------
--Catalogo
CREATE TABLE pe_vinculacion.va_tipos_visitas_academicas
(
    id_tipo_visita_academica SERIAL NOT NULL,
    tipo_visita_academica VARCHAR(100) NOT NULL,
    CONSTRAINT pk_id_tipo_visita_academica PRIMARY KEY(id_tipo_visita_academica)
);

ALTER TABLE pe_vinculacion.va_tipos_visitas_academicas
ADD COLUMN tipo_valido boolean;


CREATE TABLE pe_vinculacion.va_solicitudes_visitas_academicas
(
    id_solicitud_visitas_academicas SERIAL NOT NULL,
    nombre_visita_academica VARCHAR(500) NOT NULL,
    periodo CHAR(1) NOT NULL,
    anio INTEGER NOT NULL,
    no_solicitud INTEGER NOT NULL,
    fecha_creacion_solicitud timestamp with time zone default now(),
    fecha_hora_salida_visita timestamp with time zone,
    fecha_hora_regreso_visita timestamp with time zone,
    id_tipo_visita_academica INTEGER NOT NULL,
    id_empresa_visita INTEGER NOT NULL,
    area_a_visitar VARCHAR,
    objetivo_visitar_area VARCHAR,
    no_alumnos INTEGER,
    observaciones_solicitud VARCHAR, --Tanto Jefe como Subdirector Academico
    valida_jefe_depto_academico timestamp with time zone,
    valida_subdirector_academico timestamp with time zone,
    CONSTRAINT pk_id_solicitud_visitas_academicas PRIMARY KEY(id_solicitud_visitas_academicas),
    CONSTRAINT fk1_id_tipo_visita_academica FOREIGN KEY(id_tipo_visita_academica) REFERENCES pe_vinculacion."va_tipos_visitas_academicas"(id_tipo_visita_academica),
    CONSTRAINT fk2_id_empresa_visita FOREIGN KEY(id_empresa_visita) REFERENCES pe_vinculacion."g_rpempresas"(idempresa)
);

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN ultima_fecha_modificacion timestamp with time zone;

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN doc_oficio_confirmacion_empresa VARCHAR(30);

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN ultima_mod_oficio_conf_empresa timestamp with time zone;

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN path_carpeta_oficio_confirmacion_empresa VARCHAR(18);

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN valida_jefe_oficina_externos_vinculacion timestamp with time zone;

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN valida_jefe_recursos_materiales timestamp with time zone;

alter table pe_vinculacion.va_solicitudes_visitas_academicas
 add constraint fk3_id_periodo
  foreign key (periodo)
  references public."E_periodos"("numPeriodo");

--Para saber que tipo de cambio se hizo en la solicitud (PENDIENTE)
CREATE TABLE pe_vinculacion.va_tipo_cambio_solicitud_visita_academica
(
    id_tipo_cambio_solicitud INTEGER NOT NULL,
    descripcion_del_cambio VARCHAR(80) NOT NULL,
    fecha_realizo_cambio_solicitud timestamp with time zone,
    CONSTRAINT pk_tipo_cambio_sol PRIMARY KEY(id_tipo_cambio_solicitud),
);

--Lista de alumnos que asisten a la visita academica
CREATE TABLE pe_vinculacion.va_alumnos_asisten_visitas_academicas
(
    id_solicitud_visitas_academicas INTEGER NOT NULL,
    "nctrAlumno" CHAR(9) NOT NULL,
    fecha_registro_alumno timestamp with time zone,
    CONSTRAINT ph_id_sol_alum_noctrl PRIMARY KEY(id_solicitud_visitas_academicas, "nctrAlumno"),
    CONSTRAINT fk1_idsol_alum_asit_ncalumno FOREIGN KEY("nctrAlumno") REFERENCES public."E_datosAlumno"("nctrAlumno"),
    CONSTRAINT fk2_id_sol_visitas_aca FOREIGN KEY(id_solicitud_visitas_academicas) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas)
);

alter table pe_vinculacion.va_alumnos_asisten_visitas_academicas
add constraint fk1_id_sol_visitas_academicas
foreign key (id_solicitud_visitas_academicas)
references pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas);

alter table pe_vinculacion.va_alumnos_asisten_visitas_academicas
add constraint fk2_idsol_alum_asit_nctralumno
foreign key ("nctrAlumno")
references public."E_datosAlumno"("nctrAlumno");

CREATE TABLE pe_vinculacion.va_responsables_visitas_academicas
(
    id_solicitud_visitas_academicas INTEGER NOT NULL,
    "rfcEmpleado" CHAR(13) NOT NULL,
    fecha_registro_responsable timestamp with time zone,
    CONSTRAINT pk_id_sol_res_rfcempleado PRIMARY KEY(id_solicitud_visitas_academicas, "rfcEmpleado"),
    CONSTRAINT fk1_id_sol_visitas_acad FOREIGN KEY(id_solicitud_visitas_academicas) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas),
    CONSTRAINT "fk2_idsol_resp_rfcEmpleado" FOREIGN KEY("rfcEmpleado") REFERENCES public."H_empleados"("rfcEmpleado")

);

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas
ADD COLUMN responsable_principal boolean;

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas
ADD COLUMN cve_depto_acad integer;

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas ALTER COLUMN cve_depto_acad SET NOT NULL;

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas
ADD COLUMN cve_depto integer;

CREATE TABLE pe_vinculacion.va_materias_imparte_responsable_visita_academica
(
	id_solicitud_materias_responsable_visitas_academicas INTEGER NOT NULL,
    "cveMateria" CHAR(4) NOT NULL,
    fecha_registro_materia_visita_academica timestamp with time zone,
    CONSTRAINT pk_id_sol_resp_mat_vis_acad PRIMARY KEY(id_solicitud_materias_responsable_visitas_academicas, "cveMateria"),
    CONSTRAINT fk1_id_sol_mat_resp_vis_aca FOREIGN KEY(id_solicitud_materias_responsable_visitas_academicas) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas),
    CONSTRAINT "fk2_cveMateria" FOREIGN KEY("cveMateria") REFERENCES public."E_catalogoMaterias"("cveMateria")
);

--Eliminar relaciones tablas "muchos a muchos"
/*ALTER TABLE pe_vinculacion.va_alumnos_asisten_visita_academica DROP CONSTRAINT fk2_id_sol_visitas_aca;

ALTER TABLE pe_vinculacion.va_alumnos_asisten_visita_academica DROP CONSTRAINT fk1_idsol_alum_asit_ncalumno;
drop table pe_vinculacion.va_alumnos_asisten_visita_academica;

---
ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas DROP CONSTRAINT fk1_id_sol_visitas_acad;
ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas DROP CONSTRAINT "fk2_idsol_resp_rfcEmpleado";

drop table pe_vinculacion.va_responsables_visitas_academicas;*/

CREATE TABLE pe_vinculacion.va_codigos_calidad_mod_visitas_academicas
(
	id_codigo_calidad_mod_va SERIAL NOT NULL,
    nombre_documento_digital VARCHAR(120),
    codigo_calidad VARCHAR(25),
    revision NUMERIC(2,0),
    CONSTRAINT pk_id_codigo_calidad_mod_va PRIMARY KEY(id_codigo_calidad_mod_va)
);

--Agregar campo tabla empresas para guardar el manifiesto de cada empresa con respecto a sus requisitos de seguridad
ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas
ADD COLUMN path_carpeta_manifiesto VARCHAR(18);

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas DROP COLUMN path_carpeta_manifiesto;

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas
ADD COLUMN politicas_seguridad_empresa VARCHAR(30);

ALTER TABLE pe_vinculacion.va_responsables_visitas_academicas DROP COLUMN politicas_seguridad_empresa;

---------------------------------Se agregaron despues por JAVISS cuando regreso---------------------------------

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN id_aut_salida_vehiculo INTEGER;

--Se agrega la llave foranea que relaciona la solicitude de vehiculo con la de visita academica (DUDA)
alter table pe_vinculacion.va_solicitudes_visitas_academicas
add constraint fk5_id_aut_salida_vehiculo
foreign key ("id_aut_salida_vehiculo")
references public."RM_aut_salidas"(id_aut_salida);

--ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas DROP CONSTRAINT fk5_id_aut_salida_vehiculo;

--Bitacora de cambios en la solicitud por parte de Roxana de Vinculacion (Agregar Tabla)
CREATE TABLE pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
(
    id_bitacora_solicitud_visita_academica SERIAL NOT NULL, --Clave primaria
    id_solicitud_visitas_academicas INTEGER NOT NULL, --Clave foranea
    fecha_hora_salida timestamp with time zone, --Campo que se pueden editar por Roxana de Vinculacion
    fecha_hora_regreso timestamp with time zone, --Campo que se pueden editar por Roxana de Vinculacion
    no_alumnos_visita INTEGER, --Campo que se pueden editar por Roxana de Vinculacion
    fecha_ultima_modificacion_solicitud timestamp with time zone default now(),
    usuario_modifica CHAR(13) NOT NULL,
    CONSTRAINT pk_id_bitacora_solicitud_visita_academica PRIMARY KEY(id_bitacora_solicitud_visita_academica),
    CONSTRAINT fk1_id_bit_sol_visit_acad FOREIGN KEY(id_solicitud_visitas_academicas) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas)
);

--Para saber cuando se modifica el documento
ALTER TABLE pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
ADD COLUMN modifico_doc_digital boolean;

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN id_estatus_solicitud_visita_academica INTEGER;

alter table pe_vinculacion.va_solicitudes_visitas_academicas
add constraint fk4_id_estatus_solicitud_visita_academica
foreign key (id_estatus_solicitud_visita_academica)
references pe_vinculacion.va_estatus_solicitud_visita_academica(id_estatus_solicitud_visita_academica);

--Estatus de la Solicitud de Visita Academica
CREATE TABLE pe_vinculacion.va_estatus_solicitud_visita_academica
(
	id_estatus_solicitud_visita_academica SERIAL NOT NULL,
	estatus VARCHAR(20) NOT NULL,
    descripcion_estatus VARCHAR(80),
    CONSTRAINT pk_id_estatus_solicitud_visita_acad PRIMARY KEY(id_estatus_solicitud_visita_academica)
);

INSERT INTO pe_vinculacion.va_estatus_solicitud_visita_academica VALUES
(1, 'PENDIENTE', null),
(2, 'ACEPTADA', null),
(3, 'SUSPENDIDA', null),
(4, 'CANCELADA', null);

--Para el reporte de incidencias de cada Solicitud
CREATE TABLE pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas
(
	id_reporte_resultados_incidencias INTEGER NOT NULL,
    fecha_creacion_reporte TIMESTAMP with time zone,
    unidades_materias_cubrieron VARCHAR,
    cumplieron_objetivos VARCHAR,
    descripcion_incidentes VARCHAR,
    valida_docente_reponsable TIMESTAMP with time zone, --docente tambien Valida escrito del reporte de incidencias
    CONSTRAINT pk_id_reporte_resultados_incidencias PRIMARY KEY(id_reporte_resultados_incidencias),
    CONSTRAINT fk1_id_reporte_resultados_incidencias FOREIGN KEY(id_reporte_resultados_incidencias) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas)

);


---------------------------------Se agregaron despues por JAVISS cuando regreso---------------------------------

-----------------------------------------------------------------------------------------------------------------
---------------------------------Consultas---------------------------------
--Obtener las materias impartidas por el reponsable de la solicitud de visita academica
select hemp."rfcEmpleado", ecm."cveMateria", ecm."dscMateria", ecm."deptoMateria"
from public."H_empleados" hemp
join public."E_gruposMaterias_new" egm
on egm."rfcEmpleadoMat" = hemp."rfcEmpleado"
join public."E_catalogoMaterias" ecm
on ecm."cveMateria" = egm."cveMateriaGpo"
where hemp."rfcEmpleado" = 'TOFR830919HN7' AND egm."perGrupoMat" = '1' AND egm."anioGrupoMat" = '2017'

--
select hemp."rfcEmpleado", ecm."cveMateria", ecm."dscMateria", ecm."deptoMateria"
from public."H_empleados" hemp
join public."E_gruposMaterias_new" egm
on egm."rfcEmpleadoMat" = hemp."rfcEmpleado"
join public."E_catalogoMaterias" ecm
on ecm."cveMateria" = egm."cveMateriaGpo"
where hemp."deptoCatedratico" = 8 AND ecm."statMateria" = true

select MAX(id_solicitud_visitas_academicas) as id_max from pe_vinculacion.va_solicitudes_visitas_academicas


--RFC suddirector academico
select * from public.h_ocupacionpuesto where
"cvePuestoGeneral" = '02' AND "cvePuestoParticular" = '02'

--RFC jefe depto academico
select * from public.h_ocupacionpuesto where "cvePuestoGeneral" = '03' AND "cvePuestoParticular" = '13';


----------------------------------------------------------------------------------------------
delete from pe_vinculacion.va_solicitudes_visitas_academicas
where id_solicitud_visitas_academicas = 3 and id_solicitud_visitas_academicas = 4;

--Eliminar campos
delete from pe_vinculacion.va_responsables_visitas_academicas
where id_solicitud_visitas_academicas = 3 and id_solicitud_visitas_academicas = 4;

delete from pe_vinculacion.va_materias_imparte_responsable_visita_academica
where id_solicitud_materias_responsable_visitas_academicas = 3 and
id_solicitud_materias_responsable_visitas_academicas = 4;

select MAX(no_solicitud) as id_max from pe_vinculacion.va_solicitudes_visitas_academicas

--Obtener las solicitudes por Empleado
select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where rva."rfcEmpleado" = 'TOFR830919HN7';

--Obtener el jefe de proyectos de Vinculacion dependiendo del depto al que pertenece el Empleado
select hemp."rfcEmpleado",
(select "cveDepartamento" from public."H_departamentos" where hemp."deptoCatedratico" = "cveDeptoAcad") cve_depto_academico
from public."H_empleados" hemp
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva."rfcEmpleado" = hemp."rfcEmpleado"
where



--Mostrar las solicitudes que le pertenezcan al jefe de proyectos de vinculacion de su departamento academico
select distinct (sva.id_solicitud_visitas_academicas), sva.*
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
join public."H_empleados" hemp
on hemp."rfcEmpleado" = rva."rfcEmpleado"
where rva.cve_depto_acad = 8 AND hemp."cveDepartamentoEmp" = 25


--Filtrado por departamento
select distinct (sva.id_solicitud_visitas_academicas), sva.*
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
join public."H_empleados" hemp
on hemp."rfcEmpleado" = rva."rfcEmpleado"
where rva.cve_depto = 6 AND rva.responsable_principal = true




--Obtener si el empleado es jefe de proyectos de vinculacion
select hemp."rfcEmpleado", dep."cveDeptoAcad", dep."cveDepartamento"
from public."H_empleados" hemp
join public."H_departamentos" dep
on dep."cveDepartamento" = hemp."cveDepartamentoEmp"
where hemp."cvePuestoGeneral" = '04' AND hemp."cvePuestoParticular" = '33' AND
	hemp."rfcEmpleado" = 'AAAC701018HU4'



--RFC--
--TOFR830919HN7
--HEMJ570525RQ2
--AAAC701018HU4
--LAEM561110770
--AERT581003JM9
--CAGK7910131J6
--HENC850905TC5

--Solicitudes que han sido validadas y que solo muestre el responsable principal
select * from pe_vinculacion.va_responsables_visitas_academicas rva
join pe_vinculacion.va_solicitudes_visitas_academicas sva
on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
where sva.valida_jefe_depto_academico != null AND sva.valida_subdirector_academico != NULL AND
rva.responsable_principal = true;

select * from pe_vinculacion.va_responsables_visitas_academicas
where id_solicitud_visitas_academicas = 11

select * from pe_vinculacion.va_responsables_visitas_academicas rva
join public."H_empleados" hemp
on hemp."rfcEmpleado" = rva."rfcEmpleado"
where  rva.id_solicitud_visitas_academicas = '10' AND rva.responsable_principal = true

select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where rva.id_solicitud_visitas_academicas = 11

--Obtener los departamentos que pertenecen a una academia
select * from public."H_departamentos" where "cveDeptoAcad" != -1 AND "cveDeptoAcad" != -2 AND "cveDeptoAcad" != 99

--Verificar si una empresa ya ha sido asignada a una solicitud
select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.g_rpempresas remp
on remp.idempresa = sva.id_empresa_visita
where sva.id_empresa_visita = 2


--Obtener los responsables de las solicitudes
select * from pe_vinculacion.va_responsables_visitas_academicas rva
join pe_vinculacion.va_solicitudes_visitas_academicas sva
on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
where rva.id_solicitud_visitas_academicas = 11;

--------Consulta para llenar documento Solicitud de Visita Academica


--Lista Estudiantes que asisten a la Visita Academica PDF
select
(select "dscDepartamento" from public."H_departamentos" where "cveDepartamento" = rva.cve_depto_acad ) as depto_doc_responsable,
sva.nombre_visita_academica,
(select domicilio from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita) as domicilio,
(select "nmbCompletoEmp" from public."H_empleados" where "rfcEmpleado" = rva."rfcEmpleado" ) docente_responsable,
(select sva.fecha_hora_salida_visita::timestamp::date) as fecha_desde_visita,
(select sva.fecha_hora_regreso_visita::timestamp::date) as fecha_hasta_visita,
(
	(case when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
    (extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
    (case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
    (extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
    (case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)

) as hora_desde,
(
	(case when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
    (extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
    (case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
    (extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
    (case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)

) as hora_hasta,
(
	(case when sva.fecha_creacion_solicitud is null then ' ' ELSE(
             extract('day' from sva.fecha_creacion_solicitud) ||' de '|| ( case
            when extract('month' from sva.fecha_creacion_solicitud)=1 then 'Enero'
            when extract('month' from sva.fecha_creacion_solicitud)=2 then 'Febrero'
            when extract('month' from sva.fecha_creacion_solicitud)=3 then 'Marzo'
            when extract('month' from sva.fecha_creacion_solicitud)=4 then 'Abril'
            when extract('month' from sva.fecha_creacion_solicitud)=5 then 'Mayo'
            when extract('month' from sva.fecha_creacion_solicitud)=6 then 'Junio'
            when extract('month' from sva.fecha_creacion_solicitud)=7 then 'Julio'
            when extract('month' from sva.fecha_creacion_solicitud)=8 then 'Agosto'
            when extract('month' from sva.fecha_creacion_solicitud)=9 then 'Septiembre'
            when extract('month' from sva.fecha_creacion_solicitud)=10 then 'Octubre'
            when extract('month' from sva.fecha_creacion_solicitud)=11 then 'Noviembre'
            when extract('month' from sva.fecha_creacion_solicitud)=12 then 'Diciembre'
            END)||' de '|| extract('year' from sva.fecha_creacion_solicitud) || ' a las ' || ( case
            when extract('hour' from sva.fecha_creacion_solicitud) < 10 then '0' else '' end) ||
            (extract('hour' from sva.fecha_creacion_solicitud)) || ':' ||
            (case when extract('minute' from sva.fecha_creacion_solicitud) < 10 then '0' else '' end) ||
            (extract('minute' from sva.fecha_creacion_solicitud)) || ' ' ||
            (case when extract('hour' from sva.fecha_creacion_solicitud) >= 12 then 'PM' else 'AM' end)
    )end)
) as valida_docente_responsable
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where sva.id_solicitud_visitas_academicas = 11 AND rva.responsable_principal = true;



--LIsta de alumnos que estan inscritos en la visita academica PDF
select
(eda."nmbAlumno") as nombre_alumno,
(eda."nctrAlumno") as no_control,
(select "dscEspecialidad" from public."E_especialidad" where "cveEspecialidad" = eda."cveEspecialidadAlu" ) as carrera_alumno,
(eda."semAlumno") as semestre_alumno
from public."E_datosAlumno" eda
join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
on aava."nctrAlumno" = eda."nctrAlumno"
where aava.id_solicitud_visitas_academicas = 18


--Consulta obtener datos de la solicitud de visita academica PDF
select
emp.nombre_presenta as encargado,
emp.cargo_presenta as cargo,
emp.nombre as empresa,
( (select desc_mun from public."X_municipios" where id_municipio = emp.idmunicipio) || ', ' ||
(select desc_estado from public."X_estados" where id_estado = emp.idestado ) ) as lugar_empresa,
sva.no_alumnos as total_alumnos,
sva.no_solicitud,
sva.area_a_visitar as area_visitar,
sva.objetivo_visitar_area as objetivo_visita,
(
	(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_salida_visita) || ' a las ' || ( case
            when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
            (case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
            (case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)
    )end)

) as fecha_hora_salida_visita,
(
	(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_regreso_visita) || ' a las ' || ( case
            when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
            (case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
            (case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)
    )end)

) as fecha_hora_regreso_visita,
(select "nmbCompletoEmp" from public.h_ocupacionpuesto where "cvePuestoGeneral" = '04'
AND "cvePuestoParticular" = '03' AND "cveDepartamentoEmp" = 6 ) as jefa_of_servicios_externos
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.g_rpempresas emp
on emp.idempresa = sva.id_empresa_visita
where sva.id_solicitud_visitas_academicas = 11;



--Lista de los docentes responsables de la visita academica para PDF
select rva."rfcEmpleado", emp."nmbCompletoEmp" from pe_vinculacion.va_responsables_visitas_academicas rva
join public."H_empleados" emp
on emp."rfcEmpleado" = rva."rfcEmpleado"
where rva.id_solicitud_visitas_academicas = 11


--Lista de carreras de los alumnos que asisten a la Visita Academica para PDF
select DISTINCT(esp."cveEspecialidad"), esp."dscEspecialidad" from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
on aava.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
join public."E_datosAlumno" eda
on eda."nctrAlumno" = aava."nctrAlumno"
join public."E_especialidad" esp
on esp."cveEspecialidad" = eda."cveEspecialidadAlu"
where sva.id_solicitud_visitas_academicas = 18;

--Reportes de Presentacion y Agradecimiento PDF
select sva.no_solicitud,
(emp.nombre_presenta) as encargado,
(emp.cargo_presenta) as cargo,
(emp.nombre) as empresa,
sva.no_alumnos,
( (select desc_mun from public."X_municipios" where id_municipio = emp.idmunicipio) || ', ' ||
(select desc_estado from public."X_estados" where id_estado = emp.idestado ) ) as lugar_empresa,
(
	(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_salida_visita) || ' a las ' || ( case
            when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
            (case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
            (case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)
    )end)
) as hora_salida_visita,
(
	(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_regreso_visita) || ' a las ' || ( case
            when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
            (case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
            (case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)
    )end)

) as fecha_hora_regreso_visita
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.g_rpempresas emp
on emp.idempresa = sva.id_empresa_visita
where sva.id_solicitud_visitas_academicas = 18;

--Reporte de Resultados e Incidentes en Visita Academica
select
(
	select hemp."nmbCompletoEmp" from public."H_empleados" hemp
    join pe_vinculacion.va_responsables_visitas_academicas rva
    on hemp."rfcEmpleado" = rva."rfcEmpleado"
    where rva.responsable_principal = true
    and rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas

) as docente_responsable,
sva.no_alumnos,
(
	(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_salida_visita)
    )end)
) as fecha_salida_visita,
(
	(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
            extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
            when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
            when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
            when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
            when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
            when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
            when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
            when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
            when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
            when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
            when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
            when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from sva.fecha_hora_regreso_visita)
    )end)

) as fecha_regreso_visita,
(
	( case when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
        (extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
        (case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
        (extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
        (case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)

) as horario_salida_visita,
(
	( case when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
        (extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
        (case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
        (extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
        (case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)

) as horario_regreso_visita,
( select nombre from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita ) as nombre_empresa,
riva.unidades_materias_cubrieron,
riva.cumplieron_objetivos,
riva.descripcion_incidentes,
riva.valida_docente_reponsable
from pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas riva
join pe_vinculacion.va_solicitudes_visitas_academicas sva
on sva.id_solicitud_visitas_academicas = riva.id_reporte_resultados_incidencias
where riva.id_reporte_resultados_incidencias = 18;


--Lista de materias que cubre la Visita Academica
select ecm."dscMateria", ecm."semMateria"
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_materias_imparte_responsable_visita_academica mirva
on mirva.id_solicitud_materias_responsable_visitas_academicas = sva.id_solicitud_visitas_academicas
join public."E_catalogoMaterias" ecm
on ecm."cveMateria" = mirva."cveMateria"
where mirva.id_solicitud_materias_responsable_visitas_academicas = 18;


--Obtener el departamento academico del docente
select
(
	select "dscDepartamento" from public."H_departamentos"
    where "cveDeptoAcad" = hemp."deptoCatedratico"
) as depto_academico
from public."H_empleados" hemp
where hemp."rfcEmpleado" = 'ROVG8201127G6';

--Obtener lista de A�os solicitudes
select DISTINCT(anio) from pe_vinculacion.va_solicitudes_visitas_academicas where id_solicitud_visitas_academicas > 0;
