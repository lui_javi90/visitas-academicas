-----TRIGGER PARA CAMBIAR EL ESTATUS DE LA SOLICITUD A ACEPTADA Y SE VALIDE PARA JEFE DE RECURSOS MATERIALES-----
CREATE OR REPLACE FUNCTION public."va_validaJefeMaterialesSolicitudVisitaAcademica"() RETURNS trigger AS
$javiss$
DECLARE

BEGIN

	--Si el campo del estatus de la solicitude automovil es vacia o nula se genera una excepcion
	IF New.id_aut_salida_estatu IS NULL THEN
          RAISE EXCEPTION 'Estatus de la Solicitud no puede ser NULO.';
    END IF;

    --Si Jefe Materiales pasa a estatus (8) AUTORIZADO
    IF (NEW.id_aut_salida_estatu = 8) THEN

      UPDATE pe_vinculacion.va_solicitudes_visitas_academicas
      SET valida_jefe_recursos_materiales = now(), id_estatus_solicitud_visita_academica = 2
      where fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL AND
      valida_subdirector_academico IS NOT NULL AND valida_jefe_oficina_externos_vinculacion IS NOT NULL AND
      valida_jefe_recursos_materiales IS NULL AND id_aut_salida_vehiculo = NEW.id_aut_salida;

    END IF;

    RETURN NEW;

END;
$javiss$ LANGUAGE plpgsql;

--DROP TRIGGER "validaJefeMaterialesSolicitudVisitaAcademica" on public."va_validaJefeMaterialesSolicitudVisitaAcademica";

CREATE TRIGGER "va_validaJefeMaterialesSolicitudVisitaAcademica"
AFTER UPDATE ON public."RM_aut_salidas"
FOR EACH ROW
EXECUTE PROCEDURE public."va_validaJefeMaterialesSolicitudVisitaAcademica"();
-----TRIGGER PARA CAMBIAR EL ESTATUS DE LA SOLICITUD A ACEPTADA Y SE VALIDE PARA JEFE DE RECURSOS MATERIALES-----

-----TRIGGER PARA CUANDO SE CAMBIA DE ESTATUS LA SOLICITUD DE VISITA ACADEMICA-----
CREATE OR REPLACE FUNCTION public."va_messageCambioEstatusSolicitudVisitaAcademica"() RETURNS trigger AS
$javiss$
DECLARE

	docente varchar(100);
    fecha TIMESTAMP with time zone;
    fecha_realiza varchar(100);
    message varchar(100);
    message_total varchar;

BEGIN

	--Si el campo del estatus de la solicitud
	IF New.id_estatus_solicitud_visita_academica IS NULL THEN
          RAISE EXCEPTION 'Estatus de la Solicitud no puede ser NULO.';
    END IF;

    --Obtenemos los mensajes pasados para concatenarlos con el que se va escribir en caso de el estatus cambie a 3 o 4
    message_total = ( select observaciones_solicitud from pe_vinculacion.va_solicitudes_visitas_academicas
                    where id_solicitud_visitas_academicas = NEW.id_solicitud_visitas_academicas );

	--SUSPENDIDA
    IF (NEW.id_estatus_solicitud_visita_academica = 3) THEN

      /*docente := 'Docente:' || 'Sistema';
      fecha = now();
      fecha_realiza = 'Fecha:' || fecha;*/
      message = 'Mensaje: Se SUSPENDIÓ la Solicitud de Visita Académica.';
      UPDATE pe_vinculacion.va_solicitudes_visitas_academicas
      SET observaciones_solicitud = message,
          id_estatus_solicitud_visita_academica = 3
      WHERE id_solicitud_visitas_academicas = OLD.id_solicitud_visitas_academicas;

    ELSEIF (NEW.id_estatus_solicitud_visita_academica = 4) THEN
	  /*docente = 'Docente:' || 'Sistema';
      fecha = now();
      fecha_realiza = 'Fecha:' || fecha;*/
      message = 'Mensaje: Se CANCELÓ la Solicitud de Visita Académica.';
      UPDATE pe_vinculacion.va_solicitudes_visitas_academicas
      SET observaciones_solicitud = message, id_estatus_solicitud_visita_academica = 4
      WHERE id_solicitud_visitas_academicas = OLD.id_solicitud_visitas_academicas;

    END IF;

    RETURN NEW;

END;
$javiss$ LANGUAGE plpgsql;

--DROP TRIGGER "va_validaJefeMaterialesSolicitudVisitaAcademica" on pe_vinculacion."va_validaJefeMaterialesSolicitudVisitaAcademica";

CREATE TRIGGER "va_messageCambioEstatusSolicitudVisitaAcademica"
AFTER UPDATE ON pe_vinculacion.va_solicitudes_visitas_academicas
FOR EACH ROW
EXECUTE PROCEDURE public."va_messageCambioEstatusSolicitudVisitaAcademica"();

-----TRIGGER PARA CUANDO SE CAMBIA DE ESTATUS LA SOLICITUD DE VISITA ACADEMICA-----

-----FUNCION PARA ACTUALIZAR PROGRAMA SERVICIO SOCIAL-----
-----FUNCION PARA ACTUALIZAR PROGRAMA SERVICIO SOCIAL-----

