---------------------------------Se agregaron despues por JAVISS cuando regreso---------------------------------

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN id_aut_salida_vehiculo INTEGER;

--Se agrega la llave foranea que relaciona la solicitude de vehiculo con la de visita academica (DUDA)
alter table pe_vinculacion.va_solicitudes_visitas_academicas
add constraint fk5_id_aut_salida_vehiculo
foreign key ("id_aut_salida_vehiculo")
references public."RM_aut_salidas"(id_aut_salida);

--ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas DROP CONSTRAINT fk5_id_aut_salida_vehiculo;

--Bitacora de cambios en la solicitud por parte de Roxana de Vinculacion (Agregar Tabla)
CREATE TABLE pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
(
    id_bitacora_solicitud_visita_academica SERIAL NOT NULL, --Clave primaria
    id_solicitud_visitas_academicas INTEGER NOT NULL, --Clave foranea
    fecha_hora_salida timestamp with time zone, --Campo que se pueden editar por Roxana de Vinculacion
    fecha_hora_regreso timestamp with time zone, --Campo que se pueden editar por Roxana de Vinculacion
    no_alumnos_visita INTEGER, --Campo que se pueden editar por Roxana de Vinculacion
    fecha_ultima_modificacion_solicitud timestamp with time zone default now(),
    usuario_modifica CHAR(13) NOT NULL,
    CONSTRAINT pk_id_bitacora_solicitud_visita_academica PRIMARY KEY(id_bitacora_solicitud_visita_academica),
    CONSTRAINT fk1_id_bit_sol_visit_acad FOREIGN KEY(id_solicitud_visitas_academicas) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas)
);

--Para saber cuando se modifica el documento
ALTER TABLE pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
ADD COLUMN modifico_doc_digital boolean;

ALTER TABLE pe_vinculacion.va_solicitudes_visitas_academicas
ADD COLUMN id_estatus_solicitud_visita_academica INTEGER;

alter table pe_vinculacion.va_solicitudes_visitas_academicas
add constraint fk4_id_estatus_solicitud_visita_academica
foreign key (id_estatus_solicitud_visita_academica)
references pe_vinculacion.va_estatus_solicitud_visita_academica(id_estatus_solicitud_visita_academica);

--Estatus de la Solicitud de Visita Academica
CREATE TABLE pe_vinculacion.va_estatus_solicitud_visita_academica
(
	id_estatus_solicitud_visita_academica SERIAL NOT NULL,
	estatus VARCHAR(20) NOT NULL,
    descripcion_estatus VARCHAR(80),
    CONSTRAINT pk_id_estatus_solicitud_visita_acad PRIMARY KEY(id_estatus_solicitud_visita_academica)
);

INSERT INTO pe_vinculacion.va_estatus_solicitud_visita_academica VALUES
(1, 'PENDIENTE', 'La Solicitud se encuentra en Validacion por parte de los Jefes correspondientes.'),
(2, 'ACEPTADA', 'La Solicitud fue aceptada por los Jefes.'),
(3, 'SUSPENDIDA', 'La Solicitud fue suspendida por algun motivo.'),
(4, 'CANCELADA', 'La Solicitud fue cancelada por algun motivo.'),
(5, 'COMPLETADA', 'La Solicitud fue completada, realizar el Reporte de Resultados e Incidencias.'),
(6, 'FINALIZADA', 'La Solicitud fue finalizada y se entrego la documentacion correspondiente.');

--Para el reporte de incidencias de cada Solicitud
CREATE TABLE pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas
(
	id_reporte_resultados_incidencias INTEGER NOT NULL,
    fecha_creacion_reporte TIMESTAMP with time zone,
    unidades_materias_cubrieron VARCHAR,
    cumplieron_objetivos VARCHAR,
    descripcion_incidentes VARCHAR,
    valida_docente_reponsable TIMESTAMP with time zone, --docente tambien Valida escrito del reporte de incidencias
    CONSTRAINT pk_id_reporte_resultados_incidencias PRIMARY KEY(id_reporte_resultados_incidencias),
    CONSTRAINT fk1_id_reporte_resultados_incidencias FOREIGN KEY(id_reporte_resultados_incidencias) REFERENCES pe_vinculacion.va_solicitudes_visitas_academicas(id_solicitud_visitas_academicas)

);

--Inserts tipo de visita
INSERT INTO pe_vinculacion.va_tipos_visitas_academicas VALUES
(1, 'VISITA ACAD�MICA'),
(2, 'VISITA DE CAMPO');

INSERT INTO pe_vinculacion.va_codigos_calidad_mod_visitas_academicas VALUES
(1, 'SOLICITUD PARA VISITAS ACAD�MICAS PROPUESTAS', 'ITC-VI-PO-001-01', '8'),
(2, 'LISTA DE ESTUDIANTES QUE ASISTEN A LA VISITA ACAD�MICA', 'TNMC-VI-PO-001-02', '1'),
(3, 'REPORTE DE RESULTADOS E INCIDENTES EN VISITA ACAD�MICA', 'TNMC-VI-PO-001-03', '1');



--Se agrego tabla de configuracion
CREATE TABLE pe_vinculacion.va_configuracion_mod_visitas_academicas
(
	id_configuracion_mod_visitas_academicas SERIAL NOT NULL,
    max_numero_solicitudes_vigentes_por_docente INTEGER, --Max 5 por periodo y a�o
    texto_legenda_inicio_reportes_pdf VARCHAR(200), --legenda al inicio de los reportes pdf
    max_numero_alumnos_por_visita_academica INTEGER, --maximo numero de alumnos por visita academica
    fecha_ultima_modificacion TIMESTAMP with time zone, --Fecha de la ultima modificacion de informacion
    CONSTRAINT pk_id_configuracion_mod_visitas_academicas PRIMARY KEY(id_configuracion_mod_visitas_academicas)
);

INSERT INTO pe_vinculacion.va_configuracion_mod_visitas_academicas VALUES
(1, 5, null, 80, now());

INSERT INTO pe_vinculacion.va_estatus_solicitud_visita_academica VALUES
(7, 'LIBERACION', 'La Solicitud se encuentra en Validacion por parte de los Jefes correspondientes.');


---- script 14 febrero
INSERT INTO pe_vinculacion.va_estatus_solicitud_visita_academica VALUES
(8, 'EN CURSO', 'La Visita esta en curso. Se asignaron los alumnos a la Visita Acad�mica.');

alter table pe_vinculacion.va_solicitudes_visitas_academicas add column val_lista_alumnos_visita_academica
timestamp with time zone;


---------------------------------Se agregaron despues por JAVISS cuando regreso---------------------------------

--Verificar que el docente no tenga mas de 5 solicitudes en el mismo perido y a�o
--Obtener el periodo y a�o actual de la BD
select count(sva.id_solicitud_visitas_academicas) as sol_vigentes
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where rva."rfcEmpleado" = 'ROVG8201127G6' AND rva.responsable_principal = true AND
sva.periodo = '1' AND sva.anio = 2020 AND
(sva.id_estatus_solicitud_visita_academica = 1 AND sva.id_estatus_solicitud_visita_academica = 2 AND
sva.id_estatus_solicitud_visita_academica = 3);

--Solicitudes que han sido ACEPTADAS para agregar materias que cubre la visitac academica y agregar responsables
select * from pe_vinculacion.va_responsables_visitas_academicas rva
join pe_vinculacion.va_solicitudes_visitas_academicas sva
on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
where rva.responsable_principal = true AND sva.id_estatus_solicitud_visita_academica = 2;

--Borrar datos de la solicitudes de visitas academicas
delete from pe_vinculacion.va_alumnos_asisten_visitas_academicas;
delete from pe_vinculacion.va_responsables_visitas_academicas;
delete from pe_vinculacion.va_materias_imparte_responsable_visita_academica;
delete from pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion;
delete from pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas;
delete from pe_vinculacion.va_solicitudes_visitas_academicas;


--Obtener las solicitudes que cumplen todas las validaciones
select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas rriva
on rriva.id_reporte_resultados_incidencias = sva.id_solicitud_visitas_academicas
where sva.fecha_creacion_solicitud is not null AND sva.valida_jefe_depto_academico is not null AND
sva.valida_subdirector_academico is not null AND sva.valida_jefe_oficina_externos_vinculacion is not null AND
sva.valida_jefe_recursos_materiales is not null AND rriva.valida_docente_reponsable is not null
AND sva.id_solicitud_visitas_academicas = 23

select * from pe_vinculacion.va_solicitudes_visitas_academicas
where fecha_creacion_solicitud is not null AND valida_jefe_depto_academico is not null AND
valida_subdirector_academico is not null AND valida_jefe_oficina_externos_vinculacion is not null AND
valida_jefe_recursos_materiales is not null AND val_lista_alumnos_visita_academica is not null AND
id_solicitud_visitas_academicas = 22

--Obtener las solicitudes propuestas o validadas por el jefe de proyectos y el subdirector academico
select
sva.no_solicitud,
( select nombre from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita) as empresa,
( select desc_estado from pe_vinculacion.g_rpempresas emp
	join public."X_estados" est
    on est.id_estado = emp.idestado
	where emp.idempresa = sva.id_empresa_visita) as estado,
sva.area_a_visitar,
sva.no_alumnos,
to_char(sva.fecha_hora_salida_visita::date, 'DD-MM-YYYY') as fec_salida,
to_char(sva.fecha_hora_regreso_visita::date, 'DD-MM-YYYY') as fec_regreso,
(
  select esp."dscEspecialidad"
  from pe_vinculacion.va_solicitudes_visitas_academicas s
  join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
  on aava.id_solicitud_visitas_academicas = s.id_solicitud_visitas_academicas
  join public."E_datosAlumno" eda
  on eda."nctrAlumno" = aava."nctrAlumno"
  join public."E_especialidad" esp
  on esp."cveEspecialidad" = eda."cveEspecialidadAlu"
  where s.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
  limit 1
) as carrera,
(select hemp."nmbCompletoEmp" from pe_vinculacion.va_responsables_visitas_academicas r
	join public."H_empleados" hemp
    on hemp."rfcEmpleado" = r."rfcEmpleado"
    where r.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas AND
    r.responsable_principal = true
) as docente_responsable,
(
  select '( '|| ecm."cveMateria" || ' )' || ' ' || ecm."dscMateria"
  from pe_vinculacion.va_materias_imparte_responsable_visita_academica mir
  join public."E_catalogoMaterias" ecm
  on ecm."cveMateria" = mir."cveMateria"
  where mir.id_solicitud_materias_responsable_visitas_academicas = sva.id_solicitud_visitas_academicas
  limit 1
) as asignaturas
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where rva.cve_depto_acad = 8 AND sva.periodo = '2' AND sva.anio = 2019 AND
sva.valida_jefe_depto_academico is not null


--
select esp."dscEspecialidad"
from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
on aava.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
join public."E_datosAlumno" eda
on eda."nctrAlumno" = aava."nctrAlumno"
join public."E_especialidad" esp
on esp."cveEspecialidad" = eda."cveEspecialidadAlu"
where sva.id_solicitud_visitas_academicas = 23
limit 1;

--Obtener el jefe de departamento academico
--SOSS670508NA4, ROVG8201127G6, HEMJ570525RQ2
select hemp."nmbCompletoEmp" from public."H_empleados" hemp
join public."H_departamentos" dep
on dep."cveDepartamento" = hemp."cveDepartamentoEmp"
where dep."cveDeptoAcad" = 2 AND hemp."cvePuestoGeneral" = '03' AND
hemp."rfcEmpleado" = 'SAPT561004HM4';

--Obtener las solicitudes a las que se les puede agregar responsables
select sva.id_solicitud_visitas_academicas
from pe_vinculacion.va_responsables_visitas_academicas rva
join pe_vinculacion.va_solicitudes_visitas_academicas sva
on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
join public."H_empleados" hemp
on hemp."rfcEmpleado" = rva."rfcEmpleado"
join public."H_departamentos" dep
on dep."cveDepartamento" = hemp."cveDepartamentoEmp"
where rva.cve_depto_acad = 8 AND --hemp."cvePuestoGeneral" = '03' AND
--hemp."rfcEmpleado" = 'HEMJ570525RQ2' AND
sva.id_estatus_solicitud_visita_academica = 2


--Todas las solicitudes que no esten canceladas
select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_estatus_solicitud_visita_academica esva
on esva.id_estatus_solicitud_visita_academica = sva.id_estatus_solicitud_visita_academica
where sva.periodo = '1' AND sva.anio = '2020'

--Obtener el docente principal de la solicitud
select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
join pe_vinculacion.va_responsables_visitas_academicas rva
on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
where sva.id_solicitud_visitas_academicas = '' AND rva.responsable_principal = true;