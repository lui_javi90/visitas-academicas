<?php
class InfoSolicitudVisitasAcademicas
{

    public static function getFechaCreacionSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when fecha_creacion_solicitud is null then 'No disponible' ELSE(
            extract('day' from fecha_creacion_solicitud) ||' de '|| ( case
            when extract('month' from fecha_creacion_solicitud)=1 then 'Enero'
            when extract('month' from fecha_creacion_solicitud)=2 then 'Febrero'
            when extract('month' from fecha_creacion_solicitud)=3 then 'Marzo'
            when extract('month' from fecha_creacion_solicitud)=4 then 'Abril'
            when extract('month' from fecha_creacion_solicitud)=5 then 'Mayo'
            when extract('month' from fecha_creacion_solicitud)=6 then 'Junio'
            when extract('month' from fecha_creacion_solicitud)=7 then 'Julio'
            when extract('month' from fecha_creacion_solicitud)=8 then 'Agosto'
            when extract('month' from fecha_creacion_solicitud)=9 then 'Septiembre'
            when extract('month' from fecha_creacion_solicitud)=10 then 'Octubre'
            when extract('month' from fecha_creacion_solicitud)=11 then 'Noviembre'
            when extract('month' from fecha_creacion_solicitud)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_creacion_solicitud))end)
            as fecha_act,
            ((case when extract('hour' from fecha_creacion_solicitud) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_creacion_solicitud)) || ':' ||
            (case when extract('minute' from fecha_creacion_solicitud) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_creacion_solicitud)) || ' ' ||
            (case when extract('hour' from fecha_creacion_solicitud) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaSalidaSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when fecha_hora_salida_visita is null then 'No disponible' ELSE(
            extract('day' from fecha_hora_salida_visita) ||' de '|| ( case
            when extract('month' from fecha_hora_salida_visita)=1 then 'Enero'
            when extract('month' from fecha_hora_salida_visita)=2 then 'Febrero'
            when extract('month' from fecha_hora_salida_visita)=3 then 'Marzo'
            when extract('month' from fecha_hora_salida_visita)=4 then 'Abril'
            when extract('month' from fecha_hora_salida_visita)=5 then 'Mayo'
            when extract('month' from fecha_hora_salida_visita)=6 then 'Junio'
            when extract('month' from fecha_hora_salida_visita)=7 then 'Julio'
            when extract('month' from fecha_hora_salida_visita)=8 then 'Agosto'
            when extract('month' from fecha_hora_salida_visita)=9 then 'Septiembre'
            when extract('month' from fecha_hora_salida_visita)=10 then 'Octubre'
            when extract('month' from fecha_hora_salida_visita)=11 then 'Noviembre'
            when extract('month' from fecha_hora_salida_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_hora_salida_visita))end)
            as fecha_act,
            ((case when extract('hour' from fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_hora_salida_visita)) || ':' ||
            (case when extract('minute' from fecha_hora_salida_visita) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_hora_salida_visita)) || ' ' ||
            (case when extract('hour' from fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaRegresoSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when fecha_hora_regreso_visita is null then 'No disponible' ELSE(
            extract('day' from fecha_hora_regreso_visita) ||' de '|| ( case
            when extract('month' from fecha_hora_regreso_visita)=1 then 'Enero'
            when extract('month' from fecha_hora_regreso_visita)=2 then 'Febrero'
            when extract('month' from fecha_hora_regreso_visita)=3 then 'Marzo'
            when extract('month' from fecha_hora_regreso_visita)=4 then 'Abril'
            when extract('month' from fecha_hora_regreso_visita)=5 then 'Mayo'
            when extract('month' from fecha_hora_regreso_visita)=6 then 'Junio'
            when extract('month' from fecha_hora_regreso_visita)=7 then 'Julio'
            when extract('month' from fecha_hora_regreso_visita)=8 then 'Agosto'
            when extract('month' from fecha_hora_regreso_visita)=9 then 'Septiembre'
            when extract('month' from fecha_hora_regreso_visita)=10 then 'Octubre'
            when extract('month' from fecha_hora_regreso_visita)=11 then 'Noviembre'
            when extract('month' from fecha_hora_regreso_visita)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_hora_regreso_visita))end)
            as fecha_act,
            ((case when extract('hour' from fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_hora_regreso_visita)) || ':' ||
            (case when extract('minute' from fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_hora_regreso_visita)) || ' ' ||
            (case when extract('hour' from fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaRegistroResponsableVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when fecha_registro_responsable is null then 'No disponible' ELSE(
            extract('day' from fecha_registro_responsable) ||' de '|| ( case
            when extract('month' from fecha_registro_responsable)=1 then 'Enero'
            when extract('month' from fecha_registro_responsable)=2 then 'Febrero'
            when extract('month' from fecha_registro_responsable)=3 then 'Marzo'
            when extract('month' from fecha_registro_responsable)=4 then 'Abril'
            when extract('month' from fecha_registro_responsable)=5 then 'Mayo'
            when extract('month' from fecha_registro_responsable)=6 then 'Junio'
            when extract('month' from fecha_registro_responsable)=7 then 'Julio'
            when extract('month' from fecha_registro_responsable)=8 then 'Agosto'
            when extract('month' from fecha_registro_responsable)=9 then 'Septiembre'
            when extract('month' from fecha_registro_responsable)=10 then 'Octubre'
            when extract('month' from fecha_registro_responsable)=11 then 'Noviembre'
            when extract('month' from fecha_registro_responsable)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_registro_responsable))end)
            as fecha_act,
            ((case when extract('hour' from fecha_registro_responsable) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_registro_responsable)) || ':' ||
            (case when extract('minute' from fecha_registro_responsable) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_registro_responsable)) || ' ' ||
            (case when extract('hour' from fecha_registro_responsable) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_responsables_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Fecha de Validacion de la Solicitud del Jefe Academico
    public static function getFechaValidacionJefeAcademicoVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when valida_jefe_depto_academico is null then 'SV' ELSE(
            extract('day' from valida_jefe_depto_academico) ||' de '|| ( case
            when extract('month' from valida_jefe_depto_academico)=1 then 'Enero'
            when extract('month' from valida_jefe_depto_academico)=2 then 'Febrero'
            when extract('month' from valida_jefe_depto_academico)=3 then 'Marzo'
            when extract('month' from valida_jefe_depto_academico)=4 then 'Abril'
            when extract('month' from valida_jefe_depto_academico)=5 then 'Mayo'
            when extract('month' from valida_jefe_depto_academico)=6 then 'Junio'
            when extract('month' from valida_jefe_depto_academico)=7 then 'Julio'
            when extract('month' from valida_jefe_depto_academico)=8 then 'Agosto'
            when extract('month' from valida_jefe_depto_academico)=9 then 'Septiembre'
            when extract('month' from valida_jefe_depto_academico)=10 then 'Octubre'
            when extract('month' from valida_jefe_depto_academico)=11 then 'Noviembre'
            when extract('month' from valida_jefe_depto_academico)=12 then 'Diciembre'
            END)||' de '||extract('year' from valida_jefe_depto_academico))end)
            as fecha_act,
            ((case when extract('hour' from valida_jefe_depto_academico) < 10 then '0' else '' end) ||
            (extract('hour' from valida_jefe_depto_academico)) || ':' ||
            (case when extract('minute' from valida_jefe_depto_academico) < 10 then '0' else '' end) ||
            (extract('minute' from valida_jefe_depto_academico)) || ' ' ||
            (case when extract('hour' from valida_jefe_depto_academico) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Fecha Validacion del Subdirector Academico
    public static function getFechaValidacionSubAcademicoVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when valida_subdirector_academico is null then 'SV' ELSE(
            extract('day' from valida_subdirector_academico) ||' de '|| ( case
            when extract('month' from valida_subdirector_academico)=1 then 'Enero'
            when extract('month' from valida_subdirector_academico)=2 then 'Febrero'
            when extract('month' from valida_subdirector_academico)=3 then 'Marzo'
            when extract('month' from valida_subdirector_academico)=4 then 'Abril'
            when extract('month' from valida_subdirector_academico)=5 then 'Mayo'
            when extract('month' from valida_subdirector_academico)=6 then 'Junio'
            when extract('month' from valida_subdirector_academico)=7 then 'Julio'
            when extract('month' from valida_subdirector_academico)=8 then 'Agosto'
            when extract('month' from valida_subdirector_academico)=9 then 'Septiembre'
            when extract('month' from valida_subdirector_academico)=10 then 'Octubre'
            when extract('month' from valida_subdirector_academico)=11 then 'Noviembre'
            when extract('month' from valida_subdirector_academico)=12 then 'Diciembre'
            END)||' de '||extract('year' from valida_subdirector_academico))end)
            as fecha_act,
            ((case when extract('hour' from valida_subdirector_academico) < 10 then '0' else '' end) ||
            (extract('hour' from valida_subdirector_academico)) || ':' ||
            (case when extract('minute' from valida_subdirector_academico) < 10 then '0' else '' end) ||
            (extract('minute' from valida_subdirector_academico)) || ' ' ||
            (case when extract('hour' from valida_subdirector_academico) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Valida jefe de oficina de Visitas Indutriales (Roxana)
    public static function getFechaValidacionJefeOficinaExternosVinculacionVisitasAcademicas($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when valida_jefe_oficina_externos_vinculacion is null then 'SV' ELSE(
            extract('day' from valida_jefe_oficina_externos_vinculacion) ||' de '|| ( case
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=1 then 'Enero'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=2 then 'Febrero'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=3 then 'Marzo'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=4 then 'Abril'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=5 then 'Mayo'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=6 then 'Junio'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=7 then 'Julio'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=8 then 'Agosto'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=9 then 'Septiembre'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=10 then 'Octubre'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=11 then 'Noviembre'
            when extract('month' from valida_jefe_oficina_externos_vinculacion)=12 then 'Diciembre'
            END)||' de '||extract('year' from valida_jefe_oficina_externos_vinculacion))end)
            as fecha_act,
            ((case when extract('hour' from valida_jefe_oficina_externos_vinculacion) < 10 then '0' else '' end) ||
            (extract('hour' from valida_jefe_oficina_externos_vinculacion)) || ':' ||
            (case when extract('minute' from valida_jefe_oficina_externos_vinculacion) < 10 then '0' else '' end) ||
            (extract('minute' from valida_jefe_oficina_externos_vinculacion)) || ' ' ||
            (case when extract('hour' from valida_jefe_oficina_externos_vinculacion) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaValidacionJefeRecursosmaterialesVisitasAcademicas($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when valida_jefe_recursos_materiales is null then 'SV' ELSE(
            extract('day' from valida_jefe_recursos_materiales) ||' de '|| ( case
            when extract('month' from valida_jefe_recursos_materiales)=1 then 'Enero'
            when extract('month' from valida_jefe_recursos_materiales)=2 then 'Febrero'
            when extract('month' from valida_jefe_recursos_materiales)=3 then 'Marzo'
            when extract('month' from valida_jefe_recursos_materiales)=4 then 'Abril'
            when extract('month' from valida_jefe_recursos_materiales)=5 then 'Mayo'
            when extract('month' from valida_jefe_recursos_materiales)=6 then 'Junio'
            when extract('month' from valida_jefe_recursos_materiales)=7 then 'Julio'
            when extract('month' from valida_jefe_recursos_materiales)=8 then 'Agosto'
            when extract('month' from valida_jefe_recursos_materiales)=9 then 'Septiembre'
            when extract('month' from valida_jefe_recursos_materiales)=10 then 'Octubre'
            when extract('month' from valida_jefe_recursos_materiales)=11 then 'Noviembre'
            when extract('month' from valida_jefe_recursos_materiales)=12 then 'Diciembre'
            END)||' de '||extract('year' from valida_jefe_recursos_materiales))end)
            as fecha_act,
            ((case when extract('hour' from valida_jefe_recursos_materiales) < 10 then '0' else '' end) ||
            (extract('hour' from valida_jefe_recursos_materiales)) || ':' ||
            (case when extract('minute' from valida_jefe_recursos_materiales) < 10 then '0' else '' end) ||
            (extract('minute' from valida_jefe_recursos_materiales)) || ' ' ||
            (case when extract('hour' from valida_jefe_recursos_materiales) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";
        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaSolicitudUltimaActualizacion($id_solicitud_visitas_academicas)
    {
        $query =
            "SELECT (case when ultima_fecha_modificacion is null then 'SV' ELSE(
            extract('day' from ultima_fecha_modificacion) ||' de '|| ( case
            when extract('month' from ultima_fecha_modificacion)=1 then 'Enero'
            when extract('month' from ultima_fecha_modificacion)=2 then 'Febrero'
            when extract('month' from ultima_fecha_modificacion)=3 then 'Marzo'
            when extract('month' from ultima_fecha_modificacion)=4 then 'Abril'
            when extract('month' from ultima_fecha_modificacion)=5 then 'Mayo'
            when extract('month' from ultima_fecha_modificacion)=6 then 'Junio'
            when extract('month' from ultima_fecha_modificacion)=7 then 'Julio'
            when extract('month' from ultima_fecha_modificacion)=8 then 'Agosto'
            when extract('month' from ultima_fecha_modificacion)=9 then 'Septiembre'
            when extract('month' from ultima_fecha_modificacion)=10 then 'Octubre'
            when extract('month' from ultima_fecha_modificacion)=11 then 'Noviembre'
            when extract('month' from ultima_fecha_modificacion)=12 then 'Diciembre'
            END)||' de '||extract('year' from ultima_fecha_modificacion))end)
            as fecha_act,
            ((case when extract('hour' from ultima_fecha_modificacion) < 10 then '0' else '' end) ||
            (extract('hour' from ultima_fecha_modificacion)) || ':' ||
            (case when extract('minute' from ultima_fecha_modificacion) < 10 then '0' else '' end) ||
            (extract('minute' from ultima_fecha_modificacion)) || ' ' ||
            (case when extract('hour' from ultima_fecha_modificacion) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_solicitudes_visitas_academicas WHERE id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFechaUltimaActualizacionOtrasConfiguraciones($id_configuracion_mod_visitas_academicas)
    {
        $query =
            "SELECT (case when fecha_ultima_modificacion is null then 'SV' ELSE(
            extract('day' from fecha_ultima_modificacion) ||' de '|| ( case
            when extract('month' from fecha_ultima_modificacion)=1 then 'Enero'
            when extract('month' from fecha_ultima_modificacion)=2 then 'Febrero'
            when extract('month' from fecha_ultima_modificacion)=3 then 'Marzo'
            when extract('month' from fecha_ultima_modificacion)=4 then 'Abril'
            when extract('month' from fecha_ultima_modificacion)=5 then 'Mayo'
            when extract('month' from fecha_ultima_modificacion)=6 then 'Junio'
            when extract('month' from fecha_ultima_modificacion)=7 then 'Julio'
            when extract('month' from fecha_ultima_modificacion)=8 then 'Agosto'
            when extract('month' from fecha_ultima_modificacion)=9 then 'Septiembre'
            when extract('month' from fecha_ultima_modificacion)=10 then 'Octubre'
            when extract('month' from fecha_ultima_modificacion)=11 then 'Noviembre'
            when extract('month' from fecha_ultima_modificacion)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_ultima_modificacion))end)
            as fecha_act,
            ((case when extract('hour' from fecha_ultima_modificacion) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_ultima_modificacion)) || ':' ||
            (case when extract('minute' from fecha_ultima_modificacion) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_ultima_modificacion)) || ' ' ||
            (case when extract('hour' from fecha_ultima_modificacion) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_configuracion_mod_visitas_academicas WHERE id_configuracion_mod_visitas_academicas = '$id_configuracion_mod_visitas_academicas'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //Valida reporte resultados e incidencias
    public static function getFormatoFechaValidaDocenteReponsableReporteResultados($id_reporte_resultados_incidencias)
    {
        $query =
            "SELECT (case when valida_docente_reponsable is null then 'SV' ELSE(
            extract('day' from valida_docente_reponsable) ||' de '|| ( case
            when extract('month' from valida_docente_reponsable)=1 then 'Enero'
            when extract('month' from valida_docente_reponsable)=2 then 'Febrero'
            when extract('month' from valida_docente_reponsable)=3 then 'Marzo'
            when extract('month' from valida_docente_reponsable)=4 then 'Abril'
            when extract('month' from valida_docente_reponsable)=5 then 'Mayo'
            when extract('month' from valida_docente_reponsable)=6 then 'Junio'
            when extract('month' from valida_docente_reponsable)=7 then 'Julio'
            when extract('month' from valida_docente_reponsable)=8 then 'Agosto'
            when extract('month' from valida_docente_reponsable)=9 then 'Septiembre'
            when extract('month' from valida_docente_reponsable)=10 then 'Octubre'
            when extract('month' from valida_docente_reponsable)=11 then 'Noviembre'
            when extract('month' from valida_docente_reponsable)=12 then 'Diciembre'
            END)||' de '||extract('year' from valida_docente_reponsable))end)
            as fecha_act,
            ((case when extract('hour' from valida_docente_reponsable) < 10 then '0' else '' end) ||
            (extract('hour' from valida_docente_reponsable)) || ':' ||
            (case when extract('minute' from valida_docente_reponsable) < 10 then '0' else '' end) ||
            (extract('minute' from valida_docente_reponsable)) || ' ' ||
            (case when extract('hour' from valida_docente_reponsable) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas WHERE
                id_reporte_resultados_incidencias = '$id_reporte_resultados_incidencias'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    /*Damos formato a la fecha*/
    public static function getFormatoFecha($fecha)
    {
        $fechaComoEntero = strtotime($fecha);

        //Obtenemos el dia, mes y año
        $dia = date("d", $fechaComoEntero);
        $mes = GetFormatoFecha::getMes(date("m", $fechaComoEntero));
        $anio = date("Y", $fechaComoEntero);

        return $dia." de ".$mes." de ".$anio;
    }
    /*Damos formato a la fecha*/

    public static function getMes($mes)
    {

      $_mes="";

      switch ($mes)
      {
        case '01': $_mes = "ENERO"; break;
        case '02': $_mes = "FEBRERO"; break;
        case '03': $_mes = "MARZO"; break;
        case '04': $_mes = "ABRIL"; break;
        case '05': $_mes = "MAYO"; break;
        case '06': $_mes = "JUNIO"; break;
        case '07': $_mes = "JULIO"; break;
        case '08': $_mes = "AGOSTO"; break;
        case '09': $_mes = "SEPTIEMBRE"; break;
        case '10': $_mes = "OCTUBRE"; break;
        case '11': $_mes = "NOVIEMBRE"; break;
        case '12': $_mes = "DICIEMBRE"; break;
        default: $_mes = "DESCONOCIDO"; break;
      }

      return $_mes;
    }

    //Si es jefe de proyectos de vinculacion de algun departamento academico
	public static function isJefeProyectosVinculacion($rfcEmpleado)
	{
		$qry_jef_proyvinc = "select * from public.h_ocupacionpuesto
								where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '04' AND
								\"cvePuestoParticular\" = '33'
							";

		$jefe_proy_vinc = Yii::app()->db->createCommand($qry_jef_proyvinc)->queryAll();

		return (sizeof($jefe_proy_vinc) > 0) ? true : false;
    }

    //Si es subdirector academico
    public static function isSubdirectorAcademico($rfcEmpleado)
	{
		$qry_subd_acad = "select * from public.h_ocupacionpuesto
							where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '02'
							AND \"cvePuestoParticular\" = '02' ";

		$subd_acad = Yii::app()->db->createCommand($qry_subd_acad)->queryAll();

		return (sizeof($subd_acad) > 0) ? true : false;
	}

    public static function esJefeDepartamentoAcademico($depto_acad, $rfcEmpleado)
    {
        /*echo $depto_acad."<br>";
        echo $rfcEmpleado;
        die;*/

        $qry_jefe_depto_acad = "select * from public.\"H_empleados\" hemp
                                join public.\"H_departamentos\" dep
                                on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
                                where dep.\"cveDeptoAcad\" = '$depto_acad' AND hemp.\"cvePuestoGeneral\" = '03' AND 
                                hemp.\"rfcEmpleado\" = '$rfcEmpleado'
                            ";

        $jefe_depto_acad = Yii::app()->db->createCommand($qry_jefe_depto_acad)->queryAll();

        return (sizeof($jefe_depto_acad) > 0 or !empty($jefe_depto_acad)) ? true : false;
    }

    public static function getDeptoCatedratico($rfcEmpleado)
    {
        //Paras saber si es jefe de proyecto de su departamento
        $qry_puest = "select hemp.\"rfcEmpleado\", hemp.\"deptoCatedratico\", dep.\"cveDepartamento\"
                        from public.\"H_empleados\" hemp
                        join public.\"H_departamentos\" dep
                        on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
                        where hemp.\"cvePuestoGeneral\" = '04' AND hemp.\"cvePuestoParticular\" = '33' AND
                            hemp.\"rfcEmpleado\" = '$rfcEmpleado' ";

        $rs = Yii::app()->db->createCommand($qry_puest)->queryAll();

        if(sizeof($rs) > 0)
        {
            if($rs[0]['cveDeptoAcad'] > 0)
                return trim($rs[0]['cveDeptoAcad']); //1
            else
                return 0;

        }else{

            return 0;
        }
    }

    //FORMATO FECHA REGISTRO DE BITACORA
    public static function getFormatoFechaSalidaBitacora($id_bitacora_solicitud_visita_academica)
    {
        $query =
            "SELECT (case when fecha_hora_salida is null then 'SV' ELSE(
            extract('day' from fecha_hora_salida) ||' de '|| ( case
            when extract('month' from fecha_hora_salida)=1 then 'Enero'
            when extract('month' from fecha_hora_salida)=2 then 'Febrero'
            when extract('month' from fecha_hora_salida)=3 then 'Marzo'
            when extract('month' from fecha_hora_salida)=4 then 'Abril'
            when extract('month' from fecha_hora_salida)=5 then 'Mayo'
            when extract('month' from fecha_hora_salida)=6 then 'Junio'
            when extract('month' from fecha_hora_salida)=7 then 'Julio'
            when extract('month' from fecha_hora_salida)=8 then 'Agosto'
            when extract('month' from fecha_hora_salida)=9 then 'Septiembre'
            when extract('month' from fecha_hora_salida)=10 then 'Octubre'
            when extract('month' from fecha_hora_salida)=11 then 'Noviembre'
            when extract('month' from fecha_hora_salida)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_hora_salida))end)
            as fecha_act,
            ((case when extract('hour' from fecha_hora_salida) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_hora_salida)) || ':' ||
            (case when extract('minute' from fecha_hora_salida) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_hora_salida)) || ' ' ||
            (case when extract('hour' from fecha_hora_salida) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
            WHERE id_bitacora_solicitud_visita_academica = '$id_bitacora_solicitud_visita_academica'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFormatoFechaRegresoBitacora($id_bitacora_solicitud_visita_academica)
    {
        $query =
            "SELECT (case when fecha_hora_regreso is null then 'SV' ELSE(
            extract('day' from fecha_hora_regreso) ||' de '|| ( case
            when extract('month' from fecha_hora_regreso)=1 then 'Enero'
            when extract('month' from fecha_hora_regreso)=2 then 'Febrero'
            when extract('month' from fecha_hora_regreso)=3 then 'Marzo'
            when extract('month' from fecha_hora_regreso)=4 then 'Abril'
            when extract('month' from fecha_hora_regreso)=5 then 'Mayo'
            when extract('month' from fecha_hora_regreso)=6 then 'Junio'
            when extract('month' from fecha_hora_regreso)=7 then 'Julio'
            when extract('month' from fecha_hora_regreso)=8 then 'Agosto'
            when extract('month' from fecha_hora_regreso)=9 then 'Septiembre'
            when extract('month' from fecha_hora_regreso)=10 then 'Octubre'
            when extract('month' from fecha_hora_regreso)=11 then 'Noviembre'
            when extract('month' from fecha_hora_regreso)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_hora_regreso))end)
            as fecha_act,
            ((case when extract('hour' from fecha_hora_regreso) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_hora_regreso)) || ':' ||
            (case when extract('minute' from fecha_hora_regreso) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_hora_regreso)) || ' ' ||
            (case when extract('hour' from fecha_hora_regreso) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
            WHERE id_bitacora_solicitud_visita_academica = '$id_bitacora_solicitud_visita_academica'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    public static function getFormatoFechaActualizacionBitacora($id_bitacora_solicitud_visita_academica)
    {
        $query =
            "SELECT (case when fecha_ultima_modificacion_solicitud is null then 'SV' ELSE(
            extract('day' from fecha_ultima_modificacion_solicitud) ||' de '|| ( case
            when extract('month' from fecha_ultima_modificacion_solicitud)=1 then 'Enero'
            when extract('month' from fecha_ultima_modificacion_solicitud)=2 then 'Febrero'
            when extract('month' from fecha_ultima_modificacion_solicitud)=3 then 'Marzo'
            when extract('month' from fecha_ultima_modificacion_solicitud)=4 then 'Abril'
            when extract('month' from fecha_ultima_modificacion_solicitud)=5 then 'Mayo'
            when extract('month' from fecha_ultima_modificacion_solicitud)=6 then 'Junio'
            when extract('month' from fecha_ultima_modificacion_solicitud)=7 then 'Julio'
            when extract('month' from fecha_ultima_modificacion_solicitud)=8 then 'Agosto'
            when extract('month' from fecha_ultima_modificacion_solicitud)=9 then 'Septiembre'
            when extract('month' from fecha_ultima_modificacion_solicitud)=10 then 'Octubre'
            when extract('month' from fecha_ultima_modificacion_solicitud)=11 then 'Noviembre'
            when extract('month' from fecha_ultima_modificacion_solicitud)=12 then 'Diciembre'
            END)||' de '||extract('year' from fecha_ultima_modificacion_solicitud))end)
            as fecha_act,
            ((case when extract('hour' from fecha_ultima_modificacion_solicitud) < 10 then '0' else '' end) ||
            (extract('hour' from fecha_ultima_modificacion_solicitud)) || ':' ||
            (case when extract('minute' from fecha_ultima_modificacion_solicitud) < 10 then '0' else '' end) ||
            (extract('minute' from fecha_ultima_modificacion_solicitud)) || ' ' ||
            (case when extract('hour' from fecha_ultima_modificacion_solicitud) >= 12 then 'PM' else 'AM' end)) as hora
            FROM pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion
            WHERE id_bitacora_solicitud_visita_academica = '$id_bitacora_solicitud_visita_academica'
        ";

        $fec_act = Yii::app()->db->createCommand($query)->queryAll();

        return $fec_act;
    }

    //FORMATO FECHA REGISTRO DE BITACORA
}

?>
