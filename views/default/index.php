<?php
	/* @var $this DefaultController */
	/* Vista Principal del Modulo de Visitas Academicas */

	$this->breadcrumbs = array(
		'Visitas Académicas'
	);
?>


<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<b>Módulo Visitas Académicas</b>
		</span>
	</h2>
</div>

<?php if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true){ ?>
<br><br><br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-align-justify"></span>&nbsp;
					<b>Jefe de Proyectos de Vinculación o Jefe de Departamento Académico</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<?php
						if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true)
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaVSolicitudesVisitasAcademicas"
							title="Solicitudes Visitas Industriales" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Consulta Solicitudes a Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true)
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaVValidadasSolicitudesVisitaAcademicas"
							title="Solicitudes Validadas de Visitas Académicas" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes Validadas de Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true)
						{

						?>
						<a href="?r=visitasacademicas/vaResponsablesVisitasAcademicas/listaSolicitudesValidadasAgregarResponsables"
							title="Agregar Responsables Solicitudes Visitas Académicas" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;&nbsp;&nbsp;Agregar Responsables Solicitudes Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true)
						{
							require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

							//Tomar del inicio de sesion del Empleado en el SII
							//$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
							$rfc_empleado = Yii::app()->user->name;
							$rfcEmpleado = trim($rfc_empleado);

							//Comprobar que es Jefe de Proyectos de Vinculacion
							$is_jefe_proyvinc = InfoSolicitudVisitasAcademicas::isJefeProyectosVinculacion($rfcEmpleado);
							//Obtener Departamento Catedratico
							$depto_catedratico = InfoSolicitudVisitasAcademicas::getDeptoCatedratico($rfcEmpleado);

							//Comprobra si es jefe de departamento academico
							$is_jefe_depto_academico = InfoSolicitudVisitasAcademicas::esJefeDepartamentoAcademico($depto_catedratico, $rfcEmpleado);
						?>
						<?php if($is_jefe_proyvinc == true or $is_jefe_depto_academico == true){ ?>
							<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirSolicitudVisitaAcademica"
								title="Solicitudes Propuestas para Visitas Académicas" class="list-group-item"
								rel="tooltip">
								<span class="glyphicon glyphicon-inbox"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes Propuestas para Visitas Académicas
							</a>
						<?php } ?>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('jefe_vinculacion') or Yii::app()->user->checkAccess('jefe_departamento_academico') or true)
						{

						?>
						<a href="?r=visitasacademicas/vaEstatusSolicitudVisitaAcademica/listaEstatusSolicitudVisitaAcademica"
							title="Estatus de la Solicitud" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estatus de la Solicitud Visita Académica
						</a>
					<?php
						}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if(Yii::app()->user->checkAccess('docente') or true){ ?>
<br><br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-align-justify"></span>&nbsp;
					<b>Docentes</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<?php
						if(Yii::app()->user->checkAccess('docente')  or true)
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas"
							title="Solicitudes Visitas Académicas" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Mis Solicitudes Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('docente')  or true)
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaDHistoricoSolicitudesVisitasAcademicas"
							title="Histórico Solicitudes Visitas Académicas" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;&nbsp;&nbsp;Histórico Solicitudes Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('docente')  or true)
						{

						?>
						<a href="?r=visitasacademicas/vaEstatusSolicitudVisitaAcademica/listaEstatusSolicitudVisitaAcademica"
							title="Estatus de la Solicitud" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estatus de la Solicitud Visita Académica
						</a>
					<?php
						}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if(Yii::app()->user->checkAccess('subdirector_academico') ){ ?>
<br><br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-align-justify"></span>&nbsp;
					<b>Subdirector Académico</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<?php
						if(Yii::app()->user->checkAccess('subdirector_academico')  )
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaSSolicitudesVisitasAcademicas"
							title="Solicitudes Visitas Industriales" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Consulta Solicitudes a Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('subdirector_academico') )
						{

						?>
						<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaSHistoricoSolicitudesVisitasAcademicas"
							title="Solicitudes Validadas de Visitas Académicas" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes Validadas de Visitas Académicas
						</a>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('subdirector_academico') )
						{
							require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

							//Tomar del inicio de sesion del Empleado en el SII
							//$rfc_empleado = Yii::app()->params['rfcSubdirectorAcademico']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
							$rfc_empleado = Yii::app()->user->name;
							$rfcEmpleado = trim($rfc_empleado);

							//Comprobar que es Jefe de Proyectos de Vinculacion
							$subd_academico = InfoSolicitudVisitasAcademicas::isSubdirectorAcademico($rfcEmpleado);

						?>
						<?php if($subd_academico == true){ ?>
							<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirSSolicitudVisitaAcademica"
								title="Solicitudes Propuestas para Visitas Académicas" class="list-group-item"
								rel="tooltip">
								<span class="glyphicon glyphicon-inbox"></span>&nbsp;&nbsp;&nbsp;&nbsp;Solicitudes Propuestas para Visitas Académicas
							</a>
						<?php } ?>
					<?php
						}
					?>

					<?php
						if(Yii::app()->user->checkAccess('subdirector_academico') )
						{

						?>
						<a href="?r=visitasacademicas/vaEstatusSolicitudVisitaAcademica/listaEstatusSolicitudVisitaAcademica"
							title="Estatus de la Solicitud" class="list-group-item"
							rel="tooltip">
							<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estatus de la Solicitud Visita Académica
						</a>
					<?php
						}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<!--Modulo de Jefe de Vinculacion-->
<?php if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true){ ?>
<br><br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">
					<span class="glyphicon glyphicon-align-justify"></span>&nbsp;
					<b>Jefa de la Oficina de Servicios Externos</b>
				</h6>
			</div>
			<div class="panel-body">
				<div class="list-group">
				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion')  or true)
					{

				?>
					<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaJVSolicitudesVisitasAcademicas"
						title="Consulta Solicitudes a Visitas Académicas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;&nbsp;&nbsp;Consulta Solicitudes a Visitas Académicas
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion')  or true)
					{

				?>
					<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaStatusSolicitudesVisitasAcademicas"
						title="Consulta Solicitudes a Visitas Académicas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estatus Solicitudes de Visitas Académicas del Semestre
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion')  or true)
					{

				?>
					<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaLiberacionSolicitudVisitaAcademica"
						title="Lista de Liberación de Visitas Académicas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Lista de Visitas Académicas Completadas pasar a Finalizada
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

				?>
					<a href="?r=visitasacademicas/vaSolicitudesVisitasAcademicas/listaHistoricoSolicitudesVisitasAcademicas"
						title="Histórico de Visitas Académicas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;&nbsp;&nbsp;Histórico de Visitas Académicas
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

					?>
					<a href="?r=visitasacademicas/vaTiposVisitasAcademicas/listaTiposVisitasAcademicas"
						title="Tipos de Visitas Académicas" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-flag"></span>&nbsp;&nbsp;&nbsp;&nbsp;Tipos de Visitas Académicas
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

					?>
					<a href="?r=visitasacademicas/gRpempresas/listaEmpresasVisitasAcademicas"
						title="Empresas Disponibles a Visitar" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-briefcase"></span>&nbsp;&nbsp;&nbsp;&nbsp;Empresas Disponibles a Visitar
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

				?>
					<a href="?r=visitasacademicas/vaCodigosCalidadModVisitasAcademicas/listaCodigosCalidadVisitasAcademicas"
						title="Codigos de Calidad del Módulo" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-barcode"></span>&nbsp;&nbsp;&nbsp;&nbsp;Codigos de Calidad del Módulo
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

				?>
					<a href="?r=visitasacademicas/vaEstatusSolicitudVisitaAcademica/listaVEstatusSolicitudVisitaAcademica"
						title="Estatus de la Solicitud" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;Estatus de la Solicitud Visita Académica
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

				?>
					<a href="?r=visitasacademicas/vaConfiguracionModVisitasAcademicas/configuracionModuloVisitaAcademica"
						title="Otras Configuraciones del Módulo" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;&nbsp;&nbsp;Otras Configuraciones del Módulo
					</a>
				<?php
					}
				?>

				<?php
					if(Yii::app()->user->checkAccess('jefe_oficina_servicios_externos_vinculacion') or true)
					{

				?>
					<a href="?r=empleado/vehiculo/admin"
						title="Otras Configuraciones del Módulo" class="list-group-item"
						rel="tooltip">
						<span class="glyphicon glyphicon-road"></span>&nbsp;&nbsp;&nbsp;&nbsp;Vehículos (Agenda)
					</a>
				<?php
					}
				?>

			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--Modulo de Jefe de Vinculacion-->


<br><br><br><br><br>
<br><br><br><br><br>
