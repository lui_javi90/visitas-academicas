<?php

	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Responsables Solicitudes Visitas Académicas' => array('vaResponsablesVisitasAcademicas/listaSolicitudesValidadasAgregarResponsables'),
        'Agregar Materia Cubre Solicitud Visita Académica'
	);

	/*ELIMINA LA MATERIA QUE CUBRE LA VISITA ACADEMICA, DEBE HABER POR LO MENOS DOS MATERIAS PARA ELIMINAR UNA MATERIA */
	$JsDeleteMateria = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-lista-add-materia-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-lista-add-materia-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*ELIMINA LA MATERIA QUE CUBRE LA VISITA ACADEMICA, DEBE HABER POR LO MENOS DOS MATERIAS PARA ELIMINAR UNA MATERIA */

?>

<br><br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Agregar Materia Cubre Solicitud Visita Académica
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Agregar Materia</b>
				</h3>
			</div>
			<div class="panel-body">

				<div class="form">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'va-solicitudes-visitas-academicas-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>

					<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

					<?php echo $form->errorSummary($modelVaMateriasImparteResponsableVisitaAcademica); ?>

					<div class="form-group">
						<?php echo $form->labelEx($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
						<?php $this->widget('ext.select2.ESelect2', array('model'=>$modelVaMateriasImparteResponsableVisitaAcademica,
                                        'attribute'=>'cveMateria',
                                        'data' => $lista_materias,
                                        'options' => array(
                                            'width' => '100%',
                                            'placeholder'=>'Buscar Materia por su Clave',
                                            'allowClear'=>true,
                                        ),
														'htmlOptions' => array(
                                                            'ajax'=>array(
                                                            'type'=>'POST',
                                                            'dataType'=>'json',
                                                            'data' => array(
                    
                                                                'cveMateria'=>'js:$(\'#VaMateriasImparteResponsableVisitaAcademica_cveMateria\').val()',
                                                            ),
                                                            'url'=>CController::createUrl('vaMateriasImparteResponsableVisitaAcademica/infoMateriaCubreSeleccionada'),
                                                            'success'=>'function(data) 
                                                            {
																if(data.status_campo == "NV")
																{
																	$(".div_logo_materia").show();
                                                                    $(".div_nombre_materia").show();
																	$(".div_cveoficial_materia").show();
																	$(".div_departamento_materia").show();
																	$(".div_logo_materia").html(\'<img class="img-circle" align="center" heigth="150" width="150" src="images/servicio_social/materia.png" />\');
																	$(".div_nombre_materia").html(\'<b><label align="left">Nombre:</label></b><input type="text" class="form-control" style="border:none; background: transparent; border-color:transparent;"  value="\'+ data.nombre_materia +\'" readonly />\');
																	$(".div_cveoficial_materia").html(\'<b><label align="left">Clave Oficial:</label></b><input type="text" class="form-control" style="border:0; background: transparent; border-color:transparent;"  value="\'+ data.cve_of_materia +\'" readonly />\');
																	$(".div_departamento_materia").html(\'<b><label align="left">Departamento:</label></b><input type="text" class="form-control" style="border:0; background: transparent; border-color:transparent;"  value="\'+ data.departamento +\'" readonly />\');
																	
																}else
																if(data.status_campo == "V")
																{
																	$(".div_logo_materia").hide();
                                                                    $(".div_nombre_materia").hide();
																	$(".div_cveoficial_materia").hide();
																	$(".div_departamento_materia").hide();
																}
															}',
														))
												)); ?>
						<?php echo $form->error($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
					</div>

					<br>
					<div class="form-group">
						<?php echo CHtml::submitButton($modelVaMateriasImparteResponsableVisitaAcademica->isNewRecord ? 'Agregar Materia' : 'Agregar Materia', array('class'=>'btn btn-success')); ?>
						<?php echo CHtml::link('Cancelar', array('vaResponsablesVisitasAcademicas/listaSolicitudesValidadasAgregarResponsables'), array('class'=>'btn btn-danger')); ?>
					</div>

					<?php $this->endWidget(); ?>

				</div><!-- form -->

			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Detalle</b>
				</h3>
			</div>
			<div class="panel-body">

				<!--Datos de la Materia Seleccionada-->
                <div align="center" class="div_logo_materia"></div>

				<br>
				<div align="center" class="div_nombre_materia"></div>

				<br>
				<div align="center" class="div_cveoficial_materia"></div>

				<br>
				<div align="center" class="div_departamento_materia"></div>

				<!--<br>
				<div align="center" class="div_msg"></div>-->

			</div>
		</div>
	</div>
</div>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Agregar Materia Cubre Solicitud Visita Académica
		</span>
	</h2>
</div>

<br><br><br><br><br>
<br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Debes tener minímo una Materia asignada a la Solicitud de Visita Académica.</b>
  </strong></p>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-lista-add-materia-visitas-academicas-grid',
    'dataProvider'=>$listaVaMateriasImparteResponsableVisitaAcademica->searchMateriasXSolicitudVisitaAcademica($id_solicitud_materias_responsable_visitas_academicas),
    //'filter'=>$listaVaMateriasImparteResponsableVisitaAcademica,
    'columns'=>array(
		//'id_solicitud_materias_responsable_visitas_academicas',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{logMateria}',
			'header'=>'Materia',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logMateria' => array
				(
					'label'=>'Materia',
					//'url'=>'',
					'imageUrl'=>'images/servicio_social/materia_32.png',
				),
			),
		),
		array(
			'header' => 'Clave <br>Materia',
			'name' => 'cveMateria',
			'filter' => false,
			'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
		),
		//'cveMateria',
		array(
			'header' => 'Nombre Materia',
			'filter' => false,
			'value' => function($data)
			{
				$cve = $data->cveMateria;
				$qry_mat = "select * from public.\"E_catalogoMaterias\" where \"cveMateria\" = '$cve' ";
				$rs = Yii::app()->db->createCommand($qry_mat)->queryAll();

				return $rs[0]['dscMateria'];
			},
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		//'fecha_registro_materia_visita_academica',
		array(
			'header' => 'Departamento',
			'filter' => false,
			'value' => function($data)
			{
				$cve_materia = trim($data->cveMateria);
				$modelECatalogoMaterias = ECatalogoMaterias::model()->findByPk($cve_materia);
				$depto = trim($modelECatalogoMaterias->deptoMateria);
				$qry_depto_acad = "select * from public.\"H_departamentos\" where \"cveDeptoAcad\" = '$depto' ";
                $rs = Yii::app()->db->createCommand($qry_depto_acad)->queryAll();
				$departamento = $rs[0]['dscDepartamento'];
				
				return ($departamento != NULL ) ? $departamento : "DESCONOCIDO";
			},
			'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delMateria}, {noDelMateria}',
			'header'=>'Eliminar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delMateria' => array
				(
					'label'=>'Eliminar Materia',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaMateriasImparteResponsableVisitaAcademica/eliminarMateriaVisitaAcademica", array("id_solicitud_materias_responsable_visitas_academicas"=>$data->id_solicitud_materias_responsable_visitas_academicas, 
																																			"cveMateria"=>$data->cveMateria))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
					'visible' => function($row, $data)
					{
						$id_mat = $data->id_solicitud_materias_responsable_visitas_academicas;
						$qry_mat_sol = "select * from pe_vinculacion.va_materias_imparte_responsable_visita_academica 
										where id_solicitud_materias_responsable_visitas_academicas = '$id_mat' ";
						$rs = Yii::app()->db->createCommand($qry_mat_sol)->queryAll();

						return (sizeof($rs) > 1) ? true : false;
					},
					'options' => array(
						'title'        => 'Eliminar la Materia Seleccionada que Cubre la Visita Académica',
						'data-confirm' => '¿En verdad quieres ELIMINAR la Materia?',
					),
					'click' => $JsDeleteMateria, 
				),
				'noDelMateria' => array
				(
					'label'=>'No se puede eliminar la Materia',
					//'url' => '',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$id_mat = $data->id_solicitud_materias_responsable_visitas_academicas;
						$qry_mat_sol = "select * from pe_vinculacion.va_materias_imparte_responsable_visita_academica 
										where id_solicitud_materias_responsable_visitas_academicas = '$id_mat' ";
						$rs = Yii::app()->db->createCommand($qry_mat_sol)->queryAll();

						return (sizeof($rs) < 2) ? true : false;
					},
					
				)
			),
		),
    ),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
