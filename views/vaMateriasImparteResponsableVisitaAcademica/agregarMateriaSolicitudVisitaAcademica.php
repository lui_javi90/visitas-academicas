<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académica' => '?r=visitasacademicas',
		'Materias Cubre Visitas Académica',
    );
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            <b>Materias Cubre Visitas Académica</b>
		</span>
	</h2>
</div>