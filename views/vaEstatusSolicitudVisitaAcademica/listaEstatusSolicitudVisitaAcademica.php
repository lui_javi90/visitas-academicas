<?php

	/* @var $this VaEstatusSolicitudVisitaAcademicaController */
	/* @var $model VaEstatusSolicitudVisitaAcademica */
	
	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Lista de Estatus de la Solicitud'
	);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Lista de Estatus de la Solicitud 
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-estatus-solicitud-visita-academica-grid',
	'dataProvider'=>$modelVaEstatusSolicitudVisitaAcademica->searchXIdEstatus(),
	'filter'=>$modelVaEstatusSolicitudVisitaAcademica,
	'columns'=>array(
		//'id_estatus_solicitud_visita_academica',
		array(
			'name' => 'estatus',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				switch($data->id_estatus_solicitud_visita_academica)
				{
					case 1:
						return '<span style="font-size:14px" class="label label-default">PENDIENTE</span>';
					break;
					case 2:
						return '<span style="font-size:14px" class="label label-info">ACEPTADA</span>';
					break;
					case 3:
						return '<span style="font-size:14px" class="label label-warning">SUSPENDIDA</span>';
					break;
					case 4:
						return '<span style="font-size:14px" class="label label-danger">CANCELADA</span>';
					break;
					case 5:
						return '<span style="font-size:14px" class="label label-success">COMPLETADA</span>';
					break;
					case 6:
						return '<span style="font-size:14px" class="label label-default">FINALIZADA</span>';
					break;
					case 7:
						return '<span style="font-size:14px" class="label label-warning">LIBERACIÓN</span>';
					break;
					case 8:
						return '<span style="font-size:14px" class="label label-primary">EN CURSO</span>';
					break;
				}
			},
			'htmlOptions' => array('width'=>'80px', 'height'=>'33px', 'class'=>'text-center')
		),
		array(
			'name' => 'descripcion_estatus',
			'filter' => false,
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
		),
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
