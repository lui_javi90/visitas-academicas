<?php
/* @var $this VaEstatusSolicitudVisitaAcademicaController */
/* @var $model VaEstatusSolicitudVisitaAcademica */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-estatus-solicitud-visita-academica-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

	<?php echo $form->errorSummary($modelVaEstatusSolicitudVisitaAcademica); ?>

	<?php if($modelVaEstatusSolicitudVisitaAcademica->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaEstatusSolicitudVisitaAcademica,'estatus'); ?>
		<?php echo $form->textField($modelVaEstatusSolicitudVisitaAcademica,'estatus',array('size'=>20,'maxlength'=>20, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaEstatusSolicitudVisitaAcademica,'estatus'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaEstatusSolicitudVisitaAcademica,'estatus'); ?>
		<?php echo $form->textField($modelVaEstatusSolicitudVisitaAcademica,
									'estatus',
									array('size'=>20,'maxlength'=>20, 'class'=>'form-control', 'readOnly'=>true)); ?>
		<?php echo $form->error($modelVaEstatusSolicitudVisitaAcademica,'estatus'); ?>
	</div>
	<?php } ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaEstatusSolicitudVisitaAcademica,'descripcion_estatus'); ?>
		<?php echo $form->textField($modelVaEstatusSolicitudVisitaAcademica,'descripcion_estatus',array('size'=>60,'maxlength'=>80, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelVaEstatusSolicitudVisitaAcademica,'descripcion_estatus'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelVaEstatusSolicitudVisitaAcademica->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaVEstatusSolicitudVisitaAcademica'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->