<?php

	/* @var $this VaEstatusSolicitudVisitaAcademicaController */
	/* @var $model VaEstatusSolicitudVisitaAcademica */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Lista de Estatus de la Solicitud' => array('vaEstatusSolicitudVisitaAcademica/listaVEstatusSolicitudVisitaAcademica'),
		'Lista de Estatus de la Solicitud'
	);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Lista de Estatus de la Solicitud 
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formEstatusSolicitudVisitaAcademica', array('modelVaEstatusSolicitudVisitaAcademica'=>$modelVaEstatusSolicitudVisitaAcademica)); ?>
			</div>
		</div>
	</div>
</div>