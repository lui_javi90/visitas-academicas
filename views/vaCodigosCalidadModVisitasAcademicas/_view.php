<?php
/* @var $this VaCodigosCalidadModVisitasAcademicasController */
/* @var $data VaCodigosCalidadModVisitasAcademicas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_codigo_calidad_mod_va')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_codigo_calidad_mod_va), array('view', 'id'=>$data->id_codigo_calidad_mod_va)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_documento_digital')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_documento_digital); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_calidad')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_calidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('revision')); ?>:</b>
	<?php echo CHtml::encode($data->revision); ?>
	<br />


</div>