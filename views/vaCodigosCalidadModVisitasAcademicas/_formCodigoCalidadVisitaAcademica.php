<?php
/* @var $this VaCodigosCalidadModVisitasAcademicasController */
/* @var $model VaCodigosCalidadModVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-codigos-calidad-mod-visitas-academicas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

	<?php echo $form->errorSummary($modelVaCodigosCalidadModVisitasAcademicas); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaCodigosCalidadModVisitasAcademicas,'nombre_documento_digital'); ?>
		<?php echo $form->textField($modelVaCodigosCalidadModVisitasAcademicas,'nombre_documento_digital',array('size'=>60,'maxlength'=>120, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaCodigosCalidadModVisitasAcademicas,'nombre_documento_digital'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaCodigosCalidadModVisitasAcademicas,'codigo_calidad'); ?>
		<?php echo $form->textField($modelVaCodigosCalidadModVisitasAcademicas,'codigo_calidad',array('size'=>25,'maxlength'=>25, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaCodigosCalidadModVisitasAcademicas,'codigo_calidad'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaCodigosCalidadModVisitasAcademicas,'revision'); ?>
		<?php echo $form->textField($modelVaCodigosCalidadModVisitasAcademicas,'revision',array('size'=>2,'maxlength'=>2, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaCodigosCalidadModVisitasAcademicas,'revision'); ?>
	</div>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelVaCodigosCalidadModVisitasAcademicas->isNewRecord ? 'Nuevo Registro' : 'Nuevo Registro', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaCodigosCalidadVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->