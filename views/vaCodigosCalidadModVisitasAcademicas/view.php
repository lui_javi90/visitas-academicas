<?php
/* @var $this VaCodigosCalidadModVisitasAcademicasController */
/* @var $model VaCodigosCalidadModVisitasAcademicas */

$this->breadcrumbs=array(
	'Va Codigos Calidad Mod Visitas Academicases'=>array('index'),
	$model->id_codigo_calidad_mod_va,
);

$this->menu=array(
	array('label'=>'List VaCodigosCalidadModVisitasAcademicas', 'url'=>array('index')),
	array('label'=>'Create VaCodigosCalidadModVisitasAcademicas', 'url'=>array('create')),
	array('label'=>'Update VaCodigosCalidadModVisitasAcademicas', 'url'=>array('update', 'id'=>$model->id_codigo_calidad_mod_va)),
	array('label'=>'Delete VaCodigosCalidadModVisitasAcademicas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_codigo_calidad_mod_va),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VaCodigosCalidadModVisitasAcademicas', 'url'=>array('admin')),
);
?>

<h1>View VaCodigosCalidadModVisitasAcademicas #<?php echo $model->id_codigo_calidad_mod_va; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_codigo_calidad_mod_va',
		'nombre_documento_digital',
		'codigo_calidad',
		'revision',
	),
)); ?>
