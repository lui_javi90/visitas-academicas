<?php
/* @var $this VaCodigosCalidadModVisitasAcademicasController */
/* @var $model VaCodigosCalidadModVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_codigo_calidad_mod_va'); ?>
		<?php echo $form->textField($model,'id_codigo_calidad_mod_va'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_documento_digital'); ?>
		<?php echo $form->textField($model,'nombre_documento_digital',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codigo_calidad'); ?>
		<?php echo $form->textField($model,'codigo_calidad',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'revision'); ?>
		<?php echo $form->textField($model,'revision',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->