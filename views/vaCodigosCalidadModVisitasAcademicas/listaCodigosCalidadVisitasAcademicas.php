<?php
	/* @var $this VaCodigosCalidadModVisitasAcademicasController */
	/* @var $model VaCodigosCalidadModVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Codigos de Calidad del Módulo Visitas Académicas'
	);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Codigos de Calidad del Módulo Visitas Académicas
		</span>
	</h2>
</div>

<div align="right" class="">
	<?php //echo CHtml::link('Nuevo Codigo Calidad', array('nuevoCodigoCalidadModVisitaAcademica'), array('class'=>'btn btn-success')); ?>
</div>

<br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-codigos-calidad-mod-visitas-academicas-grid',
	'dataProvider'=>$modelVaCodigosCalidadModVisitasAcademicas->searchListaCodigosCalidad(),
	'filter'=>$modelVaCodigosCalidadModVisitasAcademicas,
	'columns'=>array(
		//'id_codigo_calidad_mod_va',
		array(
			'name' => 'nombre_documento_digital',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'name' => 'codigo_calidad',
			'filter' => false,
			'htmlOptions' => array('width'=>'150px', 'class'=>'text-center')
		),
		array(
			'name' => 'revision',
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editSolicitud}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editSolicitud' => array
				(
					'label'=>'Editar Codigo Calidad',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaCodigosCalidadModVisitasAcademicas/editarCodigoCalidadModVisitaAcademica", array("id_codigo_calidad_mod_va"=>$data->id_codigo_calidad_mod_va))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
        ),
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>