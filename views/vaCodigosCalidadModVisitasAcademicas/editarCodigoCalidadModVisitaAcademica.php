<?php
	/* @var $this VaCodigosCalidadModVisitasAcademicasController */
	/* @var $model VaCodigosCalidadModVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Codigos de Calidad del Módulo Visitas Académicas' => array('vaCodigosCalidadModVisitasAcademicas/listaCodigosCalidadVisitasAcademicas'),
		'Editar Codigo de Calidad'
	);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Codigo de Calidad
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title">Editar</h6>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formCodigoCalidadVisitaAcademica', array('modelVaCodigosCalidadModVisitasAcademicas'=>$modelVaCodigosCalidadModVisitasAcademicas)); ?>
			</div>
		</div>
	</div>
</div>