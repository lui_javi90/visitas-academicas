<?php
/* @var $this VaCodigosCalidadModVisitasAcademicasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Va Codigos Calidad Mod Visitas Academicases',
);

$this->menu=array(
	array('label'=>'Create VaCodigosCalidadModVisitasAcademicas', 'url'=>array('create')),
	array('label'=>'Manage VaCodigosCalidadModVisitasAcademicas', 'url'=>array('admin')),
);
?>

<h1>Va Codigos Calidad Mod Visitas Academicases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
