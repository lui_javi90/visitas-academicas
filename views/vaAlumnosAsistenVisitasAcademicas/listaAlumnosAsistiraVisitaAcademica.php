<?php

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Consulta Solicitudes a Visitas Académicas Docentes',
    );
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Consulta Solicitudes a Visitas Académicas Docentes
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <b>Agregar</b>
                </h5>
            </div>
            <div align="center" class="panel-body">
                <?php echo CHtml::link('Agregar Alumno a Visita Académica', array('agregarAlumnoAVisitaAcademica'), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-solicitudes-visitas-academicas-grid',
    'dataProvider'=>$modelvaAlumnosAsistenVisitasAcademicas->search(), //Filtrar por id de la solicitud de visita academica
    'filter'=>$modelvaAlumnosAsistenVisitasAcademicas,
    'columns'=>array(
        'id_solicitud_visitas_academicas', //Nombre de la visita academica
        array(
            'header' => 'No. de Control',
            'name' => 'nctrAlumno',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_registro_alumno',
            'filter' => false,
            'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{quitarAlumnoVisitaAcademica}',
			'header'=>'Quitar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'quitarAlumnoVisitaAcademica' => array
				(
					'label'=>'Quitar Alumno Visita Académica',
                    'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/eliminarAlumnoVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas, 
                                                                                                                                "nctrAlumno"=>$data->nctrAlumno))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'options' => array(
						'title'        => 'Eliminar Alumno de Visita Académica',
						'data-confirm' => '¿En verdad quieres ELIMINAR al Alumno?',
					),
					//'click' => $JsDelSolicitudS, 
				),
			),
		),
    ),
)); ?>

<br><br><br><br>
<br><br><br><br>

