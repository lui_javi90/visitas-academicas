<?php

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Consulta Solicitudes a Visitas Académicas Docentes' => array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
        'Agregar Nuevo Alumno a Visita Académica'
    );

    //QUITAR ALUMNO DE LA LISTA DE LA VISITA ACADEMICA
    $JsDelAlumnoListaVisitaAcademica = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-lista_alumnos-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-lista_alumnos-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
    }';
    //QUITAR ALUMNO DE LA LISTA DE LA VISITA ACADEMICA
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Detalle Visita Académica
		</span>
	</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <b>Detalle Visita Académica</b>
                </h5>
            </div>
            <div class="panel-body">

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('nombre_visita_academica')); ?>:</b>
                    <?php echo $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_empresa_visita')); ?>:</b>
                    <?php echo $empresa; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('no_solicitud')); ?>:</b>
                    <?php echo $modelVaSolicitudesVisitasAcademicas->no_solicitud; ?></p>

            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Agregar Nuevo Alumno a Visita Académica
		</span>
	</h2>
</div>

<br>
<br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Una vez ingresado el No. de Control en el campo debes dar clic en cualquier parte de la pantalla para que el Sistema te detecte al alumno
  que quieres inscribir en la Visita Académica.</b>
  </strong></p>
</div>

<div class="row"><!--Row 1-->
    <div class="col-md-7">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <b>Agregar Alumno</b>
                </h5>
            </div>
            <div class="panel-body">

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'va-agregar-alumno-visita-academica-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

                    <?php echo $form->errorSummary($modelVaAlumnosAsistenVisitasAcademicas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaAlumnosAsistenVisitasAcademicas,'nctrAlumno'); ?>
                        <?php echo $form->textField($modelVaAlumnosAsistenVisitasAcademicas,
                                                    'nctrAlumno',
                                                    array('class'=>'form-control', 'size'=>9,'maxlength'=>8, 'required'=>'required',
                                                    'ajax'=>array(
                                                        'type'=>'POST',
                                                        'dataType'=>'json',
                                                        'data' => array(
            
                                                            'nctrAlumno'=>'js:$(\'#VaAlumnosAsistenVisitasAcademicas_nctrAlumno\').val()',
                                                        ),
                                                        'url'=>CController::createUrl('vaAlumnosAsistenVisitasAcademicas/infoAlumnoSeleccionado'),
                                                        'success'=>'function(data)
                                                        {
                                                            if(data.status_campo == "NV")
                                                            {
                                                                $(".div_foto_alumno").show();
                                                                $(".div_nombre_alumno").show();
                                                                $(".div_carrera_alumno").show();
                                                                $(".div_semestre_alumno").show();
                                                                $(".div_foto_alumno").html(\'<img class="img-circle" align="center" heigth="150" width="150" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=\'+ data.no_ctrl +\' " />\');
                                                                $(".div_nombre_alumno").html(\'<b><label align="left">Nombre:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color: transparent;"  value="\'+ data.nombre_alumno +\'" readonly />\');
                                                                $(".div_carrera_alumno").html(\'<b><label align="left">Carrera:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color: transparent;"  value="\'+ data.carrera +\'" readonly />\');
                                                                $(".div_semestre_alumno").html(\'<b><label align="left">Semestre:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color: transparent;"  value="\'+ data.semestre +\'" readonly />\');
                                                            
                                                            }else
                                                            if(data.status_campo == "NEA"){

                                                                $(".div_foto_alumno").hide();
                                                                $(".div_nombre_alumno").hide();
                                                                $(".div_carrera_alumno").hide();
                                                                $(".div_semestre_alumno").hide();

                                                                $(".div_alumno_no_existe").show();
                                                                $(".div_alumno_no_existe").html(\'<b><label align="left">No. de Control No Existe en el Sistema.</label></b>\');
                                                                $(".div_campo_vacio").hide();

                                                            }else
                                                            if(data.status_campo == "V"){

                                                                $(".div_foto_alumno").hide();
                                                                $(".div_nombre_alumno").hide();
                                                                $(".div_carrera_alumno").hide();
                                                                $(".div_semestre_alumno").hide();

                                                                $(".div_alumno_no_existe").hide();
                                                                $(".div_campo_vacio").show();
                                                                $(".div_campo_vacio").html(\'<b><label align="left">Campo esta vacio. Teclea No. de Control.</label></b>\');

                                                            }
                                                        }',
                                                    )
                                                    )); ?>
                        <?php echo $form->error($modelVaAlumnosAsistenVisitasAcademicas,'nctrAlumno'); ?>
                    </div>
                    
                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelVaAlumnosAsistenVisitasAcademicas->isNewRecord ? 'Registrar Alumno' : 'Registrar Alumno', array('class'=>'btn btn-primary')); ?>
                        <?php echo CHtml::link('Cancelar Registro', array('listaAlumnosAsistiraVisitaAcademica'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->

            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <b>Perfíl del Alumno</b>
                </h3>
            </div>
            <div class="panel-body">

                <!--Datos del Alumno Seleccionado-->
                <div align="center" class="div_foto_alumno"></div>

                <div align="center" class="div_nombre_alumno"></div>

                <div align="center" class="div_carrera_alumno"></div>

                <div align="center" class="div_semestre_alumno"></div>
                <!--Datos del Alumno Seleccionado-->

                <!--Campos vacios-->
                <!--No Existe Alumno en la BD con ese no. control-->
                <div align="center" class="div_alumno_no_existe"></div>

                <!--No. control Vacio-->
                <div align="center" class="div_campo_vacio"></div>
                <!--Campos vacios-->

            </div>
        </div>
    </div>
</div><!--Row 1-->

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Alumnos Inscritos en la Visita Académica
		</span>
	</h2>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-lista_alumnos-visitas-academicas-grid',
    'dataProvider'=>$modelListaVaAlumnosAsistenVisitasAcademicas->searchXAlumnosInscritosVisita($id_solicitud_visitas_academicas),
    'filter'=>$modelListaVaAlumnosAsistenVisitasAcademicas,
    'columns'=>array(
        //'nctrAlumno',
        array(
            'header' => 'Foto Alumno',
            'filter' => false,
            'type'=>'raw',
            'value' => function($data)
            {
                return CHtml::image("https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=".trim($data->nctrAlumno), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
            },
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(

            'header' => 'No. de Control',
            'name' => 'nctrAlumno',
            'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Nombre (s)',
            'filter' => false,
            'value' => function($data)
            {
                $no_ctrl = $data->nctrAlumno;

                $qry_name = "select * from public.\"E_datosAlumno\" where \"nctrAlumno\" = '$no_ctrl' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['nmbSoloAlumno'];

            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Apellido Paterno',
            'filter' => true,
            'value' => function($data)
            {
                $no_ctrl = $data->nctrAlumno;

                $qry_name = "select * from public.\"E_datosAlumno\" where \"nctrAlumno\" = '$no_ctrl' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['apellPaternoAlu'];
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Apellido Materno',
            'filter' => true,
            'value' => function($data)
            {
                $no_ctrl = $data->nctrAlumno;

                $qry_name = "select * from public.\"E_datosAlumno\" where \"nctrAlumno\" = '$no_ctrl' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['apellMaternoAlu'];
            },
            'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        //'fecha_registro_alumno',
        array(
			'class'=>'CButtonColumn',
			'template'=>'{quitAlumListaVisitaAcademica}',
			'header'=>'Quitar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'quitAlumListaVisitaAcademica' => array
				(
					'label'=>'Quitar de la lista',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/eliminarAlumnoVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas, "nctrAlumno"=>$data->nctrAlumno))',
                    'imageUrl'=>'images/servicio_social/cancelado_32.png',
                    'options' => array(
						'title'        => 'Eliminar Alumno de Lista de la Visita Académica',
						'data-confirm' => '¿En verdad quieres ELIMINAR el Alumno de la Lista?',
					),
					'click' => $JsDelAlumnoListaVisitaAcademica, 
				),
			),
        ),
    ),
)); ?>

<br><br><br><br>
<br><br><br><br>
