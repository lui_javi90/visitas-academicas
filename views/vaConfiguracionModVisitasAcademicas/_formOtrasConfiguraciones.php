<?php
/* @var $this VaConfiguracionModVisitasAcademicasController */
/* @var $model VaConfiguracionModVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-configuracion-mod-visitas-academicas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

	<?php echo $form->errorSummary($modelVaConfiguracionModVisitasAcademicas); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaConfiguracionModVisitasAcademicas,'max_numero_solicitudes_vigentes_por_docente'); ?>
		<?php echo $form->textField($modelVaConfiguracionModVisitasAcademicas,
									'max_numero_solicitudes_vigentes_por_docente',
									array('maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelVaConfiguracionModVisitasAcademicas,'max_numero_solicitudes_vigentes_por_docente'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaConfiguracionModVisitasAcademicas,'texto_legenda_inicio_reportes_pdf'); ?>
		<?php echo $form->textField($modelVaConfiguracionModVisitasAcademicas,
									'texto_legenda_inicio_reportes_pdf',
									array('size'=>60,'maxlength'=>200, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaConfiguracionModVisitasAcademicas,'texto_legenda_inicio_reportes_pdf'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaConfiguracionModVisitasAcademicas,'max_numero_alumnos_por_visita_academica'); ?>
		<?php echo $form->textField($modelVaConfiguracionModVisitasAcademicas,
									'max_numero_alumnos_por_visita_academica',
									array('maxlength'=>3, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelVaConfiguracionModVisitasAcademicas,'max_numero_alumnos_por_visita_academica'); ?>
	</div>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaConfiguracionModVisitasAcademicas,'fecha_ultima_modificacion'); ?>
		<?php //echo $form->textField($modelVaConfiguracionModVisitasAcademicas,'fecha_ultima_modificacion'); ?>
		<?php //echo $form->error($modelVaConfiguracionModVisitasAcademicas,'fecha_ultima_modificacion'); ?>
	</div>-->

	<div class="form-group">
		<?php echo CHtml::submitButton($modelVaConfiguracionModVisitasAcademicas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-success')); ?>
		<?php echo CHtml::link('Cancelar', array('configuracionModuloVisitaAcademica'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->