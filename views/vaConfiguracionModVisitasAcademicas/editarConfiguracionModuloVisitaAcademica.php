<?php
/* @var $this VaConfiguracionModVisitasAcademicasController */
/* @var $model VaConfiguracionModVisitasAcademicas */

$this->breadcrumbs=array(
	'Visitas Académicas' => '?r=visitasacademicas',
	'Otras Configuraciones del Módulo' => array('vaConfiguracionModVisitasAcademicas/configuracionModuloVisitaAcademica'),
	'Editar Configuraciones'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Configuraciones
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4 class="panel-title">
					Editar Configuraciones
				</h4>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formOtrasConfiguraciones', array('modelVaConfiguracionModVisitasAcademicas'=>$modelVaConfiguracionModVisitasAcademicas)); ?>
			</div>
		</div>
	</div>
</div>

<br><br><br><br><br>
<br><br><br><br><br>