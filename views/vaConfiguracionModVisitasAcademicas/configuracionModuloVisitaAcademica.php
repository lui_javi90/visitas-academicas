<?php
/* @var $this VaConfiguracionModVisitasAcademicasController */
/* @var $model VaConfiguracionModVisitasAcademicas */

$this->breadcrumbs=array(
	'Visitas Académicas' => '?r=visitasacademicas',
	'Otras Configuraciones del Módulo'
);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Otras Configuraciones del Módulo
		</span>
	</h2>
</div>

<br><br><br><br><br>
<br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  Última fecha de actualización el día <b><?php echo $fec_ult_act; ?></b>
  </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-configuracion-mod-visitas-academicas-grid',
	'dataProvider'=>$modelVaConfiguracionModVisitasAcademicas->searchBusqueda(),
	//'filter'=>$modelVaConfiguracionModVisitasAcademicas,
	'columns'=>array(
		//'id_configuracion_mod_visitas_academicas',
		array(
			'name' => 'max_numero_solicitudes_vigentes_por_docente',
			'filter' => false,
			'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
		),
		array(
			'name' => 'texto_legenda_inicio_reportes_pdf',
			'filter' => false,
			'value' => function($data)
			{
				return ($data->texto_legenda_inicio_reportes_pdf == null) ? "Sin texto agregado" : $data->texto_legenda_inicio_reportes_pdf;
			},
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
		),
		array(
			'name' => 'max_numero_alumnos_por_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		/*array(
			'name' => 'fecha_ultima_modificacion',
			'filter' => false,
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

				$id = $data->id_configuracion_mod_visitas_academicas;
				$fec = InfoSolicitudVisitasAcademicas::getFechaUltimaActualizacionOtrasConfiguraciones($id);

				return ($fec[0]['fecha_act'] == 'SV') ? "Desconocida" : $fec[0]['fecha_act'].' a las '.$fec[0]['hora'];
			},
			'htmlOptions' => array('width'=>'180px', 'class'=>'text-center')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editConfiguraciones}',
			'header'=>'Editar <br>Configuraciones',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editConfiguraciones' => array
				(
					'label'=>'Editar Configuraciones',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaConfiguracionModVisitasAcademicas/editarConfiguracionModuloVisitaAcademica", array("id_configuracion_mod_visitas_academicas"=>$data->id_configuracion_mod_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
	),
)); ?>

<br><br><br><br><br>
<br><br><br><br><br>
