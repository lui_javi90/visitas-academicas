<?php
/* @var $this VaTiposVisitasAcademicasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Va Tipos Visitas Academicases',
);

$this->menu=array(
	array('label'=>'Create VaTiposVisitasAcademicas', 'url'=>array('create')),
	array('label'=>'Manage VaTiposVisitasAcademicas', 'url'=>array('admin')),
);
?>

<h1>Va Tipos Visitas Academicases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
