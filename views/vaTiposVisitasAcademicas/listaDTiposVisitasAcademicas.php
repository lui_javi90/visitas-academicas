<?php
	/* @var $this VaTiposVisitasAcademicasController */
	/* @var $model VaTiposVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Tipos de Visitas Académicas'
    );
    
?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Tipos de Visitas Académicas
		</span>
	</h2>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-tipos-visitas-academicas-grid',
	'dataProvider'=>$modelVaTiposVisitasAcademicas->searchListadoTiposVisistasAcademicas(),
	'filter'=>$modelVaTiposVisitasAcademicas,
	'columns'=>array(
        //'id_tipo_visita_academica',
        array(
			'class'=>'CButtonColumn',
			'template'=>'{logoTipos}',
			'header'=>'Tipos',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logoTipos' => array
				(
					'label'=>'Tipos Visita Académica',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
				),
			),
		),
		array(
			'name' => 'tipo_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'700px', 'class'=>'text-center')
		),
	),
)); ?>