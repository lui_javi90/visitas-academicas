<?php
/* @var $this VaTiposVisitasAcademicasController */
/* @var $model VaTiposVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-tipos-visitas-academicas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

	<?php echo $form->errorSummary($modelVaTiposVisitasAcademicas); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelVaTiposVisitasAcademicas,'tipo_visita_academica'); ?>
		<?php echo $form->textField($modelVaTiposVisitasAcademicas,'tipo_visita_academica',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelVaTiposVisitasAcademicas,'tipo_visita_academica'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($modelVaTiposVisitasAcademicas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaTiposVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->