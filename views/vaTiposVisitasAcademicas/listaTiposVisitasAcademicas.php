<?php
	/* @var $this VaTiposVisitasAcademicasController */
	/* @var $model VaTiposVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Tipos de Visitas Académicas'
	);

	/*JS PARA VALIDAR LA ELIMINACION DE LA PLAZA SELECCIONADA DEL EMPLEADO*/
	$valTipoVisitaAcad = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-tipos-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-tipos-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*JS PARA VALIDAR ENVIO A EVALUACION DEL REPORTE DE SERVICIO SOCIAL (INTERNO)*/

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Tipos de Visitas Académicas
		</span>
	</h2>
</div>

<br><br><br>
<div align="right">
	<?php echo CHtml::link('Nuevo Tipo de Visita Académica', array('nuevoTipoVisitaAcademica'), array('class'=>'btn btn-success')); ?>
</div>

<br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-tipos-visitas-academicas-grid',
	'dataProvider'=>$modelVaTiposVisitasAcademicas->searchListadoTiposVisistasAcademicas(),
	'filter'=>$modelVaTiposVisitasAcademicas,
	'columns'=>array(
		//'id_tipo_visita_academica',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{logoTipos}',
			'header'=>'Tipos',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logoTipos' => array
				(
					'label'=>'Tipos Visita Académica',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
				),
			),
		),
		array(
			'name' => 'tipo_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'700px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarTipoVisita}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarTipoVisita' => array
				(
					'label'=>'Editar Tipo Visita Académica',
                    'url'=>'Yii::app()->createUrl("visitasacademicas/vaTiposVisitasAcademicas/editarTipoVisitaAcademica", array("id_tipo_visita_academica"=>$data->id_tipo_visita_academica))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					//'visible' =>'$data->id_estado_servicio_social > 1 AND $data->id_estado_servicio_social != 7' //Para visualizar su carta de terminación
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{tipoVistaHab},{tipoVistaDeshab}',
			'header'=>'Deshabilitar',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'tipoVistaHab' => array
				(
					'label'=>'Deshabilitar Tipo de Visita Académica',
                    'url'=>'Yii::app()->createUrl("visitasacademicas/vaTiposVisitasAcademicas/deshabilitarTipoVisitaAcademica", array("id_tipo_visita_academica"=>$data->id_tipo_visita_academica, "val"=>0))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' =>'$data->tipo_valido == 1', //Para visualizar su carta de terminación
					'options' => array(
						'title'        => 'Desactivar Tipo de Visita Académica',
						'data-confirm' => '¿En verdad quieres DESACTIVAR el Tipo de Visita Académica?',
					),
					'click'   => $valTipoVisitaAcad, 
				),
				'tipoVistaDeshab' => array
				(
					'label'=>'Habilitar Tipo de Visita Académica',
                    'url'=>'Yii::app()->createUrl("visitasacademicas/vaTiposVisitasAcademicas/deshabilitarTipoVisitaAcademica", array("id_tipo_visita_academica"=>$data->id_tipo_visita_academica))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' =>'$data->tipo_valido == 0', //Para visualizar su carta de terminación
					'options' => array(
						'title'        => 'Activar Tipo de Visita Académica',
						'data-confirm' => '¿En verdad quieres ACTIVAR el Tipo de Visita Académica?',
					),
					'click'   => $valTipoVisitaAcad, 
				),
			),
        ),
	),
)); ?>
