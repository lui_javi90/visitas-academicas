<?php
/* @var $this VaTiposVisitasAcademicasController */
/* @var $model VaTiposVisitasAcademicas */

$this->breadcrumbs=array(
	'Visitas Académicas' => '?r=visitasacademicas',
	'Tipos de Visitas Académicas' => array('VaTiposVisitasAcademicas/listaTiposVisitasAcademicas'),
	'Editar Tipo de Visita Académica'
);

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Tipo de Visita Académica
		</span>
	</h2>
</div>

<div clasa="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Editar</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formTipoVisitaAcademica', array('modelVaTiposVisitasAcademicas'=>$modelVaTiposVisitasAcademicas)); ?>
			</div>
		</div>
	</div>
</div>

<br><br><br><br><br>
<br><br><br><br><br>