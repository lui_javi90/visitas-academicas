<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */

$this->breadcrumbs=array(
	'Va Bitacora Cambios Solicitud Vinculacions'=>array('index'),
	$model->id_bitacora_solicitud_visita_academica,
);

$this->menu=array(
	array('label'=>'List VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('index')),
	array('label'=>'Create VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('create')),
	array('label'=>'Update VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('update', 'id'=>$model->id_bitacora_solicitud_visita_academica)),
	array('label'=>'Delete VaBitacoraCambiosSolicitudVinculacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_bitacora_solicitud_visita_academica),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('admin')),
);
?>

<h1>View VaBitacoraCambiosSolicitudVinculacion #<?php echo $model->id_bitacora_solicitud_visita_academica; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_bitacora_solicitud_visita_academica',
		'fecha_hora_salida',
		'fecha_hora_regreso',
		'no_alumnos_visita',
		'fecha_ultima_modificacion_solicitud',
	),
)); ?>
