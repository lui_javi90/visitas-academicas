<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_bitacora_solicitud_visita_academica'); ?>
		<?php echo $form->textField($model,'id_bitacora_solicitud_visita_academica'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_hora_salida'); ?>
		<?php echo $form->textField($model,'fecha_hora_salida'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_hora_regreso'); ?>
		<?php echo $form->textField($model,'fecha_hora_regreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_alumnos_visita'); ?>
		<?php echo $form->textField($model,'no_alumnos_visita'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_ultima_modificacion_solicitud'); ?>
		<?php echo $form->textField($model,'fecha_ultima_modificacion_solicitud'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->