<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $data VaBitacoraCambiosSolicitudVinculacion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_bitacora_solicitud_visita_academica')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_bitacora_solicitud_visita_academica), array('view', 'id'=>$data->id_bitacora_solicitud_visita_academica)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_hora_salida')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_hora_salida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_hora_regreso')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_hora_regreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_alumnos_visita')); ?>:</b>
	<?php echo CHtml::encode($data->no_alumnos_visita); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ultima_modificacion_solicitud')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ultima_modificacion_solicitud); ?>
	<br />


</div>