<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */

$this->breadcrumbs=array(
	'Va Bitacora Cambios Solicitud Vinculacions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('index')),
	array('label'=>'Manage VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('admin')),
);
?>

<h1>Create VaBitacoraCambiosSolicitudVinculacion</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>