<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-bitacora-cambios-solicitud-vinculacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_bitacora_solicitud_visita_academica'); ?>
		<?php echo $form->textField($model,'id_bitacora_solicitud_visita_academica'); ?>
		<?php echo $form->error($model,'id_bitacora_solicitud_visita_academica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_hora_salida'); ?>
		<?php echo $form->textField($model,'fecha_hora_salida'); ?>
		<?php echo $form->error($model,'fecha_hora_salida'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_hora_regreso'); ?>
		<?php echo $form->textField($model,'fecha_hora_regreso'); ?>
		<?php echo $form->error($model,'fecha_hora_regreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_alumnos_visita'); ?>
		<?php echo $form->textField($model,'no_alumnos_visita'); ?>
		<?php echo $form->error($model,'no_alumnos_visita'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ultima_modificacion_solicitud'); ?>
		<?php echo $form->textField($model,'fecha_ultima_modificacion_solicitud'); ?>
		<?php echo $form->error($model,'fecha_ultima_modificacion_solicitud'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->