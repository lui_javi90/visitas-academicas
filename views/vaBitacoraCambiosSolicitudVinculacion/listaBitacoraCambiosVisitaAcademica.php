<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */

$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Mis Solicitudes a Visitas Académicas' => array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
		'Bitacora de Solicitud Visita Académica'
);

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Bitacora de Solicitud Visita Académica
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-bitacora-cambios-solicitud-vinculacion-grid', //searchBitacoraXSolicitudVisitaAcademica
	'dataProvider'=>$modelVaBitacoraCambiosSolicitudVinculacion->searchBitacoraXSolicitudVisitaAcademica($id_solicitud_visitas_academicas),
	'filter'=>$modelVaBitacoraCambiosSolicitudVinculacion,
	'columns'=>array(
		/*array(
			'header' => 'No. <br>Solicitud',
			'name' => 'idSolicitudVisitasAcademicas.no_solicitud',
			//'filter' => false,
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),*/
		/*array(
			'header' => 'Foto',
            'type'=>'raw',
			'value' => function($data)
			{
				$rfc = $data->usuario_modifica;

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'30px','class'=>'text-center')
		),*/
		array(
			'class' => 'ComponentDatosJefeOfServiciosExternos',
			'htmlOptions' => array('width'=>'100px', 'class'=>'text-center')
		),
		array(
			'header' => 'No. de <br>Alumnos',
			'name' => 'no_alumnos_visita',
			'filter' => false,
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		//'id_bitacora_solicitud_visita_academica',
        //'id_solicitud_visitas_academicas',
		array(
			'name' => 'fecha_hora_salida',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaSalidaBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
        array(
            'name' => 'fecha_hora_regreso',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaRegresoBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_ultima_modificacion_solicitud',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaActualizacionBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
		array(
			'header' => 'Modificó Doc <br>Digital',
			'filter' => false,
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->modifico_doc_digital == 1) ? '<span style="font-size:16px" class="label label-success">SI</span>' : '<span style="font-size:16px" class="label label-danger">NO</span>';
			},
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
	),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>