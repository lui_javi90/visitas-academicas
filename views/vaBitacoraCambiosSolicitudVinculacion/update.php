<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $model VaBitacoraCambiosSolicitudVinculacion */

$this->breadcrumbs=array(
	'Va Bitacora Cambios Solicitud Vinculacions'=>array('index'),
	$model->id_bitacora_solicitud_visita_academica=>array('view','id'=>$model->id_bitacora_solicitud_visita_academica),
	'Update',
);

$this->menu=array(
	array('label'=>'List VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('index')),
	array('label'=>'Create VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('create')),
	array('label'=>'View VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('view', 'id'=>$model->id_bitacora_solicitud_visita_academica)),
	array('label'=>'Manage VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('admin')),
);
?>

<h1>Update VaBitacoraCambiosSolicitudVinculacion <?php echo $model->id_bitacora_solicitud_visita_academica; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>