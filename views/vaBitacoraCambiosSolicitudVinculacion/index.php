<?php
/* @var $this VaBitacoraCambiosSolicitudVinculacionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Va Bitacora Cambios Solicitud Vinculacions',
);

$this->menu=array(
	array('label'=>'Create VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('create')),
	array('label'=>'Manage VaBitacoraCambiosSolicitudVinculacion', 'url'=>array('admin')),
);
?>

<h1>Va Bitacora Cambios Solicitud Vinculacions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
