<?php
	/* @var $this GRpempresasController */
	/* @var $model GRpempresas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Empresas Disponibles para Visita Académica',
	);

	/*JS PARA VALIDAR LA ELIMINACION DE LA EMPRESA SELECCIONADA*/
	$JsDelEmpresa = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#lista-grpempresas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#lista-grpempresas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*JS PARA VALIDAR LA ELIMINACION DE LA EMPRESA SELECCIONADA*/
?>


<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Empresas Disponibles para Visita Académica
		</span>
	</h2>
</div>


<br><br><br><br><br>
<div align="right" class="">
	<?php //echo CHtml::link('Nueva Empresa Visita', array('nuevaEmpresaVisitaAcademica'), array('class'=> 'btn btn-success')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'lista-grpempresas-grid',
	'dataProvider'=>$modelGRpempresas->searchEmpresasVisitaAcademica(),
	'filter'=>$modelGRpempresas,
	'columns'=>array(
		//'idempresa',
		array(
			'header' => 'RFC <br>Empresa',
			'name' => 'rfc',
			//'filter' => false,
			'htmlOptions' => array('width'=>'50px', 'class'=>'text-center')
		),
		array(
			'name' => 'nombre',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'header' => 'Sector',
			'filter' => CHtml::activeDropDownList($modelGRpempresas,
											'idsector',
											$lista_sectores_empresa,
											array('prompt'=>'-- Filtrar por --')
			),
			'value' => function($data)
			{
				$id = $data->idsector;
				$qry_sec = "select * from pe_vinculacion.g_rpsectorempresa where idsector = '$id' ";
				$rs = Yii::app()->db->createCommand($qry_sec)->queryAll();

				return $rs[0]['descripcion'];
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header' => 'Estado',
			'filter' => CHtml::activeDropDownList($modelGRpempresas,
							'idestado',
							$lista_estados,
							array('prompt'=>'-- Filtrar por --')
			),
			'value' => function($data)
			{
				$id_edo = $data->idestado;
				$qry_estado = "select * from public.\"X_estados\" where id_estado = '$id_edo' ";
				$rs = Yii::app()->db->createCommand($qry_estado)->queryAll();

				return $rs[0]['desc_estado'];
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Municipio',
			//'filter' => false,
			'filter' =>  CHtml::activeDropDownList($modelGRpempresas,
						'idmunicipio',
						CHtml::listData(
							XMunicipios::model()->findAll(
								" id_estado > '00' ",
								array('order'=>'id_municipio ASC')
							),
							'id_municipio',
							'desc_mun'
						),
						array('prompt'=>'-- Filtrar por --')
			),
			'value' => function($data)
			{
				$id_mun = $data->idmunicipio;
				$id_edo = $data->idestado;
				$qry_mun = "select * from public.\"X_municipios\" where id_municipio = '$id_mun' ";
				$rs = Yii::app()->db->createCommand($qry_mun)->queryAll();

				return $rs[0]['desc_mun'];
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detEmpresaVisitaAcademica}',
			'header'=>'Detalle Empresa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detEmpresaVisitaAcademica' => array
				(
					'label'=>'Detalle de la Empresa',
					'url'=>'Yii::app()->createUrl("visitasacademicas/gRpempresas/detalleEmpresaVisitaAcademica", array("idempresa"=>$data->idempresa))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#parmdialog").dialog("open"); 
						
						return false;
					}',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editEmpresaVisitaAcademica}',
			'header'=>'Editar Empresa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editEmpresaVisitaAcademica' => array
				(
					'label'=>'Editar Empresa',
					'url'=>'Yii::app()->createUrl("visitasacademicas/gRpempresas/editarEmpresaVisitaAcademica", array("idempresa"=>$data->idempresa))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{manEmpresaVisitaAcademica}',
			'header'=>'Manifiesto Empresa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'manEmpresaVisitaAcademica' => array
				(
					'label'=>'Manifiesto de requisitos de seguridad de la Empresa',
					'url'=>'Yii::app()->createUrl("visitasacademicas/gRpempresas/agregarManifiestoRequisitosSeguridadIngresoEmpresa", array("idempresa"=>$data->idempresa))',
					'imageUrl'=>'images/servicio_social/subir_manifiesto_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delEmpresaVisitaAcademica}',
			'header'=>'Eliminar Empresa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delEmpresaVisitaAcademica' => array
				(
					'label'=>'Eliminar Empresa',
					'url'=>'Yii::app()->createUrl("visitasacademicas/gRpempresas/eliminarEmpresaVisitaAcademica", array("idempresa"=>$data->idempresa))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
					'options' => array(
						'title'        => 'Eliminar la Empresa Seleccionada',
						'data-confirm' => '¿En verdad quieres ELIMINAR la Empresa?',
					),
					'click' => $JsDelEmpresa
				),
			),
		),*/
	),
)); ?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'parmdialog',
	'options' => array(
		'title' => 'Detalle de la Empresa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 750,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
