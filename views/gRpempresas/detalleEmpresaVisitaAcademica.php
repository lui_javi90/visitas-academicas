<?php
	/* @var $this GRpempresasController */
	/* @var $model GRpempresas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Empresas Disponibles a Visita Académica' => array('gRpempresas/listaEmpresasVisitasAcademicas'),
		'Detalle de Empresa'
	);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle de Empresa
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Detalle</b>
				</h3>
			</div>
			<div class="panel-body">

				<div class="row">
					<div align="center" style="vertical-align:center;" class="col-md-3">
						<?php echo '<img align="center" heigth="300" width="150" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/logo_empresa_default.png"/>';  ?>
					</div>

					<div class="col-md-9">

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('rfc')); ?>:</b>
						<?php echo $modelGRpempresas->nombre; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('nombre')); ?>:</b>
						<?php echo $modelGRpempresas->rfc; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('idsector')); ?>:</b>
						<?php echo $sector; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('domicilio')); ?>:</b>
						<?php echo $modelGRpempresas->domicilio; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('colonia')); ?>:</b>
						<?php echo $modelGRpempresas->colonia; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('codigopostal')); ?>:</b>
						<?php echo $modelGRpempresas->codigopostal; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('idestado')); ?>:</b>
						<?php echo $estado; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('idmunicipio')); ?>:</b>
						<?php echo $municipio; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('email')); ?>:</b>
						<?php echo $modelGRpempresas->email; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('telefono')); ?>:</b>
						<?php echo $modelGRpempresas->telefono; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('nombre_presenta')); ?>:</b>
						<?php echo $modelGRpempresas->nombre_presenta; ?>
						</p>

						<p><b><?php echo CHtml::encode($modelGRpempresas->getAttributeLabel('cargo_presenta')); ?>:</b>
						<?php echo $modelGRpempresas->cargo_presenta; ?>
						</p>
					
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<br><br><br>
