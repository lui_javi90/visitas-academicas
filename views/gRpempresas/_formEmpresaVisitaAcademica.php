<?php
/* @var $this GRpempresasController */
/* @var $model GRpempresas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'grpempresas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>

	<?php echo $form->errorSummary($modelGRpempresas); ?>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'nombre'); ?>
		<?php echo $form->textField($modelGRpempresas,'nombre', array('class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelGRpempresas,'nombre'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'rfc'); ?>
		<?php echo $form->textField($modelGRpempresas,'rfc',array('size'=>13,'maxlength'=>13, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'rfc'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'idsector'); ?>
		<?php echo $form->dropDownList($modelGRpempresas,
										'idsector',
										$lista_sectores,
										array('prompt'=>'-- Elige Sector --', 'class'=>'form-control', 'required'=>'required')
										); ?>
		<?php echo $form->error($modelGRpempresas,'idsector'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'domicilio'); ?>
		<?php echo $form->textField($modelGRpempresas,'domicilio', array('class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'domicilio'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'colonia'); ?>
		<?php echo $form->textField($modelGRpempresas,'colonia', array('class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'colonia'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'codigopostal'); ?>
		<?php echo $form->textField($modelGRpempresas,'codigopostal',array('size'=>6,'maxlength'=>6, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'codigopostal'); ?>
	</div>

	<div class="form-group">
		<?php 
				$htmlOptions = array('prompt'=>'-- Selecciona Estado --','class'=>'form-control', 'required'=>'required',
					'ajax' => array(
						'url' => $this->createUrl('getMunicipiosEstado'),
						'type' => 'POST',
						'update' => '#GRpempresas_idmunicipio'
					),
				);
		?>
		<?php echo $form->labelEx($modelGRpempresas,'idestado'); ?>
		<?php echo $form->dropDownList($modelGRpempresas,
									'idestado',
									$lista_estados,
									$htmlOptions,
								); ?>
		<?php echo $form->error($modelGRpempresas,'idestado'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'idmunicipio'); ?>
		<?php echo $form->dropDownList($modelGRpempresas,
									'idmunicipio',
									array(),
									//$lista_municipios,
									array('prompt'=>'-- Selecciona Municipio --','class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelGRpempresas,'idmunicipio'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'email'); ?>
		<?php echo $form->textField($modelGRpempresas,'email', array('class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'telefono'); ?>
		<?php echo $form->textField($modelGRpempresas,'telefono',array('size'=>60,'maxlength'=>250, 'class'=>'form-control')); ?>
		<?php echo $form->error($modelGRpempresas,'telefono'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'nombre_presenta'); ?>
		<?php echo $form->textField($modelGRpempresas,'nombre_presenta',array('size'=>60,'maxlength'=>120, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelGRpempresas,'nombre_presenta'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($modelGRpempresas,'cargo_presenta'); ?>
		<?php echo $form->textField($modelGRpempresas,'cargo_presenta',array('size'=>60,'maxlength'=>120, 'class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelGRpempresas,'cargo_presenta'); ?>
	</div>

	<br>
	<div class="form-group">
		<?php echo CHtml::submitButton($modelGRpempresas->isNewRecord ? 'Guadar Registro' : 'Guardar', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaEmpresasVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->