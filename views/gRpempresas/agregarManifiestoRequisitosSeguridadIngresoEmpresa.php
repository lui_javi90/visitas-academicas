<?php
	/* @var $this GRpempresasController */
	/* @var $model GRpempresas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Empresas Disponibles para Visita Académica' => array('gRpempresas/listaEmpresasVisitasAcademicas'),
        'Agregar Manifiesto de Seguridad para Ingreso a la Empresa'
    );
    
?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Agregar Manifiesto de Seguridad para Ingreso a la Empresa
		</span>
	</h2>
</div>

<br>
<?php print_r($modelGRpempresas); ?>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h5 class="panel-title">
                    <b>Agregar Manifiesto</b>
                </h5>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this VaSolicitudesVisitasAcademicasController */
                /* @var $model VaSolicitudesVisitasAcademicas */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'va-solicitudes-visitas-academicas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
                )); ?>

                    <!--<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>-->

                    <?php echo $form->errorSummary($modelGRpempresas); ?>

                    <div class="col-md-6">
                        <p align="left"><strong>Subir imagen o documento para los requisitos de seguridad de la Empresa.</strong></p>

                        <br>
                        <p><strong>NOTA:</strong> El documento debe cumplir con los requisitos definidos.</p>

                        <p><strong>* Tamaño maximo de la imagen de 1MB.</strong></p>
                        <p><strong>* Solo formatos .jpg o jpeg.</strong></p>

                        <br>
                        <div class="form-group">
                            <?php echo $form->labelEx($modelGRpempresas,'doc_oficio_confirmacion_empresa'); ?>
                            <?php echo $form->fileField($modelGRpempresas,'doc_oficio_confirmacion_empresa', array('class'=>'form-control')); ?>
                            <?php echo $form->error($modelGRpempresas,'doc_oficio_confirmacion_empresa'); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>