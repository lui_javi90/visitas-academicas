<?php
/* @var $this GRpempresasController */
/* @var $model GRpempresas */

$this->breadcrumbs=array(
	'Visitas Académicas' => '?r=visitasacademicas',
	'Empresas Disponibles para Visita Académica' => array('gRpempresas/listaEmpresasVisitasAcademicas'),
	'Nueva Empresa Visita'
);

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Nueva Empresa Visita Académica
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<h3 class="panel-title">
					<b>Nueva</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formEmpresaVisitaAcademica', array('modelGRpempresas'=>$modelGRpempresas,
																				'lista_sectores' => $lista_sectores,
																				'lista_estados' => $lista_estados,
																				//'lista_municipios' => $lista_municipios
																	)); ?>
			</div>
		</div>
	</div>
</div>