<?php

	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Responsables Solicitudes Visitas Académicas'
	);

?>

<br><br><br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Responsables Solicitudes Visitas Académicas 
		</span>
	</h2>
</div>

<br>
<?php if($is_jefe_proyvinc == true || $is_jefe_depto_academico == true){ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-add-resp-solicitudes-val-visitas-academicas-grid',
    'dataProvider'=>$modelVaResponsablesVisitasAcademicas->searchXJefeDeptoOProyecto($depto_catedratico, $rfcEmpleado),
    'filter'=>$modelVaResponsablesVisitasAcademicas,
    'columns'=>array(
        //'id_solicitud_visitas_academicas',
        array(
			'header' => 'No. <br>Solicitud',
            //'name' => 'no_solicitud',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->id_solicitud_visitas_academicas;
                $qry_no_sol = "select * from pe_vinculacion.va_solicitudes_visitas_academicas 
                                where id_solicitud_visitas_academicas = '$id' ";
                $no_sol = Yii::app()->db->createCommand($qry_no_sol)->queryAll();

                return $no_sol[0]['no_solicitud'];
            },
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
			'filter' => false,
			'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'rfcEmpleado',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Nombres Completo',
            'value' => function($data)
            {
                $rfc = $data->rfcEmpleado;
                $qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['nmbEmpleado'].' '.$name[0]['apellPaterno'].' '.$name[0]['apellMaterno'];

            },
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Solicitud',
            'value' => function($data)
            {
                $id = $data->id_solicitud_visitas_academicas;
                $qry_sol = "select * from pe_vinculacion.va_solicitudes_visitas_academicas where id_solicitud_visitas_academicas = '$id' ";
                $name_sol = Yii::app()->db->createCommand($qry_sol)->queryAll();

                return $name_sol[0]['nombre_visita_academica'];
            },
            'htmlOptions' => array('width'=>'350px','class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{addRespSolicitud}',
			'header'=>'Agregar Responsable',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addRespSolicitud' => array
				(
					'label'=>'Agregar Responsable',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaResponsablesVisitasAcademicas/agregarResponsableSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/agregarr_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{addMateriaSolicitud}',
			'header'=>'Agregar Materia',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addMateriaSolicitud' => array
				(
					'label'=>'Agregar Materia cubre Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaMateriasImparteResponsableVisitaAcademica/agregarMateriaCubreSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/add_32.png',
				),
			),
		),
    ),
)); ?>

<?php }else{ ?>

	<!--CUANDO EL QUE SE LOGEA NO ES JEFE DE PROYECTOS DE VINCULACION DE ALGUN DEPTO ACADEMICO, NO PODRA EDITAR LAS SOLICITUDES-->
	<br><br><br>
	<div class="alert alert-danger">
		<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			No eres Jefe de proyectos de Vinculación o Jefe de Departamento Académico.
		</strong></p>
	</div>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-add-resp-solicitudes-val-visitas-academicas-grid',
    'dataProvider'=>$modelVaResponsablesVisitasAcademicas->searchNoSolicitudesXJefe(),
    'filter'=>$modelVaResponsablesVisitasAcademicas,
    'columns'=>array(
        //'id_solicitud_visitas_academicas',
        array(
			'header' => 'No. <br>Solicitud',
            //'name' => 'no_solicitud',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->id_solicitud_visitas_academicas;
                $qry_no_sol = "select * from pe_vinculacion.va_solicitudes_visitas_academicas 
                                where id_solicitud_visitas_academicas = '$id' ";
                $no_sol = Yii::app()->db->createCommand($qry_no_sol)->queryAll();

                return $no_sol[0]['no_solicitud'];
            },
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
			'filter' => false,
			'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'rfcEmpleado',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Nombres Completo',
            'value' => function($data)
            {
                $rfc = $data->rfcEmpleado;
                $qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['nmbEmpleado'].' '.$name[0]['apellPaterno'].' '.$name[0]['apellMaterno'];

            },
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Solicitud',
            'value' => function($data)
            {
                $id = $data->id_solicitud_visitas_academicas;
                $qry_sol = "select * from pe_vinculacion.va_solicitudes_visitas_academicas where id_solicitud_visitas_academicas = '$id' ";
                $name_sol = Yii::app()->db->createCommand($qry_sol)->queryAll();

                return $name_sol[0]['nombre_visita_academica'];
            },
            'htmlOptions' => array('width'=>'350px','class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{addRespSolicitud}',
			'header'=>'Agregar Responsable',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addRespSolicitud' => array
				(
					'label'=>'Agregar Responsable',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaResponsablesVisitasAcademicas/agregarResponsableSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/agregarr_32.png',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{addMateriaSolicitud}',
			'header'=>'Agregar Materia',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addMateriaSolicitud' => array
				(
					'label'=>'Agregar Materia cubre Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaMateriasImparteResponsableVisitaAcademica/agregarMateriaCubreSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/add_32.png',
				),
			),
		),
    ),
)); ?>

<?php } ?>

<br><br><br><br><br>
<br><br><br><br><br>