<?php

	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Responsables Solicitudes Visitas Académicas' => array('vaResponsablesVisitasAcademicas/listaSolicitudesValidadasAgregarResponsables'),
        'Agregar Responsable Solicitud Visita Académica'
    );
    
    /*ELIMINAR RESPONSABLE DE VISITA ACADEMICA MIENTRAS NO SEA EL PRINCIPAL */
    $JsDeleteResponsable = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-lista-resp-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-lista-resp-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
    }';
    /*ELIMINAR RESPONSABLE DE VISITA ACADEMICA MIENTRAS NO SEA EL PRINCIPAL */

?>

<br><br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Agregar Responsable Solicitud Visita Académica
		</span>
	</h2>
</div>

<br>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel panel-heading">
                <h3 class="panel-title">
                    <b>Agregar Responsable</b>
                </h3>
            </div>
            <div class="panel-body">
                
               <!--<div class="col-md-5">-->
                    <div class="form">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'va-add-resp-solicitudes-visitas-academicas-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation'=>false,
                    )); ?>

                        <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

                        <?php echo $form->errorSummary($VaResponsablesVisitasAcademicas); ?>

                        <div class="form-group">
                            <?php echo $form->labelEx($VaResponsablesVisitasAcademicas,'rfcEmpleado'); ?>
                            <?php $this->widget('ext.select2.ESelect2', array('model'=>$VaResponsablesVisitasAcademicas,
                                        'attribute'=>'rfcEmpleado',
                                        'data' => $lista_responsables,
                                        'options' => array(
                                            'width' => '100%',
                                            'placeholder'=>'Buscar Empleado por su RFC',
                                            'allowClear'=>true,
                                        ),
                                        'htmlOptions' => array(
                                            //'class'=>'form-control', 'required'=>'required',
                                            'ajax'=>array(
                                            'type'=>'POST',
                                            'dataType'=>'json',
                                            'data' => array(

                                                'rfcEmpleado'=>'js:$(\'#VaResponsablesVisitasAcademicas_rfcEmpleado\').val()',
                                            ),
                                            'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
                                            'success'=>'function(data) 
                                            {
                                                if(data.status_campo == "NV")
                                                {
                                                    $(".div_foto_responsable").show();
                                                    $(".div_nombre_responsable").show();
                                                    $(".div_departamento_responsable").show();
                                                    $(".div_puesto_responsable").show();
                                                    $(".div_foto_responsable").html(\'<img class="img-circle" align="center" heigth="150" width="150" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=\'+ data.rfc_empleado +\' " />\');
                                                    $(".div_nombre_responsable").html(\'<b><label align="left">Nombre:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color: transparent;"  value="\'+ data.nombre_completo +\'" readonly />\');
                                                    $(".div_departamento_responsable").html(\'<b><label align="left">Departamento:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color: transparent;"  value="\'+ data.departamento +\'" readonly />\');
                                                    $(".div_puesto_responsable").html(\'<b><label align="left">Puesto:</label></b><input type="text" class="form-control" style="border:none; background:transparent; border-color:transparent;"  value="\'+ data.puesto +\'" readonly />\');
                                                
                                                }else
                                                if(data.status_campo == "NER"){

                                                    $(".div_foto_responsable").hide();
                                                    $(".div_nombre_responsable").hide();
                                                    $(".div_departamento_responsable").hide();
                                                    $(".div_puesto_responsable").hide();

                                                }else
                                                if(data.status_campo == "V"){

                                                    $(".div_foto_responsable").hide();
                                                    $(".div_nombre_responsable").hide();
                                                    $(".div_departamento_responsable").hide();
                                                    $(".div_puesto_responsable").hide();

                                                }
                                            }',
                                        )), 
                                        
                            )); ?>
                            <?php /*echo $form->dropDownList($VaResponsablesVisitasAcademicas,
                                                            'rfcEmpleado',
                                                            $lista_responsables, //Lista de Responsables
                                                            //array('style'=>'width:600px', 'prompt'=>'-- Agregar Responsable --', 'class'=>'form-control')); 
                                                            array(
                                                                'style'=>'width:auto', 'prompt'=>'-- Agregar Responsable --', 'class'=>'form-control', 'required'=>'required',
                                                                'ajax'=>array(
                                                                'type'=>'POST',
                                                                'dataType'=>'json',
                                                                'data' => array(
                    
                                                                    'rfcEmpleado'=>'js:$(\'#VaResponsablesVisitasAcademicas_rfcEmpleado\').val()',
                                                                ),
                                                                'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
                                                                'success'=>'function(data) 
                                                                {
                                                                    if(data.status_campo == "NV")
                                                                    {
                                                                        $(".div_foto_responsable").show();
                                                                        $(".div_nombre_responsable").show();
                                                                        $(".div_departamento_responsable").show();
                                                                        $(".div_puesto_responsable").show();
                                                                        $(".div_foto_responsable").html(\'<img class="img-circle" align="center" heigth="150" width="150" src="https://sii.itcelaya.edu.mx/sii/items/getFoto.php?nctr_rfc=\'+ data.rfc_empleado +\' " />\');
                                                                        $(".div_nombre_responsable").html(\'<b><label align="left">Nombre:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.nombre_completo +\'" readonly />\');
                                                                        $(".div_departamento_responsable").html(\'<b><label align="left">Departamento:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.departamento +\'" readonly />\');
                                                                        $(".div_puesto_responsable").html(\'<b><label align="left">Puesto:</label></b><input type="text" class="form-control" style="border:0; border-color:transparent;"  value="\'+ data.puesto +\'" readonly />\');
                                                                    
                                                                    }else
                                                                    if(data.status_campo == "NER"){

                                                                        $(".div_foto_responsable").hide();
                                                                        $(".div_nombre_responsable").hide();
                                                                        $(".div_departamento_responsable").hide();
                                                                        $(".div_puesto_responsable").hide();

                                                                    }else
                                                                    if(data.status_campo == "V"){

                                                                        $(".div_foto_responsable").hide();
                                                                        $(".div_nombre_responsable").hide();
                                                                        $(".div_departamento_responsable").hide();
                                                                        $(".div_puesto_responsable").hide();

                                                                    }
                                                                }',
                                                                )) 
                                                            );*/ ?>
                            <?php echo $form->error($VaResponsablesVisitasAcademicas,'rfcEmpleado'); ?>
                        </div>

                        <br>
                        <div class="form-group">
                            <?php echo CHtml::submitButton($VaResponsablesVisitasAcademicas->isNewRecord ? 'Registrar Responsable' : 'Registrar Responsable', array('class'=>'btn btn-success')); ?>
                            <?php echo CHtml::link('Cancelar', array('listaSolicitudesValidadasAgregarResponsables'), array('class'=>'btn btn-danger')); ?>
                        </div>

                    <?php $this->endWidget(); ?>

                    </div><!-- form -->

                <!--</div>-->

            </div>
        </div>
    </div>
    <div class="col-md-6">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <b>Perfíl del Responsable</b>
                </h3>
            </div>
            <div class="panel-body">

                <!--Datos del Responsable Seleccionado-->
                <div align="center" class="div_foto_responsable"></div>
               
                <br>
                <div align="center" class="div_nombre_responsable"></div>

                <br>
                <div align="center" class="div_departamento_responsable"></div>

                <br>
                <div align="center" class="div_puesto_responsable"></div>
                <!--Datos del Responsable Seleccionado-->
                
            </div>
        </div>

    </div>
</div>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Responsables Registrados en la Solicitud Visita Académica
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
    <div class="col-md-9">
    </div>
    <div class="col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <b>No. de Solicitud</b>
                </h6>
            </div>
            <div align="center" class="panel-body">
                <?php echo '<span style="font-size:16px" class="label label-warning">'.$modelVaSolicitudesVisitasAcademicas->no_solicitud.'</span>'; ?>
            </div>
        </div>
    </div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-lista-resp-solicitudes-visitas-academicas-grid',
    'dataProvider'=>$listaVaResponsablesVisitasAcademicas->searchListaResponsablesSolicitudVisitaAcademica($id_solicitud_visitas_academicas),
    'filter'=>$listaVaResponsablesVisitasAcademicas,
    'columns'=>array(
        //'id_solicitud_visitas_academicas',
        /*array(
			'header' => 'No. <br>Solicitud',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->id_solicitud_visitas_academicas;
                $qry_no_sol = "select * from pe_vinculacion.va_solicitudes_visitas_academicas 
                                where id_solicitud_visitas_academicas = '$id' ";
                $no_sol = Yii::app()->db->createCommand($qry_no_sol)->queryAll();

                return $no_sol[0]['no_solicitud'];
            },
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
        ),*/
        array(
            'header' => 'Responsable <br>Principal',
			'type' => 'raw',
			'value' => function($data)
			{
                $rfc = trim($data->rfcEmpleado);
                return CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
                //return $data->rfcEmpleado;
            },
            'filter' => false,
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'rfcEmpleado',
            'filter' => false,
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Nombres Completo',
            'value' => function($data)
            {
                $rfc = $data->rfcEmpleado;
                $qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['nmbEmpleado'].' '.$name[0]['apellPaterno'].' '.$name[0]['apellMaterno'];

            },
            'htmlOptions' => array('width'=>'400px','class'=>'text-center')
        ),
        array(
            'header' => 'Principal <br>Reponsable',
            'type' => 'raw',
			'value' => function($data)
			{
				return ($data->responsable_principal == 1) ? '<span style="font-size:16px" class="label label-success">SI</span>' : '<span style="font-size:16px" class="label label-danger">NO</span>';
            },
            'filter' => false,
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{delResponsable},{noDeleteResponsable}',
			'header'=>'Eliminar Responsable',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delResponsable' => array
				(
					'label'=>'Eliminar Responsable de la Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaResponsablesVisitasAcademicas/eliminarResponsableVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas, "rfcEmpleado"=>$data->rfcEmpleado))',
                    'imageUrl'=>'images/servicio_social/eliminar_32.png',
                    'visible' => function($row, $data)
                    {
                        return ($data->responsable_principal == true) ? false : true;
                    },
                    'options' => array(
						'title'        => 'Eliminar Reponsable Seleccionado',
						'data-confirm' => '¿En verdad quieres ELIMINAR al Responsable?',
					),
					'click' => $JsDeleteResponsable, 
                ),
                'noDeleteResponsable' => array
				(
					'label'=>'No puedes eliminar al Responsable Principal',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaResponsablesVisitasAcademicas/agregarResponsableSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
                    'imageUrl'=>'images/servicio_social/bloquedo_32.png',
                    'visible' => function($row, $data)
                    {
                        return ($data->responsable_principal == true) ? true : false;
                    }
				),
			),
        ),
    )
)); ?>



<br><br><br><br><br>
<br><br><br><br><br>

