<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Estatus Solicitudes Visitas Académicas' => array('vaSolicitudesVisitasAcademicas/listaStatusSolicitudesVisitasAcademicas'),
		'Editar Solicitud Visita Académica',
	);
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Solicitud Visita Académica
		</span>
	</h2>
</div>

<br>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4 class="panel-title">
					<b>Editar</b>
				</h4>
			</div>
			<div class="panel-body">

				<?php
				/* @var $this VaSolicitudesVisitasAcademicasController */
				/* @var $model VaSolicitudesVisitasAcademicas */
				/* @var $form CActiveForm */
				?>

				<div class="form">

				<?php $form=$this->beginWidget('CActiveForm', array(
				    'id'=>'va-solicitudes-visitas-academicas-form',
				    // Please note: When you enable ajax validation, make sure the corresponding
				    // controller action is handling ajax validation correctly.
				    // There is a call to performAjaxValidation() commented in generated controller code.
				    // See class documentation of CActiveForm for details on this.
				    'enableAjaxValidation'=>false,
				)); ?>

				<!--<div class="row">-->
					<div class="panel panel-info">
						<div class="panel-heading">
							<h4 class="panel-title">
								<b>Docente Responsable Principal</b>
							</h4>
						</div>

						<div class="panel-body">
							<div class="col-md-2">

								<?php echo CHtml::image("items/getFoto.php?nctr_rfc=".$info_empleado->rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:120px;height:120px;")); ?>

							</div>
							<div class="col-md-10">

								<b>Solicitante Principal: </b>
								<?php echo $info_empleado->siglaGradoMaxEstudio.' '.$info_empleado->nmbEmpleado.' '.$info_empleado->apellPaterno.' '.$info_empleado->apellMaterno; ?>
								<br><br>
								<b>Departamento: </b>
								<?php echo $departamento; ?>
								<br><br>
								<b>Puesto: </b>
								<?php echo $cargo; ?>
								<br><br>
								<?php if(!$modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
								<b>Fecha de creación de la Solicitud: </b>
								<?php echo $fec_creacion[0]['fecha_act'].' a las '.$fec_creacion[0]['hora']; ?>
								<?php }else{ ?>
								<b>Fecha de creación de la Solicitud: </b> -
								<?php  } ?>
								<br>

							</div>
						</div>
					</div>
				<!--</div>-->

				<br><br>

				<div align="center" class="alert alert-info">
				    <p><strong>
				       Ultima modificación realizada a la Solicitud el día <u><?php echo ($fec_ult_edic[0]['fecha_act'] != 'SV') ? $fec_ult_edic[0]['fecha_act'].' a las '.$fec_ult_edic[0]['hora'] : '--'; ?></u>
				    </strong></p>
				</div>

				<br><br>

				    <b><p align="center" class="note">Campos con <span class="required">*</span> son requeridos.</p>

				    <?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas); ?>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
				        <?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica',array('size'=>60,'maxlength'=>500, 'class'=>'form-control')); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
				        <?php 
					        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					            'model' => $modelVaSolicitudesVisitasAcademicas,
					            'attribute' => 'fecha_hora_salida_visita',
					            // additional javascript options for the date picker plugin
					            'options' => array(
									'showAnim'=>'fold',
					                'showPeriod' => false,
					                'showTime' => false,
					                'hourText' => 'Hr. Sal',
					                'minuteText' => 'Min. Sal',
									'ampm' => true,
									'dateFormat' => 'dd.mm.yy'
					            ),
					            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
					        ));
				  		?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
				        <?php 
				        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					            'model' => $modelVaSolicitudesVisitasAcademicas,
					            'attribute' => 'fecha_hora_regreso_visita',
					            // additional javascript options for the date picker plugin
					            'options' => array(
									'showAnim'=>'fold',
									'showButtonPanel'=>true,
									'autoSize'=>true,
					                'showPeriod' => false,
					                'showTime' => false,
					                'hourText' => 'Hr. Sal',
					                'minuteText' => 'Min. Sal',
									'ampm' => true,
									'dateFormat' => 'dd.mm.yy'
					            ),
					            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
					        ));
				        ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'periodo'); ?>
				        <?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
				        							'periodo',
				        							$lista_periodos,
				        							array('class'=>'form-control','disabled'=>'disabled')); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'periodo'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'anio'); ?>
				        <?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,
				        							'anio',
				        							array('class'=>'form-control', 'readOnly'=>true)
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'anio'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_solicitud'); ?>
				        <?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,
				        							'no_solicitud',
				        							array('class'=>'form-control', 'readOnly'=>true)
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_solicitud'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
				        <?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
				        							'id_tipo_visita_academica',
				        							$tipos_visitas,
				        							array('class'=>'form-control')
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
				        <?php /*echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
				        							'id_empresa_visita',
				        							$lista_empresas_visitas,
				        							array('class'=>'form-control')
				        						);*/ ?>
				        <?php
					        $this->widget('ext.select2.ESelect2', array('model'=>$modelVaSolicitudesVisitasAcademicas,
					                  'attribute'=>'id_empresa_visita',
					                  'data' => $lista_empresas_visitas,
					                  'options' => array(
					                      'width' => '100%',
					                      'placeholder'=>'Buscar Empresa por Nombre', //Deberia ser por RFC por no lo tienen todas
					                      'allowClear'=>true,
					                  ),
					                  'htmlOptions' => array(
					                      'required'=>'required',
					                      //'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
					                  ),

					      	));
					    ?>
				       
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
				        <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,
				        							'area_a_visitar',
				        							array('class'=>'form-control', 'rows'=>'3')
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
				        <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,
				        							'objetivo_visitar_area',
				        							array('class'=>'form-control', 'rows'=>'3')
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
				        <?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,
				        							'no_alumnos',
				        							array('class'=>'form-control', 'readOnly'=>true)
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
				    </div>

				    <div class="form-group">
				        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_estatus_solicitud_visita_academica'); ?>
				        <?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
				        							'id_estatus_solicitud_visita_academica',
				        							$lista_edo_sol,
				        							array('class'=>'form-control', 'disabled'=>'disabled')
				        						); ?>
				        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_estatus_solicitud_visita_academica'); ?>
				    </div>

				    <div class="form-group">
				        <?php //echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'val_lista_alumnos_visita_academica'); ?>
				        <?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'val_lista_alumnos_visita_academica'); ?>
				        <?php //echo $form->error($modelVaSolicitudesVisitasAcademicas,'val_lista_alumnos_visita_academica'); ?>
				    </div>

				    <br>
				    <div class="form-group">
				        <?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-success')); ?>
						<?php echo CHtml::link('Cancelar', array('listaStatusSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
				    </div>

				<?php $this->endWidget(); ?>

				</div><!-- form -->

			</div>
		</div>
	</div>
</div>


<br><br><br><br><br>
