<?php

	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Estatus Solicitudes Visitas Académicas',
    );

    /*ELIMINACION DE LA SOLICITUD DE VISITA ACADEMICA*/
    $JsDelSolRVisAcad = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-status-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-status-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*ELIMINACION DE LA SOLICITUD DE VISITA ACADEMICA*/

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Estatus Solicitudes Visitas Académicas
		</span>
	</h2>
</div>

<br><br><br>
<div class="row">
	<div class="col-md-3">

		<div class="panel panel-success">
			<div class="panel-heading">
				<h4 class="panel-title">
					Periodo:
				</h4>
			</div>
			<div align="center" class="panel-body">
				<span style="font-size:16px" class="label label-default">
					<?php echo ($periodo == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE"; ?>
					
				</span>
			</div>

		</div>

	</div>

	<div class="col-md-6">

	</div>

	<div class="col-md-3">

		<div class="panel panel-success">
			<div class="panel-heading">
				<h4 class="panel-title">
					Año:
				</h4>
			</div>
			<div align="center" class="panel-body">
				<span style="font-size:16px" class="label label-default"><?php echo $anio; ?></span>
			</div>
		</div>

	</div>

</div>


<br><br><br>
<div align="center" class="alert alert-info">
	<p><strong>
		Mientras la Solicitud de Visita Académica este en estatus PENDIENTE se puede editar.
	</strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-status-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchXTodasSolicitudesSemestre($periodo, $anio),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'5px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				//$rfc = $data->hEmpleadoses->rfcEmpleado;
				//echo $rfc;
				//die;
				$qry_resp = "select \"rfcEmpleado\" 
							from pe_vinculacion.va_responsables_visitas_academicas
							where id_solicitud_visitas_academicas = '$id' AND responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();
				$rfc = $rfc[0]['rfcEmpleado'];

				return CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
				
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),*/
		array(
			'class' => 'ComponentDatosDocente',
			'htmlOptions' => array('width'=>'50px','class'=>'text-center') 
		),
        array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'450px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'-- Filtrar --')
			),
			'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
        ),
        /*array(
			'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:14px" class="label label-success">'.$data->anio.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Periodo',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$data->periodo' ";

                $periodo = Yii::app()->db->createCommand($qry_per)->queryAll();

                return '<span style="font-size:14px" class="label label-success">'.$periodo[0]['dscPeriodo'].'<span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),*/
        array(
        	'class' => 'ComponentValidacionSolicitante',
        	'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
        	'class' => 'ComponentValidacionJefeProyVinculacion',
        	'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
        	'class' => 'ComponentValidacionSubAcademico',
        	'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
        	'class' => 'ComponentValidacionOfServiciosExternos',
        	'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
        	'class' => 'ComponentValidacionRecursosMateriales',
        	'htmlOptions' => array('width'=>'160px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estatus',
            //'filter' => false,
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
            									'id_estatus_solicitud_visita_academica',
            									CHtml::listData(
            										VaEstatusSolicitudVisitaAcademica::model()->findAll(
            											array(
														   'condition' => 'id_estatus_solicitud_visita_academica > :ID',
														   'params' => array(
														   		':ID' => 0,
														   )

														)
            										),
            										'id_estatus_solicitud_visita_academica',
            										'estatus'
            									),
            									array('prompt'=>'-- Filtrar --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_estatus_solicitud_visita_academica;
                $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findByPk($id);
                if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                    throw new CHttpException(404,'No existe registro de ese Estatus.');

                return '<span style="font-size:12px" class="label label-default">'.$modelVaEstatusSolicitudVisitaAcademica->estatus.'</span>';
            },
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{statSolicitud}',
			'header'=>'Detalle',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detailSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#detail_1").dialog("open"); 
						
						return false;
					}',
				),
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{statSolicitud},{noEdit}',
			'header'=>'Editar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statSolicitud' => array
				(
					'label'=>'Editar la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
                    'visible' => function($row, $data)
                    {
                        $v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
                        $v5 = $data->valida_jefe_recursos_materiales;

                        $v6 = $data->id_estatus_solicitud_visita_academica;

                        //(($v1 == NULL OR $v2 == NULL OR $v3 == NULL AND $v4 == NULL) AND

                        return ($v1 != NULL AND ($v2 == NULL AND $v3 == NULL) AND $v6 == 1) ? true : false;
                    }
					
				),
				'noEdit' => array
				(
					'label'=>'Opción Bloqueada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
                    {
                        $v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica;

                        //(($v1 != NULL AND $v2 != NULL AND $v3 != NULL AND $v4 != NULL AND $v5 != NULL) AND

                        return ($v1 != NULL AND (($v2 != NULL OR $v3 != NULL) OR $v6 > 1)) ? true : false;
                    }
				)
			),
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{elimSolVisitaAcademica},{NoElimSolVisitaAcademica}',
			'header'=>'Eliminar',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'elimSolVisitaAcademica' => array
				(
					'label'=>'Eliminar Solicitud Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/cancelSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						$v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica;

						return ($v6 == 1 OR $v6 == 2 OR $v6 == 3) ? true : false;
					},
					'options' => array(
						//'title'        => 'Activado',
						'data-confirm' => 'Confirmar la Eliminación de la Solicitud de Visita Académica?', // custom attribute to hold confirmation message
					),
					'click' => $JsDelSolRVisAcad, // JS string which processes AJAX request
				),
				'NoElimSolVisitaAcademica' => array
				(
					'label'=>'Opción Bloqueada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica;

						return ($v6 > 3 AND $v6 < 9) ? true : false;
					}
				),
			),
		),
	),
)); ?>


<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'detail_1',
	'options' => array(
		'title' => 'Detalle de la Visita Académica',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 720,
		/*'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),*/
	),

));?>

	<iframe scrolling="yes" id="parm-frame" width="1200" height="720"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>



<br><br><br><br><br>
<br><br><br><br><br>