<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Industriales' => '?r=visitasacademicas',
		'Lista Solicitudes Visitas Industriales' => array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
		'Editar la Solicitud'
	);
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			<b>Editar la Solicitud</b>
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Editar</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formNuevaSolicitudVisitaAcademica', array('modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
																						'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
																						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
																						'materias_asignadas_responsable' => $materias_asignadas_responsable, //No se utiliza
																						'tipos_visitas' => $tipos_visitas,
																						'lista_empresas_visitas' => $lista_empresas_visitas,
																						'info_empleado' => $info_empleado,
																						'departamento' => $departamento,
																						'cargo' => $cargo,
																						'fec_creacion' => $fec_creacion,
																						'unica_materia' => $unica_materia
																				)); ?>
			</div>
		</div>
	</div>
</div>

<br><br><br><br><br>
<br><br><br><br><br>
