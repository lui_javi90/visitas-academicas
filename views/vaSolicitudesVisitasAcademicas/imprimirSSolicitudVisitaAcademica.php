<style>
    .hoja{
        background-color: white;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 10px;
    }
    .letra2{
        font-size: 12px;
    }
    .div2{
        background-color: white;
        padding: -4px;
        padding-left: -1px;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    table
    {
        border-collapse: collapse;
    }
    table, th, td {

        border: 1px solid black;
        bgcolor: #9E9E9E;
    }
    th { background-color: #1B5E20; }
    .table1
    {
        border-collapse: collapse;
    }
    .table1, .th1, .td1 {

        border: 0px solid black;
    }
    .mytable{
      border: 1px solid black;
      border-right: 1px solid black;
      border-collapse: collapse;
      color: #000000;
      width: 100%;
      background-color: #FFFFFF;
    }
    .mytable-head {
      border: 1px solid black;
      color: #000000;
      width: 100%;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
</style>

<div class="hoja">
    <img height="90px" align="center" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--Encabezado del documento-->
    <div class="div2">
        <h6 class="bold center letra2">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="bold center letra2">SOLICITUD PARA VISITAS ACADÉMICAS PROPUESTAS</h6>
        <hr>
    </div>
    <!--Encabezado del documento-->

    <!--Datos de la Solicitud-->
    <div class="div2">
        <span class="bold letra2">DEPARTAMENTO:</span> <?php echo "SUBDIRECCIÓN ACADÉMICA"; ?>
        <br>
        <span class="bold letra2">FECHA:</span> <?php echo $fec_actual; ?>
        <br>
        <span class="bold letra2">PERIODO ESCOLAR:</span> <?php echo ($periodo == 1) ? "ENERO-JUNIO" : "AGOSTO-DICIEMBRE".' '.$anio; ?>
        <br>
        <span class="bold letra2">TIPO DE VISITA ACADÉMICA:</span> <?php echo "VISITAS ACADÉMICAS Y VISITAS DE CAMPO"; ?>
    </div>
    <!--Datos de la Solicitud-->

    <br>
    <!--Tabla de Registro de Solicitud-->
    <div class="div2">
        <table class="mytable" align="center">
            <tr width="100%">
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="5%">
                    <span class="bold">No.</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="22%">
                    <span class="bold">Empresa / Ciudad:</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="20%">
                    <span class="bold">Área a observar y <br>objetivo</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="5%">
                    <span class="bold">Fecha / <br>Turno</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="15%">
                    <span class="bold">Carrera</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="5%">
                    <span class="bold">No. de <br>alumnos</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="18%">
                    <span class="bold">Solicitante</span>
                </th>
                <th bgcolor="#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="10%">
                    <span class="bold">Asignatura</span>
                </th>
            <tr>
            <?php

                $no_sol_aprob = sizeof($lista_sol_validadas); //Numero de Solicitudes a Visitas Academicas del Semestre Actual
                $contador = 0;
                //$hoja = 0;
                for($i = 0; $i < $no_sol_aprob; $i++)
                {
                    $contador++;
                    //$hoja++;
            ?>
            <tr>
                <td style="border: 1px solid black;" width="5%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['no_solicitud']; ?>
                </td>
                <td style="border: 1px solid black;" width="22%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['empresa'].', '.$lista_sol_validadas[$i]['estado']; ?>
                </td>
                <td style="border: 1px solid black;" width="20%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['area_a_visitar']; ?>
                </td>
                <td style="border: 1px solid black;" width="5%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['fec_salida'].' a '.$lista_sol_validadas[$i]['fec_regreso'].'<br>/'.'M-V'; ?>
                </td>
                <td style="border: 1px solid black;" width="15%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['carrera']; ?>
                </td>
                <td style="border: 1px solid black;" width="5%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['no_alumnos']; ?>
                </td>
                <td style="border: 1px solid black;" width="18%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['docente_responsable']; ?>
                </td>
                <td style="border: 1px solid black;" width="10%" align="center" class="bold letra2">
                    <?php echo $lista_sol_validadas[$i]['asignaturas']; ?>
                </td>
                
            </tr>
                <?php 
                    /*echo ($contador >= 5) ? "<br><br><br><br>" : "";
                    if($contador >= 4)
                    {
                        echo "<br><br><br><br>";
                        $contador = 0;
                    }*/
                ?>
                <?php } ?>
        </table>
    </div>
    <!--Tabla de Registro de Solicitud-->

    <?php
        echo (sizeof($lista_sol_validadas) >= 4) ? "<br><br><br><br><br>" : ""; 
    ?>

    <!--Validaciones del jefe Académico y Subdirector Académico-->
    <br>
    <div class="div2">
        <table class="table1" width="100%" align="center">
            <tr>
                <td width="50%" align="center" class="td1 bold center letra2">
                    <br><br>
                    <?php 
                        echo (sizeof($lista_sol_validadas) > 0) ? "<span class=\"bold letra2\">El Jefe del Departamento Académico correspondiente <br> validó las Solicitudes de Visitas Académicas." : ""; 
                    ?>
                    <br>
                    <span class="bold"><?php echo "______________________________________________________"; ?></span>
                    <br>
                    <span class="bold"><?php echo "NOMBRE Y FIRMA"; ?></span>
                    <br>
                    <span class="bold"><?php echo "JEFE DEL DEPARTAMENTO ACADÉMICO"; ?></span>
                    <br>
                </td>
                <td width="50%" align="center" class="td1 bold center letra2">
                    <br>
                    <?php 

                        echo (sizeof($lista_sol_validadas) > 0) ? "<span class=\"bold letra2\">El Subdirector Académico de nombre <br>".$val_subd_academico."<br> validó las Solicitudes de Visitas Académicas." : ""; 
                    ?>
                    <br>
                    <span class="bold"><?php echo "______________________________________________________"; ?></span>
                    <br>
                    <?php echo "NOMBRE Y FIRMA"; ?>
                    <br>
                    <?php echo "SUBDIRECTOR ACADÉMICO"; ?>
                    <br>
                </td>
            </tr>
        </table>
    </div>
    <!--Validaciones del jefe Académico y Subdirector Académico-->

    <!--Marcas inferiores del documento-->
    <br><br>
    <div class="div2">
        <span class="bold letra2">c.c.p. Subdirección Académica.</span>
        <br>
        <span class="bold letra2">c.c.p. Archivo.</span>
    </div>
    <!--Marcas inferiores del documento-->
</div>