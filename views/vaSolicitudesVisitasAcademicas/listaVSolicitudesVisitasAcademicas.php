<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Consulta de Solicitudes de Visitas Académicas Docentes',
	);

	/*JS PARA VALIDAR LA ELIMINACION DE LA SOLICITUD SELECCIONADA*/
	$JsDelSolicitudV = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-vinc-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-vinc-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*JS PARA VALIDAR LA ELIMINACION DE LA SOLICITUD SELECCIONADA*/

	/*VALIDAR LA SOLICITUD POR PARTE DEL JEFE DE PROYECTO DE VINCULACION*/
	$ValSolicitudJefeProyAcad = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-vinc-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-vinc-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*VALIDAR LA SOLICITUD POR PARTE DEL JEFE DE PROYECTO DE VINCULACION*/

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Consulta de Solicitudes de Visitas Académicas Docentes
		</span>
	</h2>
</div>

<br><br><br><br><br>
<?php if($is_jefe_proyvinc == true or $is_jefe_depto_academico == true){ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-vinc-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchListaSolicitudesVisitasAcademicasVigentesJefeProyVinculacion($depto_catedratico),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			//'filter' => false,
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" 
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";
				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		//'periodo',
		//'anio',
		/*array(
			'name' => 'no_alumnos',
			'filter' => false,
			'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				//if($modelVaTiposVisitasAcademicas === NULL)
					//throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return ($modelVaTiposVisitasAcademicas->tipo_visita_academica != NULL) ? $modelVaTiposVisitasAcademicas->tipo_visita_academica : null;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'--Filtrar por --')
			),
			'htmlOptions' => array('width'=>'130px', 'class'=>'text-center')
		),
		array(
			'header' => 'Departamento',
			/*'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por Depto --')
			),*/
			'filter' => false,
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				//obtenemos el rfc del docente responsable principal
				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];

				//Ontenemos el depto catedratico del Empleado
				//Obtener el depto academico del docente responsable de la visita academica
				$qry_depto = "select 
								(
									select \"dscDepartamento\" from public.\"H_departamentos\" 
									where \"cveDeptoAcad\" = hemp.\"deptoCatedratico\"
								) as depto_academico
								from public.\"H_empleados\" hemp
								where hemp.\"rfcEmpleado\" = '$rfc' 
							";

				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['depto_academico'];

			},
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'class' => 'ComponentValidaSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'310px', 'class'=>'text-center')
		),
		/*'observaciones_solicitud',
		'valida_jefe_depto_academico',
		'valida_subdirector_academico',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class' => 'ComponentEstatusSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'335px', 'class'=>'text-center')
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{editSolicitud},{noEditSolicitud}',
			'header'=>'Editar Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editSolicitud' => array
				(
					'label'=>'Editar de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => function($row, $data)
					{
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$val_subdirector = $data->valida_subdirector_academico;

						return ($val_jefe_depto === null AND $val_subdirector === null) ? true : false;
					}
				),
				'noEditSolicitud' => array
				(
					'label'=>'Sin Permiso de Edición',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$val_subdirector = $data->valida_subdirector_academico;

						return ($val_jefe_depto != null AND $val_subdirector != null) ? true : false;
					}
				)
			),
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delSolicitud},{bloqSolicitud}',
			'header'=>'Hacer <br>Observación',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delSolicitud' => array
				(
					'label'=>'Hacer Observación',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/rechazarVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;
						/*$val_subdirector = $data->valida_subdirector_academico;
						$val_of_serv_ext = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
						$val_jefe_mat = $data->valida_jefe_recursos_materiales;*/

						return ($id != 5 OR $id != 1) ? true : false;
					},
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#mensaje_vinc").dialog("open"); 
						
						return false;
					}',
				),
				'bloqSolicitud' => array
				(
					'label'=>'Observaciones Bloquedas',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/rechazarVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;
						/*$val_subdirector = $data->valida_subdirector_academico;
						$val_of_serv_ext = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
						$val_jefe_mat = $data->valida_jefe_recursos_materiales;*/

						return ( $id == 5) ? true : false;
					},
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{valSolicitudJPA},{noValSolicitudJPA},{bloqSolicitud}',
			'header'=>'Validar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'valSolicitudJPA' => array
				(
					'label'=>'Solicitud Validada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/validaJefeDeptoAcademico", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;
						/*$val_subdirector = $data->valida_subdirector_academico;
						$val_of_serv_ext = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
						$val_jefe_mat = $data->valida_jefe_recursos_materiales;*/

						return ($val_docente_resp != NULL AND $val_jefe_depto != NULL) ? true : false;
					},
					
				),
				'noValSolicitudJPA' => array
				(
					'label'=>'Validar Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/validaJefeDeptoAcademico", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;
						/*$val_subdirector = $data->valida_subdirector_academico;
						$val_of_serv_ext = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
						$val_jefe_mat = $data->valida_jefe_recursos_materiales;*/

						return ($val_docente_resp != NULL AND $val_jefe_depto == NULL AND $id != 3) ? true : false;
					},
					'options' => array(
						'title'        => 'Validar la Solicitud de Visita Académica',
						'data-confirm' => '¿En verdad quieres VALIDAR la Solicitud?',
					),
					'click' => $ValSolicitudJefeProyAcad,
				),
				'bloqSolicitud' => array
				(
					'label'=>'Solicitud Bloqueada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/validaJefeDeptoAcademico", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;
						/*$val_subdirector = $data->valida_subdirector_academico;
						$val_of_serv_ext = $data->valida_jefe_oficina_externos_vinculacion; //ROXANA
						$val_jefe_mat = $data->valida_jefe_recursos_materiales;*/

						return ($val_docente_resp != NULL AND $val_jefe_depto == NULL AND $id == 3) ? true : false;
					},
				)
			),
		),
	),
)); ?>

<?php }else{ ?>

	<!--CUANDO EL QUE SE LOGEA NO ES JEFE DE PROYECTOS DE VINCULACION DE ALGUN DEPTO ACADEMICO, NO PODRA EDITAR LAS SOLICITUDES-->
	<div class="alert alert-danger">
		<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			No eres Jefe de proyectos de Vinculación o Jefe de Departamento Académico.
		</strong></p>
	</div>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-vinc-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchCuandoNoEsSubdirectorAcademico(),
	//'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			//'filter' => false,
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" 
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";
				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		//'periodo',
		//'anio',
		/*array(
			'name' => 'no_alumnos',
			'filter' => false,
			'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				//if($modelVaTiposVisitasAcademicas === NULL)
					//throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return ($modelVaTiposVisitasAcademicas->tipo_visita_academica != NULL) ? $modelVaTiposVisitasAcademicas->tipo_visita_academica : null;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'--Filtrar por --')
			),
			'htmlOptions' => array('width'=>'130px', 'class'=>'text-center')
		),
		array(
			'header' => 'Departamento',
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por Depto --')
			),
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				//obtenemos el rfc del docente responsable principal
				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];

				//Ontenemos el depto catedratico del Empleado
				//Obtener el depto academico del docente responsable de la visita academica
				$qry_depto = "select 
								(
									select \"dscDepartamento\" from public.\"H_departamentos\" 
									where \"cveDeptoAcad\" = hemp.\"deptoCatedratico\"
								) as depto_academico
								from public.\"H_empleados\" hemp
								where hemp.\"rfcEmpleado\" = '$rfc' 
							";

				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['depto_academico'];

			},
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'class' => 'ComponentValidaSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'310px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{editSolicitud},{noEditSolicitud}',
			'header'=>'Editar Solicitud',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editSolicitud' => array
				(
					'label'=>'Editar de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => function($row, $data)
					{
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$val_subdirector = $data->valida_subdirector_academico;

						return ($val_jefe_depto === null AND $val_subdirector === null) ? true : false;
					}
				),
				'noEditSolicitud' => array
				(
					'label'=>'Sin Permiso de Edición',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$val_subdirector = $data->valida_subdirector_academico;

						return ($val_jefe_depto != null AND $val_subdirector != null) ? true : false;
					}
				)
			),
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delSolicitud}',
			'header'=>'Eliminar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delSolicitud' => array
				(
					'label'=>'Eliminar la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/eliminarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
					'options' => array(
						'title'        => 'Eliminar la Solicitud de Visita Académica Seleccionada',
						'data-confirm' => '¿En verdad quieres ELIMINAR la Solicitud?',
					),
					'click' => $JsDelSolicitudV, 
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{valSolicitudJPA},{noValSolicitudJPA}',
			'header'=>'Validar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons' => array
			(
				'valSolicitudJPA' => array
				(
					'label'=>'Solicitud Validada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/validaJefeDeptoAcademico", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL) ? true : false;
					}
					
				),
				'noValSolicitudJPA' => array
				(
					'label'=>'Validar Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/validaJefeDeptoAcademico", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$id = $data->id_estatus_solicitud_visita_academica;

						return ($val_docente_resp != null && $val_jefe_proy_vinc == null && $id != 3 ) ? true : false;
					},
					'options' => array(
						'title'        => 'Validar la Solicitud de Visita Académica',
						'data-confirm' => '¿En verdad quieres VALIDAR la Solicitud?',
					),
					'click' => $ValSolicitudJefeProyAcad,
				),
			),
		),
	),
)); ?>

<?php } ?>

<br><br><br><br><br>
<br><br><br><br><br>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'mensaje_vinc',
	'options' => array(
		'title' => 'Hacer Observación a la Solicitud',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1250, 
		'height' => 430,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	<?php //echo $this->renderPartial('_formMensajeDialog', array('model'=>$model)); ?>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>