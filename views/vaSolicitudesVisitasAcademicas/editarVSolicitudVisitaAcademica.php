<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Detalle Solicitud de Visita Académica',
	);
	
	/*ELIMINAR RESPONSABLES DE LA VISITA ACADEMICA*/
	$JsDelResponsable = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-editdoc-responsables-visita-academica-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-editdoc-responsables-visita-academica-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*ELIMINAR RESPONSABLES DE LA VISITA ACADEMICA*/

	/*ELIMINAR MATERIAS DE LA VISITA ACADEMICA*/
	$JsDelMateria = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-editdoc-materias-imparte-responsables-visita-academica-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-editdoc-materias-imparte-responsables-visita-academica-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*ELIMINAR MATERIAS DE LA VISITA ACADEMICA*/

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle Solicitud de Visita Académica
		</span>
	</h2>
</div>

<div class="row">
	<div class="cool-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Editar</b>
				</h3>
			</div>
			<div class="panel-body">

			<div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'va-edit-doc-solicitudes-visitas-academicas-form',
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation'=>false,
			)); ?>

			<span align="center"><b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b></span>

			<?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas, $modelVaResponsablesVisitasAcademicas, $modelVaMateriasImparteResponsableVisitaAcademica); ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
				<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica',array('maxlength'=>500,'class'=>'form-control')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
				<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica',array('maxlength'=>500,'class'=>'form-control')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
			</div>
			<?php } ?>

			<!--<div class="form-group">
				<?php //echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
				<?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
				<?php //echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
			</div>-->

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_salida_visita'); ?>
				<?php
				$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $modelVaSolicitudesVisitasAcademicas,
					'attribute' => 'fecha_hora_salida_visita',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim'=>'fold',
						'showPeriod' => false,
						'showTime' => false,
						'hourText' => 'Hr. Sal',
						'minuteText' => 'Min. Sal',
						'ampm' => true,
						//'dateFormat' => 'dd.mm.yy'
					),
					'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
				));
				?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_salida_visita'); ?>
				<?php
				$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $modelVaSolicitudesVisitasAcademicas,
					'attribute' => 'fecha_hora_salida_visita',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim'=>'fold',
						'showPeriod' => false,
						'showTime' => false,
						'hourText' => 'Hr. Sal',
						'minuteText' => 'Min. Sal',
						'ampm' => true,
						//'dateFormat' => 'dd.mm.yy'
					),
					'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
				));
				?>
			</div>
			<?php } ?>

			<!--<div class="form-group">
				<?php //echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
				<?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
				<?php //echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
			</div>-->

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_regreso_visita'); ?>
				<?php
				$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $modelVaSolicitudesVisitasAcademicas,
					'attribute' => 'fecha_hora_regreso_visita',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim'=>'fold',
						'showPeriod' => false,
						'showTime' => false,
						'hourText' => 'Hr. Reg',
						'minuteText' => 'Min. Reg',
						'ampm' => true,
						//'dateFormat' => 'dd.mm.yy'
					),
					'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
				));
				?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_regreso_visita'); ?>
				<?php
				$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model' => $modelVaSolicitudesVisitasAcademicas,
					'attribute' => 'fecha_hora_regreso_visita',
					// additional javascript options for the date picker plugin
					'options' => array(
						'showAnim'=>'fold',
						'showPeriod' => false,
						'showTime' => false,
						'hourText' => 'Hr. Reg',
						'minuteText' => 'Min. Reg',
						'ampm' => true,
						//'dateFormat' => 'dd.mm.yy'
					),
					'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
				));
				?>
			</div>
			<?php } ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
				<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
												'id_tipo_visita_academica',
												$tipos_visitas,
												array('prompt'=>'-- Selecciona el Tipo de Visita --', 'class'=>'form-control', 'required'=>'required'
											)); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
				<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
												'id_tipo_visita_academica',
												$tipos_visitas,
												array('prompt'=>'-- Selecciona el Tipo de Visita --', 'class'=>'form-control', 'required'=>'required'
											)); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
			</div>
			<?php } ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
				<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
											'id_empresa_visita',
											$lista_empresas_visitas,
											array('prompt'=>'-- Empresa a Visitar --', 'class'=>'form-control', 'required'=>'required'
											)); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
				<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
											'id_empresa_visita',
											$lista_empresas_visitas,
											array('prompt'=>'-- Empresa a Visitar --', 'class'=>'form-control', 'required'=>'required'
											)); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
			</div>
			<?php } ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
				<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'area_a_visitar', array('class'=>'form-control', 'rows' => 2, 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
				<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'area_a_visitar', array('class'=>'form-control', 'rows' => 2, 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
			</div>
			<?php } ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
				<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area', array('class'=>'form-control', 'rows' => 5, 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
				<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area', array('class'=>'form-control', 'rows' => 5, 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
			</div>
			<?php } ?>

			<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
				<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'no_alumnos', array('class'=>'form-control', 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
			</div>
			<?php }else{ ?>
			<div class="form-group">
				<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
				<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'no_alumnos', array('class'=>'form-control', 'required'=>'required')); ?>
				<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
			</div>
			<?php } ?>

			<div class="form-group">
				<?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar Solicitud' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
				<?php echo CHtml::link('Cancelar', array('listaSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
			</div>

				
			<?php $this->endWidget(); ?>

			</div><!-- form -->

			</div>
		</div>
	</div>
</div>
<br>

<hr>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Responsables Solicitud de Visita Académica
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-8">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Agregar Nuevo Responsable</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php echo CHtml::dropDownList('id_responsable_'. $VaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas, 
												$VaResponsablesVisitasAcademicas->rfcEmpleado,
												$lista_responsables, //Lista de Responsables
												array(
													'prompt'=>'-- Agregar Nuevo Responsable --', 'class'=>'form-control', 'required'=>'required',
													'ajax'=>array(
													'type'=>'POST',
													'dataType'=>'json',
													'data' => array(
															'rfcEmpleado' => 'js:this.value',
															'id_solicitud_visitas_academicas' => $VaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas
														),
													'url'=>CController::createUrl('vaSolicitudesVisitasAcademicas/agregarNuevoResponsableVisitaAcademica'),
												))
												
								);
				?>
			</div>
		</div>
	</div>
	<div class="col-md-2">
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-editdoc-responsables-visita-academica-grid',
	'dataProvider'=>$modelVaResponsablesVisitasAcademicas->searchListaResponsablesSolicitudVisitaAcademica($id_visita),
	'filter'=>$modelVaResponsablesVisitasAcademicas,
	'columns'=>array(
		array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return CHtml::image("items/getFoto.php?nctr_rfc=".$data->rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'20px','class'=>'text-center')
        ),
		array(
			'name' => 'rfcEmpleado',
			'filter' => false,
			'htmlOptions' => array('width'=>'40px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable',
			'value' => function($data)
			{
				$rfc = $data->rfcEmpleado;
				$qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs = Yii::app()->db->createCommand($qry_name)->queryAll();

				return $rs[0]['nmbEmpleado'].' '.$rs[0]['apellPaterno'].' '.$rs[0]['apellMaterno'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'header' => 'Fecha se Registró Responsable',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

				$id = $data->id_solicitud_visitas_academicas;
				$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaRegistroResponsableVisitaAcademica($id);

				return $fecha_creac_sol[0]['fecha_act'].' a las '.$fecha_creac_sol[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'180px', 'class'=>'text-center')
		),
		array(
			'header' => 'Principal <br>Reponsable',
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->responsable_principal == 1) ? '<span style="font-size:16px" class="label label-success">SI</span>' : '<span style="font-size:16px" class="label label-danger">NO</span>';
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delResponsable}',
			'header'=>'Eliminar Responsable',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delResponsable' => array
				(
					'label'=>'Eliminar Responsable',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaResponsablesVisitasAcademicas/eliminarResponsableVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas,
																																				"rfcEmpleado"=>$data->rfcEmpleado))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
					'options' => array(
						'title'        => 'Eliminar Responsable Seleccionado',
						'data-confirm' => '¿En verdad quieres ELIMINAR ese Responsable?',
					),
					'visible' => '$data->responsable_principal != 1',
					'click' => $JsDelResponsable, 
				),
			),
		),
	),
)); ?>
<br>

<hr>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Materias Cubre Solicitud de Visita Académica
		</span>
	</h2>
</div>

<br><br>
<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Agregar Nuevo Materia Cubre Visita</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php echo CHtml::dropDownList('id_materia_'. $VaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas, 
												$VaMateriasImparteResponsableVisitaAcademica->cveMateria,
												$materias_asignadas_responsable, //Lista de Responsables
												array(
													'prompt'=>'-- Agregar Nuevo Materia --', 'class'=>'form-control', 'required'=>'required',
													'ajax'=>array(
													'type'=>'POST',
													'dataType'=>'json',
													'data' => array(
															'cveMateria' => 'js:this.value',
															'id_solicitud_materias_responsable_visitas_academicas' => $VaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas
														),
													'url'=>CController::createUrl('vaSolicitudesVisitasAcademicas/agregarNuevaMateriaVisitaAcademica'),
												))
												
								);
				?>
			</div>
		</div>
	</div>
	<div class="col-md-2">
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-editdoc-materias-imparte-responsables-visita-academica-grid',
	'dataProvider'=>$modelVaMateriasImparteResponsableVisitaAcademica->searchMateriasImparteResponsableVisitaAcademica($id_visita),
	'filter'=>$modelVaMateriasImparteResponsableVisitaAcademica,
	'columns'=>array(
		array(
			'class'=>'CButtonColumn',
			'template'=>'{logoMateria}',
			'header'=>'Materia',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logoMateria' => array
				(
					'label'=>'Editar Horario del Programa',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/materia_32.png',
				),
			),
		),
		array(
			'name' => 'cveMateria',
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:16px" class="label label-info">'.$data->cveMateria.'</span>';
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Nombre de la Materia',
			'value' => function($data)
			{
				$cve = $data->cveMateria;
				$qry_mat = "select * from public.\"E_catalogoMaterias\" where \"cveMateria\" = '$cve' ";
				$rs = Yii::app()->db->createCommand($qry_mat)->queryAll();

				return $rs[0]['dscMateria'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delMateria}',
			'header'=>'Eliminar Materia',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'delMateria' => array
				(
					'label'=>'Eliminar Materia',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/eliminarMateriaVisitaAcademica", array("id_solicitud_materias_responsable_visitas_academicas"=>$data->id_solicitud_materias_responsable_visitas_academicas,
																																			"cveMateria"=>$data->cveMateria))',
					'imageUrl'=>'images/servicio_social/eliminar_32.png',
					'options' => array(
						'title'        => 'Eliminar Materia Seleccionada',
						'data-confirm' => '¿En verdad quieres ELIMINAR esa Materia?',
					),
					'visible' => function($row, $data)
					{
						$id = $data->id_solicitud_materias_responsable_visitas_academicas;
						$qry_mat = "select * from pe_vinculacion.va_materias_imparte_responsable_visita_academica 
									where id_solicitud_materias_responsable_visitas_academicas = '$id' ";
						
						$rs = Yii::app()->db->createCommand($qry_mat)->queryAll();

						return (sizeof($rs) > 1) ? true : false;
					},
					'click' => $JsDelMateria,
				),
			),
		),
	),
));
?>

<br><br><br><br><br>
<br><br><br><br><br>

