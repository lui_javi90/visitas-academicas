<br>
<!--<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Detalle de la Visita Académica
		</span>
	</h2>
</div>-->

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4 class="panel-title">
					<b>Información</b>
				</h4>
			</div>
			<div class="panel-body">

				<p><b><?php echo "Nombre de la Visita Académica"; ?>:</b>
			    <?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->nombre_visita_academica); ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('no_solicitud')); ?>:</b>
			    <span style="font-size:14px" class="label label-success"><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->no_solicitud); ?></span></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('periodo')); ?>:</b>
			    <?php echo $periodo; ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('anio')); ?>:</b>
			    <span style="font-size:14px" class="label label-success"><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->anio); ?></span></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_creacion_solicitud')); ?>:</b>
			    <?php echo ($fecha_creacion['0']['fecha_act'] != 'No disponible') ? $fecha_creacion['0']['fecha_act'].' a las '.$fecha_creacion['0']['hora'] : "--"; ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_salida_visita')); ?>:</b>
			    <?php echo ($fec_salida_visita[0]['fecha_act'] != 'No disponible') ? $fec_salida_visita[0]['fecha_act'].' a las '.$fec_salida_visita[0]['hora'] : "--"; ?></p>
			  

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_regreso_visita')); ?>:</b>
			    <?php echo ($fec_regreso_visita[0]['fecha_act'] != 'No disponible') ? $fec_regreso_visita[0]['fecha_act'].' a las '.$fec_regreso_visita[0]['hora'] : "--"; ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_tipo_visita_academica')); ?>:</b>
			    <?php echo $tipo_visita; ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_empresa_visita')); ?>:</b>
			    <?php echo $empresa; ?></p>
			 

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('area_a_visitar')); ?>:</b>
			    <?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->area_a_visitar); ?></p>
			   

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('objetivo_visitar_area')); ?>:</b>
			    <?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->objetivo_visitar_area); ?></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('no_alumnos')); ?>:</b>
			    <span style="font-size:14px" class="label label-success"><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->no_alumnos); ?></span></p>

			    <p><b><?php echo "No. de Solicitud de Vehículo" ?>:</b>
			    <span style="font-size:14px" class="label label-success"><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->id_aut_salida_vehiculo); ?></span></p>

			    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_estatus_solicitud_visita_academica')); ?>:</b>
			    <span style="font-size:14px" class="label label-default"><?php echo $estatus_sol; ?></span></p>
			    

			</div>
		</div>
	</div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Docente Responsable de la Visita Académica
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-resp-solicitudes-visitas-academicas-grid',
    'dataProvider'=>$modelVaResponsablesVisitasAcademicas->searchListaResponsablesSolicitudVisitaAcademica($id_solicitud_visitas_academicas),
    //'filter'=>$modelVaResponsablesVisitasAcademicas,
    'columns'=>array(
        //'id_solicitud_visitas_academicas',
		array(
			'header' => 'Responsable <br>Principal',
			'filter' => false,
			'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'name' => 'rfcEmpleado',
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
        array(
            'header' => 'Nombres Completo',
            'value' => function($data)
            {
                $rfc = $data->rfcEmpleado;
                $qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
                $name = Yii::app()->db->createCommand($qry_name)->queryAll();

                return $name[0]['nmbEmpleado'].' '.$name[0]['apellPaterno'].' '.$name[0]['apellMaterno'];

            },
            'htmlOptions' => array('width'=>'300px','class'=>'text-center')
        ),
        array(
            'header' => 'Responsable Principal',
            'type' => 'raw',
            'value' => function($data)
            {
            	return ($data->responsable_principal == true) ? '<span style="font-size:14px" class="label label-success">SI</span>' : '<span style="font-size:14px" class="label label-danger">NO</span>';
            },
            'htmlOptions' => array('width'=>'50px','class'=>'text-center')
        ),
    )
)); ?>


<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Materias Cubre Visita Académica
		</span>
	</h2>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-materias-cubre-visitas-academicas-grid',
    'dataProvider'=>$modelVaMateriasImparteResponsableVisitaAcademica->searchMateriasXSolicitudVisitaAcademica($id_solicitud_visitas_academicas),
    //'filter'=>$listaVaMateriasImparteResponsableVisitaAcademica,
    'columns'=>array(
		//'id_solicitud_materias_responsable_visitas_academicas',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{logMateria}',
			'header'=>'Materia',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logMateria' => array
				(
					'label'=>'Materia',
					//'url'=>'',
					'imageUrl'=>'images/servicio_social/materia_32.png',
				),
			),
		),
		array(
			'header' => 'Clave <br>Materia',
			'name' => 'cveMateria',
			'filter' => false,
			'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
		),
		//'cveMateria',
		array(
			'header' => 'Nombre Materia',
			'filter' => false,
			'value' => function($data)
			{
				$cve = $data->cveMateria;
				$qry_mat = "select * from public.\"E_catalogoMaterias\" where \"cveMateria\" = '$cve' ";
				$rs = Yii::app()->db->createCommand($qry_mat)->queryAll();

				return $rs[0]['dscMateria'];
			},
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		//'fecha_registro_materia_visita_academica',
		array(
			'header' => 'Departamento',
			'filter' => false,
			'value' => function($data)
			{
				$cve_materia = trim($data->cveMateria);
				$modelECatalogoMaterias = ECatalogoMaterias::model()->findByPk($cve_materia);
				$depto = trim($modelECatalogoMaterias->deptoMateria);
				$qry_depto_acad = "select * from public.\"H_departamentos\" where \"cveDeptoAcad\" = '$depto' ";
                $rs = Yii::app()->db->createCommand($qry_depto_acad)->queryAll();
				$departamento = $rs[0]['dscDepartamento'];
				
				return ($departamento != NULL ) ? $departamento : "DESCONOCIDO";
			},
			'htmlOptions' => array('width'=>'220px', 'class'=>'text-center')
		),
	)
)); ?>


<br><br><br><br>