<style>
div.ex1 {
  /*background-color: lightblue;*/
  width: auto;
  height: auto;
  overflow: auto;
}
</style>

<br>
<div align="center" class="alert alert-warning">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Todos los mensajes enviados para la Solicitud son mostrados acontinuación.</b>
  </strong></p>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h6 class="panel-title"><b>Historial de Mensajes</b></h6>
			</div>
            <div class="panel-body ex1">
                <div align="center" class="jumbotron">
                <?php
                    echo "<b>".$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud."</b>";
                    echo "<br>";
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<br>