<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Consulta Solicitudes a Visitas Académicas Docentes' => array('vaSolicitudesVisitasAcademicas/listaJVSolicitudesVisitasAcademicas'),
        'Editar Solicitud de Visita Académica'
    );
    
?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">

            Editar Solicitud de Visita Académica
                    
		</span>
	</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <b>Editar Solicitud</b>
                </h6>
            </div>
            <div class="panel-body">
                <?php
                /* @var $this VaSolicitudesVisitasAcademicasController */
                /* @var $model VaSolicitudesVisitasAcademicas */
                /* @var $form CActiveForm */
                ?>

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'va-solicitudes-visitas-academicas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('autocomplete'=>'off', 'enctype'=>'multipart/form-data')
                )); ?>

                    <!--<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>-->

                    <?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas); ?>

                    <div class="col-md-6">

                        <div align="center">
                            <b>Nombre Vista Académica:</b> <?php echo $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica; ?>
                        </div>

                        <br><br>
                        <p align="left"><strong>Subir imagen o documento es Obligatorio para validar la Solicitud.</strong></p>

                        <br>
                        <p><strong>NOTA:</strong> El documento debe cumplir con los requisitos definidos.</p>

                        <p><strong>* Tamaño maximo de la imagen de 512 KB.</strong></p>
                        <p><strong>* Solo formatos .jpg o jpeg.</strong></p>

                        <br>
                        <!--Si devuelve 1 es que ya esta validado por todos los jefes menos el de Materiales-->
                        <?php if($valido_val_jefes == 1){ ?>
                        <div class="form-group">
                            <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'doc_oficio_confirmacion_empresa'); ?>
                            <?php echo $form->fileField($modelVaSolicitudesVisitasAcademicas,
                                                        'doc_oficio_confirmacion_empresa', 
                                                        array('class'=>'form-control', 'disabled'=>'disabled')); ?>
                            <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'doc_oficio_confirmacion_empresa'); ?>
                        </div>
                        <?php }else{ ?>
                        <div class="form-group">
                            <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'doc_oficio_confirmacion_empresa'); ?>
                            <?php echo $form->fileField($modelVaSolicitudesVisitasAcademicas,
                                                        'doc_oficio_confirmacion_empresa', 
                                                        array('class'=>'form-control')); ?>
                            <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'doc_oficio_confirmacion_empresa'); ?>
                        </div>
                        <?php } ?>

                    </div>
                    <div align="center" class="col-md-6">
                        <p align="center"><strong>Documento actual</strong></p>
                        <br><br>
                        <?php 
                            $path = Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.trim($modelVaSolicitudesVisitasAcademicas->path_carpeta_oficio_confirmacion_empresa).'/'.trim($modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa);

                            $imageData = base64_encode(file_get_contents($path));

                            echo ($modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa == NULL or empty($modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa)) ? '<img align="center" height="350" width="300" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/carta_default.png"/>' : '<img align="center" height="350" width="300" src="data:image/jpeg;base64,'.$imageData.'">';
                        ?>

                        <br><br><br><br>
                    </div>

                    <hr>

                    <?php if($valido_val_jefes == 0){ ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
                        <?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
                        <?php 
                        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                            'model' => $modelVaSolicitudesVisitasAcademicas,
                            'attribute' => 'fecha_hora_salida_visita',
                            // additional javascript options for the date picker plugin
                            'options' => array(
                                'showAnim'=>'fold',
                                'showButtonPanel'=>true,
                                'autoSize'=>true,
                                'showPeriod' => false,
                                'showTime' => false,
                                'hourText' => 'Hr. Reg',
                                'minuteText' => 'Min. Reg',
                                'ampm' => true,
                                //'dateFormat' => 'dd.mm.yy'
                            ),
                            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
                        ));
                        ?>
                        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
                        <?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
                        <?php 
                        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
                            'model' => $modelVaSolicitudesVisitasAcademicas,
                            'attribute' => 'fecha_hora_regreso_visita',
                            // additional javascript options for the date picker plugin
                            'options' => array(
                                'showAnim'=>'fold',
                                'showButtonPanel'=>true,
                                'autoSize'=>true,
                                'showPeriod' => false,
                                'showTime' => false,
                                'hourText' => 'Hr. Reg',
                                'minuteText' => 'Min. Reg',
                                'ampm' => true,
                                //'dateFormat' => 'dd.mm.yy'
                            ),
                            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
                        ));
                        ?>
                        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
                        <?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'no_alumnos', array('size'=>1,'maxlength'=>2, 'class'=>'form-control', 'required'=>'required')); ?>
                        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
                    </div>

                    <?php }else{ ?>

                    <div align="center">
                        <h3><span style="font-size:16px" class="label label-success">VALIDADA POR JEFE DE OFIC. DE SERVICIOS EXTERNOS</span></h3>
                    </div>

                    <br>

                    <?php } ?>

                    <br>
                    <div class="form-group">
                        <?php if($valido_val_jefes == 0){ ?>
                            <?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar Cambios' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php } ?>

                        <?php echo CHtml::link('Cancelar', array('listaJVSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->

            </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Bitacora de la Solicitud
		</span>
	</h2>
</div>

<br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-bitacora-cambios-solicitud-vinculacion-grid',
    'dataProvider'=>$modelVaBitacoraCambiosSolicitudVinculacion->searchBitacoraXSolicitudVisitaAcademica($id_solicitud_visitas_academicas),
    //'filter'=>$modelVaBitacoraCambiosSolicitudVinculacion,
    'columns'=>array(
        //'id_bitacora_solicitud_visita_academica',
        array(
            'class'=>'CButtonColumn',
			'template'=>'{detail}',
			'header'=>' * ',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detail' => array
				(
					'label'=>'Detalle registro de Bitacora',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/cartas_servsocial_32.png',
				),
            ),
        ),
        array(
            'header' => 'No. de <br>Alumnos',
            'name' => 'no_alumnos_visita',
            'filter' => false,
            'htmlOptions' => array('width'=>'10px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_hora_salida',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaSalidaBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_hora_regreso',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaRegresoBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'name' => 'fecha_ultima_modificacion_solicitud',
            'filter' => false,
            'value' => function($data)
            {
                require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
                $fec = InfoSolicitudVisitasAcademicas::getFormatoFechaActualizacionBitacora($data->id_bitacora_solicitud_visita_academica);

                return ($fec[0]['fecha_act'] != 'SV') ? $fec[0]['fecha_act'].' a las '.$fec[0]['hora'] : "NO DEFINIDO";
            },
            'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'class'=>'CButtonColumn',
			'template'=>'{editoDocDig}, {noEditDocDig}',
			'header'=>'Editó <br>Documento',
			'htmlOptions'=>array('width:50px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editoDocDig' => array
				(
					'label'=>'Editó Documento',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
                    'imageUrl' => 'images/servicio_social/aceptar_32.png',
                    'visible' => ' $data->modifico_doc_digital == 1 '
                ),
                'noEditDocDig' => array
				(
					'label'=>'No Editó Documento',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
                    'imageUrl' => 'images/servicio_social/rechazar_32.png',
                    'visible' => ' $data->modifico_doc_digital == 0 '
				),
            ),
        ),
        //'usuario_modifica', --Solo visto por el Docente que creo la solicitud de visita academica
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>