<style>
    .hoja{
        background-color: transparent;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 10px;
    }
    .letra2{
        font-size: 12px;
    }
    .div2{
        background-color: white;
        padding: -4px;
        padding-left: -1px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        line-height: 12pt;
    }
    .interlineado3{
        letter-spacing: 2pt;       /*para separar entre letras */
        word-spacing: 2.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado4{
        letter-spacing: 5pt;
    }
    .interlineado4{
        letter-spacing: 4.5pt;
    }
    .interlineado5{
        letter-spacing: .8pt;       /*para separar entre letras */
        word-spacing: 2pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado6{
        letter-spacing: 1pt;       /*para separar entre letras */
        word-spacing: 5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    p {
        font-family: serif;
    }
    .estilo1{

        font-family: serif;
    }
</style>

<div class="hoja">
    <img height="90px" align="center" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--En caso de que haya mes especial-->
    <br><br>
    <div align="center" class="">
        <?php if($legenda_mes != null or !empty($legenda_mes)){ ?>
        <b><?php echo ' " '.$legenda_mes.' " '; ?></b>
        <?php } ?>
    </div>
    <!--En caso de que haya mes especial-->

    <!--Encabezado del documento-->
    <br><br><br>
    <div class="div2 right">
        <span class="estilo1 letra2 right">Celaya, Guanajuato, &nbsp;<?php echo "<span style='background-color:black;color:white;' class='bold'>".$fec_actual."</span>"; ?></span>
        <br>
        <span class="estilo1 letra2 right">DEPARTAMENTO: &nbsp;<?php echo "GESTIÓN TEC. Y VINC."; ?></span>
        <br>
        <span class="estilo1 letra2 right">No. DE OFICIO: &nbsp;<?php echo ($datos_empresa_sol[0]['no_solicitud'] < 10) ? '0'.$datos_empresa_sol[0]['no_solicitud'] : $datos_empresa_sol[0]['no_solicitud']; ?></span>
        <br>
        <span class="estilo1 letra2 right">ASUNTO: &nbsp;<?php echo "Presentación y Agradecimiento"; ?></span>
    </div>
    <!--Encabezado del documento-->

    <!--Destinatario y Presente-->
    <br>
    <div class="interlineado2 div2 left">
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_empresa_sol[0]['encargado']); ?></h6> <!--DESTINATARIO-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_empresa_sol[0]['cargo']); ?></h6> <!--PUESTO-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_empresa_sol[0]['empresa']); ?></h6> <!--EMPRESA-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_empresa_sol[0]['lugar_empresa']); ?></h6> <!--DIRECCION-->
        <h6 class="bold estilo1 letra2 interlineado3">PRESENTE</h6>
    </div>
    <!--Destinatario y Presente-->

    <!--Cuerpo de la Solicitud-->
    <br>
    <div class="interlineado5 div2 letra2">
        <p ALIGN="justify">
        El que suscribe, tiene a bien presentar a sus finas atenciones al (la) <span class="bold"><?php echo $lista_doc_respon; ?></span>,
        profesor (es) de este instituto que acude (n) como responsable (s) de los grupo de <span class="bold"><?php echo $lista_carreras; ?></span>,
        <span class="bold"><?php echo $datos_empresa_sol[0]['no_alumnos']; ?></span> estudiantes para visitar el día <span class="bold"><?php echo $datos_empresa_sol[0]['hora_salida_visita']; ?></span>
        en el turno <span class="bold"><?php echo "Matutino/Vespertino"; ?></span>, las instalaciones de esa organización que tan atinadamente dirige.
        </p>
    </div>

    <br>
    <div class="interlineado6 div2 letra2">
        <p ALIGN="justify">
        Así mismo, hago patente nuestro sincero agradecimiento por su excelente 
        disposición y apoyo para recibir a nuestros profesores y estudiantes, quienes
        se verán complacidos con la oportunidad que ustedes les brindan para conocer 
        el campo de acción en el que se desenvolverían como futuros profesionistas. 
        </p>
    </div>

    <br>
    <div class="interlineado6 div2 letra2">
        <p ALIGN="justify">
        Al vernos favorecidos con su apoyo en el logro de nuestros objetivos, sólo nos
        resta manifestarle la seguridad de nuestra distinguida consideración.
        </p>
    </div>
    <!--Cuerpo de la Solicitud-->

    <!--Despedida-->
    <br>
    <div class="interlineado1 div2 left">
        <h6 class="estilo1 interlineado5 bold letra2">A T E N T A M E N T E</h6>
        <i class="estilo1 interlineado1 bold letra2">La técnica por un México mejor &reg;</i>
    </div>
    <!--Despedida-->

    <!--Firma del Jefe del Área-->
    <div class="left div2">
        
        <?php
        
        //LLevara una validacion por parte de Erendira como Jefa
        if($fecha_val_sol[0]['fecha_act'] != 'SV')
        {

            $rfc_jefe_vinc = trim($val_jefe_vinculacion[0]['rfcEmpleado']);

            echo "<br>";
            //echo '<img align="center" height="60" src="firmas/LUMC781102KT1.jpg" />'; //Firma de jefe ofic de Servicio Social
            echo "<img height='50' src="."items/getFirma.php?nctr_rfc=".$rfc_jefe_vinc.">";
            echo "<br>";
            echo ($fecha_val_sol[0]['fecha_act'] != 'SV') ? "El Jefe de Departamento valido la solicitud el dia <br>".$fecha_val_sol[0]['fecha_act']." a las ".$fecha_val_sol[0]['hora'] : "";
            echo "<br>";
            echo "<span class=\"bold\">".$val_jefe_vinculacion[0]['nmbCompletoEmp']."</span>";
            
            //echo "<span class=\"bold letra2\">El docente responsable de nombre ".$datos_visita[0]['docente_responsable']." <br>validó la Solicitud el dia ".$datos_visita[0]['valida_docente_responsable']."</span>";
        }else{

            echo "<br><br>";
        }
        ?>
        <?php //echo "___________________________________________________"; ?>
        <br>
        <h6 class="estilo1 bold interlineado1 letra3">JEFE DEL DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
    </div>
    <!--Firma del Jefe del Área-->

    <!--Pie de pagina-->
    <br><br>
    <div class="div2">
        <h6 class="estilo1 interlineado1 letra3">C.p. Archivo Docente.</h6>
        <h6 class="estilo1 interlineado1 letra3">XXXX/xxxx</h6>
    </div>
    <!--Pie de pagina-->

</div>