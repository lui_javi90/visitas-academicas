<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
        'Histórico de Visitas Académicas'
    );

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Histórico de Visitas Académicas
		</span>
	</h2>
</div>

<?php if($is_subd_academico == true){ ?>

    <br><br><br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-subd-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchListaSolicitudesVisitasAcademicasValidadasSubdAcademico($cveDepto),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			//'filter' => false,
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				//$rfc = $data->hEmpleadoses->rfcEmpleado;
				//echo $rfc;
				//die;
				$qry_resp = "select \"rfcEmpleado\" 
							from pe_vinculacion.va_responsables_visitas_academicas
							where id_solicitud_visitas_academicas = '$id' AND responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();
				$rfc = $rfc[0]['rfcEmpleado'];

				return CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
				
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Tipo Visita <br>Académica',
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'--Filtrar por --')
			),
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'htmlOptions' => array('width'=>'110px', 'class'=>'text-center')
		),
		array(
			'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:14px" class="label label-success">'.$data->anio.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Periodo',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$data->periodo' ";

                $periodo = Yii::app()->db->createCommand($qry_per)->queryAll();

                return '<span style="font-size:14px" class="label label-success">'.$periodo[0]['dscPeriodo'].'<span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
		array(
			//FILTRAR POR DEPARTAMENTO ACADEMICO DEL DOCENTE
			'header' => 'Departamento',
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por --')
						),
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				//obtenemos el rfc del docente responsable principal
				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];

				//Obtener el depto academico del docente responsable de la visita academica
				$qry_depto = "select 
								(
									select \"dscDepartamento\" from public.\"H_departamentos\" 
									where \"cveDeptoAcad\" = hemp.\"deptoCatedratico\"
								) as depto_academico
								from public.\"H_empleados\" hemp
								where hemp.\"rfcEmpleado\" = '$rfc' 
							";

				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['depto_academico'];

			},
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Validada por <br>Subdirector Académico',
			'filter' => false,
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

				$val_subd_academico = InfoSolicitudVisitasAcademicas::getFechaValidacionSubAcademicoVisitaAcademica($data->id_solicitud_visitas_academicas);

				return ($val_subd_academico[0]['fecha_act'] === 'SV' ) ? 'Sin Validar' : $val_subd_academico[0]['fecha_act'].' a las '.$val_subd_academico[0]['hora'];
			},
			'htmlOptions' => array('width'=>'230px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statFinSolicitud},{statCancSolicitud},{statPenSolicitud},{statAcepSolicitud},{statSuspSolicitud},{statCompSolicitud},{statLibSolicitud},{statEnCursSolicitud}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statFinSolicitud' => array
				(
					'label'=>'Solicitud Finalizada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 6) ? true : false;
					}
				),
				'statCancSolicitud' => array
				(
					'label'=>'Solicitud Cancelada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 4) ? true : false;
					}
				),
				'statPenSolicitud' => array
				(
					'label'=>'Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/pendiente_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					}
				),
				'statAcepSolicitud' => array
				(
					'label'=>'Solicitud Aceptada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 2) ? true : false;
					}
				),
				'statSuspSolicitud' => array
				(
					'label'=>'Solicitud Suspendida',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 3) ? true : false;
					}
				),
				'statCompSolicitud' => array
				(
					'label'=>'Solicitud Completada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 5) ? true : false;
					}
				),
				'statLibSolicitud' => array
				(
					'label'=>'Solicitud en Liberación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 7) ? true : false;
					}
				),
				'statEnCursSolicitud' => array
				(
					'label'=>'Solicitud en Curso',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 8) ? true : false;
					}
				)
			),
		),
	),
)); ?>

<?php }else{ ?>

<!--CUANDO EL QUE SE LOGEA NO ES SUBDIRECTOR ACADEMICO, NO PODRA EDITAR LAS SOLICITUDES-->

<br><br><br><br><br>
<div class="alert alert-danger">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		No eres Subdirector Academico.
    </strong></p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-subd-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchCuandoNoEsSubdirectorAcademico(),
	//'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
        //'id_solicitud_visitas_academicas',
        array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			//'filter' => false,
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				//$rfc = $data->hEmpleadoses->rfcEmpleado;
				//echo $rfc;
				//die;
				$qry_resp = "select \"rfcEmpleado\" 
							from pe_vinculacion.va_responsables_visitas_academicas
							where id_solicitud_visitas_academicas = '$id' AND responsable_principal = true ";

				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();
				$rfc = $rfc[0]['rfcEmpleado'];

				return CHtml::image("items/getFoto.php?nctr_rfc=".$rfc, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
				
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Tipo Visita <br>Académica',
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'--Filtrar por --')
			),
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'htmlOptions' => array('width'=>'110px', 'class'=>'text-center')
		),
		array(
			'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:14px" class="label label-success">'.$data->anio.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Periodo',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$data->periodo' ";

                $periodo = Yii::app()->db->createCommand($qry_per)->queryAll();

                return '<span style="font-size:14px" class="label label-success">'.$periodo[0]['dscPeriodo'].'<span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
		array(
			//FILTRAR POR DEPARTAMENTO ACADEMICO DEL DOCENTE
			'header' => 'Departamento',
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por --')
						),
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				//obtenemos el rfc del docente responsable principal
				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];

				//Obtener el depto academico del docente responsable de la visita academica
				$qry_depto = "select 
								(
									select \"dscDepartamento\" from public.\"H_departamentos\" 
									where \"cveDeptoAcad\" = hemp.\"deptoCatedratico\"
								) as depto_academico
								from public.\"H_empleados\" hemp
								where hemp.\"rfcEmpleado\" = '$rfc' 
							";

				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['depto_academico'];

			},
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statFinSolicitud},{statCancSolicitud},{statPenSolicitud},{statAcepSolicitud},{statSuspSolicitud},{statCompSolicitud},{statLibSolicitud},{statEnCursSolicitud}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statFinSolicitud' => array
				(
					'label'=>'Solicitud Finalizada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 6) ? true : false;
					}
				),
				'statCancSolicitud' => array
				(
					'label'=>'Solicitud Cancelada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 4) ? true : false;
					}
				),
				'statPenSolicitud' => array
				(
					'label'=>'Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/pendiente_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					}
				),
				'statAcepSolicitud' => array
				(
					'label'=>'Solicitud Aceptada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 2) ? true : false;
					}
				),
				'statSuspSolicitud' => array
				(
					'label'=>'Solicitud Suspendida',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 3) ? true : false;
					}
				),
				'statCompSolicitud' => array
				(
					'label'=>'Solicitud Completada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 5) ? true : false;
					}
				),
				'statLibSolicitud' => array
				(
					'label'=>'Solicitud en Liberación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 7) ? true : false;
					}
				),
				'statEnCursSolicitud' => array
				(
					'label'=>'Solicitud en Curso',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 8) ? true : false;
					}
				)
			),
		),
	),
)); ?>

<?php } ?>




<br><br><br><br><br>
<br><br><br><br><br>