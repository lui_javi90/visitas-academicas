
<style>
.tatablamateriasresponsable {
    display:none;
}
</style>
<?php
/* @var $this VaSolicitudesVisitasAcademicasController */
/* @var $model VaSolicitudesVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'va-solicitudes-visitas-academicas-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>

	<?php //if(){ ?>
	<div class="col-md-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><b>Información Responsable: </b></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2">

						<?php echo CHtml::image("items/getFoto.php?nctr_rfc=".$info_empleado->rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:120px;height:120px;")); ?>

					</div>
					<div class="col-md-10">

						<b>Solicitante Principal: </b>
						<?php echo $info_empleado->siglaGradoMaxEstudio.' '.$info_empleado->nmbEmpleado.' '.$info_empleado->apellPaterno.' '.$info_empleado->apellMaterno; ?>
						<br><br>
						<b>Departamento: </b>
						<?php echo $departamento; ?>
						<br><br>
						<b>Puesto: </b>
						<?php echo $cargo; ?>
						<br><br>
						<?php if(!$modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
						<b>Fecha de creación de la Solicitud: </b>
						<?php echo $fec_creacion[0]['fecha_act'].' a las '.$fec_creacion[0]['hora']; ?>
						<?php }else{ ?>
						<b>Fecha de creación de la Solicitud: </b> -
						<?php  } ?>
						<br>

					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //} ?>

	<span align="center"><b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b></span>

	<?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas, $modelVaMateriasImparteResponsableVisitaAcademica); ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
		<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica',array('maxlength'=>500,'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
		<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica',array('maxlength'=>500,'class'=>'form-control')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'nombre_visita_academica'); ?>
	</div>
	<?php } ?>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
		<?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
		<?php //echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
	</div>-->

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_salida_visita'); ?>
        <?php
        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'model' => $modelVaSolicitudesVisitasAcademicas,
            'attribute' => 'fecha_hora_salida_visita',
            // additional javascript options for the date picker plugin
            'options' => array(
				'showAnim'=>'fold',
                'showPeriod' => false,
                'showTime' => false,
                'hourText' => 'Hr. Sal',
                'minuteText' => 'Min. Sal',
				'ampm' => true,
				'dateFormat' => 'dd.mm.yy'
            ),
            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
        ));
        ?>
        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
    </div>
	<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_salida_visita'); ?>
        <?php
        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'model' => $modelVaSolicitudesVisitasAcademicas,
            'attribute' => 'fecha_hora_salida_visita',
            // additional javascript options for the date picker plugin
            'options' => array(
				'showAnim'=>'fold',
				'showButtonPanel'=>true,
				'autoSize'=>true,
                'showPeriod' => false,
                'showTime' => false,
                'hourText' => 'Hr. Sal',
                'minuteText' => 'Min. Sal',
				'ampm' => true,
				'dateFormat' => 'dd.mm.yy'
            ),
            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
        ));
        ?>
        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_salida_visita'); ?>
    </div>
	<?php } ?>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
		<?php //echo $form->textField($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
		<?php //echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
	</div>-->

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_regreso_visita'); ?>
        <?php
        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'model' => $modelVaSolicitudesVisitasAcademicas,
            'attribute' => 'fecha_hora_regreso_visita',
            // additional javascript options for the date picker plugin
            'options' => array(
				'showAnim'=>'fold',
				'showButtonPanel'=>true,
				'autoSize'=>true,
                'showPeriod' => false,
                'showTime' => false,
                'hourText' => 'Hr. Reg',
                'minuteText' => 'Min. Reg',
				'ampm' => true,
				'dateFormat' => 'dd.mm.yy'
            ),
            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
        ));
        ?>
        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
    </div>
	<?php }else{ ?>
	<div class="form-group">
        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas, 'fecha_hora_regreso_visita'); ?>
        <?php
        $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'model' => $modelVaSolicitudesVisitasAcademicas,
            'attribute' => 'fecha_hora_regreso_visita',
            // additional javascript options for the date picker plugin
            'options' => array(
				'showAnim'=>'fold',
                'showPeriod' => false,
                'showTime' => false,
                'hourText' => 'Hr. Reg',
                'minuteText' => 'Min. Reg',
				'ampm' => true,
				'dateFormat' => 'dd.mm.yy'
            ),
            'htmlOptions' => array('size' => 8, 'maxlength' => 19, 'class'=>'form-control', 'readOnly'=>true),
        ));
        ?>
        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'fecha_hora_regreso_visita'); ?>
    </div>
	<?php } ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
		<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
										'id_tipo_visita_academica',
										$tipos_visitas,
										array('prompt'=>'-- Selecciona el Tipo de Visita --', 'class'=>'form-control', 'required'=>'required'
									)); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
		<?php echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
										'id_tipo_visita_academica',
										$tipos_visitas,
										array('prompt'=>'-- Selecciona el Tipo de Visita --', 'class'=>'form-control', 'required'=>'required'
									)); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_tipo_visita_academica'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
		<?php /*echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
									'id_empresa_visita',
									$lista_empresas_visitas,
									array('prompt'=>'-- Empresa a Visitar --', 'class'=>'form-control', 'required'=>'required'
									));*/ ?>
		<?php
					        $this->widget('ext.select2.ESelect2', array('model'=>$modelVaSolicitudesVisitasAcademicas,
					                  'attribute'=>'id_empresa_visita',
					                  'data' => $lista_empresas_visitas,
					                  'options' => array(
					                      'width' => '100%',
					                      'placeholder'=>'Buscar Empresa por Nombre', //Deberia ser por RFC por no lo tienen todas
					                      'allowClear'=>true,
					                  ),
					                  'htmlOptions' => array(
					                      'required'=>'required',
					                      //'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
					                  ),

					      	));
					   
		?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
		<?php /*echo $form->dropDownList($modelVaSolicitudesVisitasAcademicas,
									'id_empresa_visita',
									$lista_empresas_visitas,
									array('prompt'=>'-- Empresa a Visitar --', 'class'=>'form-control', 'required'=>'required'
									));*/ ?>
		<?php
					        $this->widget('ext.select2.ESelect2', array('model'=>$modelVaSolicitudesVisitasAcademicas,
					                  'attribute'=>'id_empresa_visita',
					                  'data' => $lista_empresas_visitas,
					                  'options' => array(
					                      'width' => '100%',
					                      'placeholder'=>'Buscar Empresa por Nombre', //Deberia ser por RFC por no lo tienen todas
					                      'allowClear'=>true,
					                  ),
					                  'htmlOptions' => array(
					                      'required'=>'required',
					                      //'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
					                  ),

					      	));
		?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'id_empresa_visita'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
		<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'area_a_visitar', array('class'=>'form-control', 'rows' => 2, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
		<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'area_a_visitar', array('class'=>'form-control', 'rows' => 2, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'area_a_visitar'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
		<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area', array('class'=>'form-control', 'rows' => 5, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
		<?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area', array('class'=>'form-control', 'rows' => 5, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'objetivo_visitar_area'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
		<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,'no_alumnos', array('class'=>'form-control', 'required'=>'required')); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
		<?php echo $form->textField($modelVaSolicitudesVisitasAcademicas,
									'no_alumnos',
									array('class'=>'form-control', 'required'=>'required')
								); ?>
		<?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'no_alumnos'); ?>
	</div>
	<?php } ?>

	<?php //if($modelVaResponsablesVisitasAcademicas->isNewRecord){ ?>
	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaResponsablesVisitasAcademicas,'rfcEmpleado'); ?>
		<?php /*echo $form->dropDownList($modelVaResponsablesVisitasAcademicas,
										'rfcEmpleado',
										$rfc_responsable,
										array('class'=>'form-control', 'required'=>'required', 'type'=>'hidden'
									));*/ ?>
		<?php //echo $form->error($modelVaResponsablesVisitasAcademicas,'rfcEmpleado'); ?>
	</div>-->
	<?php //} ?>



  <!--Combo de Materias a buscar por clave de la materia-->
  <?php if($modelVaMateriasImparteResponsableVisitaAcademica->isNewRecord){ ?>
  <div align="center">
		<b>Si tu Visita Académica cubre más de una Materia el Jefe de Proyectos de Vinculación podrá agregar las que faltan despues.</b>
	</div>
  <div class="form-group">
      <?php echo $form->labelEx($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
      <?php $this->widget('ext.select2.ESelect2', array('model'=>$modelVaMateriasImparteResponsableVisitaAcademica,
                  'attribute'=>'cveMateria',
                  'data' => $materias_asignadas_responsable,
                  'options' => array(
                      'width' => '100%',
                      'placeholder'=>'Buscar Materia por su Clave',
                      'allowClear'=>true,
                  ),
                  'htmlOptions' => array(
                      'required'=>'required',
                      'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
                  ),

      )); ?>
      <?php echo $form->error($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
  </div>
  <?php } ?>

  <!--Si solo hay un materia registrada en la solicitud entonces se muestra la materia-->
  <?php if(!$modelVaMateriasImparteResponsableVisitaAcademica->isNewRecord AND $unica_materia == true){ ?>
  <div class="form-group">
      <?php echo $form->labelEx($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
      <?php $this->widget('ext.select2.ESelect2', array('model'=>$modelVaMateriasImparteResponsableVisitaAcademica,
                  'attribute'=>'cveMateria',
                  'data' => $materias_asignadas_responsable,
                  'options' => array(
                      'width' => '100%',
                      'placeholder'=>'Buscar Materia por su Clave',
                      'allowClear'=>true,
                  ),
                  'htmlOptions' => array(
                      'required'=>'required',
                      'url'=>CController::createUrl('vaResponsablesVisitasAcademicas/infoResponsableSeleccionado'),
                  ),

      )); ?>
      <?php echo $form->error($modelVaMateriasImparteResponsableVisitaAcademica,'cveMateria'); ?>
  </div>
  <?php } ?>
  <!--Combo de Materias a buscar por clave de la materia-->

	<div class="form-group">
		<?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar Solicitud' : 'Guardar Cambios', array('class'=>'btn btn-primary')); ?>
		<?php echo CHtml::link('Cancelar', array('listaSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
