<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Industriales' => '?r=visitasacademicas',
		'Lista Solicitudes Visitas Industriales' => array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
		'Editar la Solicitud'
	);
?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar la Solicitud 
		</span>
	</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <b>Mensaje</b>
                </h3>
            </div>
            <div class="panel-body">
                <div align="center" class="">

                    <b>Responsable logeado: </b><?php echo $info_empleado->nmbEmpleado.' '.$info_empleado->apellPaterno.' '.$info_empleado->apellMaterno; ?>
                    <br>
                    <b>RFC: </b><?php echo $rfcEmpleado; ?>
                    
                </div>
                <br>
                <div align="center" class="">
                    <p><b>Debes ser Responsable principal de la Solicitud para poder realizar actualizaciones a la información,
                    debes acudir con el Responsable principal de la Solicitud.</b></p>
                </div>

                <br>
                <hr>
                <br>

                <h3 align="center" class=""><b>Responsable Principal de la Solicitud</b></h3>

                <div align="center" class="">
                    <br>
                    <?php echo CHtml::image("items/getFoto.php?nctr_rfc=".$rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:150px;height:150px;")); ?>
                    <br>
                    <b>Nombre Responsable: </b><?php echo $info_empleado->nmbEmpleado.' '.$info_empleado->apellPaterno.' '.$info_empleado->apellMaterno; ?>
                    <br>
                    <b>RFC: </b><?php echo $rfcEmpleado; ?>
                    <br>
                    <b>Departamento: </b><?php echo $departamento; ?>
                </diV>
            </div>
        </div>
    </div>
</div>

<br><br><br><br><br>