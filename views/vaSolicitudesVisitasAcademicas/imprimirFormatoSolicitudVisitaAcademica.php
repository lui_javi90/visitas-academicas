<style>
    .hoja{
        background-color: transparent;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 10px;
    }
    .letra2{
        font-size: 12px;
    }
    .div2{
        background-color: white;
        padding: -4px;
        padding-left: -1px;
    }
    .interlineado1{
        line-height: 8pt;
    }
    .interlineado2{
        line-height: 12pt;
    }
    .interlineado3{
        letter-spacing: 2pt;       /*para separar entre letras */
        word-spacing: 2.5pt;        /* para separacion entre palabras */
        line-height: 15pt;        /* para la separacion entre lineas */
        /*text-indent: 30pt;*/       /* para sangrias */
    }
    .interlineado4{
        letter-spacing: 5pt;
    }
    .interlineado4{
        letter-spacing: 4.5pt;
    }
    p {
        font-family: serif;
    }
    .estilo1{

        font-family: serif;
    }
</style>

<div class="hoja">
    <img height="90px" align="center" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--En caso de que haya mes especial-->
    <br>
    <div align="center" class="">
        <?php if($legenda_mes != null or !empty($legenda_mes)){ ?>
            <b><?php echo ' " '.$legenda_mes.' " '; ?></b>
        <?php } ?>
    </div>
    <!--En caso de que haya mes especial-->

    <!--Encabezado del documento-->
    <br>
    <div class="div2 right">
        <span class="estilo1 letra2 right">Celaya, Guanajuato, &nbsp;<?php echo "<span style='background-color:black;color:white;' class='bold'>".$fec_actual."</span>"; ?></span>
        <br>
        <span class="estilo1 letra2 right">DEPARTAMENTO: &nbsp;<?php echo "GESTIÓN TEC. Y VINC."; ?></span>
        <br>
        <?php $no_sol = ($datos_solicitud_visita[0]['no_solicitud'] < 10) ? '000'.$datos_solicitud_visita[0]['no_solicitud'] : $datos_solicitud_visita[0]['no_solicitud'];  ?>
        <span class="estilo1 letra2 right">No. DE OFICIO: &nbsp;<?php echo "DGTV/V.I./".$no_sol."/".$anio_solicitud; ?></span>
        <br>
        <span class="estilo1 letra2 right">ASUNTO: &nbsp;<?php echo "<span class=\"bold\">Solicitud de Visita</span>"; ?></span>
    </div>
    <!--Encabezado del documento-->

    <!--Destinatario y Presente-->
    <br>
    <div class="div2 left">
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_solicitud_visita[0]['encargado']); ?></h6> <!--DESTINATARIO-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_solicitud_visita[0]['cargo']); ?></h6> <!--PUESTO-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_solicitud_visita[0]['empresa']); ?></h6> <!--EMPRESA-->
        <h6 class="bold estilo1 letra2 interlineado1"><?php echo strtoupper($datos_solicitud_visita[0]['lugar_empresa']); ?></h6> <!--EMPRESA-->
        <h6 class="bold estilo1 letra2 interlineado4">PRESENTE</h6> <!--PRESENTE-->
    </div>
    <!--Destinatario y Presente-->

    <!--Cuerpo de la Solicitud-->
    <br>
    <div class="interlineado3 div2 letra2">
        <p ALIGN="justify">
        El que el presente sirva para saludarle y con la finalidad de reforzar los conocimientos adquiridos en el aula, solicitarle 
        sea autorizada una visita a las instalaciones de la empresa que usted atinadamente dirige, a un grupo de <span class="bold"><?php echo $datos_solicitud_visita[0]['total_alumnos']; ?> 
        estudiantes</span> de la (s) carrera (s) de <span class="bold"><?php echo $lista_carreras_alumnos; ?></span> de este Instituto, quienes acudirán bajo la responsabilidad de <span class="bold"><?php echo $lista_docentes_responsables; ?></span>
        catedráticos de este instituto.

        <br><br>El área a observar y objetivo de la visita es: <span class="bold"><?php echo $datos_solicitud_visita[0]['area_visitar']; ?></span>. 
        
        <br><br>De ser aceptada la visita, desearía que se programara para el dia <span class="bold"><?php echo $datos_solicitud_visita[0]['fecha_hora_salida_visita']; ?></span> a 
        <span class="bold"><?php echo $datos_solicitud_visita[0]['fecha_hora_regreso_visita']; ?></span> en el turno <span class="bold"><?php echo "Matutino/Vespertino"; ?></span>.

        <br><br>Para cualquier aclaración puede comunicarse con <span class="bold"><?php echo $datos_solicitud_visita[0]['jefa_of_servicios_externos']; ?></span>, al telefono <span class="bold">01 (461) 61.17575</span>
        a la extensión <span class="bold">2106</span> o al correo electrónico <span class="bold">(visitasindustriales@itcelaya.edu.mx)</span>.

        <br><br>
        De la misma manera solicito nos comunique requisitos de seguridad y presentación que deberán cubrir los participantes en la visita.
        <br><br>
        Agradezco la atención que tenga a bien brindar a la presente y me despido.
        </p>
    </div>
    <!--Cuerpo de la Solicitud-->

    <!--Despedida-->
    <div class="interlineado1 div2 left">
        <h6 class="estilo1 interlineado5 bold letra2">A T E N T A M E N T E</h6>
        <i class="estilo1 interlineado1 bold letra2">La técnica por un México mejor &reg;</i>
    </div>
    <!--Despedida-->

    <!--Firma del Jefe del Área-->
    <div class="left div2">
        
        <?php

         //LLevara una validacion por parte de Erendira como Jefa
        if($fecha_val_sol[0]['fecha_act'] != 'SV')
        {
            $rfc_jefe_vinc = trim($val_jefe_vinculacion[0]['rfcEmpleado']);
            //$rfc_jefe_vinc = "LUMC781102KT1";
            echo "<br>";
            echo "<img height='50' src="."items/getFirma.php?nctr_rfc=".$rfc_jefe_vinc.">";
            echo "<br>";
            echo ($fecha_val_sol[0]['fecha_act'] != 'SV') ? "El Jefe de Departamento valido la solicitud el dia <br>".$fecha_val_sol[0]['fecha_act']." a las ".$fecha_val_sol[0]['hora'] : "";
            echo "<br>";
            echo "<span class=\"bold\">".$val_jefe_vinculacion[0]['nmbCompletoEmp']."</span>";

        }else{

            echo "<br><br>";
        }
        ?>
        <?php //echo "___________________________________________________"; ?>
        <br>
        <h6 class="estilo1 bold interlineado1 letra3">JEFE DEL DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
    </div>
    <!--Firma del Jefe del Área-->

    <!--Pie de pagina-->
    <br>
    <div class="div2">
        <h6 class="estilo1 interlineado1 letra3">C.p. Archivo Docente.</h6>
        <h6 class="estilo1 interlineado1 letra3">XXXX/xxxx</h6>
    </div>
    <!--Pie de pagina-->
</div>