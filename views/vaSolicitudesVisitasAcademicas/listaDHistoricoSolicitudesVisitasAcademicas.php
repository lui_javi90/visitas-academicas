<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Histórico Solicitudes Visitas Académicas',
    );
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Histórico Solicitudes Visitas Académicas
		</span>
	</h2>
</div>


<br><br><br>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchXSolicitudesHistoricasDocenteResponsable($rfcEmpleado),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
        array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'450px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'-- Filtrar por --')
			),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Año',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'anio',
                                                array('2019'=>'2019','2020'=>'2020'),
                                                array('prompt'=>'-- Año --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                return '<span style="font-size:14px" class="label label-success">'.$data->anio.'</span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Periodo',
            'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
                                                'periodo',
                                                array('1'=>'ENERO-JUNIO','2'=>'AGOSTO-DICIEMBRE'),
                                                array('prompt'=>'-- Periodo --')
            ),
            'type' => 'raw',
            'value' => function($data)
            {
                $qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$data->periodo' ";

                $periodo = Yii::app()->db->createCommand($qry_per)->queryAll();

                return '<span style="font-size:14px" class="label label-success">'.$periodo[0]['dscPeriodo'].'<span>';
            },
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estatus',
            'filter' => false,
            'type' => 'raw',
            'value' => function($data)
            {
                $id = $data->id_estatus_solicitud_visita_academica;
                $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findByPk($id);
                if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                    throw new CHttpException(404,'No existe registro de ese Estatus.');

                return ($modelVaEstatusSolicitudVisitaAcademica->id_estatus_solicitud_visita_academica != 4) ? '<span style="font-size:14px" class="label label-default">'.$modelVaEstatusSolicitudVisitaAcademica->estatus.'</span>' : '<span style="font-size:14px" class="label label-danger">'.$modelVaEstatusSolicitudVisitaAcademica->estatus.'</span>';
            },
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{statSolicitud}',
			'header'=>'Detalle <br>Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statSolicitud' => array
				(
					'label'=>'Detalle Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleDHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
                    /*'visible' => function($row, $data)
                    {
                        $v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion;
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica;

                        //Realizo reporte de resultados e incidencias
                        $id = $data->id_solicitud_visitas_academicas;
                        $model = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($id);
                        if($model === NULL)
                            throw new CHttpException(404,'No existe registro de reporte de resultados e incidencias de la solicitud.');

                        $v7 = $model->valida_docente_reponsable;

                        return ($v1 != NULL AND $v2 != NULL AND $v3 != NULL AND $v4 != NULL AND $v5 != NULL AND $v6 == 6 AND $v7 != NULL) ? true : false;
                    }*/
					
				),
			),
        ),
	),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>