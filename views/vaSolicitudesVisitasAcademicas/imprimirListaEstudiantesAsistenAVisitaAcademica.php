<style>
    .hoja{
        background-color: white;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 10px;
    }
    .letra2{
        font-size: 12px;
    }
    .div2{
        background-color: white;
        padding: -4px;
        padding-left: -1px;
    }
    table
    {
        border-collapse: collapse;
    }
    table, th, td {

        border: 1px solid black;
        bgcolor: #9E9E9E;
    }
    th { background-color: #1B5E20; }
    .table1
    {
        border-collapse: collapse;
    }
    .table1, .th1, .td1 {

        border: 0px solid black;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    .mytable{
      border: 1px solid black;
      border-right: 1px solid black;
      border-collapse: collapse;
      color: #000000;
      width: 100%;
      background-color: #FFFFFF;
    }
    .mytable-head {
      border: 1px solid black;
      color: #000000;
      width: 100%;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
</style>

<div class="hoja">
    <img height="90px" align="center" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--Encabezado del documento-->
    <div class="div2">
        <h6 class="bold center letra2">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="bold center letra2">LISTA DE ESTUDIANTES QUE ASISTEN A LA VISITA ACADÉMICA</h6>
        <hr>
    </div>
    <!--Encabezado del documento-->

    <div align="center" class="center div2">
        <span class="center bold letra2">DEPARTAMENTO DE 
        <u><?php echo "  _".$datos_visita[0]['depto_doc_responsable']."_"; ?></u></span>
        <?php //echo "________________________________"; ?>
    </div>

    <!--Datos de la Lista de Estudiantes-->
    <br>
    <div class="div2">
        <table class="mytable" width="100%">
            <tr width="100%">
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="10%">
                    <span class="bold">FECHA DE LA VISITA</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="23%">
                    <span class="bold">DOMICILIO DE LA EMPRESA O <br> DE LA VISITA</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="50%">
                    <span class="bold">DOCENTE RESPONSABLE</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="17%">
                    <span class="bold">HORARIO DE LA VISITA</span>
                </th>
            </tr>
            <tr>
                <td style="border: 1px solid black;" width="10%" align="center" class="bold letra2">
                    <?php echo $datos_visita[0]['fecha_desde_visita'].' a <br>'.$datos_visita[0]['fecha_hasta_visita']; ?>
                </td>
                <td style="border: 1px solid black;" width="23%" align="center" class="bold letra2">
                    <?php echo $datos_visita[0]['domicilio']; ?>
                </td>
                <td style="border: 1px solid black;" width="50%" align="center" class="bold letra2">
                    <?php echo $datos_visita[0]['docente_responsable']; ?>
                </td>
                <td style="border: 1px solid black;" width="17%" align="center" class="bold letra2">
                    <?php echo $datos_visita[0]['hora_desde'].' a <br>'.$datos_visita[0]['hora_hasta']; ?>
                </td>
            </tr>
        </table>
    </div>
    <!--Datos de la Lista de Estudiantes-->

    <!--Lista de Estudiantes-->
    <br>
    <div class="div2">
        <table class="mytable" width="100%">
            <tr width="100%">
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="10%">
                    <span class="bold">No.</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="45%">
                    <span class="bold">NOMBRE DEL ESTUDIANTE</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="13%">
                    <span class="bold">No. CONTROL</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="27%">
                    <span class="bold">CARRERA</span>
                </th>
                <th bgcolor = "#9E9E9E" style="color:#FFFFFF;" class="bold center letra1" align="center" width="5%">
                    <span class="bold">SEMESTRE</span>
                </th>
            </tr>
            <?php
                $no_alumnos = sizeof($lista_alumnos);
                $numero = 0;
                for($j = 0; $j < $no_alumnos; $j++){ 
                
                //Contador de Alumnos
                $numero = $numero + 1;
            ?>
            <tr>
                <td style="border: 1px solid black;" width="10%" align="center" class="bold letra2">
                    <?php echo $numero; ?>
                </td>
                <td style="border: 1px solid black;" width="45%" align="center" class="bold letra2">
                    <?php echo $lista_alumnos[$j]['nombre_alumno']; ?>
                </td>
                <td style="border: 1px solid black;" width="13%" align="center" class="bold letra2">
                    <?php echo $lista_alumnos[$j]['no_control']; ?>
                </td>
                <td style="border: 1px solid black;" width="27%" align="center" class="bold letra2">
                    <?php echo $lista_alumnos[$j]['carrera_alumno']; ?>
                </td>
                <td style="border: 1px solid black;" width="5%" align="center" class="bold letra2">
                    <?php echo $lista_alumnos[$j]['semestre_alumno']; ?>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
    <!--Lista de Estudiantes-->

    <!--Nombre y firma del Responsable-->
    <br><br>
    <div align="center" class="div2">
        <span class="bold letra2"><?php echo "NOMBRE Y FIRMA DEL PROFESOR"; ?></span>
        <!--<br><br><br><br><br>-->
        <?php if($datos_visita[0]['valida_docente_responsable'] != NULL){ ?>

            <br><br><br>
            <?php echo "<span class=\"bold letra2\">El docente responsable de nombre ".$datos_visita[0]['docente_responsable']." <br>validó la Solicitud el dia ".$datos_visita[0]['valida_docente_responsable']."</span>"; ?>

        <?php }else{ ?>

            <br><br><br>

        <?php } ?>
        <br>
        <span class="bold"><?php echo "_______________________________________________"; ?></span>
    </div>
    <!--Nombre y firma del Responsable-->

</div>