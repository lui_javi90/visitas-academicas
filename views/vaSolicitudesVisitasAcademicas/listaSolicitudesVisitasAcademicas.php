<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Mis Solicitudes a Visitas Académicas',
	);

	/*JS PARA VALIDAR LA ELIMINACION DE LA SOLICITUD SELECCIONADA*/
	$JsDelSolicitud = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*JS PARA VALIDAR LA ELIMINACION DE LA SOLICITUD SELECCIONADA*/

	/*VALIDAR LISTA ALUMNOS DE VISITA ACADEMICA */
	$JsValAlumnosSolicitud = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
	/*VALIDAR LISTA ALUMNOS DE VISITA ACADEMICA */

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Mis Solicitudes a Visitas Académicas
		</span>
	</h2>
</div>

<br><br><br><br><br>
<div class="row">
	<div class="col-md-3">
		<div class="row"><!--Row 1-->
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">
						<b>Del Periodo y Año:</b>
					</h3>
				</div>
				<div align="center" class="panel-body">

					<span style="font-size:16px" class="label label-success"><?php echo $periodo; ?></span>
					<br><br>
					<span style="font-size:16px" class="label label-success"><?php echo $anio; ?></span>
					
				</div>
			</div>
		</div><!--Row 1-->
		
		<!--<div class="row">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">
						<b>Periodo:</b>
					</h3>
				</div>
				<div align="center" class="panel-body">
					
				</div>
			</div>
		</div><!--Row 2-->
	</div>

	<div class="col-md-6">
		<div class="row">
		</div>

		<div class="row">
		</div>
	</div>
	<div class="col-md-3">
		<div class="row">
			<div align="center" class="panel panel-info">
				<div class="panel-heading">
					<h3 align="left" class="panel-title">
						<b>Acciones:</b>
					</h3>
				</div>
				<div align="center" class="panel-body">
						
					<?php echo CHtml::link('Nueva Solicitud Visita Académica', array('nuevaSolicitudVisitaAcademica'), array('class'=>'center btn btn-success')); ?>
						
				</div>
			</div>
		</div>

		<div class="row">
		</div>
	</div>
</div>

<br><br><br><br><br>
<br>
<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Una vez sea VALIDADA tu Solicitud de Visita Académica podras visualizar los reportes (según vayas avanzando) y otras opciones.</b>
  </strong></p>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchListaSolicitudesVisitasAcademicasVigentes($rfc_empleado),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'8px', 'class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Alumnos <br>Asistirán',
			'name' => 'no_alumnos',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),*/
		array(
			'class' => 'ComponentValidaSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{docEmpresaValVisitaAcademica},{noDocDigValVisitaAcademica}',
			'header'=>'Doc Digital <br>Empresa',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'docEmpresaValVisitaAcademica' => array
				(
					'label'=>'Agregar Alumnos Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/showOficioConfirmacionEmpresa", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/observaciones_32.png',
					'visible' => function($row, $data)
					{
						return ($data->doc_oficio_confirmacion_empresa != NULL) ? true : false;
					},
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#javis2").dialog("open"); 
						
						return false;
					}',
				),
				'noDocDigValVisitaAcademica' => array
				(
					'label'=>'Documento Digital No Disponible',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/showOficioConfirmacionEmpresa", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						return ($data->doc_oficio_confirmacion_empresa == NULL) ? true : false;
					}
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editSolicitud},{noEditSolicitud}',
			'header'=>'Editar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons' => array
			(
				'editSolicitud' => array
				(
					'label'=>'Editar de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/editar_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_depto = $data->valida_jefe_depto_academico;
						$val_subdirector = $data->valida_subdirector_academico;

						return ($val_docente_resp === NULL AND $val_jefe_depto === null AND $val_subdirector === null) ? true : false;
					}
				),
				'noEditSolicitud' => array
				(
					'label'=>'Sin Permiso de Edición por Validación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						//$val_jefe_depto = $data->valida_jefe_depto_academico;
						//$val_subdirector = $data->valida_subdirector_academico;

						return ($val_docente_resp != NULL) ? true : false;
					}
				)
			),
		),
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{impSolicitud}',
			'header'=>'Imprimir Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impSolicitud' => array
				(
					'label'=>'Imprimir la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/printer.png',
				),
			),
		),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{formSolicitud},{noShowFormSolicitud}',
			'header'=>'Reporte Solicitud Visita',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'formSolicitud' => array
				(
					'label'=>'Lista Estudiantes Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirFormatoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser 2

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND ($id_stat == 2 OR $id_stat == 5 OR $id_stat == 7 OR $id_stat == 8)) ? true : false;
					}
				),
				'noShowFormSolicitud' => array
				(
					'label'=>'Solicitud Bloqueada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirFormatoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser diferente 2

						return ($val_docente_resp == NULL OR $val_jefe_proy_vinc == NULL OR $val_subd_academico == NULL OR $val_jefe_of_servext == NULL OR $val_jefe_materiales == NULL AND ($id_stat != 2 OR $id_stat != 5 OR $id_stat != 7 OR $id_stat != 8)) ? true : false;
					}
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{impSolPresYAgrad}, {noShowImpSolPresYAgrad}',
			'header'=>'Imprimir Presentacion y<br>Agradecimiento',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'impSolPresYAgrad' => array
				(
					'label'=>'Imprimir Presentacion',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirFormatoSolicitudPresentacionYAgradecimiento", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser 2

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND ($id_stat == 2 OR $id_stat == 5 OR $id_stat == 7 OR $id_stat == 8)) ? true : false;
					}
				),
				'noShowImpSolPresYAgrad' => array
				(
					'label'=>'Reporte Bloqueado',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirFormatoSolicitudPresentacionYAgradecimiento", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser diferente 2

						return ($val_docente_resp == NULL OR $val_jefe_proy_vinc == NULL OR $val_subd_academico == NULL OR $val_jefe_of_servext == NULL OR $val_jefe_materiales == NULL AND ($id_stat != 2 OR $id_stat != 5 OR $id_stat != 7 OR $id_stat != 8)) ? true : false;
					}
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{listaSolicitud},{noShowListaSolicitud}',
			'header'=>'Reporte Lista Alumnos',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'listaSolicitud' => array
				(
					'label'=>'Lista Estudiantes Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirListaEstudiantesAsistenAVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/printer.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser 2

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND ($id_stat == 2 OR $id_stat == 5 OR $id_stat == 7 OR $id_stat == 8)) ? true : false;
					}
				),
				'noShowListaSolicitud' => array
				(
					'label'=>'Lista Bloqueada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirListaEstudiantesAsistenAVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser diferente 2

						return ($val_docente_resp == NULL OR $val_jefe_proy_vinc == NULL OR $val_subd_academico == NULL OR $val_jefe_of_servext == NULL OR $val_jefe_materiales == NULL AND ($id_stat != 2 OR $id_stat != 5 OR $id_stat != 7 OR $id_stat != 8)) ? true : false;
					}
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{addAlumnosVisitaAcademica},{valAlumnosAgregadosVisita},{valListaAlumVisitaAcademica},{noShowAddAlumnosVisitaAcademica}',
			'header'=>'Agregar Alumnos Lista',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'addAlumnosVisitaAcademica' => array
				(
					'label'=>'Agregar Alumnos Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/agregarAlumnoAVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/agregarr_32.png',
					'visible' => function($row, $data)
					{
						//DEspues de que asignan camion y conductor y la valida jefe materiales se puede empezar a agregar a los alumnos
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser 2
						$val_lista_alum = $data->val_lista_alumnos_visita_academica;

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND $id_stat == 2 AND $val_lista_alum == null) ? true : false;
					}
				),
				'valAlumnosAgregadosVisita' => array
				(
					'label'=>'Validar Alumnos Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/validarAlumnosVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser 2
						$val_lista_alum = $data->val_lista_alumnos_visita_academica;

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND $id_stat == 2 AND $val_lista_alum == null) ? true : false;
					},
					'options' => array(
						'title'        => 'Validar Lista Alumnos de Visita Académica Seleccionada',
						'data-confirm' => '¿En verdad quieres VALIDAR la Lista de Alumnos?',
					),
					'click' => $JsValAlumnosSolicitud,
				),
				'valListaAlumVisitaAcademica' => array
				(
					'label'=>'Validados Alumnos Visita Académica',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/validarAlumnosVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser diferente
						$val_lista_alum = $data->val_lista_alumnos_visita_academica;

						return ($val_docente_resp != NULL AND $val_jefe_proy_vinc != NULL AND $val_subd_academico != NULL AND $val_jefe_of_servext != NULL AND $val_jefe_materiales != NULL AND $id_stat != 2 AND $val_lista_alum != null) ? true : false;
					},
				),
				'noShowAddAlumnosVisitaAcademica' => array
				(
					'label'=>'Vista No Disponible',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/validarAlumnosVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						$val_docente_resp = $data->fecha_creacion_solicitud;
						$val_jefe_proy_vinc = $data->valida_jefe_depto_academico;
						$val_subd_academico = $data->valida_subdirector_academico;
						$val_jefe_of_servext = $data->valida_jefe_oficina_externos_vinculacion;
						$val_jefe_materiales = $data->valida_jefe_recursos_materiales;
						$id_stat = $data->id_estatus_solicitud_visita_academica; //debe ser diferente 2
						$val_lista_alum = $data->val_lista_alumnos_visita_academica;

						return ($val_docente_resp == NULL OR $val_jefe_proy_vinc == NULL OR $val_subd_academico == NULL OR $val_jefe_of_servext == NULL OR $val_jefe_materiales == NULL OR $id_stat != 2 AND $val_lista_alum == null) ? true : false;
					}
				)
			),
		),
		/*array(
			'class' => 'ComponentPrueba',
			'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
		),*/
		array(
			'class' => 'ComponentReporteResultadosIncidentesVisitaAcademica',
			'htmlOptions' => array('width'=>'75px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{BitacoraSolVisitaAcademica}',
			'header'=>'Bitacora',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'BitacoraSolVisitaAcademica' => array
				(
					'label'=>'Agregar Alumnos Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaBitacoraCambiosSolicitudVinculacion/listaBitacoraCambiosVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/historial_docs_servsocial_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{obSolVisitaAcademica}',
			'header'=>'Mis <br>Observ.',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'obSolVisitaAcademica' => array
				(
					'label'=>'Observaciones a la Solicitud Visita Académica',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/obsSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
					'click' => 'function(){
						$("#parm-frame1").attr("src",$(this).attr("href")); $("#javis3").dialog("open"); 
						
						return false;
					}',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statFinSolicitud},{statCancSolicitud},{statPenSolicitud},{statAcepSolicitud},{statSuspSolicitud},{statCompSolicitud},{statLibSolicitud},{statEnCursSolicitud}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statFinSolicitud' => array
				(
					'label'=>'Solicitud Finalizada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 6) ? true : false;
					}
				),
				'statCancSolicitud' => array
				(
					'label'=>'Solicitud Cancelada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 4) ? true : false;
					}
				),
				'statPenSolicitud' => array
				(
					'label'=>'Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/pendiente_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					}
				),
				'statAcepSolicitud' => array
				(
					'label'=>'Solicitud Aceptada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 2) ? true : false;
					}
				),
				'statSuspSolicitud' => array
				(
					'label'=>'Solicitud Suspendida',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 3) ? true : false;
					}
				),
				'statCompSolicitud' => array
				(
					'label'=>'Solicitud Completada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 5) ? true : false;
					}
				),
				'statLibSolicitud' => array
				(
					'label'=>'Solicitud en Liberación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 7) ? true : false;
					}
				),
				'statEnCursSolicitud' => array
				(
					'label'=>'Solicitud en Curso',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 8) ? true : false;
					}
				)
			),
		),//fin
	),
)); ?>


<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'javis2',
	'options' => array(
		'title' => 'Documento de la Empresa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 800, 
		'height' => 750,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'javis3',
	'options' => array(
		'title' => 'Observaciones',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1220, 
		'height' => 700,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame1" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<br><br><br><br><br>
<br><br><br><br><br>
