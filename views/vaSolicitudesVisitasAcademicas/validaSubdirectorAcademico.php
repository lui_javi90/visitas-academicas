<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Valida Subdirector Académico',
    );
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Valida Subdirector Académico
		</span>
	</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <b>Valida Subdirector Académico</b>
                </h3>
            </div>
            <div class="panel-body">
                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'va-valjefe-solicitudes-visitas-academicas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

                    <?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
                        <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud', array('maxlength'=>500, 'class'=>'form-control')); ?>
                        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
                    </div>
                    
                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar' : 'Guardar', array('class'=>'btn btn-success')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->

            </div>
        </div>
    </div>
</div>