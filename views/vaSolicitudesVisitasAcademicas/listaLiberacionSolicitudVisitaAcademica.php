<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Lista de Liberación de Visitas Académicas',
    );

    /*LIBERAR LA SOLICITUD DE VISITA ACADEMICA, PASA A HISTORICO*/
    $LibSolicitudVisitaAcad = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-liberar-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-liberar-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
    }';
    /*LIBERAR LA SOLICITUD DE VISITA ACADEMICA, PASA A HISTORICO*/
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Lista de Liberación de Visitas Académicas
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'va-liberar-solicitudes-visitas-academicas-grid',
    'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchXSolicitudVisitaParaLiberacion($periodo, $anio),
    'filter'=>$modelVaSolicitudesVisitasAcademicas,
    'columns'=>array(
        //'id_solicitud_visitas_academicas',
        array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
        array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'500px', 'class'=>'text-center')
        ),
        array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'-- Filtrar por --')
			),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
        ),
        array(
            'header' => 'Estatus',
            'filter' => false,
            'value' => function($data)
            {
                $id = $data->id_estatus_solicitud_visita_academica;
                $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findByPk($id);
                if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                    throw new CHttpException(404,'No existe registro de ese Estatus.');

                return $modelVaEstatusSolicitudVisitaAcademica->estatus;
            },
            'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
        ),
        array(
			'class'=>'CButtonColumn',
			'template'=>'{libSolicitud},{noLibSolicitud}',
			'header'=>'Liberar <br>Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'libSolicitud' => array
				(
					'label'=>'Solicitud Liberada',
					//'url'=>'',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
                    'visible' => function($row, $data)
                    {
                        $v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion;
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica; //Debe ser COMPLETADA (5) Y PASA A FINALIZADA (6)
                        $v7 = $data->val_lista_alumnos_visita_academica;

                        //Realizo reporte de resultados e incidencias
                        $id = $data->id_solicitud_visitas_academicas;
                        $model = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($id);
                        if($model === NULL)
                            throw new CHttpException(404,'No existe registro de reporte de resultados e incidencias de la solicitud.');

                        $v8 = $model->valida_docente_reponsable;

                        return ($v1 != NULL AND $v2 != NULL AND $v3 != NULL AND $v4 != NULL AND $v5 != NULL AND $v6 != 5 AND $v7 != NULL AND $v8 != NULL) ? true : false;
                    }
					
				),
				'noLibSolicitud' => array
				(
					'label'=>'Liberar Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/liberarSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
                    'visible' => function($row, $data)
                    {
                        $v1 = $data->fecha_creacion_solicitud;
                        $v2 = $data->valida_jefe_depto_academico;
                        $v3 = $data->valida_subdirector_academico;
                        $v4 = $data->valida_jefe_oficina_externos_vinculacion;
                        $v5 = $data->valida_jefe_recursos_materiales;
                        $v6 = $data->id_estatus_solicitud_visita_academica; //Debe ser COMPLETADA (5) Y PASA A FINALIZADA (6)
                        $v7 = $data->val_lista_alumnos_visita_academica;

                        //Realizo reporte de resultados e incidencias
                        $id = $data->id_solicitud_visitas_academicas;
                        $model = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($id);
                        if($model === NULL)
                            throw new CHttpException(404,'No existe registro de reporte de resultados e incidencias de la solicitud.');

                        $v8 = $model->valida_docente_reponsable;

                        return ($v1 != NULL AND $v2 != NULL AND $v3 != NULL AND $v4 != NULL AND $v5 != NULL AND $v6 == 5 AND $v7 != NULL AND $v8 != NULL) ? true : false;
                    },
					'options' => array(
						'title'        => 'Liberar la Solicitud de Visita Académica',
						'data-confirm' => '¿En verdad quieres LIBERAR la Solicitud?',
					),
					'click' => $LibSolicitudVisitaAcad, 
				),
			),
		),
        //'periodo',
        //'anio',
        //'fecha_creacion_solicitud',
        /*
        'fecha_hora_salida_visita',
        'fecha_hora_regreso_visita',
        'id_tipo_visita_academica',
        'id_empresa_visita',
        'area_a_visitar',
        'objetivo_visitar_area',
        'no_alumnos',
        'observaciones_solicitud',
        'valida_jefe_depto_academico',
        'valida_subdirector_academico',
        'ultima_fecha_modificacion',
        'doc_oficio_confirmacion_empresa',
        'ultima_mod_oficio_conf_empresa',
        'path_carpeta_oficio_confirmacion_empresa',
        'valida_jefe_oficina_externos_vinculacion',
        'valida_jefe_recursos_materiales',
        'id_aut_salida_vehiculo',
        'id_estatus_solicitud_visita_academica',
        array(
            'class'=>'CButtonColumn',
        ),*/
    ),
)); ?>


<br><br><br><br><br>
<br><br><br><br><br>