<?php

	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Mis Solicitudes a Visitas Académicas' => array('vaSolicitudesVisitasAcademicas/listaSSolicitudesVisitasAcademicas'),
		'Detalle de la Solicitud'
	);

?>

<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Detalle de la Solicitud 
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><b>Detalle</b></h3>
			</div>
			<div class="panel-body">

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('nombre_visita_academica')); ?>:</b>
				<?php echo $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('periodo')); ?>:</b>
				<?php echo $periodo; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('anio')); ?>:</b>
				<?php echo '<span style="font-size:16px" class="label label-success">'.$modelVaSolicitudesVisitasAcademicas->anio.'</span>'; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_salida_visita')); ?>:</b>
				<?php echo $fecha_sal[0]['fecha_act'].' a las '.$fecha_sal[0]['hora']; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_regreso_visita')); ?>:</b>
				<?php echo $fecha_reg[0]['fecha_act'].' a las '.$fecha_reg[0]['hora']; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_tipo_visita_academica')); ?>:</b>
				<?php echo $tipo_visita_acad; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_empresa_visita')); ?>:</b>
				<?php echo $empresa; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('area_a_visitar')); ?>:</b>
				<?php echo $modelVaSolicitudesVisitasAcademicas->area_a_visitar; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('objetivo_visitar_area')); ?>:</b>
				<?php echo $modelVaSolicitudesVisitasAcademicas->objetivo_visitar_area; ?></p>

				<p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('no_alumnos')); ?>:</b>
				<?php echo '<span style="font-size:16px" class="label label-success">'.$modelVaSolicitudesVisitasAcademicas->no_alumnos.'</span>'; ?></p>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"><b>No. de Solicitud</b></h3>
				</div>
				<div align="center" class="panel-body">
					<br>
					<?php echo '<span style="font-size:18px" class="label label-success">'.$modelVaSolicitudesVisitasAcademicas->no_solicitud.'</span>'; ?>
					<br><br>
				</div>
			</div>
		</div>
	<!--</div>

	<div class="row">-->
		<div class="col-md-4">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"><b>Fecha de creación</b></h3>
				</div>
				<div align="center" class="panel-body">
					<br>
					<?php echo '<span style="font-size:16px" class="label label-default">'.$fecha_creac_sol[0]['fecha_act'].' a las '.$fecha_creac_sol[0]['hora'].'</span>'; ?>
					<br><br>
				</div>
			</div>
		</div>
	</div>
</div>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Responsables de la Visita Académica
		</span>
	</h2>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-responsables-visita-academica-grid',
	'dataProvider'=>$modelVaResponsablesVisitasAcademicas->searchListaResponsablesSolicitudVisitaAcademica($id),
	'filter'=>$modelVaResponsablesVisitasAcademicas,
	'columns'=>array(
		array(
			'header' => 'Foto Perfíl',
            'type'=>'raw',
			'value' => function($data)
			{
				return CHtml::image("items/getFoto.php?nctr_rfc=".$data->rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			//'value'=>'(!empty($data->image))?CHtml::image(Yii::app()->assetManager->publish('.$assetsDir.'$data->image),"",array("style"=>"width:25px;height:25px;")):"no image"',
			},
			'htmlOptions' => array('width'=>'20px','class'=>'text-center')
        ),
		array(
			'name' => 'rfcEmpleado',
			'filter' => false,
			'htmlOptions' => array('width'=>'40px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable',
			'value' => function($data)
			{
				$rfc = $data->rfcEmpleado;
				$qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs = Yii::app()->db->createCommand($qry_name)->queryAll();

				return $rs[0]['nmbEmpleado'].' '.$rs[0]['apellPaterno'].' '.$rs[0]['apellMaterno'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'320px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Apellido Paterno',
			'value' => function($data)
			{
				$rfc = $data->rfcEmpleado;
				$qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs = Yii::app()->db->createCommand($qry_name)->queryAll();

				return $rs[0]['apellPaterno'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),
		array(
			'header' => 'Apellido Materno',
			'value' => function($data)
			{
				$rfc = $data->rfcEmpleado;
				$qry_name = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs = Yii::app()->db->createCommand($qry_name)->queryAll();

				return $rs[0]['apellMaterno'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'80px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Fecha se Registró Responsable',
			'value' => function($data)
			{
				require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

				$id = $data->id_solicitud_visitas_academicas;
				$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaRegistroResponsableVisitaAcademica($id);

				return $fecha_creac_sol[0]['fecha_act'].' a las '.$fecha_creac_sol[0]['hora'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'180px', 'class'=>'text-center')
		),
		array(
			'header' => 'Principal <br>Reponsable',
			'type' => 'raw',
			'value' => function($data)
			{
				return ($data->responsable_principal == 1) ? '<span style="font-size:16px" class="label label-success">SI</span>' : '<span style="font-size:16px" class="label label-danger">NO</span>';
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'12px', 'class'=>'text-center')
		)
	),
)); ?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Materias abarca la Visita Académica
		</span>
	</h2>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-materias_imparte-responsables-visita-academica-grid',
	'dataProvider'=>$modelVaMateriasImparteResponsableVisitaAcademica->searchMateriasImparteResponsableVisitaAcademica($id),
	'filter'=>$modelVaMateriasImparteResponsableVisitaAcademica,
	'columns'=>array(
		array(
			'class'=>'CButtonColumn',
			'template'=>'{logoMateria}',
			'header'=>'Materia',
			'htmlOptions'=>array('width:75px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'logoMateria' => array
				(
					'label'=>'Materia',
					//'url'=>'Yii::app()->createUrl("serviciosocial/ssHorarioDiasHabilesProgramas/editarHorariosProgramas", array("id_programa"=>$data->id_programa))',
					'imageUrl'=>'images/servicio_social/materia_32.png',
				),
			),
		),
		array(
			'name' => 'cveMateria',
			'type' => 'raw',
			'value' => function($data)
			{
				return '<span style="font-size:16px" class="label label-info">'.$data->cveMateria.'</span>';
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Nombre de la Materia',
			'value' => function($data)
			{
				$cve = $data->cveMateria;
				$qry_mat = "select * from public.\"E_catalogoMaterias\" where \"cveMateria\" = '$cve' ";
				$rs = Yii::app()->db->createCommand($qry_mat)->queryAll();

				return $rs[0]['dscMateria'];
			},
			'filter' => false,
			'htmlOptions' => array('width'=>'400px', 'class'=>'text-center')
		)
	),
));
?>

<br><br><br><br><br>

