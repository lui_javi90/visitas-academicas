<?php
/* @var $this VaSolicitudesVisitasAcademicasController */
/* @var $model VaSolicitudesVisitasAcademicas */

$this->breadcrumbs=array(
	'Visitas Industriales' => '?r=visitasacademicas',
	'Lista Solicitudes Visitas Industriales' => array('VaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
	'Nueva Solicitud Visita Académica'
);

?>

<br><br><br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Nueva Solicitud Visita Académica
		</span>
	</h2>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title">
					Nueva
				</h2>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formNuevaSolicitudVisitaAcademica', array('modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
																						//'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
																						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
																						'tipos_visitas' => $tipos_visitas,
																						'lista_empresas_visitas' => $lista_empresas_visitas,
																						'rfc_responsable' => $rfc_responsable,
																						'info_empleado' => $info_empleado,
																						'materias_asignadas_responsable' => $materias_asignadas_responsable,
																						'departamento' => $departamento,
																						'cargo' => $cargo
																			)); ?>
			</div>
		</div>
	</div>
</diV>