<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
        'Visitas Académicas' => '?r=visitasacademicas',
        'Consulta Solicitudes a Visitas Académicas Docentes' => array('vaSolicitudesVisitasAcademicas/listaJVSolicitudesVisitasAcademicas'),
		'Rechazar Solicitud a Visita Académica',
    );
    
?>

<br><br><br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <b>Detalle de Solicitud</b>
                </h6>
            </div>
            <div class="panel-body">
                <div style="vertical-align:50;" align="center" class="col-md-2">
                    <?php echo CHtml::image("items/getFoto.php?nctr_rfc=".$rfcEmpleado, '', array('class'=>'img-circle','style' =>"width:100px;height:100px;")); ?>
                </div>
                <div class="col-md-10">
                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('nombre_visita_academica')); ?>:</b>
                    <?php echo $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_salida_visita')); ?>:</b>
                    <?php echo $fecha_sal[0]['fecha_act'].' a las '.$fecha_sal[0]['hora']; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('fecha_hora_regreso_visita')); ?>:</b>
                    <?php echo $fecha_reg[0]['fecha_act'].' a las '.$fecha_reg[0]['hora']; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_empresa_visita')); ?>:</b>
                    <?php echo $empresa; ?></p>

                    <p><b><?php echo CHtml::encode($modelVaSolicitudesVisitasAcademicas->getAttributeLabel('id_tipo_visita_academica')); ?>:</b>
                    <?php echo $tipo_visita_acad; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            Rechazar Solicitud a Visita Académica
		</span>
	</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <b>Motivo Solicitud</b>
                </h6>
            </div>
            <div class="panel-body">

                <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'va-solicitudes-visitas-academicas-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                    <!--<b><p class="note">Campos con <span class="required">*</span> son requeridos.</p></b>-->

                    <?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
                        <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud', array('rows'=>10, 'class'=>'form-control')); ?>
                        <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton($modelVaSolicitudesVisitasAcademicas->isNewRecord ? 'Guardar Cambios' : 'Guadar Cambios', array('class'=>'btn btn-primary')); ?>
                        <?php echo CHtml::link('Cancelar', array('listaJVSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->

            </div>
        </div>
    </div>
</div>


<br><br><br><br>
<br><br><br><br>