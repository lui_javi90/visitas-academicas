<style>
    .hoja{
        background-color: white;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .right{
        text-align: right;
    }
    .left{
        text-align: left;
    }
    .letra1{
        font-size: 10px;
    }
    .letra2{
        font-size: 12px;
    }
    .div2{
        background-color: white;
        padding: -4px;
        padding-left: -1px;
    }
    table
    {
        border-collapse: collapse;
    }
    table, th, td {

        border: 1px solid black;
        bgcolor: #9E9E9E;
    }
    hr{
        border: none;
        height: 5px;
        padding: 0px;
        padding-left: 0px;
        color: #1B5E20;
    }
    .mytable{
      border: 1px solid black;
      border-right: 1px solid black;
      border-collapse: collapse;
      color: #000000;
      width: 100%;
      background-color: #FFFFFF;
    }
    .mytable-head {
      border: 1px solid black;
      color: #000000;
      width: 100%;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .mytable-body {
      border: 1px solid black;
      border-top: 0;
      margin-top: 0;
      padding-top: 0;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    .table2{
        border: 1px solid white;
        border-top: 1px solid white;
        border-right: 1px solid white;
        border-left: 1px solid white;
        border-bottom: 1px solid white;
        border-collapse: collapse;
    }
</style>

<div class="hoja">
    <img height="90px" align="center" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio_social/banner_ssocial.jpg" />

    <!--Encabezado del documento-->
    <div class="div2">
        <h6 class="bold center letra2">DEPARTAMENTO DE GESTIÓN TECNOLÓGICA Y VINCULACIÓN</h6>
        <h6 class="bold center letra2">REPORTE DE RESULTADOS E INCIDENTES VISITA ACADÉMICA</h6>
        <hr>
    </div>
    <!--Encabezado del documento-->

    <!--Fecha del Reporte-->
    <div align="right" class="div2">
        <span class="center bold letra2">FECHA:</span>
        <?php 
            //Fecha de visita o fecha en que se imprime el reporte de incidentes
            echo "<u>".$fec_actual."</u>";
        ?>
    </div>
    <!--Fecha del Reporte-->

    <!--Informacion Superior Reporte-->
    <br>
    <div class="div2">
        <table class="mytable" width="100%">
            <tr width="100%">
                <th class="bold center letra1" align="center" width="20%">
                    <span class="bold">Nombre del <br>docente <br>responsable:</span>
                </th>
                <th class="bold center letra1" align="center" width="20%">
                    <span class="bold">Carrera:</span>
                </th>
                <th class="bold center letra1" align="center" width="10%">
                    <span class="bold">Número de <br>Estudiantes <br>H-M:</span>
                </th>
                <th class="bold center letra1" align="center" width="20%">
                    <span class="bold">Fecha en que <br>realizó la <br>visita:</span>
                </th>
                <th class="bold center letra1" align="center" width="10%">
                    <span class="bold">Horario en que <br>se realizó la <br>visita:</span>
                </th>
                <th class="bold center letra1" align="center" width="20%">
                    <span class="bold">Nombre de la <br>Empresa:</span>
                </th>
            </tr>
            <tr>
                <td style="border: 1px solid black;" width="20%" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['docente_responsable']; ?> <!--Nombre del Responsable-->
                </td>
                <td style="border: 1px solid black;" width="20%" align="center" class="letra2">
                    <?php echo $lista_carreras; ?> <!--Carrera-->
                </td>
                <td style="border: 1px solid black;" width="10%" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['no_alumnos'] ?> <!--Numero de estudiantes-->
                </td>
                <td style="border: 1px solid black;" width="20%" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['fecha_salida_visita'].' - '.$datos_reporte_incidencias[0]['fecha_regreso_visita']; ?> <!--Fecha en que se realizo la visita-->
                </td>
                <td style="border: 1px solid black;" width="10%" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['horario_salida_visita'].' - '.$datos_reporte_incidencias[0]['horario_regreso_visita']; ?> <!--Horario en que realizo la visita-->
                </td>
                <td style="border: 1px solid black;" width="20%" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['nombre_empresa']; ?> <!--Nombre de la Empresa-->
                </td>
            </tr>
        </table>
    </div>
    <!--Informacion Superior Reporte-->

    <!--Informacion sobre la visita-->
    <br>
    <div class="div2">
        <table class="table2" width="100%" class="table">
            <tr width="100%">
                <td style="padding-left: 0px; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-top: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF;" width="8%">
                    <span>Materia:</span>
                </td>
                <td style="padding-left: 0px; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-top: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF;" width="70%">
                    <span class="bold"><u><?php echo $lista_mat_cubre; ?></u></span>
                </td>
                <td style="padding-left: 0px; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-top: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF;" width="10%">
                    <span>Semestre:</span>
                </td>
                <td style="padding-left: 0px; border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-top: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF;" width="12%">
                    <span class="bold"><u><?php echo $lista_semestre_cubre; ?></u></span>
                </td>
            </tr>
        </table>
    </div>

    <div class="div2">
        <br>
        <span>Unidades de la materia que se cubrieron con la visita académica:</span><br>
        <span class="bold"><u><?php echo $datos_reporte_incidencias[0]['unidades_materias_cubrieron']; ?></u></span>
        <br><br>
        <span>¿Se cumplieron con los objetivos de la visita académica? Explique:</span><br>
        <span class="bold"><u><?php echo $datos_reporte_incidencias[0]['cumplieron_objetivos']; ?></u></span>
        </p>
    </div>
    <br>
    <!--Informacion sobre la visita-->

    <!--Tabla de Incidentes-->
    <br>
    <div class="div2">
        <table class="mytable" width="100%">
            <tr height="10%" width="100%">
                <th class="left letra2" width="100%">
                    <span class="bold">Incidentes:</span>
                </th>
            </tr>
            <tr height="10%" width="100%">
                <td style="border: 1px solid black;" width="100%" height="150" align="center" class="letra2">
                    <?php echo $datos_reporte_incidencias[0]['descripcion_incidentes']; ?>
                </td>
            </tr>
        </table>
        <p class="letra2">
        &nbsp;&nbsp;&nbsp;<span class="bold">NOTA:</span> El informe deberá ser entregado como máximo 5 días hábiles posteriores a la realización de la <br>&nbsp;&nbsp;&nbsp;visita académica.
        </p>
    </div>
    <!--Tabla de Incidentes-->

    <!--Firma del docente Responsable-->
    <br><br>
    <div align="center" class="div2">
        <table align="center" class="mytable" width="20">
            <tr>
                <td style="border: 1px solid white; border-top: 1px solid white; border-bottom: 1px solid white; border-right: 1px solid black;" width="30%" height="100" align="center" class="letra2">
                    <?php 
                        //echo "sdsds"; 
                    ?>
                </td>
                <td style="border: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;" width="40%" height="100" align="center" class="letra2">
                    <?php 
                        if($fec_val_docente[0]['fecha_act'] != 'SV')
                        {
                            echo "<span class=\"bold letra2\">El docente responsable de nombre ".$datos_reporte_incidencias[0]['docente_responsable'].
                            " <br>validó la Solicitud el dia ".$fec_val_docente[0]['fecha_act'].' a las '.$fec_val_docente[0]['hora']."</span>";
                        }
                    ?>
                </td>
                <td style="border: 1px solid white; border-top: 1px solid white; border-bottom: 1px solid white;" width="30%" height="100" align="center" class="letra2">
                    <?php 
                        //echo "sdsds"; 
                    ?>
                </td>
            </tr>
            <tr>
                <td style="border: 1px solid white; border-top: 1px solid white; border-bottom: 1px solid white; border-right: 1px solid black;" width="30%" height="8" align="center" class="letra1">
                    
                </td>
                <td style="border: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;" width="40%" height="6" align="center" class="letra1">
                    FIRMA DEL DOCENTE RESPONSABLE
                </td>
                <td style="border: 1px solid white; border-left: 1px solid white; border-top: 1px solid white; border-bottom: 1px solid white;" width="30%" height="8" align="center" class="letra1">
                
                </td>
            </tr>
        </table>
    </div>
    <!--Firma del docente Responsable-->

    <!--Pie de pagina-->
    <br><br>
    <div class="div2">
        <span class="bold letra2">c.c.p. Depto. Académico.</span>
        <br>
        <span class="bold letra2">c.c.p. Archivo.</span>
    </div>
    <!--Pie de pagina-->

</div>