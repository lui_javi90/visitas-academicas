
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
            <b>Nombre: </b><?php echo $name_empresa; ?><br>
            <b>No. de Solicitud: </b><?php echo $num_solicitud; ?><br>
		</span>
	</h2>
</div>

<br>
<div class="form" id="mensaje_vinc">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'mensaje-vinc-form',
    'enableAjaxValidation'=>true,
)); 
?>

    <?php echo $form->errorSummary($modelVaSolicitudesVisitasAcademicas); ?>

    <?php if($modelVaSolicitudesVisitasAcademicas->isNewRecord){ ?>
        <div class="form-group">
            <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
            <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,
                                        'observaciones_solicitud',
                                        array('class' => 'form-control', 'rows'=>4)
                                    ); ?>
            <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
        </div>
    <?php }else{ ?>
        <div class="form-group">
            <?php echo $form->labelEx($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
            <?php echo $form->textArea($modelVaSolicitudesVisitasAcademicas,
                                    'observaciones_solicitud',
                                    array('class' => 'form-control', 'rows'=>4, 'value'=>"")
                                ); ?>
            <?php echo $form->error($modelVaSolicitudesVisitasAcademicas,'observaciones_solicitud'); ?>
        </div>

    <?php } ?>

    <div class="form-group">
        <?php echo CHtml::ajaxSubmitButton(Yii::t('','Guardar Cambios'),
                        CHtml::normalizeUrl(array('vaSolicitudesVisitasAcademicas/rechazarVSolicitudVisitaAcademica','render'=>false)),
                        array('success'=>'js: function(data) {
                        $("#VaSolicitudesVisitasAcademicas_observaciones_solicitud").append(data);
                        $("#mensaje_vinc").dialog("close");
                    }'),array('id'=>'closeInstitutionDialog'), array('class'=>'btn btn-primary')); ?>
    </div>

<?php $this->endWidget(); ?>

</div>