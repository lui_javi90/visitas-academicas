<?php
	/* @var $this VaSolicitudesVisitasAcademicasController */
	/* @var $model VaSolicitudesVisitasAcademicas */

	$this->breadcrumbs=array(
		'Visitas Académicas' => '?r=visitasacademicas',
		'Consulta Solicitudes a Visitas Académicas Docentes',
	);
	
	$JsLibSolicitud = 'js:function(__event)
	{
		__event.preventDefault(); // disable default action

		var $this = $(this), // link/button
			confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
			url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link

		if(confirm(confirm_message)) // Si se confirma la operacion entonces...
		{
			// perform AJAX request
			$("#va-jefevinc-solicitudes-visitas-academicas-grid").yiiGridView("update",
			{
				type	: "POST", // important! we only allow POST in filters()
				dataType: "json",
				url		: url,
				success	: function(data)
				{
					console.log("Success:", data);
					$("#va-jefevinc-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
				},
				error	: function(xhr)
				{
					console.log("Error:", xhr);
				}
			});
		}
	}';
    
?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Consulta Solicitudes a Visitas Académicas Docentes
		</span>
	</h2>
</div>

<br><br><br><br><br>
<br>
<div class="alert alert-warning">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Una vez en estatus COMPLETADA la Solicitud de Visita Académica pasará a la Lista de Completadas para su posterior pase a estatus de 
  FINALIZADA.</b>
  </strong></p>
</div>

<div class="alert alert-info">
  <p><strong>
  <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
  <b>Una vez regresan de la Visita, pasar la Solicitud de Visita Académica a estatus LIBERACIÓN para que se realice el Reporte de 
  Resultados e Incidencias.</b>
  </strong></p>
</div>

<?php if($is_jefe_serext == true){ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-jefevinc-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchListaSolicitudesVisitasAcademicasJefeVinculacion($cveDepto),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" 
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";
				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'-- Filtrar por --')
			),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Periodo <br>Escolar',
			'filter' => false,
			'value' => function($data)
			{
				$per = $data->periodo;
				$qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$per' ";
				$rs = Yii::app()->db->createCommand($qry_per)->queryAll();

				return ($rs[0]['dscPeriodo'] === null) ? '-' : $rs[0]['dscPeriodo'];
			},
			'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
		),
		array(
			'name' => 'anio',
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Alumnos <br>Asistirán',
			'name' => 'no_alumnos',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Departamento',
			'filter' => false,
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];
				//Ontenemos el depto catedratico del Empleado
				$qry_cat = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs_da = Yii::app()->db->createCommand($qry_cat)->queryAll();
				$depto_acad = $rs_da[0]['deptoCatedratico'];

				$qry_depto = "select * from public.\"H_empleados\" hemp
							  join public.\"H_departamentos\" dep
							  on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
							  where dep.\"cveDeptoAcad\" = '$depto_acad' ";
				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['dscDepartamento'];

			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por Depto --')
			),
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'class' => 'ComponentValidaSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'310px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarSolicitud}',
			'header'=>'Editar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarSolicitud' => array
				(
					'label'=>'Editar la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/agregar_32.png',
				),
			),
		),
		array(
			'class' => 'ComponentEstatusSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{messagSolicitud},{noShowMessageSolicitud}',
			'header'=>'Mensajes de la<br>Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'messagSolicitud' => array
				(
					'label'=>'Mensajes Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/messageJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					},
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#message_servext").dialog("open"); 
						
						return false;
					}',
				),
				'noShowMessageSolicitud' => array
				(
					'label'=>'Mensajes Solicitud Bloqueado',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/messageJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica > 1) ? true : false;
					},
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statFinSolicitud},{statCancSolicitud},{statPenSolicitud},{statAcepSolicitud},{statSuspSolicitud},{statCompSolicitud},{statLibSolicitud},{statEnCursSolicitud}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statFinSolicitud' => array
				(
					'label'=>'Solicitud Finalizada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 6) ? true : false;
					}
				),
				'statCancSolicitud' => array
				(
					'label'=>'Solicitud Cancelada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 4) ? true : false;
					}
				),
				'statPenSolicitud' => array
				(
					'label'=>'Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/pendiente_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					}
				),
				'statAcepSolicitud' => array
				(
					'label'=>'Solicitud Aceptada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 2) ? true : false;
					}
				),
				'statSuspSolicitud' => array
				(
					'label'=>'Solicitud Suspendida',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 3) ? true : false;
					}
				),
				'statCompSolicitud' => array
				(
					'label'=>'Solicitud Completada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 5) ? true : false;
					}
				),
				'statLibSolicitud' => array
				(
					'label'=>'Solicitud en Liberación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 7) ? true : false;
					}
				),
				'statEnCursSolicitud' => array
				(
					'label'=>'Solicitud en Curso',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 8) ? true : false;
					}
				)
			),
		),//fin
	),
)); ?>

<?php }else{ ?>

	<!--CUANDO EL QUE SE LOGEA NO ES JEFE DE OFICINA DE SERVICIOS EXTERNOS, NO PODRA EDITAR LAS SOLICITUDES-->
	<br><br><br><br><br>
	<div class="alert alert-danger">
		<p><strong>
			<span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
			No eres Jefe de la Oficina de Servicios Externos.
		</strong></p>
	</div>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-jefevinc-solicitudes-visitas-academicas-grid',
	'dataProvider'=>$modelVaSolicitudesVisitasAcademicas->searchCuandoNoEsSubdirectorAcademico(),
	'filter'=>$modelVaSolicitudesVisitasAcademicas,
	'columns'=>array(
		//'id_solicitud_visitas_academicas',
		array(
			'header' => 'No. <br>Solicitud',
			'name' => 'no_solicitud',
			'htmlOptions' => array('width'=>'7px', 'class'=>'text-center')
		),
		array(
			'header' => 'Responsable <br>Principal',
            'type'=>'raw',
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;
				$qry_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" hemp 
							on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" 
							where  rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";
				$rfc = Yii::app()->db->createCommand($qry_resp)->queryAll();

				return CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc[0]['rfcEmpleado']), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));
			},
			'htmlOptions' => array('width'=>'50px','class'=>'text-center')
		),
		array(
			'name' => 'nombre_visita_academica',
			'filter' => false,
			'htmlOptions' => array('width'=>'300px', 'class'=>'text-center')
		),
		array(
			'header' => 'Tipo Visita <br>Académica',
			'value' => function($data)
			{
				$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($data->id_tipo_visita_academica);

				if($modelVaTiposVisitasAcademicas === NULL)
					throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

				return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
							'id_tipo_visita_academica',
							CHtml::listData(
								VaTiposVisitasAcademicas::model()->findAllByAttributes(
									array('tipo_valido'=>true),array('order'=>'id_tipo_visita_academica ASC')
								),
								'id_tipo_visita_academica',
								'tipo_visita_academica'
							),
							array('prompt'=>'-- Filtrar por --')
			),
			'htmlOptions' => array('width'=>'120px', 'class'=>'text-center')
		),
		/*array(
			'header' => 'Periodo <br>Escolar',
			'filter' => false,
			'value' => function($data)
			{
				$per = $data->periodo;
				$qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$per' ";
				$rs = Yii::app()->db->createCommand($qry_per)->queryAll();

				return ($rs[0]['dscPeriodo'] === null) ? '-' : $rs[0]['dscPeriodo'];
			},
			'htmlOptions' => array('width'=>'70px', 'class'=>'text-center')
		),
		array(
			'name' => 'anio',
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),
		array(
			'header' => 'Alumnos <br>Asistirán',
			'name' => 'no_alumnos',
			'filter' => false,
			'htmlOptions' => array('width'=>'20px', 'class'=>'text-center')
		),*/
		array(
			'header' => 'Departamento',
			'filter' => false,
			'value' => function($data)
			{
				$id = $data->id_solicitud_visitas_academicas;

				$qry_sol_depto = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
								  join pe_vinculacion.va_responsables_visitas_academicas rva
								  on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  where rva.id_solicitud_visitas_academicas = '$id' AND rva.responsable_principal = true ";

				$rs = Yii::app()->db->createCommand($qry_sol_depto)->queryAll();

				//Obtenemos el depto del Responsable
				$rfc = $rs[0]['rfcEmpleado'];
				//Ontenemos el depto catedratico del Empleado
				$qry_cat = "select * from public.\"H_empleados\" where \"rfcEmpleado\" = '$rfc' ";
				$rs_da = Yii::app()->db->createCommand($qry_cat)->queryAll();
				$depto_acad = $rs_da[0]['deptoCatedratico'];

				$qry_depto = "select * from public.\"H_empleados\" hemp
							  join public.\"H_departamentos\" dep
							  on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
							  where dep.\"cveDeptoAcad\" = '$depto_acad' ";
				$rs_d = Yii::app()->db->createCommand($qry_depto)->queryAll();

				return $rs_d[0]['dscDepartamento'];

			},
			'filter' => CHtml::activeDropDownList($modelVaSolicitudesVisitasAcademicas,
											'cveDepto',
											$lista_deptos, //Lista deptos
											array('prompt'=>'-- Filtrar por Depto --')
			),
			'htmlOptions' => array('width'=>'200px', 'class'=>'text-center')
		),
		array(
			'class' => 'ComponentValidaSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'310px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{detSolicitud}',
			'header'=>'Detalle Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'detSolicitud' => array
				(
					'label'=>'Detalle de la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/detalle_32.png',
				),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{editarSolicitud}',
			'header'=>'Editar Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'editarSolicitud' => array
				(
					'label'=>'Editar la Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/editarJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/agregar_32.png',
				),
			),
		),
		array(
			'class' => 'ComponentEstatusSolicitudVisitaAcademica',
			'htmlOptions' => array('width'=>'280px', 'class'=>'text-center')
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{messagSolicitud},{noShowMessageSolicitud}',
			'header'=>'Mensajes de la<br>Solicitud',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'messagSolicitud' => array
				(
					'label'=>'Mensajes Solicitud',
					'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/messageJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/observacion_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					},
					'click' => 'function(){
						$("#parm-frame").attr("src",$(this).attr("href")); $("#message_servext").dialog("open"); 
						
						return false;
					}',
				),
				'noShowMessageSolicitud' => array
				(
					'label'=>'Mensajes Solicitud Bloqueado',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/messageJVSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/bloquedo_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica > 1) ? true : false;
					},
				)
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{statFinSolicitud},{statCancSolicitud},{statPenSolicitud},{statAcepSolicitud},{statSuspSolicitud},{statCompSolicitud},{statLibSolicitud},{statEnCursSolicitud}',
			'header'=>'Estatus',
			'htmlOptions'=>array('width:70px', 'class'=>'text-center'),
			'buttons'=>array
			(
				'statFinSolicitud' => array
				(
					'label'=>'Solicitud Finalizada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 6) ? true : false;
					}
				),
				'statCancSolicitud' => array
				(
					'label'=>'Solicitud Cancelada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/cancelado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 4) ? true : false;
					}
				),
				'statPenSolicitud' => array
				(
					'label'=>'Solicitud Pendiente',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/pendiente_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 1) ? true : false;
					}
				),
				'statAcepSolicitud' => array
				(
					'label'=>'Solicitud Aceptada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/aceptar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 2) ? true : false;
					}
				),
				'statSuspSolicitud' => array
				(
					'label'=>'Solicitud Suspendida',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/quitar_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 3) ? true : false;
					}
				),
				'statCompSolicitud' => array
				(
					'label'=>'Solicitud Completada',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/no_aprobado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 5) ? true : false;
					}
				),
				'statLibSolicitud' => array
				(
					'label'=>'Solicitud en Liberación',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/fecha_asignada_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 7) ? true : false;
					}
				),
				'statEnCursSolicitud' => array
				(
					'label'=>'Solicitud en Curso',
					//'url'=>'Yii::app()->createUrl("visitasacademicas/vaSolicitudesVisitasAcademicas/detalleSHistoricoSolicitudVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
					'imageUrl'=>'images/servicio_social/serv_finalizado_32.png',
					'visible' => function($row, $data)
					{
						return ($data->id_estatus_solicitud_visita_academica == 8) ? true : false;
					}
				)
			),
		),//fin
	),
)); ?>

<?php } ?>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id' => 'message_servext',
	'options' => array(
		'title' => 'Mensajes Solicitud',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    	'resizable'=>false,
		'width' => 1200, 
		'height' => 700,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

));?>

	<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>
	
<?php
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<br><br><br><br><br>
<br><br><br><br><br>