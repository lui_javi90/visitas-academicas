<?php
/* @var $this VaReporteResultadosIncidenciasVisitasAcademicasController */
/* @var $model VaReporteResultadosIncidenciasVisitasAcademicas */

$this->breadcrumbs=array(
	'Va Reporte Resultados Incidencias Visitas Academicases'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List VaReporteResultadosIncidenciasVisitasAcademicas', 'url'=>array('index')),
	array('label'=>'Create VaReporteResultadosIncidenciasVisitasAcademicas', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#va-reporte-resultados-incidencias-visitas-academicas-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Va Reporte Resultados Incidencias Visitas Academicases</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'va-reporte-resultados-incidencias-visitas-academicas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_reporte_resultados_incidencias',
		'fecha_creacion_reporte',
		'unidades_materias_cubrieron',
		'cumplieron_objetivos',
		'descripcion_incidentes',
		'valida_docente_reponsable',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
