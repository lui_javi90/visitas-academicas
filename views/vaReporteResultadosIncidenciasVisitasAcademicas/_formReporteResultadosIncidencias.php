<?php
/* @var $this VaReporteResultadosIncidenciasVisitasAcademicasController */
/* @var $modelVaReporteResultadosIncidenciasVisitasAcademicas VaReporteResultadosIncidenciasVisitasAcademicas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'va-reporte-resultados-incidencias-visitas-academicas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<b><p class="note">Campos <span class="required">*</span> son requeridos.</p></b>

	<br>
	<?php echo $form->errorSummary($modelVaReporteResultadosIncidenciasVisitasAcademicas); ?>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'id_reporte_resultados_incidencias'); ?>
		<?php //echo $form->textField($modelVaReporteResultadosIncidenciasVisitasAcademicas,'id_reporte_resultados_incidencias'); ?>
		<?php //echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'id_reporte_resultados_incidencias'); ?>
	</div>

	<div class="form-group">
		<?php //echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'fecha_creacion_reporte'); ?>
		<?php //echo $form->textField($modelVaReporteResultadosIncidenciasVisitasAcademicas,'fecha_creacion_reporte'); ?>
		<?php //echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'fecha_creacion_reporte'); ?>
	</div>-->

	<?php if($modelVaReporteResultadosIncidenciasVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'unidades_materias_cubrieron'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'unidades_materias_cubrieron', array('class'=>'form-control', 'rows'=>2, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'unidades_materias_cubrieron'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'unidades_materias_cubrieron'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'unidades_materias_cubrieron', array('class'=>'form-control', 'rows'=>2, 'readOnly'=>true)); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'unidades_materias_cubrieron'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaReporteResultadosIncidenciasVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'cumplieron_objetivos'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'cumplieron_objetivos', array('class'=>'form-control', 'rows'=>2, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'cumplieron_objetivos'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'cumplieron_objetivos'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'cumplieron_objetivos', array('class'=>'form-control', 'rows'=>2, 'readOnly'=>true)); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'cumplieron_objetivos'); ?>
	</div>
	<?php } ?>

	<?php if($modelVaReporteResultadosIncidenciasVisitasAcademicas->isNewRecord){ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'descripcion_incidentes'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'descripcion_incidentes', array('class'=>'form-control', 'rows'=>7, 'required'=>'required')); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'descripcion_incidentes'); ?>
	</div>
	<?php }else{ ?>
	<div class="form-group">
		<?php echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'descripcion_incidentes'); ?>
		<?php echo $form->textArea($modelVaReporteResultadosIncidenciasVisitasAcademicas,
									'descripcion_incidentes', array('class'=>'form-control', 'rows'=>7, 'readOnly'=>true)); ?>
		<?php echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'descripcion_incidentes'); ?>
	</div>
	<?php } ?>

	<!--<div class="form-group">
		<?php //echo $form->labelEx($modelVaReporteResultadosIncidenciasVisitasAcademicas,'valida_docente_reponsable'); ?>
		<?php //echo $form->textField($modelVaReporteResultadosIncidenciasVisitasAcademicas,'valida_docente_reponsable'); ?>
		<?php //echo $form->error($modelVaReporteResultadosIncidenciasVisitasAcademicas,'valida_docente_reponsable'); ?>
	</div>-->

	<div class="form-group">
		<?php
			if($modelVaReporteResultadosIncidenciasVisitasAcademicas->isNewRecord)
				echo CHtml::submitButton($modelVaReporteResultadosIncidenciasVisitasAcademicas->isNewRecord ? 'Guardar' : 'Guardar Cambios', array('class'=>'btn btn-primary')); 
		?>
		<?php echo CHtml::link('Cancelar', array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'), array('class'=>'btn btn-danger')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->