<?php
/* @var $this VaReporteResultadosIncidenciasVisitasAcademicasController */
/* @var $model VaReporteResultadosIncidenciasVisitasAcademicas */

$this->breadcrumbs=array(
	'Visitas Académicas' => '?r=visitasacademicas',
	'Mis Solicitudes a Visitas Académicas' => array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'),
	'Editar Reporte de Resultados e Incidencias de Visita Académica'
);

?>

<br>
<div class="row">
 	<h2 class="subTitulo" align="center">
		<span class="subTitulo_inside">
			Editar Reporte de Resultados e Incidencias de Visita Académica 
		</span>
	</h2>
</div>

<br><br><br>
<div class="alert alert-info">
    <p><strong>
        <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;
		Una vez realizado y guardado el formulario de Reporte de Resultados e Incidencias se validará el Reporte.
    </strong></p>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<b>Editar</b>
				</h3>
			</div>
			<div class="panel-body">
				<?php $this->renderPartial('_formReporteResultadosIncidencias', array('modelVaReporteResultadosIncidenciasVisitasAcademicas'=>$modelVaReporteResultadosIncidenciasVisitasAcademicas)); ?>
			</div>
		</div>
	</div>
</div>