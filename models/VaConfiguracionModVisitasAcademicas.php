<?php

/**
 * This is the model class for table "pe_vinculacion.va_configuracion_mod_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_configuracion_mod_visitas_academicas':
 * @property integer $id_configuracion_mod_visitas_academicas
 * @property integer $max_numero_solicitudes_vigentes_por_docente
 * @property string $texto_legenda_inicio_reportes_pdf
 * @property integer $max_numero_alumnos_por_visita_academica
 * @property string $fecha_ultima_modificacion
 */
class VaConfiguracionModVisitasAcademicas extends CActiveRecord
{
	
	public function tableName()
	{
		return 'pe_vinculacion.va_configuracion_mod_visitas_academicas';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('max_numero_solicitudes_vigentes_por_docente, max_numero_alumnos_por_visita_academica', 'numerical', 'integerOnly'=>true),
			array('texto_legenda_inicio_reportes_pdf', 'length', 'max'=>200),
			array('fecha_ultima_modificacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_configuracion_mod_visitas_academicas, max_numero_solicitudes_vigentes_por_docente, texto_legenda_inicio_reportes_pdf, max_numero_alumnos_por_visita_academica, fecha_ultima_modificacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_configuracion_mod_visitas_academicas' => 'Id Configuracion Mod Visitas Academicas',
			'max_numero_solicitudes_vigentes_por_docente' => 'Maximo Número de Solicitudes Vigentes Por Docente',
			'texto_legenda_inicio_reportes_pdf' => 'Texto Legenda Inicio Reportes de PDF',
			'max_numero_alumnos_por_visita_academica' => 'Maximo Número de Alumnos Por Visita Academica',
			'fecha_ultima_modificacion' => 'Fecha de Última Modificación',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_configuracion_mod_visitas_academicas',$this->id_configuracion_mod_visitas_academicas);
		$criteria->compare('max_numero_solicitudes_vigentes_por_docente',$this->max_numero_solicitudes_vigentes_por_docente);
		$criteria->compare('texto_legenda_inicio_reportes_pdf',$this->texto_legenda_inicio_reportes_pdf,true);
		$criteria->compare('max_numero_alumnos_por_visita_academica',$this->max_numero_alumnos_por_visita_academica);
		$criteria->compare('fecha_ultima_modificacion',$this->fecha_ultima_modificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10

			)
		));
	}

	public function searchBusqueda()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_configuracion_mod_visitas_academicas',$this->id_configuracion_mod_visitas_academicas);
		$criteria->compare('max_numero_solicitudes_vigentes_por_docente',$this->max_numero_solicitudes_vigentes_por_docente);
		$criteria->compare('texto_legenda_inicio_reportes_pdf',$this->texto_legenda_inicio_reportes_pdf,true);
		$criteria->compare('max_numero_alumnos_por_visita_academica',$this->max_numero_alumnos_por_visita_academica);
		$criteria->compare('fecha_ultima_modificacion',$this->fecha_ultima_modificacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
