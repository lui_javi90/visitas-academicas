<?php

/**
 * This is the model class for table "pe_vinculacion.va_materias_imparte_responsable_visita_academica".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_materias_imparte_responsable_visita_academica':
 * @property integer $id_solicitud_materias_responsable_visitas_academicas
 * @property string $cveMateria
 * @property string $fecha_registro_materia_visita_academica
 */
class VaMateriasImparteResponsableVisitaAcademica extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_materias_imparte_responsable_visita_academica';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_solicitud_materias_responsable_visitas_academicas, cveMateria', 'required'),
			array('id_solicitud_materias_responsable_visitas_academicas', 'numerical', 'integerOnly'=>true),
			array('cveMateria', 'length', 'max'=>4),
			array('fecha_registro_materia_visita_academica', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_solicitud_materias_responsable_visitas_academicas, cveMateria, fecha_registro_materia_visita_academica', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_solicitud_materias_responsable_visitas_academicas' => 'Id Solicitud Materias Responsable Visitas Academicas',
			'cveMateria' => 'Clave de Materia',
			'fecha_registro_materia_visita_academica' => 'Fecha Registró Materia',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('"id_solicitud_materias_responsable_visitas_academicas"',$this->id_solicitud_materias_responsable_visitas_academicas);
		$criteria->compare('"cveMateria"',$this->cveMateria,true);
		$criteria->compare('"fecha_registro_materia_visita_academica"',$this->fecha_registro_materia_visita_academica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	public function searchMateriasImparteResponsableVisitaAcademica($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->condition = "id_solicitud_materias_responsable_visitas_academicas = '$id' ";
		$criteria->order = " id_solicitud_materias_responsable_visitas_academicas ASC ";

		$criteria->compare('"id_solicitud_materias_responsable_visitas_academicas"',$this->id_solicitud_materias_responsable_visitas_academicas);
		$criteria->compare('"cveMateria"',$this->cveMateria,true);
		$criteria->compare('"fecha_registro_materia_visita_academica"',$this->fecha_registro_materia_visita_academica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	public function searchMateriasXSolicitudVisitaAcademica($id_solicitud_materias_responsable_visitas_academicas)
	{

		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->condition = "id_solicitud_materias_responsable_visitas_academicas = '$id_solicitud_materias_responsable_visitas_academicas' ";
		$criteria->order = "id_solicitud_materias_responsable_visitas_academicas ASC";

		$criteria->compare('"id_solicitud_materias_responsable_visitas_academicas"',$this->id_solicitud_materias_responsable_visitas_academicas);
		$criteria->compare('"cveMateria"',$this->cveMateria,true);
		$criteria->compare('"fecha_registro_materia_visita_academica"',$this->fecha_registro_materia_visita_academica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
