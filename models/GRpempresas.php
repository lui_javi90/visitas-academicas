<?php

/**
 * This is the model class for table "pe_vinculacion.g_rpempresas".
 *
 * The followings are the available columns in table 'pe_vinculacion.g_rpempresas':
 * @property integer $idempresa
 * @property string $nombre
 * @property string $rfc
 * @property integer $idsector
 * @property string $domicilio
 * @property string $colonia
 * @property string $codigopostal
 * @property string $idestado
 * @property string $idmunicipio
 * @property string $email
 * @property string $telefono
 * @property string $nombre_presenta
 * @property string $cargo_presenta
 *
 * The followings are the available model relations:
 * @property VaSolicitudesVisitasAcademicas[] $vaSolicitudesVisitasAcademicases
 * @property GRpsectorempresa $idsector
 * @property XEstados $idestado
 * @property XMunicipios $idmunicipio
 * @property GRpsolicitudEmpresa[] $gRpsolicitudEmpresas
 * @property GRpsolicitud[] $gRpsolicituds
 */
class GRpempresas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.g_rpempresas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, idsector, idestado, idmunicipio', 'required'),
			array('idsector', 'numerical', 'integerOnly'=>true),
			array('rfc', 'length', 'max'=>13),
			array('codigopostal', 'length', 'max'=>6),
			array('idestado', 'length', 'max'=>2),
			array('idmunicipio', 'length', 'max'=>5),
			array('telefono', 'length', 'max'=>250),
			array('nombre_presenta, cargo_presenta', 'length', 'max'=>120),
			array('domicilio, colonia, email', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idempresa, nombre, rfc, idsector, domicilio, colonia, codigopostal, idestado, idmunicipio, email, telefono, nombre_presenta, cargo_presenta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vaSolicitudesVisitasAcademicases' => array(self::HAS_MANY, 'VaSolicitudesVisitasAcademicas', 'id_empresa_visita'),
			'idsector' => array(self::BELONGS_TO, 'GRpsectorempresa', 'idsector'),
			'idestado' => array(self::BELONGS_TO, 'XEstados', 'idestado'),
			'idmunicipio' => array(self::BELONGS_TO, 'XMunicipios', 'idmunicipio'),
			'gRpsolicitudEmpresas' => array(self::HAS_MANY, 'GRpsolicitudEmpresa', 'idempresa'),
			'gRpsolicituds' => array(self::HAS_MANY, 'GRpsolicitud', 'idempresa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idempresa' => 'Idempresa',
			'nombre' => 'Nombre',
			'rfc' => 'RFC de la Empresa',
			'idsector' => 'Sector',
			'domicilio' => 'Domicilio',
			'colonia' => 'Colonia',
			'codigopostal' => 'Codigo Postal',
			'idestado' => 'Estado',
			'idmunicipio' => 'Municipio',
			'email' => 'Email',
			'telefono' => 'Telefono',
			'nombre_presenta' => 'Nombre del Encargado',
			'cargo_presenta' => 'Cargo',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idempresa',$this->idempresa);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('rfc',$this->rfc,true);
		$criteria->compare('idsector',$this->idsector);
		$criteria->compare('domicilio',$this->domicilio,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('codigopostal',$this->codigopostal,true);
		$criteria->compare('idestado',$this->idestado,true);
		$criteria->compare('idmunicipio',$this->idmunicipio,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('nombre_presenta',$this->nombre_presenta,true);
		$criteria->compare('cargo_presenta',$this->cargo_presenta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}

	public function searchEmpresasVisitaAcademica()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->order = "idempresa ASC";

		$criteria->compare('idempresa',$this->idempresa);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('rfc',$this->rfc,true);
		$criteria->compare('idsector',$this->idsector);
		$criteria->compare('domicilio',$this->domicilio,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('codigopostal',$this->codigopostal,true);
		$criteria->compare('idestado',$this->idestado,true);
		$criteria->compare('idmunicipio',$this->idmunicipio,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('nombre_presenta',$this->nombre_presenta,true);
		$criteria->compare('cargo_presenta',$this->cargo_presenta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
