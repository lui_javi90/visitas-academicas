<?php

/**
 * This is the model class for table "pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion':
 * @property integer $id_bitacora_solicitud_visita_academica
 * @property integer $id_solicitud_visitas_academicas
 * @property string $fecha_hora_salida
 * @property string $fecha_hora_regreso
 * @property integer $no_alumnos_visita
 * @property string $fecha_ultima_modificacion_solicitud
 * @property string $usuario_modifica
 *
 * The followings are the available model relations:
 * @property VaSolicitudesVisitasAcademicas $idSolicitudVisitasAcademicas
 */
class VaBitacoraCambiosSolicitudVinculacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_bitacora_cambios_solicitud_vinculacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_solicitud_visitas_academicas, usuario_modifica', 'required'),
			array('id_solicitud_visitas_academicas, no_alumnos_visita', 'numerical', 'integerOnly'=>true),
			array('usuario_modifica', 'length', 'max'=>13),
			array('fecha_hora_salida, fecha_hora_regreso, fecha_ultima_modificacion_solicitud', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_bitacora_solicitud_visita_academica, id_solicitud_visitas_academicas, fecha_hora_salida, fecha_hora_regreso, no_alumnos_visita, fecha_ultima_modificacion_solicitud, usuario_modifica', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idSolicitudVisitasAcademicas' => array(self::BELONGS_TO, 'VaSolicitudesVisitasAcademicas', 'id_solicitud_visitas_academicas'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_bitacora_solicitud_visita_academica' => 'Id Bitacora Solicitud Visita Academica',
			'id_solicitud_visitas_academicas' => 'Id Solicitud Visitas Academicas',
			'fecha_hora_salida' => 'Fecha y Hora Salida',
			'fecha_hora_regreso' => 'Fecha y Hora Regreso',
			'no_alumnos_visita' => 'No. de Alumnos',
			'fecha_ultima_modificacion_solicitud' => 'Fecha de Modificación',
			'usuario_modifica' => 'Usuario Modificó',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_bitacora_solicitud_visita_academica',$this->id_bitacora_solicitud_visita_academica);
		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('fecha_hora_salida',$this->fecha_hora_salida,true);
		$criteria->compare('fecha_hora_regreso',$this->fecha_hora_regreso,true);
		$criteria->compare('no_alumnos_visita',$this->no_alumnos_visita);
		$criteria->compare('fecha_ultima_modificacion_solicitud',$this->fecha_ultima_modificacion_solicitud,true);
		$criteria->compare('usuario_modifica',$this->usuario_modifica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 20
			)
		));
	}

	//Bitacora por cada Solicitud de Visita Academica
	public function searchBitacoraXSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
		$criteria->order = "id_solicitud_visitas_academicas ASC";

		$criteria->compare('id_bitacora_solicitud_visita_academica',$this->id_bitacora_solicitud_visita_academica);
		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('fecha_hora_salida',$this->fecha_hora_salida,true);
		$criteria->compare('fecha_hora_regreso',$this->fecha_hora_regreso,true);
		$criteria->compare('no_alumnos_visita',$this->no_alumnos_visita);
		$criteria->compare('fecha_ultima_modificacion_solicitud',$this->fecha_ultima_modificacion_solicitud,true);
		$criteria->compare('usuario_modifica',$this->usuario_modifica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 20
			)
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
