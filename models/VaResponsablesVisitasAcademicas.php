<?php

/**
 * This is the model class for table "pe_vinculacion.va_responsables_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_responsables_visitas_academicas':
 * @property integer $id_solicitud_visitas_academicas
 * @property string $rfcEmpleado
 * @property string $fecha_registro_responsable
 * @property boolean $responsable_principal
 * @property integer $cve_depto_acad
 * @property integer $cve_depto
 */
class VaResponsablesVisitasAcademicas extends CActiveRecord
{
    public $cve_depto;

    public function tableName()
    {
        return 'pe_vinculacion.va_responsables_visitas_academicas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_solicitud_visitas_academicas, rfcEmpleado, cve_depto_acad', 'required'),
            array('id_solicitud_visitas_academicas, cve_depto_acad, cve_depto', 'numerical', 'integerOnly'=>true),
            array('rfcEmpleado', 'length', 'max'=>13),
            array('fecha_registro_responsable, responsable_principal', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_solicitud_visitas_academicas, rfcEmpleado, fecha_registro_responsable, responsable_principal, cve_depto_acad, cve_depto, cve_depto', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_solicitud_visitas_academicas' => 'Id Solicitud Visitas Academicas',
            'rfcEmpleado' => 'RFC Empleado',
            'fecha_registro_responsable' => 'Fecha Registro de Responsable',
            'responsable_principal' => 'Es Responsable Principal',
            'cve_depto_acad' => 'Cve Depto Acad',
            'cve_depto' => 'Cve Depto',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
        $criteria->compare('"rfcEmpleado"',$this->rfcEmpleado,true);
        $criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('cve_depto_acad',$this->cve_depto_acad);
        $criteria->compare('cve_depto',$this->cve_depto);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
				'pageSize' => 10
			)
        ));
	}
	
	public function searchListaResponsablesSolicitudVisitaAcademica($id)
	{
        // @todo Please modify the following code to remove attributes that should not be searched.
        //echo $id;
        //die;

        $criteria = new CDbCriteria;
        /*$criteria->alias = "rva";
        $criteria->select = "*";
        $criteria->join = "join public.\"H_empleados\" hemp
                            on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";*/
        $criteria->condition = " id_solicitud_visitas_academicas = '$id' ";
        //$criteria->order = "id_solicitud_visitas_academicas ASC";

        /*
        select * from pe_vinculacion.va_responsables_visitas_academicas rva
        join pe_vinculacion.va_solicitudes_visitas_academicas sva
        on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
        where rva.id_solicitud_visitas_academicas = 11
        */

		$criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
		$criteria->compare('"rfcEmpleado"',$this->rfcEmpleado, true);
		$criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('"cve_depto_acad"',$this->cve_depto_acad);
        $criteria->compare('"cve_depto"',$this->cve_depto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
    }

    public function searchXResponsablePrincipal($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = '$id' and responsable_principal = true ";

        $criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
		$criteria->compare('"rfcEmpleado"',$this->rfcEmpleado, true);
		$criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('"cve_depto_acad"',$this->cve_depto_acad);
        $criteria->compare('"cve_depto"',$this->cve_depto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
    }
    
    public function searchListaSolicitudesValidadasXResponsablePrincipal()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "rva";
        $criteria->select = "*";
        $criteria->join = "join pe_vinculacion.va_solicitudes_visitas_academicas sva
                            on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas";
        $criteria->condition = "sva.valida_jefe_depto_academico != null AND sva.valida_subdirector_academico != NULL AND
                                    rva.responsable_principal = true ";
        $criteria->order = "rva.id_solicitud_visitas_academicas ASC ";
        /*select * from pe_vinculacion.va_responsables_visitas_academicas rva
        join pe_vinculacion.va_solicitudes_visitas_academicas sva
        on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
        where sva.valida_jefe_depto_academico != null AND sva.valida_subdirector_academico != NULL AND
        rva.responsable_principal = true;
        */

		$criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
		$criteria->compare('"rfcEmpleado"',$this->rfcEmpleado,true);
		$criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('cve_depto_acad',$this->cve_depto_acad);
        $criteria->compare('cve_depto',$this->cve_depto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
    }

    public function searchXSupervisorPrincipal()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "rva";
        $criteria->select = "*";
        $criteria->join = " join pe_vinculacion.va_solicitudes_visitas_academicas sva
                            on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas ";
        $criteria->condition = "rva.responsable_principal = true AND sva.id_estatus_solicitud_visita_academica = 2 ";
        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

        /*select * from pe_vinculacion.va_responsables_visitas_academicas rva
        join pe_vinculacion.va_solicitudes_visitas_academicas sva
        on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
        where rva.responsable_principal = true AND sva.id_estatus_solicitud_visita_academica = 2;*/

        $criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
        $criteria->compare('"rfcEmpleado"',$this->rfcEmpleado,true);
        $criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('"cve_depto_acad"',$this->cve_depto_acad);
        $criteria->compare('"cve_depto"',$this->cve_depto);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
				'pageSize' => 50
			)
        ));
	}

    public function searchXJefeDeptoOProyecto($depto_academico, $rfcEmpleado)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "rva";
        $criteria->select = "*";
        $criteria->join = "join pe_vinculacion.va_solicitudes_visitas_academicas sva
                        on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
                        join public.\"H_empleados\" hemp
                        on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
                        join public.\"H_departamentos\" dep
                        on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\" ";
        $criteria->condition = " rva.cve_depto_acad = '$depto_academico' AND sva.id_estatus_solicitud_visita_academica = 2";
        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";
        /*dep.\"cveDeptoAcad\" = '$depto_academico' AND hemp.\"cvePuestoGeneral\" = '03' AND
                                 hemp.\"rfcEmpleado\" = '$rfcEmpleado' AND*/
                                 
        /*select sva.id_solicitud_visitas_academicas 
        from pe_vinculacion.va_responsables_visitas_academicas rva
        join pe_vinculacion.va_solicitudes_visitas_academicas sva
        on sva.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas
        join public."H_empleados" hemp
        on hemp."rfcEmpleado" = rva."rfcEmpleado"
        join public."H_departamentos" dep
        on dep."cveDepartamento" = hemp."cveDepartamentoEmp"
        where dep."cveDeptoAcad" = 8 AND hemp."cvePuestoGeneral" = '03' AND
        hemp."rfcEmpleado" = 'HEMJ570525RQ2' AND sva.id_estatus_solicitud_visita_academica = 2
        */

        $criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
        $criteria->compare('"rfcEmpleado"',$this->rfcEmpleado,true);
        $criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('"cve_depto_acad"',$this->cve_depto_acad);
        $criteria->compare('"cve_depto"',$this->cve_depto);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
        ));

    }

    public function searchNoSolicitudesXJefe()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = 0 ";

        $criteria->compare('"id_solicitud_visitas_academicas"',$this->id_solicitud_visitas_academicas);
        $criteria->compare('"rfcEmpleado"',$this->rfcEmpleado,true);
        $criteria->compare('"fecha_registro_responsable"',$this->fecha_registro_responsable,true);
        $criteria->compare('"responsable_principal"',$this->responsable_principal);
        $criteria->compare('"cve_depto_acad"',$this->cve_depto_acad);
        $criteria->compare('"cve_depto"',$this->cve_depto);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
        ));

    }

    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}