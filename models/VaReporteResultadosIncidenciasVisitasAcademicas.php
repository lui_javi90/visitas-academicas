<?php

/**
 * This is the model class for table "pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas':
 * @property integer $id_reporte_resultados_incidencias
 * @property string $fecha_creacion_reporte
 * @property string $unidades_materias_cubrieron
 * @property string $cumplieron_objetivos
 * @property string $descripcion_incidentes
 * @property string $valida_docente_reponsable
 *
 * The followings are the available model relations:
 * @property VaSolicitudesVisitasAcademicas $idReporteResultadosIncidencias
 */
class VaReporteResultadosIncidenciasVisitasAcademicas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_reporte_resultados_incidencias', 'required'),
			array('id_reporte_resultados_incidencias', 'numerical', 'integerOnly'=>true),
			array('fecha_creacion_reporte, unidades_materias_cubrieron, cumplieron_objetivos, descripcion_incidentes, valida_docente_reponsable', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_reporte_resultados_incidencias, fecha_creacion_reporte, unidades_materias_cubrieron, cumplieron_objetivos, descripcion_incidentes, valida_docente_reponsable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idReporteResultadosIncidencias' => array(self::BELONGS_TO, 'VaSolicitudesVisitasAcademicas', 'id_reporte_resultados_incidencias'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_reporte_resultados_incidencias' => 'Id Reporte Resultados Incidencias',
			'fecha_creacion_reporte' => 'Fecha Creacion Reporte',
			'unidades_materias_cubrieron' => 'Unidades de la materia que se cubrieron con la visita académica:',
			'cumplieron_objetivos' => '¿Se cumplieron con los objetivos de la visita académica? Explique:',
			'descripcion_incidentes' => 'Incidentes:',
			'valida_docente_reponsable' => 'Valida Docente Reponsable',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_reporte_resultados_incidencias',$this->id_reporte_resultados_incidencias);
		$criteria->compare('fecha_creacion_reporte',$this->fecha_creacion_reporte,true);
		$criteria->compare('unidades_materias_cubrieron',$this->unidades_materias_cubrieron,true);
		$criteria->compare('cumplieron_objetivos',$this->cumplieron_objetivos,true);
		$criteria->compare('descripcion_incidentes',$this->descripcion_incidentes,true);
		$criteria->compare('valida_docente_reponsable',$this->valida_docente_reponsable,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
