<?php

/**
 * This is the model class for table "pe_vinculacion.va_alumnos_asisten_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_alumnos_asisten_visitas_academicas':
 * @property integer $id_solicitud_visitas_academicas
 * @property string $nctrAlumno
 * @property string $fecha_registro_alumno
 */
class VaAlumnosAsistenVisitasAcademicas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_alumnos_asisten_visitas_academicas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_solicitud_visitas_academicas, nctrAlumno', 'required'),
			array('id_solicitud_visitas_academicas', 'numerical', 'integerOnly'=>true),
			array('nctrAlumno', 'length', 'max'=>9),
			array('fecha_registro_alumno', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_solicitud_visitas_academicas, nctrAlumno, fecha_registro_alumno', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_solicitud_visitas_academicas' => 'Id Solicitud Visitas Academicas',
			'nctrAlumno' => 'No. Control Alumno',
			'fecha_registro_alumno' => 'Fecha Registro del Alumno',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nctrAlumno',$this->nctrAlumno,true);
		$criteria->compare('fecha_registro_alumno',$this->fecha_registro_alumno,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}

	public function searchFiltroXSolicitudDeVisitaAcademica($rfcEmpleado)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
		$criteria->order = " id_solicitud_visitas_academicas ASC";

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nctrAlumno',$this->nctrAlumno,true);
		$criteria->compare('fecha_registro_alumno',$this->fecha_registro_alumno,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}

	public function searchXAlumnosInscritosVisita($id_solicitud_visitas_academicas)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
		$criteria->order = " id_solicitud_visitas_academicas ASC";

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nctrAlumno',$this->nctrAlumno,true);
		$criteria->compare('fecha_registro_alumno',$this->fecha_registro_alumno,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 25
			)
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
