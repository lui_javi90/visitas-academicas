<?php

/**
 * This is the model class for table "pe_vinculacion.va_codigos_calidad_mod_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_codigos_calidad_mod_visitas_academicas':
 * @property integer $id_codigo_calidad_mod_va
 * @property string $nombre_documento_digital
 * @property string $codigo_calidad
 * @property string $revision
 */
class VaCodigosCalidadModVisitasAcademicas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_codigos_calidad_mod_visitas_academicas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_documento_digital', 'length', 'max'=>120),
			array('codigo_calidad', 'length', 'max'=>25),
			array('revision', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_codigo_calidad_mod_va, nombre_documento_digital, codigo_calidad, revision', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_codigo_calidad_mod_va' => 'Id Codigo Calidad Mod Va',
			'nombre_documento_digital' => 'Nombre del Documento Digital',
			'codigo_calidad' => 'Codigo de Calidad',
			'revision' => 'Revisión',
		);
	}


	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_codigo_calidad_mod_va',$this->id_codigo_calidad_mod_va);
		$criteria->compare('nombre_documento_digital',$this->nombre_documento_digital,true);
		$criteria->compare('codigo_calidad',$this->codigo_calidad,true);
		$criteria->compare('revision',$this->revision,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	public function searchListaCodigosCalidad()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->order = "id_codigo_calidad_mod_va ASC";

		$criteria->compare('id_codigo_calidad_mod_va',$this->id_codigo_calidad_mod_va);
		$criteria->compare('nombre_documento_digital',$this->nombre_documento_digital,true);
		$criteria->compare('codigo_calidad',$this->codigo_calidad,true);
		$criteria->compare('revision',$this->revision,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 10
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VaCodigosCalidadModVisitasAcademicas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
