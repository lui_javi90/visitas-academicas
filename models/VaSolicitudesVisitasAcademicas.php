<?php

/**
 * This is the model class for table "pe_vinculacion.va_solicitudes_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_solicitudes_visitas_academicas':
 * @property integer $id_solicitud_visitas_academicas
 * @property string $nombre_visita_academica
 * @property string $periodo
 * @property integer $anio
 * @property integer $no_solicitud
 * @property string $fecha_creacion_solicitud
 * @property string $fecha_hora_salida_visita
 * @property string $fecha_hora_regreso_visita
 * @property integer $id_tipo_visita_academica
 * @property integer $id_empresa_visita
 * @property string $area_a_visitar
 * @property string $objetivo_visitar_area
 * @property integer $no_alumnos
 * @property string $observaciones_solicitud
 * @property string $valida_jefe_depto_academico
 * @property string $valida_subdirector_academico
 * @property string $ultima_fecha_modificacion
 * @property string $doc_oficio_confirmacion_empresa
 * @property string $ultima_mod_oficio_conf_empresa
 * @property string $path_carpeta_oficio_confirmacion_empresa
 * @property string $valida_jefe_oficina_externos_vinculacion
 * @property string $valida_jefe_recursos_materiales
 * @property integer $id_aut_salida_vehiculo
 * @property integer $id_estatus_solicitud_visita_academica
 *
 * The followings are the available model relations:
 * @property VaTiposVisitasAcademicas $idTipoVisitaAcademica
 * @property GRpempresas $idEmpresaVisita
 * @property EPeriodos $periodo
 * @property VaEstatusSolicitudVisitaAcademica $idEstatusSolicitudVisitaAcademica
 * @property ECatalogoMaterias[] $eCatalogoMateriases
 * @property EDatosAlumno[] $eDatosAlumnos
 * @property HEmpleados[] $hEmpleadoses
 * @property VaBitacoraCambiosSolicitudVinculacion[] $vaBitacoraCambiosSolicitudVinculacions
 * @property VaReporteResultadosIncidenciasVisitasAcademicas $vaReporteResultadosIncidenciasVisitasAcademicas
 */
class VaSolicitudesVisitasAcademicas extends CActiveRecord
{
    public $cveDepto;

    public function tableName()
    {
        return 'pe_vinculacion.va_solicitudes_visitas_academicas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre_visita_academica, periodo, anio, no_solicitud, id_tipo_visita_academica, id_empresa_visita', 'required'),
            array('anio, no_solicitud, id_tipo_visita_academica, id_empresa_visita, no_alumnos, id_aut_salida_vehiculo, id_estatus_solicitud_visita_academica', 'numerical', 'integerOnly'=>true),
            array('nombre_visita_academica', 'length', 'max'=>500),
            array('periodo', 'length', 'max'=>1),
            array('doc_oficio_confirmacion_empresa', 'length', 'max'=>30),
            array('path_carpeta_oficio_confirmacion_empresa', 'length', 'max'=>18),
            array('fecha_creacion_solicitud, fecha_hora_salida_visita, fecha_hora_regreso_visita, area_a_visitar, objetivo_visitar_area, observaciones_solicitud, valida_jefe_depto_academico, valida_subdirector_academico, ultima_fecha_modificacion, ultima_mod_oficio_conf_empresa, valida_jefe_oficina_externos_vinculacion, valida_jefe_recursos_materiales', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_solicitud_visitas_academicas, nombre_visita_academica, periodo, anio, no_solicitud, fecha_creacion_solicitud, fecha_hora_salida_visita, fecha_hora_regreso_visita, id_tipo_visita_academica, id_empresa_visita, area_a_visitar, objetivo_visitar_area, no_alumnos, observaciones_solicitud, valida_jefe_depto_academico, valida_subdirector_academico, ultima_fecha_modificacion, doc_oficio_confirmacion_empresa, ultima_mod_oficio_conf_empresa, path_carpeta_oficio_confirmacion_empresa, valida_jefe_oficina_externos_vinculacion, valida_jefe_recursos_materiales, id_aut_salida_vehiculo, id_estatus_solicitud_visita_academica, cveDepto', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idTipoVisitaAcademica' => array(self::BELONGS_TO, 'VaTiposVisitasAcademicas', 'id_tipo_visita_academica'),
            'idEmpresaVisita' => array(self::BELONGS_TO, 'GRpempresas', 'id_empresa_visita'),
            'periodo' => array(self::BELONGS_TO, 'EPeriodos', 'periodo'),
            'idEstatusSolicitudVisitaAcademica' => array(self::BELONGS_TO, 'VaEstatusSolicitudVisitaAcademica', 'id_estatus_solicitud_visita_academica'),
            'eCatalogoMateriases' => array(self::MANY_MANY, 'ECatalogoMaterias', 'va_materias_imparte_responsable_visita_academica(id_solicitud_materias_responsable_visitas_academicas, cveMateria)'),
            'eDatosAlumnos' => array(self::MANY_MANY, 'EDatosAlumno', 'va_alumnos_asisten_visitas_academicas(id_solicitud_visitas_academicas, nctrAlumno)'),
            'hEmpleadoses' => array(self::MANY_MANY, 'HEmpleados', 'va_responsables_visitas_academicas(id_solicitud_visitas_academicas, rfcEmpleado)'),
            'vaBitacoraCambiosSolicitudVinculacions' => array(self::HAS_MANY, 'VaBitacoraCambiosSolicitudVinculacion', 'id_solicitud_visitas_academicas'),
            'vaReporteResultadosIncidenciasVisitasAcademicas' => array(self::HAS_ONE, 'VaReporteResultadosIncidenciasVisitasAcademicas', 'id_reporte_resultados_incidencias'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id_solicitud_visitas_academicas' => 'Id Solicitud Visitas Academicas',
            'nombre_visita_academica' => 'Nombre Visita Académica',
            'periodo' => 'Periodo',
            'anio' => 'Año',
            'no_solicitud' => 'No. de Solicitud',
            'fecha_creacion_solicitud' => 'Fecha Creación Solicitud',
            'fecha_hora_salida_visita' => 'Fecha Hora Salida',
            'fecha_hora_regreso_visita' => 'Fecha Hora Regreso',
            'id_tipo_visita_academica' => 'Tipo Visita Académica',
            'id_empresa_visita' => 'Empresa a Visitar',
            'area_a_visitar' => 'Área a Visitar',
            'objetivo_visitar_area' => 'Objetivo Visitar Área',
            'no_alumnos' => 'No. de Alumnos',
            'observaciones_solicitud' => 'Motivo se rechazó la Solicitud',
            'valida_jefe_depto_academico' => 'Valida Jefe Depto Academico',
            'valida_subdirector_academico' => 'Valida Subdirector Academico',
            'ultima_fecha_modificacion' => 'Última Fecha Modificación',
            'doc_oficio_confirmacion_empresa' => 'Documento Oficio Confirmación de la Empresa',
            'ultima_mod_oficio_conf_empresa' => 'Ultima Fecha Modificación del Oficio Confirmación Empresa',
            'path_carpeta_oficio_confirmacion_empresa' => 'Path Carpeta Oficio Confirmacion Empresa',
            'valida_jefe_oficina_externos_vinculacion' => 'Valida Jefe Oficina Visitas Industriales',
            'valida_jefe_recursos_materiales' => 'Valida Jefe de Recursos Materiales',
            'id_aut_salida_vehiculo' => 'Id Aut Salida Vehiculo',
            'id_estatus_solicitud_visita_academica' => 'Estatus Solicitud Visita Academica',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
        $criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
        $criteria->compare('periodo',$this->periodo,true);
        $criteria->compare('anio',$this->anio);
        $criteria->compare('no_solicitud',$this->no_solicitud);
        $criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
        $criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
        $criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
        $criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
        $criteria->compare('id_empresa_visita',$this->id_empresa_visita);
        $criteria->compare('area_a_visitar',$this->area_a_visitar,true);
        $criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
        $criteria->compare('no_alumnos',$this->no_alumnos);
        $criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
        $criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
        $criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
                'pageSize' => 50
            )
        ));
	}

    //Vista general de las solicitudes del Solicitante por RFC
	public function searchListaSolicitudesVisitasAcademicasVigentes($rfcEmpleado)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "sva";
        $criteria->select = "*";
        $criteria->join = "join pe_vinculacion.va_responsables_visitas_academicas rva
                            on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas";
        $criteria->condition = "rva.\"rfcEmpleado\" = '$rfcEmpleado' AND (sva.id_estatus_solicitud_visita_academica != 6 AND sva.id_estatus_solicitud_visita_academica != 4)";
        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

        /*select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
        join pe_vinculacion.va_responsables_visitas_academicas rva
        on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
        where rva."rfcEmpleado" = 'TOFR830919HN7';*/

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXSolicitudesHistoricasDocenteResponsable($rfcEmpleado)
    {
        $criteria = new CDbCriteria;
        $criteria->alias = "sva";
        $criteria->select = "*";
        $criteria->join = "join pe_vinculacion.va_responsables_visitas_academicas rva
                            on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas";
        $criteria->condition = "rva.\"rfcEmpleado\" = '$rfcEmpleado' AND (id_estatus_solicitud_visita_academica = 6 OR id_estatus_solicitud_visita_academica = 4)";
        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

        /*select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
        join pe_vinculacion.va_responsables_visitas_academicas rva
        on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
        where rva."rfcEmpleado" = 'TOFR830919HN7';*/

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXValidacionJefeProyectosVinculacion($cveDepto)
    {
         // @todo Please modify the following code to remove attributes that should not be searched.
         $criteria = new CDbCriteria;
         $criteria->alias = "sva";

         if($cveDepto != NULL)
         {

            $criteria->join = " join pe_vinculacion.va_responsables_visitas_academicas rva
                                     on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
                                     join public.\"H_empleados\" hemp
                                     on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";
            $criteria->condition = " rva.cve_depto_acad = '$cveDepto' AND
                                     (fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL)";

         }else{

            $criteria->condition = " id_solicitud_visitas_academicas = 0 ";
         }

         $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

         /*
         select distinct (sva.id_solicitud_visitas_academicas), sva.*
         from pe_vinculacion.va_solicitudes_visitas_academicas sva
         join pe_vinculacion.va_responsables_visitas_academicas rva
         on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
         join public."H_empleados" hemp
         on hemp."rfcEmpleado" = rva."rfcEmpleado"
         where rva.cve_depto_acad = 4
         */

         $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
         $criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
         $criteria->compare('periodo',$this->periodo,true);
         $criteria->compare('anio',$this->anio);
         $criteria->compare('no_solicitud',$this->no_solicitud);
         $criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
         $criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
         $criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
         $criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
         $criteria->compare('id_empresa_visita',$this->id_empresa_visita);
         $criteria->compare('area_a_visitar',$this->area_a_visitar,true);
         $criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
         $criteria->compare('no_alumnos',$this->no_alumnos);
         $criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
         $criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
         $criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
         $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
         $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
         $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
         $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
         $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
         $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
         $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
         $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

         return new CActiveDataProvider($this, array(
             'criteria'=>$criteria,
             'pagination' => array(
                 'pageSize' => 50
             )
         ));
    }

    //Filtrar las Solicitudes por Departamento Academico del empleado que la creo
    public function searchListaSolicitudesVisitasAcademicasVigentesJefeProyVinculacion($depto_catedratico)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;

        if($depto_catedratico != NULL)
        {
            $criteria->alias = "sva";
            $criteria->join = " join pe_vinculacion.va_responsables_visitas_academicas rva
                                    on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
                                    join public.\"H_empleados\" hemp
                                    on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";
            $criteria->condition = " rva.cve_depto_acad = '$depto_catedratico' AND
                                    (fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NULL AND
                                    valida_subdirector_academico IS NULL AND valida_jefe_oficina_externos_vinculacion IS NULL AND
                                    valida_jefe_recursos_materiales IS NULL)";

            $criteria->order = "sva.id_solicitud_visitas_academicas ASC";
        }else{

            $criteria->condition = " id_solicitud_visitas_academicas = 0 ";
            $criteria->order = "id_solicitud_visitas_academicas ASC";
        }



        /*
        select distinct (sva.id_solicitud_visitas_academicas), sva.*
        from pe_vinculacion.va_solicitudes_visitas_academicas sva
        join pe_vinculacion.va_responsables_visitas_academicas rva
        on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
        join public."H_empleados" hemp
        on hemp."rfcEmpleado" = rva."rfcEmpleado"
        where rva.cve_depto_acad = 4
        */

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchCuandoNoEsSubdirectorAcademico()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = 0 ";


        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchListaSolicitudesVisitasAcademicasValidadasSubdAcademico($cveDepto)
    {
        $criteria = new CDbCriteria;
        $criteria->alias = "sva";

        if($cveDepto != NULL)
        {

            $criteria->select = "distinct(sva.id_solicitud_visitas_academicas), sva.* ";
            $criteria->join = " join pe_vinculacion.va_responsables_visitas_academicas rva
                                on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
                                join public.\"H_empleados\" hemp
                                on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";
            $criteria->condition = " hemp.\"deptoCatedratico\" = '$cveDepto' AND rva.responsable_principal = true AND
                                    (fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL AND
                                    valida_subdirector_academico IS NOT NULL)";

        }else{

            $criteria->condition = " fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL AND
                                     valida_subdirector_academico IS NOT NULL
                                  ";
        }

        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    //Filtrar las Solicitudes del Subdirector Academico que hayan sido validadas por el Jefe de Proyectos Academicos
    public function searchListaSolicitudesVisitasAcademicasVigentesSubdAcademico($cveDepto)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "sva";

        if($cveDepto != NULL)
        {

            $criteria->select = "distinct(sva.id_solicitud_visitas_academicas), sva.* ";
            $criteria->join = " join pe_vinculacion.va_responsables_visitas_academicas rva
                                on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
                                join public.\"H_empleados\" hemp
                                on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";
            $criteria->condition = " hemp.\"deptoCatedratico\" = '$cveDepto' AND rva.responsable_principal = true AND
                                    (fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL AND
                                    valida_subdirector_academico IS NULL AND valida_jefe_oficina_externos_vinculacion IS NULL AND
                                    valida_jefe_recursos_materiales IS NULL)";

        }else{

            $criteria->condition = " fecha_creacion_solicitud IS NOT NULL AND valida_jefe_depto_academico IS NOT NULL AND
                                     valida_subdirector_academico IS NULL AND valida_jefe_oficina_externos_vinculacion IS NULL AND
                                     valida_jefe_recursos_materiales IS NULL
                                  ";
        }

        $criteria->order = "sva.id_solicitud_visitas_academicas ASC";

		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    //Puede verlas todas pero No puede editar y validar nada
    public function searchListaSolicitudesVisitasAcademicasJefeVinculacion($cveDepto)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = "sva";
        $criteria->select = "distinct(sva.id_solicitud_visitas_academicas), sva.* ";
        $criteria->join = " join pe_vinculacion.va_responsables_visitas_academicas rva
                                on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
                                join public.\"H_empleados\" hemp on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\" ";

        if($cveDepto != NULL)
        {

            $criteria->condition = " hemp.\"deptoCatedratico\" = '$cveDepto' AND rva.responsable_principal = true AND
                                    (sva.fecha_creacion_solicitud IS NOT NULL AND sva.valida_jefe_depto_academico IS NOT NULL AND
                                    sva.valida_subdirector_academico IS NULL AND (sva.valida_jefe_oficina_externos_vinculacion IS NULL OR sva.valida_jefe_oficina_externos_vinculacion IS NOT NULL) AND
                                    (sva.valida_jefe_recursos_materiales IS NULL OR sva.valida_jefe_recursos_materiales IS NOT NULL) AND (sva.id_estatus_solicitud_visita_academica != 4 AND sva.id_estatus_solicitud_visita_academica != 5 AND sva.id_estatus_solicitud_visita_academica != 6)";

        }else{

            $criteria->condition = "rva.responsable_principal = true AND sva.fecha_creacion_solicitud IS NOT NULL AND
                                    sva.valida_jefe_depto_academico IS NOT NULL AND sva.valida_subdirector_academico IS NOT NULL AND
                                    (sva.valida_jefe_oficina_externos_vinculacion IS NULL OR sva.valida_jefe_oficina_externos_vinculacion IS NOT NULL) AND
                                    (sva.valida_jefe_recursos_materiales IS NULL OR sva.valida_jefe_recursos_materiales IS NOT NULL) AND (sva.id_estatus_solicitud_visita_academica != 4 AND sva.id_estatus_solicitud_visita_academica != 5 AND sva.id_estatus_solicitud_visita_academica != 6)";

        }

        $criteria->order = "id_solicitud_visitas_academicas ASC";

        /*
        select distinct (sva.id_solicitud_visitas_academicas), sva.*
        from pe_vinculacion.va_solicitudes_visitas_academicas sva
        join pe_vinculacion.va_responsables_visitas_academicas rva
        on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
        join public."H_empleados" hemp
        on hemp."rfcEmpleado" = rva."rfcEmpleado"
        where rva.cve_depto_acad = 8 AND hemp."cveDepartamentoEmp" = 25
        */


		$criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXSolicitudVisitaParaLiberacion($periodo, $anio)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " periodo = '$periodo' AND anio = '$anio' AND id_estatus_solicitud_visita_academica = 5 ";
        $criteria->order = "id_solicitud_visitas_academicas ASC";


        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXHistoricoSolicitudesVisitasAcademicas()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " id_estatus_solicitud_visita_academica = 6 OR id_estatus_solicitud_visita_academica = 4"; //Finalizadas y cancelados
        $criteria->order = "id_solicitud_visitas_academicas ASC";

        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' "; //Finalizadas
        $criteria->order = "id_solicitud_visitas_academicas ASC";

        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
		$criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
		$criteria->compare('periodo',$this->periodo,true);
		$criteria->compare('anio',$this->anio);
		$criteria->compare('no_solicitud',$this->no_solicitud);
		$criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
		$criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
		$criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('id_empresa_visita',$this->id_empresa_visita);
		$criteria->compare('area_a_visitar',$this->area_a_visitar,true);
		$criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
		$criteria->compare('no_alumnos',$this->no_alumnos);
		$criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
		$criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
		$criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
		));
    }

    public function searchXTodasSolicitudesSemestre($periodo, $anio)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " periodo = '$periodo'  AND anio = '$anio' ";
        $criteria->order = " id_solicitud_visitas_academicas ASC";

        $criteria->compare('id_solicitud_visitas_academicas',$this->id_solicitud_visitas_academicas);
        $criteria->compare('nombre_visita_academica',$this->nombre_visita_academica,true);
        $criteria->compare('periodo',$this->periodo,true);
        $criteria->compare('anio',$this->anio);
        $criteria->compare('no_solicitud',$this->no_solicitud);
        $criteria->compare('fecha_creacion_solicitud',$this->fecha_creacion_solicitud,true);
        $criteria->compare('fecha_hora_salida_visita',$this->fecha_hora_salida_visita,true);
        $criteria->compare('fecha_hora_regreso_visita',$this->fecha_hora_regreso_visita,true);
        $criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
        $criteria->compare('id_empresa_visita',$this->id_empresa_visita);
        $criteria->compare('area_a_visitar',$this->area_a_visitar,true);
        $criteria->compare('objetivo_visitar_area',$this->objetivo_visitar_area,true);
        $criteria->compare('no_alumnos',$this->no_alumnos);
        $criteria->compare('observaciones_solicitud',$this->observaciones_solicitud,true);
        $criteria->compare('valida_jefe_depto_academico',$this->valida_jefe_depto_academico,true);
        $criteria->compare('valida_subdirector_academico',$this->valida_subdirector_academico,true);
        $criteria->compare('ultima_fecha_modificacion',$this->ultima_fecha_modificacion,true);
        $criteria->compare('doc_oficio_confirmacion_empresa',$this->doc_oficio_confirmacion_empresa,true);
        $criteria->compare('ultima_mod_oficio_conf_empresa',$this->ultima_mod_oficio_conf_empresa,true);
        $criteria->compare('path_carpeta_oficio_confirmacion_empresa',$this->path_carpeta_oficio_confirmacion_empresa,true);
        $criteria->compare('valida_jefe_oficina_externos_vinculacion',$this->valida_jefe_oficina_externos_vinculacion,true);
        $criteria->compare('valida_jefe_recursos_materiales',$this->valida_jefe_recursos_materiales,true);
        $criteria->compare('id_aut_salida_vehiculo',$this->id_aut_salida_vehiculo);
        $criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 50
            )
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
