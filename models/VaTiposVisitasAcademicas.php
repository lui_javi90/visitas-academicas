<?php

/**
 * This is the model class for table "pe_vinculacion.va_tipos_visitas_academicas".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_tipos_visitas_academicas':
 * @property integer $id_tipo_visita_academica
 * @property string $tipo_visita_academica
 * @property boolean $tipo_valido
 *
 * The followings are the available model relations:
 * @property VaSolicitudesVisitasAcademicas[] $vaSolicitudesVisitasAcademicases
 */
class VaTiposVisitasAcademicas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_vinculacion.va_tipos_visitas_academicas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_visita_academica', 'required'),
			array('tipo_visita_academica', 'length', 'max'=>100),
			array('tipo_valido', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tipo_visita_academica, tipo_visita_academica, tipo_valido', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vaSolicitudesVisitasAcademicases' => array(self::HAS_MANY, 'VaSolicitudesVisitasAcademicas', 'id_tipo_visita_academica'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id_tipo_visita_academica' => 'Id Tipo Visita Academica',
			'tipo_visita_academica' => 'Tipo de Visita Académica',
			'tipo_valido' => 'Tipo es Valido',
		);
	}

	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('tipo_visita_academica',$this->tipo_visita_academica,true);
		$criteria->compare('tipo_valido',$this->tipo_valido);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchListadoTiposVisistasAcademicas()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->order = 'id_tipo_visita_academica ASC';

		$criteria->compare('id_tipo_visita_academica',$this->id_tipo_visita_academica);
		$criteria->compare('tipo_visita_academica',$this->tipo_visita_academica,true);
		$criteria->compare('tipo_valido',$this->tipo_valido);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 50
			)
		));
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
