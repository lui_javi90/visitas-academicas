<?php

/**
 * This is the model class for table "pe_vinculacion.va_estatus_solicitud_visita_academica".
 *
 * The followings are the available columns in table 'pe_vinculacion.va_estatus_solicitud_visita_academica':
 * @property integer $id_estatus_solicitud_visita_academica
 * @property string $estatus
 * @property string $descripcion_estatus
 *
 * The followings are the available model relations:
 * @property VaSolicitudesVisitasAcademicas[] $vaSolicitudesVisitasAcademicases
 */
class VaEstatusSolicitudVisitaAcademica extends CActiveRecord
{
	
	public function tableName()
	{
		return 'pe_vinculacion.va_estatus_solicitud_visita_academica';
	}

	
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estatus', 'required'),
			array('estatus', 'length', 'max'=>20),
			array('descripcion_estatus', 'length', 'max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_estatus_solicitud_visita_academica, estatus, descripcion_estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vaSolicitudesVisitasAcademicases' => array(self::HAS_MANY, 'VaSolicitudesVisitasAcademicas', 'id_estatus_solicitud_visita_academica'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_estatus_solicitud_visita_academica' => 'Id Estatus Solicitud Visita Academica',
			'estatus' => 'Estatus',
			'descripcion_estatus' => 'Descripcion',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('descripcion_estatus',$this->descripcion_estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchXIdEstatus()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->order = " id_estatus_solicitud_visita_academica ASC ";

		$criteria->compare('id_estatus_solicitud_visita_academica',$this->id_estatus_solicitud_visita_academica);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('descripcion_estatus',$this->descripcion_estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => 25
			)
		));
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
