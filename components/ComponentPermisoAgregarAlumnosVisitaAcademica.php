<script>

</script>
<?php
class ComponentPermisoAgregarAlumnosVisitaAcademica extends CButtonColumn
{
    public $header = "Alumnos <br>Asisten";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        $modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($data->id_solicitud_visitas_academicas);
        if($modelVaSolicitudesVisitasAcademicas === NULL)
            throw new CHttpException(404,'No existe Solicitud de Visista Académica con ese id.');

        //Si la visita fue aceptada entonces se agregan los alumnos ACEPTADA (2)
        if($modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica == 2)
        {
            //Verificamos los botones a mostrar
            $btn_add_alumnos = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/agregarr_32.png','LLenar Reporte Incidencias');
            $d3 = CHtml::link($btn_add_alumnos, '?r=visitasacademicas/vaAlumnosAsistenVisitasAcademicas/agregarAlumnoAVisitaAcademica&id_solicitud_visitas_academicas='.$data->id_solicitud_visitas_academicas);

            $btn_val = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/no_aprobado_32.png','Validar Alumnos Agregados'); 
            $d4 = CHtml::link($btn_val, 
            '?r=visitasacademicas/vaSolicitudesVisitasAcademicas/validarAlumnosVisitaAcademica&id_solicitud_visitas_academicas='.$data->id_solicitud_visitas_academicas
        );

            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">
                            Agregar Alumnos
                        </th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" class="text-center">'.$d3.'</td>
                    </tr>
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">
                            Validar Alumnos
                        </th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" class="text-center">'.$d4.'</td>
                    </tr>
                    <br>
                </table>';

        }else{

            //Cualquier estatus de la Solicitud diferente a 2 (ACEPTADA) no se permite agregar mas alumnos
            $hidepdf = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/bloquedo_32.png','No se puede agregar Alumnos');
            echo CHtml::link($hidepdf, '');

        }
    }
}

?>