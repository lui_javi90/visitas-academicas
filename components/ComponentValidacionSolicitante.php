<?php

class ComponentValidacionSolicitante extends CButtonColumn
{

    public $header = "Solicitante";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
    	/*Para llamar el metodo estatico*/
        require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

    	$id = $data->id_solicitud_visitas_academicas;
    	$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id);

    	if($modelVaSolicitudesVisitasAcademicas  != NULL)
    	{
    		echo "<br>";

            echo ($modelVaSolicitudesVisitasAcademicas->fecha_creacion_solicitud != NULL) ? '<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/aprobado_32.png"/>' : '<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/cancelado_32.png"/>';
            echo "<br><br>";

            //Formato a las fecha de validacion del supervisor
            $fecha_val_of = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($data->id_solicitud_visitas_academicas);

            echo ($fecha_val_of[0]['fecha_act'] != 'No disponible') ? '<b>'.$fecha_val_of[0]['fecha_act']." a las ".$fecha_val_of[0]['hora'].'<b>' : '<b>'.'--','<b>';

            echo "<br><br>";

    	}else{

    		echo "<b>No hay datos.</b>";
    	}
    }
}


?>