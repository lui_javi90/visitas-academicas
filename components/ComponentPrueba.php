<?php

    class ComponentPrueba extends CButtonColumn
    {
        public $header = "Alumnos <br>Asisten";
        public $template = '{addAlumnos},{valAlumnosAgregadosVisita}';

        public function init()
        {

            parent::init();
            //parent::renderDataCellContent($row, $data);
            $this->addAlumnosButton();
            $this->valAlumAgregadosButton();

        }

        public function addAlumnosButton()
        {

            $this->buttons['addAlumnos'] = array(

                'label' => 'Agregar Alumnos a Visita Académica',
                'url' => 'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/agregarAlumnoAVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
                'imageUrl' => 'images/servicio_social/agregarr_32.png'

            );

        }

        public function valAlumAgregadosButton()
        {
            $this->buttons['valAlumnosAgregadosVisita'] = array(

                'label' => 'Eliminar la Solicitud de Visita Académica Seleccionada',
                'url' => 'Yii::app()->createUrl("visitasacademicas/vaAlumnosAsistenVisitasAcademicas/validarAlumnosVisitaAcademica", array("id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas))',
                'imageUrl' => 'images/servicio_social/no_aprobado_32.png',
                'options' => array(
                    'title'        => 'Eliminar la Solicitud de Visita Académica Seleccionada',
                    'data-confirm' => '¿En verdad quieres ELIMINAR la Solicitud?',
                ),
                'click' => 'js:function(__event)
                {
                    __event.preventDefault(); // disable default action
            
                    var $this = $(this), // link/button
                        confirm_message = $this.data("confirm"), // read confirmation message from custom attribute
                        url = $this.attr("href"); // read AJAX URL with parameters from HREF attribute on the link
            
                    if(confirm(confirm_message)) // Si se confirma la operacion entonces...
                    {
                        // perform AJAX request
                        $("#va-solicitudes-visitas-academicas-grid").yiiGridView("update",
                        {
                            type	: "POST", // important! we only allow POST in filters()
                            dataType: "json",
                            url		: url,
                            success	: function(data)
                            {
                                console.log("Success:", data);
                                $("#va-solicitudes-visitas-academicas-grid").yiiGridView("update"); // refresh gridview via AJAX
                            },
                            error	: function(xhr)
                            {
                                console.log("Error:", xhr);
                            }
                        });
                    }
                }', 

            );
        }
    }

?>