<?php

class ComponentEstatusSolicitudVisitaAcademica extends CButtonColumn
{
    public $header = "Estatus de <br>Solicitud";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        //$criteria1 = new CDbCriteria;
        $id = $data->id_solicitud_visitas_academicas;
        //Verificamos que tenga todas las validaciones
        $Qry = "select * from pe_vinculacion.va_solicitudes_visitas_academicas
                where fecha_creacion_solicitud is not null AND valida_jefe_depto_academico is not null AND
                valida_subdirector_academico is not null AND valida_jefe_oficina_externos_vinculacion is not null AND
                valida_jefe_recursos_materiales is not null AND val_lista_alumnos_visita_academica is not null AND 
                id_solicitud_visitas_academicas = '$id' ";
                                
                                
        $model = Yii::app()->db->createCommand($Qry)->queryAll();
        
        if(sizeof($model) > 0)
        {
            //Validar si ha realizado el reporte de resultados e incidencias
            $criteria1 = new CDbCriteria;
            $criteria1->condition = " id_reporte_resultados_incidencias = '$id' ";
            $model = VaReporteResultadosIncidenciasVisitasAcademicas::model()->find($criteria1);
            
            $criteria2 = new CDbCriteria;
            if($model != NULL)
            {
                
                $criteria2->condition = " id_estatus_solicitud_visita_academica != 1 AND id_estatus_solicitud_visita_academica != 2 AND 
                id_estatus_solicitud_visita_academica != 6 AND id_estatus_solicitud_visita_academica != 8";
                $criteria2->order = " id_estatus_solicitud_visita_academica ASC";

                $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findAll($criteria2);

                if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                    throw new CHttpException(404,'No hay datos de los Estatus de las Visitas.');

            }else{

                $criteria2->condition = " id_estatus_solicitud_visita_academica != 1 AND id_estatus_solicitud_visita_academica != 2 AND 
                id_estatus_solicitud_visita_academica != 5 AND id_estatus_solicitud_visita_academica != 6 ";
                $criteria2->order = " id_estatus_solicitud_visita_academica ASC";

                $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findAll($criteria2);

                if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                    throw new CHttpException(404,'No hay datos de los Estatus de las Visitas.');

            }


        }else{

            $criteria1 = new CDbCriteria;
            $criteria1->condition = " id_estatus_solicitud_visita_academica != 2 AND id_estatus_solicitud_visita_academica != 5 AND 
                                    id_estatus_solicitud_visita_academica != 6 AND id_estatus_solicitud_visita_academica != 6 AND
                                    id_estatus_solicitud_visita_academica != 7 AND id_estatus_solicitud_visita_academica != 8 ";
            $criteria1->order = " id_estatus_solicitud_visita_academica ASC";

            $modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findAll($criteria1);

            if($modelVaEstatusSolicitudVisitaAcademica === NULL)
                throw new CHttpException(404,'No hay datos de los Estatus de las Visitas.');

        }
            
        //Lista de estatus que puede tener la Solicitud de Visita Academica
        $lista_estatus = CHtml::listData($modelVaEstatusSolicitudVisitaAcademica, "id_estatus_solicitud_visita_academica", "estatus");

        echo CHtml::dropDownList('id_estatus_solicitud_visita_academica' . $data->id_solicitud_visitas_academicas, $data->id_estatus_solicitud_visita_academica,
                                $lista_estatus,
                                array(
                                    'class'=> 'form-control',
                                    'ajax' => array(
                                        'type' => 'POST',
                                        'beforeSend' => 'function(){$(".bar").show();}',
                                        'complete' => 'function(){$(".bar").hide();}',
                                        'url' => ('?r=visitasacademicas/vaSolicitudesVisitasAcademicas/editarEstatusSolicitudVisitaAcademica'),
                                        'data' => array(
                                            'id_estatus_solicitud_visita_academica' => 'js:this.value', 
                                            'id_solicitud_visitas_academicas' => $data->id_solicitud_visitas_academicas
                                        ),
                                    ),
                                ),
        );
        
    }


}//Fin de Clase ComponentEstatusSolicitudVisitaAcademica


?>