<?php

class ComponentDatosDocente extends CButtonColumn
{
	public $header = "Docente";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
    	$id = $data->id_solicitud_visitas_academicas;
    	//$val = 'admin';
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = '$id' AND \"rfcEmpleado\" is not null AND responsable_principal = true ";
        $model = VaResponsablesVisitasAcademicas::model()->find($criteria);

        if($model != NULL)
        {
        	$rfc = $model->rfcEmpleado;
        	$modelHEmpleados = HEmpleados::model()->findByPk($rfc);
        	echo CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

            echo "<br>";
            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">Docente</th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" colspan="1" class="text-center">'.$modelHEmpleados->nmbCompletoEmp.'</td>
                    </tr>
                </table>';

        }else{

        	echo "<br>";
            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">Docente</th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" colspan="1" class="text-center">'.'--'.'</td>
                    </tr>
                </table>';
        }
        
    }
}


?>