<?php

class ComponentDatosJefeOfServiciosExternos extends CButtonColumn
{

    public $header = "Empleado";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        //Obtenemos el rfc del empleado que modifico la solicitud del docente
        $rfc = trim($data->usuario_modifica);

        //Realizamos la consulta para obtener el nombre
        $modelHEmpleados = HEmpleados::model()->findByPk($rfc);

        $val = 'admin';
        $criteria = new CDbCriteria;
        $criteria->condition = " id_solicitud_visitas_academicas = '$data->id_solicitud_visitas_academicas' AND usuario_modifica != '$val' ";
        $model = VaBitacoraCambiosSolicitudVinculacion::model()->find($criteria);

        if($model != NULL AND $rfc != null)
        {

            echo CHtml::image("items/getFoto.php?nctr_rfc=".trim($rfc), '', array('class'=>'img-circle','style' =>"width:100px;height:100px;"));

            echo "<br>";
            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">Empleado que Modificó</th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" colspan="1" class="text-center">'.$rfc.'</td>
                    </tr>
                </table>';

        }else{

            echo "<br>";
            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                    <tr>
                        <th style="width:30%" rowspan="1" colspan="1">Empleado que Modificó</th>
                    </tr>
                    <tr>
                        <td style="width:70%" rowspan="1" class="text-center">'.''.'</td>
                    </tr>
                </table>';
        }
    }
}

?>