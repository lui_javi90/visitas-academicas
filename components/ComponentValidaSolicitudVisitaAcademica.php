<?php
class ComponentValidaSolicitudVisitaAcademica extends CButtonColumn
{

    public $header = "Validación de Solicitud";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
        //models
        require_once Yii::app()->basePath.'/modules/autorizacionSalida/models/RMAutSalidasDetalle.php';
        require_once Yii::app()->basePath.'/modules/autorizacionSalida/models/RMAutSalidas.php';
        require_once Yii::app()->basePath.'/modules/autorizacionSalida/models/RMAutSalidasEstatus.php';

        //Validacion y formato de fechas
        $fec_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($data->id_solicitud_visitas_academicas);
        $fec_jefe_acad = InfoSolicitudVisitasAcademicas::getFechaValidacionJefeAcademicoVisitaAcademica($data->id_solicitud_visitas_academicas);
        $fec_subd_acad = InfoSolicitudVisitasAcademicas::getFechaValidacionSubAcademicoVisitaAcademica($data->id_solicitud_visitas_academicas);
        $fec_jefe_of_va = InfoSolicitudVisitasAcademicas::getFechaValidacionJefeOficinaExternosVinculacionVisitasAcademicas($data->id_solicitud_visitas_academicas);
        $fec_jefe_rec_mat = InfoSolicitudVisitasAcademicas::getFechaValidacionJefeRecursosmaterialesVisitasAcademicas($data->id_solicitud_visitas_academicas);

        //Dias transcurridos entre cada validacion
        //Creacion Solicitud - jefe Proyectos Vinculacion
        $dias_trans_sja = $this->diasTranscurridos($data->fecha_creacion_solicitud, $data->valida_jefe_depto_academico);
        //jefe Proyectos Vinculacion - Subdirector Academico
        $dias_trans_jasa = $this->diasTranscurridos($data->valida_jefe_depto_academico, $data->valida_subdirector_academico);
        //Subdirector Academico - Jefe de Of. de Visitas Industriales
        $dias_trans_sajova = $this->diasTranscurridos($data->valida_subdirector_academico, $data->valida_jefe_oficina_externos_vinculacion);
        // Jefe de Of. de Visitas Industriales - Jefe Depto. Recursos Materiales
        $dias_trans_jovajrm = $this->diasTranscurridos($data->valida_jefe_oficina_externos_vinculacion, $data->valida_jefe_recursos_materiales);
        //Total de dias transcurridos entre la creacion de la solicitud y la ultima validacion (Jefe Depto. Recursos Materiales)
        $dias_trans_ssa = $this->diasTranscurridos($data->fecha_creacion_solicitud, $data->valida_jefe_recursos_materiales);
        $dias = ($dias_trans_ssa != NULL) ? ' días' : '-';

        //Validar si hay fecha de validacion de la Solicitud
        $val_1 = ($fec_jefe_acad[0]['fecha_act'] === 'SV' ) ? 'Sin Validar' : $fec_jefe_acad[0]['fecha_act'].' a las '.$fec_jefe_acad[0]['hora'];
        $val_2 = ($fec_subd_acad[0]['fecha_act'] === 'SV') ? 'Sin Validar' : $fec_subd_acad[0]['fecha_act'].' a las '.$fec_subd_acad[0]['hora'];
        $val_3 = ($fec_jefe_of_va[0]['fecha_act'] === 'SV') ? 'Sin Validar' : $fec_jefe_of_va[0]['fecha_act'].' a las '.$fec_jefe_of_va[0]['hora'];
        $val_4 = ($fec_jefe_rec_mat[0]['fecha_act'] === 'SV') ? 'Sin Validar' : $fec_jefe_rec_mat[0]['fecha_act'].' a las '.$fec_jefe_rec_mat[0]['hora'];

        //Estatus del autobus en caso de que haya ido la Solicitud a Jefe de Materiales
        //Caso 1: cuando no se ha ligado la solicitud de visita con la solicitud de vehiculo
        //Caso 2: cuando no se ha asignado Chofer
        //Caso 3: cuando no se ha asignado Automovil
        $mensaje_auto = null;
        $mensaje_chofer = null;
        $id_sol_aut = $data->id_aut_salida_vehiculo;
        $modelRMAutSalidas = ($id_sol_aut != NULL OR $id_sol_aut != 0) ? RMAutSalidas::model()->findByPk($id_sol_aut) : null;

        //Estatus de la Solicitud del autobus
        if($modelRMAutSalidas === NULL)
        {
          //$mensaje_auto = "Sin Asignación de Vehículo";
          $mensaje_auto = "<span style=\"font-size:9px\" class=\"label label-danger\">Sin Asignación</span>";
          $mensaje_chofer = "<span style=\"font-size:9px\" class=\"label label-danger\"> de Solicitud de Vehículo</span>";

        }else{

          $modelRMAutSalidasEstatus = RMAutSalidasEstatus::model()->findByPk($modelRMAutSalidas->id_aut_salida_estatu);

          if($modelRMAutSalidas->id_aut_salida_estatu == 1)
          {
            $mensaje_auto = "<span style=\"font-size:11px\" class=\"label label-default\">".$modelRMAutSalidasEstatus->descrip_estatus.' ( '.$modelRMAutSalidas->id_aut_salida.' ) '."</span>";
            $mensaje_chofer = "";

          }elseif($modelRMAutSalidas->id_aut_salida_estatu == 4 || $modelRMAutSalidas->id_aut_salida_estatu == 6 || $modelRMAutSalidas->id_aut_salida_estatu == 7)
          {
            $mensaje_auto = "<span style=\"font-size:11px\" class=\"label label-warning\">".$modelRMAutSalidasEstatus->descrip_estatus.' ( '.$modelRMAutSalidas->id_aut_salida.' ) '."</span>";
            $mensaje_chofer = "";

          }elseif($modelRMAutSalidas->id_aut_salida_estatu == 8)
          {
            $mensaje_auto = "<span style=\"font-size:11px\" class=\"label label-success\">".$modelRMAutSalidasEstatus->descrip_estatus.' ( '.$modelRMAutSalidas->id_aut_salida.' ) '."</span>";
            $mensaje_chofer = "";

          }elseif($modelRMAutSalidas->id_aut_salida_estatu == 2)
          {
            $mensaje_auto = "<span style=\"font-size:11px\" class=\"label label-danger\">".$modelRMAutSalidasEstatus->descrip_estatus.' ( '.$modelRMAutSalidas->id_aut_salida.' ) '."</span>";
            $mensaje_chofer = "";
          }

        }


        /*Se crea la tabla*/
        echo '<table style="width:100%" class="table">
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Solicitante</th>
                    <td style="width:70%" rowspan="1" class="text-center">'.$fec_sol[0]['fecha_act'].' a las '.$fec_sol[0]['hora'].'</td>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Jefe Proyecto Vinculación</th>
                    <td style="width:70%" rowspan="1" class="text-center">'.$dias_trans_sja.' '.$val_1.'</td>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Subdirector Académico</th>
                    <td style="width:70%" rowspan="1" class="text-center">'.$dias_trans_jasa.' '.$val_2.'</td>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Jefa Oficina Visitas Académicas </th>
                    <td style="width:70%" rowspan="1" class="text-center">'.
                    /*CHtml::link('Ver Documento', array('vaSolicitudesVisitasAcademicas/showOficioConfirmacionEmpresa',"id_solicitud_visitas_academicas"=>$data->id_solicitud_visitas_academicas), 
                                                        array('click' => 'function(){

                                                          $("#javis").dialog("open"); return false;

                                                          }')
                                                  ).'<br>'.*/
                    $dias_trans_sajova.' '.$val_3.'<br>'.
                    '</td>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Jefe Depto. de Materiales</th>
                    <td style="width:70%" rowspan="1" class="text-center">'
                      .$dias_trans_jovajrm.' '.$val_4.'<br>'.$mensaje_auto.'<br>'.$mensaje_chofer.
                    '</td>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">Total</th>
                    <td style="width:70%" rowspan="1" class="text-center">'.$dias_trans_ssa.' '.$dias.'</td>';
        echo  '</tr>
             </table>';
    }

    /*Dias transcurridos entre cada evluacion del report bimestral*/
  public function diasTranscurridos($fec_inic, $fec_fin)
  {
    $diasTranscurridos;

    if($fec_inic != NULL && $fec_fin != NULL)
    {
      $fechaIni = strtotime($fec_inic);
      $fechaFin = strtotime($fec_fin);
      $segundosTranscurridos = $fechaFin - $fechaIni;
      $diasTranscurridos = $segundosTranscurridos / 86400; //Segundos por dia.

      return "( + ".intval($diasTranscurridos)." )";

    }else{

      return null;
    }

  }
  /*Dias transcurridos entre cada evluacion del report bimestral*/

  /* Para saber cuántos días han pasado, podemos dividir los segundos transcurridos entre 86,400
  * Un día = 24 horas * 60 minutos * 60 segundos = 86,400 segundos
  */

}//Fin de la clase

?>

<?php

//$this->baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/detailview'


/*CJuiDialog::begin([
	'id' => 'javis',
	'options' => array(
		'title' => 'Detalle de la Empresa',
		'draggable' => false,
		'show' => 'puff',
		'hide' => 'explode',
		'autoOpen' => false,
		'modal' => true,
		'scrolling'=>'no',
    'resizable'=>false,
		'width' => 1200, 
		'height' => 750,
		'buttons' => array(
			array('text'=>'Cerrar Ventana','class'=>'btn btn-danger','click'=> 'js:function(){$(this).dialog("close");}'),
		),
	),

]);*/ ?>

	<!--<iframe scrolling="no" id="parm-frame" width="1200" height="750"></iframe>-->
	
<?php
	//$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


