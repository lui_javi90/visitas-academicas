<?php

class ComponentReporteResultadosIncidentesVisitaAcademica extends CButtonColumn
{
    public $header = "Reporte <br>Resultados <br>Incidencias";

    public function init() {}

    public function renderDataCellContent($row, $data)
    {
        
        
        $id_sol = $data->id_solicitud_visitas_academicas;
        $criteria = new CDbCriteria;

        //Validaciones jefes 7 = Liberacion de Visita Academica
        $criteria->condition = " (fecha_creacion_solicitud is not null AND valida_jefe_depto_academico is not null AND
                    valida_subdirector_academico is not null AND valida_jefe_oficina_externos_vinculacion is not null AND
                    valida_jefe_recursos_materiales is not null) AND 
                    (id_estatus_solicitud_visita_academica = 7 OR id_estatus_solicitud_visita_academica = 5) AND 
                    id_solicitud_visitas_academicas = '$id_sol' ";
        //$criteria->condition = " id_solicitud_visitas_academicas = '$id_sol' AND id_estatus_solicitud_visita_academica = 5";
        
        $modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);

        if($modelVaSolicitudesVisitasAcademicas)
        {
            $hpdf = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/bloquedo_32.png','Imprimir Autorización');
            //echo CHtml::link($hidepdf, '');

            $modelVaReporteResultadosIncidenciasVisitasAcademicas = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

            //Reporte PDF
            $showpdf = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/printer.png','Imprimir Autorización');
            $data1 = ($modelVaReporteResultadosIncidenciasVisitasAcademicas != NULL) ? CHtml::link($showpdf, '?r=visitasacademicas/vaSolicitudesVisitasAcademicas/imprimirReporteResultadosEIncidentesVisitaAcademica&id_solicitud_visitas_academicas='.$data->id_solicitud_visitas_academicas) : CHtml::link($hpdf, '');

            //LLenar Reporte Nuevo de Incidencias PDF
            $llenaReporteIncidencias = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/agregar_32.png','LLenar Reporte Incidencias');
            $d3 = CHtml::link($llenaReporteIncidencias, '?r=visitasacademicas/vaReporteResultadosIncidenciasVisitasAcademicas/nuevoReporteResultadosIncidenciasVisitaAcademica&id_solicitud_visitas_academicas='.$data->id_solicitud_visitas_academicas);

            //Editar Reporte de Incidencias PDF
            $llenaReporteIncidencias = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/agregar_32.png','LLenar Reporte Incidencias');
            $d4 = CHtml::link($llenaReporteIncidencias, '?r=visitasacademicas/vaReporteResultadosIncidenciasVisitasAcademicas/editarReporteResultadosIncidenciasVisitaAcademica&id_solicitud_visitas_academicas='.$data->id_solicitud_visitas_academicas);

            $modelVaReporteResultadosIncidenciasVisitasAcademicas = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
            $data2 = ($modelVaReporteResultadosIncidenciasVisitasAcademicas != NULL) ? $d4 : $d3;

            /*Se crea la tabla*/
            echo '<table style="width:100%" class="table">
                <tr>
                    <th style="width:30%" rowspan="1" colspan="1">
                        Realizar Reporte
                    </th>
                </tr>
                <tr>
                    <td style="width:70%" rowspan="1" class="text-center">'.$data2.'</td>
                </tr>
                <tr>
                <th style="width:30%" rowspan="1" colspan="1">
                    Imprimir Reporte
                </th>
                </tr>
                <tr>
                    <td style="width:70%" rowspan="1" class="text-center">'.$data1.'</td>
                </tr>
                <br>
              </table>';
        
        }else{

            $hidepdf = CHtml::image(Yii::app()->baseUrl.'/images/servicio_social/bloquedo_32.png','Imprimir Autorización');
            echo CHtml::link($hidepdf, '');

        }
    }

}