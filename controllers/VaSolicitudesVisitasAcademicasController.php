<?php

class VaSolicitudesVisitasAcademicasController extends Controller
{

	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
								 'detalleVSolicitudVisitaAcademica', //Jefe proyecto Vinculacion
								 'editarVSolicitudVisitaAcademica', //Vinculacion
								 'listaVSolicitudesVisitasAcademicas', //Jefe Proyecto Vinculacion
								 'agregarNuevoResponsableVisitaAcademica', //Subdirector o Jefe Proyecto Vinculacion
								 'agregarNuevaMateriaVisitaAcademica', //Subdirector o Jefe Proyecto Vinculacion
								 'validaJefeDeptoAcademico', //Validar Solicitud Jefe Proyecto Vinculacion
								 'imprimirSolicitudVisitaAcademica', //Imprimir Solicitudes Validadas Jefe Proyectos de Vinculacion
								 'imprimirListaEstudiantesAsistenAVisitaAcademica', //Lista de estudiantes que asisten a la Visita Académica
								 'imprimirFormatoSolicitudVisitaAcademica', //Formato Solicitud de Visita Academica
								 'imprimirReporteResultadosEIncidentesVisitaAcademica', //Reporte de Resultados e Incidentes
								 'imprimirFormatoSolicitudPresentacionYAgradecimiento', //Formato Presentacion y Agradecimiento
								 'rechazarJVSolicitudVisitaAcademica', //Rechazar la Solicitud por parte de Roxana de Vinculacion
								 'liberarSolicitudVisitaAcademica', //Liberar Solicitud y pasar a Historico
								 'listaVValidadasSolicitudesVisitaAcademicas', //Jefes Proyectos Vinculacion
								 'detalleVValidadaSolicitudVisitaAcademica', //Detalle Validadas Jefes de Proyectos de Vinculacion
								 'rechazarVSolicitudVisitaAcademica', //Mensaje del rechazo de la solicitud Jefes de Proyectos de Vinculacion
								 'editarEstatusSolicitudVisitaAcademica', //Editar el Estatus de la Solicitud
								 'liberarJVSolicitudVisitaAcademica' //Estatus liberacion para realizar Reporte de Resultados e Incidencias
				),
				'roles' => array('jefe_vinculacion', 'jefe_departamento_academico')
				//'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('listaSolicitudesVisitasAcademicas', //Vista Docente
								  'nuevaSolicitudVisitaAcademica', //Vista Docente
								  'detalleSolicitudVisitaAcademica', //Vista Docente
								  'showOficioConfirmacionEmpresa', //Mostrar en un modal el oficio de confirmacion de la Empresa
								  'editarSolicitudVisitaAcademica', //Vista Docente
								  'imprimirFormatoSolicitudVisitaAcademica', //Formato Solicitud de Visita Academica
								  'imprimirFormatoSolicitudPresentacionYAgradecimiento', //Formato Presentacion y Agradecimiento
								  'imprimirListaEstudiantesAsistenAVisitaAcademica', //Lista de estudiantes que asisten a la Visita Académica
								  'imprimirReporteResultadosEIncidentesVisitaAcademica', //Reporte de Resultados e Incidentes
								  'obsSolicitudVisitaAcademica', //Observaciones de la Solicitud de Visita Academica
								  'listaDHistoricoSolicitudesVisitasAcademicas', //Historico Solicitudes Docentes Responsables
								  'detalleDHistoricoSolicitudVisitaAcademica' //Detalle Historico Docentes Responsables

				),
				'roles' => array('docente')
				//'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('imprimirFormatoSolicitudVisitaAcademica', //Formato Solicitud de Visita Academica
								 'imprimirFormatoSolicitudPresentacionYAgradecimiento', //Formato Presentacion y Agradecimiento
								 'imprimirListaEstudiantesAsistenAVisitaAcademica', //Lista de estudiantes que asisten a la Visita Académica
								 'imprimirReporteResultadosEIncidentesVisitaAcademica', //Reporte de Resultados e Incidentes
								 'listaSSolicitudesVisitasAcademicas', //Subdirector Academico
								 'detalleSSolicitudVisitaAcademica', //Subdirector Academico
								 'rechazarSSolicitudVisitaAcademica', // Mensaje del rechazo de Subdirector Academico
								 'validaSubdirectorAcademico', //Valida Subdirector Academico
								 'listaSHistoricoSolicitudesVisitasAcademicas', //Historico Subdirector Academico
								 'detalleSHistoricoSolicitudVisitaAcademica', //Detalle Historico Subdirector Academico
								 'imprimirSSolicitudVisitaAcademica' //Imprimir Solicitudes Validadas Subdirector Academico

				),
				'roles' => array('subdirector_academico')
				//'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('imprimirFormatoSolicitudVisitaAcademica', //Formato Solicitud de Visita Academica
								'imprimirFormatoSolicitudPresentacionYAgradecimiento', //Formato Presentacion y Agradecimiento
								'imprimirListaEstudiantesAsistenAVisitaAcademica', //Lista de estudiantes que asisten a la Visita Académica
								'imprimirReporteResultadosEIncidentesVisitaAcademica', //Reporte de Resultados e Incidentes
								'listaLiberacionSolicitudVisitaAcademica', //Lista de Solicitudes para liberacion y pase a historico
								'listaHistoricoSolicitudesVisitasAcademicas', //Historico de Solicitudes ROXANA
								'detalleHistoricoSolicitudVisitaAcademica', //Detalle Historico Solicitud Visita Academica
								'listaJVSolicitudesVisitasAcademicas', //ROXANA
								'detalleJVSolicitudVisitaAcademica', //ROXANA
								'editarJVSolicitudVisitaAcademica', //Vista de Roxana Vinculacion para editar fechas de la solicitud de visita academica
								'messageJVSolicitudVisitaAcademica', //Mensajes de la Solicitud ROXANA
								'listaStatusSolicitudesVisitasAcademicas', //Estatus de las Solicitudes del Semestre
								'detailSolicitudVisitaAcademica', //Detalle de la Solicitud (modal)
								'editSolicitudVisitaAcademica', //Editar la Solicitud (TODA) Oficina Visitas Academicas
								'cancelSolicitudVisitaAcademica' //Eliminar Solicitud
				),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			)
		);
	}

	public function actionCancelSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
		$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 4; //Solicitud CANCELADA

		try
		{
			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				
				Yii::app()->user->setFlash('success', "Solicitud Cancelada correctamente!!!");
				//Si se realizo correctamente el cambio
				$transaction->commit();
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al Cancelar la Solicitud.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Cancelar la solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}
	}

	public function actionEditSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{

		/*Para llamar el metodo estatico*/
        require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		//Obtenemos el RFC del Empleado Solicitante
		$info_empleado = $this->infoEmpleado($this->getDocenteResponsableSolicitudVisita($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas));

		//Datos del formulario
		$tipos_visitas = $this->getTipoVisitas();

		//Datos del formulario
		$lista_empresas_visitas = $this->getEmpresasVisitas();

		$lista_edo_sol = $this->getEstatusSolicitudes();

		$lista_periodos = $this->Periodos();

		$fec_ult_edic = InfoSolicitudVisitasAcademicas::getFechaSolicitudUltimaActualizacion($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

        if(isset($_POST['VaSolicitudesVisitasAcademicas']))
        {

            $modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];
            $modelVaSolicitudesVisitasAcademicas->ultima_fecha_modificacion = date('Y-m-d H:i:s');

            if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
            {

	            try
	            {

		            if($modelVaSolicitudesVisitasAcademicas->save())
		            {
		            	Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
		            	//Si se realizo correctamente el cambio
						$transaction->commit();
		                $this->redirect(array('listaStatusSolicitudesVisitasAcademicas'));

		            }else{

		            	Yii::app()->user->setFlash('danger', "Error!!! no se actualizó la información de la Solicitud.");
		            	//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();

		            }

		        }catch(Exception $e)
		        {
		        	Yii::app()->user->setFlash('danger', "Error!!! no se pudo actualizar la información.");
		        	//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
		        }

		    }else{

		    	//die("1q");
		    	Yii::app()->user->setFlash('danger', "Error!!! La Fecha de Salida No puede ser mayor a la de Regreso.");
		    }
        }

		$this->render('editSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'info_empleado' => $info_empleado,
					  'tipos_visitas' => $tipos_visitas,
					  'lista_empresas_visitas' => $lista_empresas_visitas,
					  'lista_edo_sol' => $lista_edo_sol,
					  'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
					  'cargo' => $this->getCargo($info_empleado->rfcEmpleado),
					  'fec_creacion' => InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas),
					  'lista_periodos' => $lista_periodos,
					  'fec_ult_edic' => $fec_ult_edic
		));
	}

	public function getDocenteResponsableSolicitudVisita($id_solicitud_visitas_academicas)
	{
		$qry_rfc_resp = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND 
						rva.responsable_principal = true
					";

		$rs = Yii::app()->db->createCommand($qry_rfc_resp)->queryAll();

		return ($rs[0]['rfcEmpleado'] == NULL OR empty($rs)) ? '--' : $rs[0]['rfcEmpleado'];
	}

	public function actionDetailSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		/*Para llamar el metodo estatico*/
        require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);

		$fecha_creacion = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

		$fec_salida_visita = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

		$fec_regreso_visita = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

		$tipo_visita = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);

		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$estatus_sol = $this->getStatusSolicitud($modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica);

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('searchListaResponsablesSolicitudVisitaAcademica');
        $modelVaResponsablesVisitasAcademicas->unsetAttributes();

        if(isset($_GET['VaResponsablesVisitasAcademicas']))
        	$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

        $modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];


		$this->render('detailSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					  'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
					  'id_solicitud_visitas_academicas' => $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas,
					  'periodo' => $periodo,
					  'fecha_creacion' => $fecha_creacion,
					  'fec_salida_visita' => $fec_salida_visita,
					  'fec_regreso_visita' => $fec_regreso_visita,
					  'tipo_visita' => $tipo_visita,
					  'empresa' => $empresa,
					  'estatus_sol' => $estatus_sol,
					  
		));
	}

	public function getStatusSolicitud($id_estatus_solicitud_visita_academica)
	{
		$modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findByPk($id_estatus_solicitud_visita_academica);
		if($modelVaEstatusSolicitudVisitaAcademica === NULL)
			throw new CHttpException(404,'No existe Solicitud con ese Estatus.');

		return $modelVaEstatusSolicitudVisitaAcademica->estatus;
	}

	public function actionListaStatusSolicitudesVisitasAcademicas()
	{
		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		//Obtener el Año y Periodo Actual
		$_periodo = Yii::app()->db->createCommand("select periodoactual(0)")->queryAll();
		$periodo = $_periodo[0]['periodoactual'];
		$_anio = Yii::app()->db->createCommand("select periodoactual(1)")->queryAll();
		$anio = $_anio[0]['periodoactual'];


		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
		{
			$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
		}

		$this->render('listaStatusSolicitudesVisitasAcademicas',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'periodo' => $periodo,
					  'anio' => $anio
		));
	}

	public function actionObsSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		$this->render('obsSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
		));
	}

	public function actionLiberarJVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
		$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 7; //LIBERACION para Reporte de Resultados e Incidencias

		try
		{
			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				//if($this->crearRegistroReporteResultadosEIncidencias($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas))
				//{
					Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
					//Si se realizo correctamente el cambio
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );
				//}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Validar la solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}

	}

	//Actualizar Estatus de la Solicitud de Visita Academica
	public function actionEditarEstatusSolicitudVisitaAcademica()
	{
		if (isset($_POST['id_solicitud_visitas_academicas']) && (isset($_POST['id_estatus_solicitud_visita_academica'])))
		{
			$id_solicitud_visitas_academicas = $_POST['id_solicitud_visitas_academicas'];
			$estatus = (int)$_POST['id_estatus_solicitud_visita_academica'];
			$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

			$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = $estatus;

			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");

			}else{

				//die("");
				throw new CHttpException(404,'Error!!! no se actualizó el estatus de la Solicitud de Visita Académica.');
			}
		}
	}

	public function actionMessageJVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);
		if($modelVaSolicitudesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existen datos de esa Solicitud.');

        $this->render('messageJVSolicitudVisitaAcademica',array(
            		  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
        ));
	}

	public function actionDetalleSHistoricoSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//$rfc_empleado = Yii::app()->params['rfcSubdirectorAcademico']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$VaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$VaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$VaSolicitudesVisitasAcademicas->attributes=$_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleSHistoricoSolicitudVisitaAcademica',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'periodo' => $periodo,
						'fecha_creac_sol' => $fecha_creac_sol,
						'fecha_sal' => $fecha_sal,
						'fecha_reg' => $fecha_reg,
						'tipo_visita_acad' => $tipo_visita_acad,
						'empresa' => $empresa,
						'id' => $id,
						'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
						'VaSolicitudesVisitasAcademicas' => $VaSolicitudesVisitasAcademicas,
						'rfcEmpleado' => $rfcEmpleado
		));
	}

	public function actionListaSHistoricoSolicitudesVisitasAcademicas($cveDepto = null)
	{

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSubdirectorAcademico'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);
		//Comprobar que es el subdirector academico
		$is_subd_academico = $this->esSubdirectorAcademico($rfcEmpleado);
		//Lista de Departamentos para filtros
		$lista_deptos = $this->getDepartamentosAcademicos();

		if($is_subd_academico)
		{
			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->cveDepto = $_GET['VaSolicitudesVisitasAcademicas']['cveDepto'];
				$cveDepto = $modelVaSolicitudesVisitasAcademicas->cveDepto;
			}

			$this->render('listaSHistoricoSolicitudesVisitasAcademicas',array(
						  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						  'cveDepto' => $cveDepto,
						  'is_subd_academico' => $is_subd_academico,
						  'lista_deptos' => $lista_deptos
			));

		}else{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
			}

			$this->render('listaSHistoricoSolicitudesVisitasAcademicas',array(
						  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						  'is_subd_academico' => $is_subd_academico,
						  'lista_deptos' => $lista_deptos
			));

		}
	}

	public function actionListaDHistoricoSolicitudesVisitasAcademicas()
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
		{
			$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
		}

        $this->render('listaDHistoricoSolicitudesVisitasAcademicas',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'rfcEmpleado' => $rfcEmpleado
        ));
	}

	public function actionRechazarSSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSubdirectorAcademico']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
		$observaciones_guardadas = $modelVaSolicitudesVisitasAcademicas->observaciones_solicitud;

		//Datos de la Empresa
		$name_empresa = $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica;
		$num_solicitud = $modelVaSolicitudesVisitasAcademicas->no_solicitud;

		// Ajax Validation enabled
		$this->performAjaxValidation($modelVaSolicitudesVisitasAcademicas);
		$flag = true;

 		if(isset($_POST['VaSolicitudesVisitasAcademicas']))
 		{
			$flag = false;
			$modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];

			//Obtener Nombre y RFC del Docente que hace la observacion
			//$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 3; //SUSPENDIDA
			$name_rfc = 'Subdirector Académico: '.$this->getNameAndRFC($rfcEmpleado);
			$fec_realiza = 'Fecha: '.date('Y-m-d H:i:s');
			$mensaje = 'Mensaje: '.$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud;
			$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud = $observaciones_guardadas.$fec_realiza.'<br>'.$name_rfc.'<br>'.$mensaje.'<br><br>';

			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se pudo actualizar la Solicitud.");
			}
		}

		$this->render('rechazarSSolicitudVisitaAcademica',array(
					 'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
					 'name_empresa' => $name_empresa,
					 'num_solicitud' => $num_solicitud
		));
	}

	public function actionRechazarVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
		$observaciones_guardadas = $modelVaSolicitudesVisitasAcademicas->observaciones_solicitud;
		//Datos de la Empresa
		$name_empresa = $modelVaSolicitudesVisitasAcademicas->nombre_visita_academica;
		$num_solicitud = $modelVaSolicitudesVisitasAcademicas->no_solicitud;

		// Ajax Validation enabled
		$this->performAjaxValidation($modelVaSolicitudesVisitasAcademicas);
		$flag = true;

 		if(isset($_POST['VaSolicitudesVisitasAcademicas']))
 		{
			$flag = false;
			$modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];

			//Obtener Nombre y RFC del docente que hace la observacion
			//$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 3;
			$name_rfc = 'Jefe Proyectos de Vinculación: '.$this->getNameAndRFC($rfcEmpleado);
			$fec_realiza = 'Fecha: '.date('Y-m-d H:i:s');
			$mensaje = 'Mensaje: '.$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud;
			$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud = $observaciones_guardadas.$fec_realiza.'<br>'.$name_rfc.'<br>'.$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud.'<br><br>';

			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se pudo actualizar la Solicitud.");
			}
		}

		$this->render('rechazarVSolicitudVisitaAcademica',array(
					 'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
					 'name_empresa' => $name_empresa,
					 'num_solicitud' => $num_solicitud
		));
	}

	public function getNameAndRFC($rfcEmpleado)
	{
		/*$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND responsable_principal = true ";
		$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->find($criteria);

		if($modelVaResponsablesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No hay datos de la Solicitud.');*/

		//Nombre del Empleado
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcEmpleado);
		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No hay datos del Empleado.');

		return $modelHEmpleados->nmbCompletoEmp.' ( '.$modelHEmpleados->rfcEmpleado.' ) ';
	}

	//Filtrar por Jefe de Proyectos de Vinculacion
	public function actionListaVValidadasSolicitudesVisitaAcademicas()
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		//Comprobar que es Jefe de Proyectos de Vinculacion
		$is_jefe_proyvinc = $this->esJefeProyectosVinculacion($rfcEmpleado);

		//Obtener Departamento Catedratico
		$depto_catedratico = $this->getDeptoCatedratico($rfcEmpleado);

		//Comprobra si es jefe de departamento academico
		$is_jefe_depto_academico = $this->esJefeDepartamentoAcademico($depto_catedratico, $rfcEmpleado);

		//Lista de deptos academicos
		$lista_deptos = $this->getDepartamentosAcademicos();

		if($is_jefe_proyvinc == true || $is_jefe_depto_academico == true)
		{
			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->cveDepto = $_GET['VaSolicitudesVisitasAcademicas']['cveDepto'];
				$cveDepto = $modelVaSolicitudesVisitasAcademicas->cveDepto;
			}

			$this->render('listaVValidadasSolicitudesVisitaAcademicas',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'depto_catedratico' => $depto_catedratico,
						'is_jefe_proyvinc' => $is_jefe_proyvinc,
						'is_jefe_depto_academico' => $is_jefe_depto_academico
			));

		}else{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
				$modelVaSolicitudesVisitasAcademicas->attributes = $_GET['VaSolicitudesVisitasAcademicas'];

			$this->render('listaVSolicitudesVisitasAcademicas',array(
						  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						  'lista_deptos' => $lista_deptos,
						  'is_jefe_proyvinc' => $is_jefe_proyvinc,
						  'is_jefe_depto_academico' => $is_jefe_depto_academico
			));
		}
	}

	public function actionDetalleDHistoricoSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$VaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$VaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$VaSolicitudesVisitasAcademicas->attributes=$_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleDHistoricoSolicitudVisitaAcademica',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'periodo' => $periodo,
						'fecha_creac_sol' => $fecha_creac_sol,
						'fecha_sal' => $fecha_sal,
						'fecha_reg' => $fecha_reg,
						'tipo_visita_acad' => $tipo_visita_acad,
						'empresa' => $empresa,
						'id' => $id,
						'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
						'VaSolicitudesVisitasAcademicas' => $VaSolicitudesVisitasAcademicas,
						'rfcEmpleado' => $rfcEmpleado
		));
	}

	public function actionDetalleVValidadaSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$VaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$VaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$VaSolicitudesVisitasAcademicas->attributes=$_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleVValidadaSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'periodo' => $periodo,
					  'fecha_creac_sol' => $fecha_creac_sol,
					  'fecha_sal' => $fecha_sal,
					  'fecha_reg' => $fecha_reg,
					  'tipo_visita_acad' => $tipo_visita_acad,
					  'empresa' => $empresa,
					  'id' => $id,
					  'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					  'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
					  'VaSolicitudesVisitasAcademicas' => $VaSolicitudesVisitasAcademicas
		));
	}

	public function actionDetalleHistoricoSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);

		//Total Mensajes de la solicitud de Visitas Academicas
		$total_mensajes = $modelVaSolicitudesVisitasAcademicas->observaciones_solicitud;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes=$_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes=$_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$VaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$VaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$VaSolicitudesVisitasAcademicas->attributes=$_GET['VaMateriasImparteResponsableVisitaAcademica'];


		$this->render('detalleHistoricoSolicitudVisitaAcademica',array(
					 'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					 'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					 'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
					 'VaSolicitudesVisitasAcademicas' => $VaSolicitudesVisitasAcademicas,
					 'id' => $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas,
					 'periodo' => $periodo,
					 'empresa' => $empresa,
					 'fecha_sal' => $fecha_sal,
					 'fecha_reg' => $fecha_reg,
					 'tipo_visita_acad' => $tipo_visita_acad,
					 'total_mensajes' => $total_mensajes
        ));
	}

	public function actionLiberarSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
			$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 6; //Finalizada

			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
				//Si se realizo correctamente el cambio
				$transaction->commit();
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}
	}

	//Historico de Solicitudes
	public function actionListaHistoricoSolicitudesVisitasAcademicas()
	{
		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];

        $this->render('listaHistoricoSolicitudesVisitasAcademicas',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  //'lista_periodos' => $lista_periodos,
					  //'lista_anios_sol' => $lista_anios_sol
        ));
	}

	public function getPeriodos()
	{
		$qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" > '0' ";

		$periodo = Yii::app()->db->createCommand($qry_per)->queryAll();
		$tam = sizeof($periodo);

		$list_per = array();
		for($i = 0; $i < $tam; $i++)
		{
			echo $periodo[$i]['numPeriodo'].'<br>';
			//$list_per[$periodo[$i]['numPeriodo']] = $list_per[$periodo[$i]['dscPeriodo']];
		}

		return $list_per;
	}

	public function getAniosValidos()
	{
		$qry_anios = "select DISTINCT(anio) as anio from pe_vinculacion.va_solicitudes_visitas_academicas where id_solicitud_visitas_academicas > 0 ";

		$anios = Yii::app()->db->createCommand($qry_anios)->queryAll();
		$tam = sizeof($anios);

		$list_anios = array();
		for($i = 0; $i < $tam; $i++)
		{
			echo $anios[$i]['anio'];
			//$list_anios[$anios[$i]['anio']] = $list_anios[$anios[$i]['anio']];
		}

		return $list_anios;
	}

	//Solicitudes a liberar en el perido actual
	public function actionListaLiberacionSolicitudVisitaAcademica()
	{
		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		//Obtener el Año y Periodo Actual
		$_periodo = Yii::app()->db->createCommand("select periodoactual(0)")->queryAll();
		$periodo = $_periodo[0]['periodoactual'];
		$_anio = Yii::app()->db->createCommand("select periodoactual(1)")->queryAll();
		$anio = $_anio[0]['periodoactual'];


        if(isset($_GET['VaSolicitudesVisitasAcademicas']))
            $modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];

        $this->render('listaLiberacionSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'periodo' => $periodo,
					  'anio' => $anio
        ));
	}

	//Mostrar el oficio de confirmacion de la Empresa donde se valida la Visita Academica (PENDIENTE para mostrar en MODAL)
	public function actionShowOficioConfirmacionEmpresa($id_solicitud_visitas_academicas)
	{
		$this->layout='//layouts/mainVacio';

		$criteria = new CDbCriteria;
		$criteria->condition = "id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);

		$this->render('showOficioConfirmacionEmpresa',array(
					  'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas
		));

	}

	//Modal de validacion de la Solicitud del Jefe Depto Academico
	public function actionValidaJefeDeptoAcademico($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			$criteria = new CDbCriteria;
			$criteria->condition = "id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
			$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);
			$modelVaSolicitudesVisitasAcademicas->valida_jefe_depto_academico = date('Y-m-d H:i:s');
			//$modelVaSolicitudesVisitasAcademicas->valida_subdirector_academico = date('Y-m-d H:i:s'); //PARA PRUEBAS

			//Verificamos que la Solicitud NO este en estatus SUSPENDIDA (3)
			if($modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica != 3)
			{
				if($modelVaSolicitudesVisitasAcademicas->save())
				{
					Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
					//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! no se pudo validar la solicitud.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
				}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se validó la solicitud porque esta en estatus SUSPENDIDA");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );

			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Validar la solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}
	}

	//Modal de Validacion de Solicitud de Subdirector Academico
	public function actionValidaSubdirectorAcademico($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			$criteria = new CDbCriteria;
			$criteria->condition = "id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
			$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);
			$modelVaSolicitudesVisitasAcademicas->valida_subdirector_academico = date('Y-m-d H:i:s');

			//Verificamos que la Solicitud NO este en estatus SUSPENDIDA (3)
			if($modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica != 3 OR $modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica != 4)
			{
				if($modelVaSolicitudesVisitasAcademicas->save())
				{
					Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
					//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! no se validó la solicitud..");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
				}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se validó la solicitud porque esta en estatus SUSPENDIDA");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Validar la solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}

	}

	//rfc del jefe de proyectos de vinculacion
	public function getRFCJefeDeptoAcademico()
	{
		$qry_jefe_acad = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '13' ";

		$rs = Yii::app()->db->createCommand($qry_jefe_acad)->queryAll();

		return $rs[0]['rfcEmpleado'];
	}

	//rfc del subdirector academico
	public function getRFCSubdirectorAcademico()
	{
		$qry_subd_acad = "select * from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '02' ";

		$rs = Yii::app()->db->createCommand($qry_subd_acad)->queryAll();

		return $rs[0]['rfcEmpleado'];

	}

	//Lista de Solicitudes vista Jefe Proyectos Vinculacion por departamento academico
	public function actionListaVSolicitudesVisitasAcademicas($cveDepto = null)
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		//Comprobar que es Jefe de Proyectos de Vinculacion
		$is_jefe_proyvinc = $this->esJefeProyectosVinculacion($rfcEmpleado);

		//Obtener Departamento Catedratico
		$depto_catedratico = $this->getDeptoCatedratico($rfcEmpleado);
		//echo $depto_catedratico;
		//die;
		//Comprobra si es jefe de departamento academico
		$is_jefe_depto_academico = $this->esJefeDepartamentoAcademico($depto_catedratico, $rfcEmpleado);

		//Lista de deptos academicos
		$lista_deptos = $this->getDepartamentosAcademicos();

		if($is_jefe_proyvinc == true || $is_jefe_depto_academico == true)
		{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes = $_GET['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->cveDepto = $_GET['VaSolicitudesVisitasAcademicas']['cveDepto'];
				$cveDepto = $modelVaSolicitudesVisitasAcademicas->cveDepto;
			}

			$this->render('listaVSolicitudesVisitasAcademicas',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'rfcEmpleado' => trim($rfc_empleado),
						'depto_catedratico' => $depto_catedratico,
						'lista_deptos' => $lista_deptos,
						'cveDepto' => $cveDepto,
						'is_jefe_proyvinc' => $is_jefe_proyvinc,
						'is_jefe_depto_academico' => $is_jefe_depto_academico
			));

		}else{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
				$modelVaSolicitudesVisitasAcademicas->attributes = $_GET['VaSolicitudesVisitasAcademicas'];

			$this->render('listaVSolicitudesVisitasAcademicas',array(
						  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						  'lista_deptos' => $lista_deptos,
						  'is_jefe_proyvinc' => $is_jefe_proyvinc,
						  'is_jefe_depto_academico' => $is_jefe_depto_academico
			));
		}
	}
	//Lista de Solicitudes vista Jefe Proyectos Vinculacion por departamento academico

	//Si es jefe de proyectos de vinculacion de algun departamento academico
	public function esJefeProyectosVinculacion($rfcEmpleado)
	{
		//Jefe Proyectos Vinculacion
		$qry_jef_proyvinc = "select * from public.h_ocupacionpuesto
								where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '04' AND
								\"cvePuestoParticular\" = '33'
							";

		$jefe_proy_vinc = Yii::app()->db->createCommand($qry_jef_proyvinc)->queryAll();

		return (sizeof($jefe_proy_vinc) > 0) ? true : false;
	}

	public function esJefeDepartamentoAcademico($depto_acad, $rfcEmpleado)
	{
		/*echo $depto_acad."<br>";
		echo $rfcEmpleado;
		die;*/

		$qry_jefe_depto_acad = "select * from public.\"H_empleados\" hemp
								join public.\"H_departamentos\" dep
								on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
								where dep.\"cveDeptoAcad\" = '$depto_acad' AND hemp.\"cvePuestoGeneral\" = '03' AND 
								hemp.\"rfcEmpleado\" = '$rfcEmpleado'
							";

		$jefe_depto_acad = Yii::app()->db->createCommand($qry_jefe_depto_acad)->queryAll();

		return (sizeof($jefe_depto_acad) > 0 or !empty($jefe_depto_acad)) ? true : false;
	}

	public function getDeptoCatedratico($rfcEmpleado)
	{
		//Paras saber si es jefe de proyecto de su departamento
		$qry_puest = "select hemp.\"rfcEmpleado\", dep.\"cveDeptoAcad\", dep.\"cveDepartamento\"
						from public.\"H_empleados\" hemp
						join public.\"H_departamentos\" dep
						on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
						where hemp.\"cvePuestoGeneral\" = '04' AND hemp.\"cvePuestoParticular\" = '33' AND
							hemp.\"rfcEmpleado\" = '$rfcEmpleado' ";

		$rs = Yii::app()->db->createCommand($qry_puest)->queryAll();

		if(sizeof($rs) > 0)
		{
			if($rs[0]['cveDeptoAcad'] > 0)
				return trim($rs[0]['cveDeptoAcad']); //1
			else
				return 0;

		}else{

			return 0;
		}

	}

	//Lista de Solicitudes Subdirector Academico
	public function actionListaSSolicitudesVisitasAcademicas($cveDepto=null)
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSubdirectorAcademico'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);
		//Comprobar que es el subdirector academico
		$is_subd_acad = $this->esSubdirectorAcademico($rfcEmpleado);
		//Lista de Departamentos para filtros
		$lista_deptos = $this->getDepartamentosAcademicos();

		if($is_subd_acad)
		{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('searchListaSolicitudesVisitasAcademicasVigentesSubdAcademico');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes = $_GET['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->cveDepto = $_GET['VaSolicitudesVisitasAcademicas']['cveDepto'];
				$cveDepto = $modelVaSolicitudesVisitasAcademicas->cveDepto;

			}

			$this->render('listaSSolicitudesVisitasAcademicas',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'lista_deptos' => $lista_deptos,
						'cveDepto' => $cveDepto,
						'is_subd_acad' => $is_subd_acad
			));

		}else{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
				$modelVaSolicitudesVisitasAcademicas->attributes = $_GET['VaSolicitudesVisitasAcademicas'];


			$this->render('listaSSolicitudesVisitasAcademicas',array(
						  'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
						  'lista_deptos' => $lista_deptos,
						  'rfcEmpleado' => $rfcEmpleado,
						  'is_subd_acad' => $is_subd_acad,
						  'cveDepto' => $cveDepto
			));
		}
	}

	public function esSubdirectorAcademico($rfcEmpleado)
	{
		$qry_subd_acad = "select * from public.h_ocupacionpuesto
							where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '02'
							AND \"cvePuestoParticular\" = '02' ";

		$subd_acad = Yii::app()->db->createCommand($qry_subd_acad)->queryAll();

		return (sizeof($subd_acad) > 0) ? true : false;
	}

	//Lista de Solicitudes vista Docente
	public function actionListaSolicitudesVisitasAcademicas()
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;

		//Año y periodo vigente
		$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //periodo actual
		$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual


		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
		$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];

		$this->render('listaSolicitudesVisitasAcademicas',array(
					'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					'rfc_empleado' => trim($rfc_empleado),
					'anio' => trim($anio[0]['periodoactual']),
					'periodo' => $this->getPeriodo(trim($periodo[0]['periodoactual']))
		));
	}

	public function actionListaJVSolicitudesVisitasAcademicas($cveDepto = null)
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcOficinaVisitasIndustriales'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		//Lista deptos academicos
		$lista_deptos = $this->getDepartamentosAcademicos();

		//Es jefe de servicios externos
		$is_jefe_serext = $this->isJefeServiciosExternos($rfcEmpleado);

		if($is_jefe_serext == true)
		{
			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->cveDepto = $_GET['VaSolicitudesVisitasAcademicas']['cveDepto'];
				$cveDepto = $modelVaSolicitudesVisitasAcademicas->cveDepto;
			}

			$this->render('listaJVSolicitudesVisitasAcademicas',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'lista_deptos' => $lista_deptos,
						'cveDepto' => $cveDepto,
						'is_jefe_serext' => $is_jefe_serext
			));

		}else{

			$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas('search');
			$modelVaSolicitudesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaSolicitudesVisitasAcademicas']))
				$modelVaSolicitudesVisitasAcademicas->attributes=$_GET['VaSolicitudesVisitasAcademicas'];

			$this->render('listaJVSolicitudesVisitasAcademicas',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'lista_deptos' => $lista_deptos,
						'is_jefe_serext' => $is_jefe_serext

			));

		}
	}

	//es jefe oficina de visitas academicas
	public function isJefeServiciosExternos($rfcEmpleado)
	{
		$qry_jefe_servext = "select * from public.h_ocupacionpuesto
								where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '04' AND
								\"cvePuestoParticular\" = '03' AND \"cveDepartamentoEmp\" = '06'
							";

		$jefe_servext = Yii::app()->db->createCommand($qry_jefe_servext)->queryAll();

		return (sizeof($jefe_servext) > 0) ? true : false;
	}

	//Lista de Deptos Academicos
	public function getDepartamentosAcademicos()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " \"cveDeptoAcad\" != -1 AND \"cveDeptoAcad\" != 99 ";
		$criteria->order = " \"cveDepartamento\" ASC ";
		$modelHDepartamentos = HDepartamentos::model()->findAll($criteria);

		$lista_deptos = CHtml::listData($modelHDepartamentos, 'cveDeptoAcad', 'dscDepartamento');

		return $lista_deptos;
	}

	//Creacion de la Solicitud para visita academicas
	public function actionNuevaSolicitudVisitaAcademica()
	{

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;
		$rfc_responsable[trim($rfc_empleado)] = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas;
		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica;

		//Datos del formulario
		$tipos_visitas = $this->getTipoVisitas();

		//Datos del formulario
		$lista_empresas_visitas = $this->getEmpresasVisitas();

		$info_empleado = $this->infoEmpleado($rfc_empleado);

		//print_r($info_empleado);
		//die;

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		if(isset($_POST['VaSolicitudesVisitasAcademicas'], $_POST['VaMateriasImparteResponsableVisitaAcademica']))
		{
			//die;
			$modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];
			//$modelVaResponsablesVisitasAcademicas->attributes = $_POST['VaResponsablesVisitasAcademicas'];
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_POST['VaMateriasImparteResponsableVisitaAcademica'];

			//Periodo y Año
			$periodo = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
			$anio = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual
			$modelVaSolicitudesVisitasAcademicas->periodo = trim($periodo[0]['periodoactual']); //periodo actual
			$modelVaSolicitudesVisitasAcademicas->anio = trim($anio[0]['periodoactual']); //Año actual
			$modelVaSolicitudesVisitasAcademicas->no_solicitud = trim($this->getNumeroSolicitud());
			$modelVaSolicitudesVisitasAcademicas->fecha_creacion_solicitud = date('Y-m-d H:i:s'); //Fecha de creacion de la Solicitud
			$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud = null;
			$modelVaSolicitudesVisitasAcademicas->valida_jefe_depto_academico = null;
			$modelVaSolicitudesVisitasAcademicas->valida_subdirector_academico = null;
			$modelVaSolicitudesVisitasAcademicas->ultima_fecha_modificacion = $modelVaSolicitudesVisitasAcademicas->fecha_creacion_solicitud;
			$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 1; //PENDIENTE

			//MAximo numero de solicitudes a visitas academicas por docente
			//id debe ser 1 (DEFAULT)
			$modelVaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk(1);
			if($modelVaConfiguracionModVisitasAcademicas === NULL)
				throw new CHttpException(404,'No existen datos de la configuración.');

			try
			{
				
				//Validar que el docente pueda hacer maximo 5 (DEFAULT) solicitudes por semestre
				if($this->numeroMaximoDeSolicitudesXDocente(trim($rfc_empleado)) < $modelVaConfiguracionModVisitasAcademicas->max_numero_solicitudes_vigentes_por_docente)
				{
					//Se validan las fechas de salida y regreso de la Visita Academica
					if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
					{
						if($this->isEnteroPositivo(trim($modelVaSolicitudesVisitasAcademicas->no_alumnos)))
						{
							if($this->maximoNumeroAlumnosSolicitud(trim($modelVaSolicitudesVisitasAcademicas->no_alumnos)))
							{
								if($modelVaSolicitudesVisitasAcademicas->save())
								{
									$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas;
									$modelVaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;
									$modelVaResponsablesVisitasAcademicas->rfcEmpleado = trim($rfc_empleado);
									$modelVaResponsablesVisitasAcademicas->fecha_registro_responsable = date('Y-m-d H:i:s');
									$modelVaResponsablesVisitasAcademicas->responsable_principal = true;
									$modelVaResponsablesVisitasAcademicas->cve_depto_acad = $info_empleado->deptoCatedratico;
									$modelVaResponsablesVisitasAcademicas->cve_depto = $info_empleado->cveDepartamentoEmp;

									if($modelVaResponsablesVisitasAcademicas->save())
									{

										$modelVaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;
										$modelVaMateriasImparteResponsableVisitaAcademica->fecha_registro_materia_visita_academica = date('Y-m-d H:i:s');

										if($modelVaMateriasImparteResponsableVisitaAcademica->save())
										{
											Yii::app()->user->setFlash('success', "Registro de Solicitud realizado correctamente!!!");
											//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
											$transaction->commit();
											$this->redirect(array('listaSolicitudesVisitasAcademicas'));

										}else{

											//die('7');
											Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos de la Solicitud.");
											//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
											$transaction->rollback();

										}

									}else{
										//die('6');
										Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos de la Solicitud.");
										//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
										$transaction->rollback();

									}

								}else{

									//die('5');
									Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos de la Solicitud.");
									//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
									$transaction->rollback();
								}

							}else{

								//die('4');
								Yii::app()->user->setFlash('danger', "Error!!! Sobrepasaste el maximo de alumnos permitidos en una Solicitud, maximo deben pueden ser 80 alumnos.");
								//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
								$transaction->rollback();
							}

						}else{

							//die('3');
							Yii::app()->user->setFlash('danger', "Error!!! El numero de alumnos que realizaran la Visita Académica debe ser entero positivo.");
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
						}

					}else{

						//die('2');
						Yii::app()->user->setFlash('danger', "Error!!! la Fecha de Salida de la Solicitud no puede ser mayor a la Fecha de Regreso de la Solicitud.");
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();

					}

				}else{

					//die('sol exc');
					Yii::app()->user->setFlash('danger', "Error!!! Has excedido el numero maximo de Solicitudes por Docente en el mismo periodo.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
				}

			}catch(Exception $e){
				//die('1s');
				Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos de la Solicitud.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();

			}
		}

		$this->render('nuevaSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
					  //'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					  'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
					  'tipos_visitas' => $tipos_visitas,
					  'lista_empresas_visitas' => $lista_empresas_visitas,
					  'rfc_responsable' => $rfc_responsable,
					  'info_empleado' => $info_empleado,
					  'materias_asignadas_responsable' => $this->getMateriasDepartamentoResponsableSolicitudVisitaAcademica($info_empleado->deptoCatedratico),
					  'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
					  'cargo' => $this->getCargo($info_empleado->rfcEmpleado)
		));
	}

	public function numeroMaximoDeSolicitudesXDocente($rfcEmpleado)
	{
		//Obtener periodo y Año
		$per = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
		$an = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual

		$periodo = trim($per[0]['periodoactual']); //periodo actual
		$anio = trim($an[0]['periodoactual']); //periodo actual

		//Verificar que el docente no tenga mas de 5 solicitudes en el mismo perido y año
		$qry_max_sol = "select count(sva.id_solicitud_visitas_academicas) as sol_vigentes
						from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where rva.\"rfcEmpleado\" = '$rfcEmpleado' AND rva.responsable_principal = true AND
						sva.periodo = '$periodo' AND sva.anio = '$anio' AND
						(sva.id_estatus_solicitud_visita_academica = 1 AND sva.id_estatus_solicitud_visita_academica = 2 AND
						sva.id_estatus_solicitud_visita_academica = 3)
					";

		$max_sol_docente = Yii::app()->db->createCommand($qry_max_sol)->queryAll();

		return $max_sol_docente[0]['sol_vigentes'];
	}

	//Maximo numero de alumnos por Solicitud
	public function maximoNumeroAlumnosSolicitud($valor)
	{
		//id = 1 (DEFAULT)
		$id = 1;
		$modelVaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk($id);
		if($modelVaConfiguracionModVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existe información de la configuración.');

		return ($valor <= $modelVaConfiguracionModVisitasAcademicas->max_numero_alumnos_por_visita_academica) ? true : false;
	}

	//Valida que el dato introducido sea un numero entero
    public function isEnteroPositivo($valor)
	{
  		$bandera = true;

  		if(!preg_match('/^[0-9]+$/', $valor)) //Si entra entonces es entero
  			$bandera = false;

  		return $bandera;
	}

	public function getNumeroSolicitud()
	{
		$id_sig = 0;
		$qry_id = "select MAX(no_solicitud) as id_max from pe_vinculacion.va_solicitudes_visitas_academicas";

		$rs = Yii::app()->db->createCommand($qry_id)->queryAll();
		$id_sig = $rs[0]['id_max'];

		if($id_sig === null or $id_sig === 0)
			$id_sig = 1;
		else
			$id_sig = $id_sig + 1;

		return $id_sig;
	}

	public function infoEmpleado($rfc_empleado)
	{
		$rfcEmpleado = trim($rfc_empleado);

		$modelHEmpleados = HEmpleados::model()->findByPk($rfcEmpleado);
		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No hay datos del Empleado.');

		return $modelHEmpleados;
	}

	public function getMateriasDepartamentoResponsableSolicitudVisitaAcademica($depto_catedratico)
	{
		//$rfcEmpleado = trim($rfc_empleado);
		$deptoCatedratico = trim($depto_catedratico);

		$qry_materias = "select hemp.\"rfcEmpleado\", ecm.\"cveMateria\", ecm.\"dscMateria\", ecm.\"deptoMateria\"
						from public.\"H_empleados\" hemp
						join public.\"E_gruposMaterias_new\" egm
						on egm.\"rfcEmpleadoMat\" = hemp.\"rfcEmpleado\"
						join public.\"E_catalogoMaterias\" ecm
						on ecm.\"cveMateria\" = egm.\"cveMateriaGpo\"
						where hemp.\"deptoCatedratico\" = '$deptoCatedratico' AND ecm.\"statMateria\" = true ";

		$listas_materias_responsable = Yii::app()->db->createCommand($qry_materias)->queryAll();
		$tam = sizeof($listas_materias_responsable);

		for($a=0; $a < $tam; $a++)
		{
			$lista_materias[$listas_materias_responsable[$a]['cveMateria']] = $listas_materias_responsable[$a]['cveMateria'] .'  -  '. $listas_materias_responsable[$a]['dscMateria'];
		}

		return $lista_materias;

	}

	//Cuando Roxana rechaza una solicitud de Visita Academica
	public function actionRechazarJVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcOficinaVisitasIndustriales']; // RFC del Roxana
		$rfc_empleado = Yii::app()->user->name;

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//Obtener el rfc del empleado que realizo la solicitud
		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;
		$criteria = new CDbCriteria;
		$criteria->condition = "id_solicitud_visitas_academicas = '$id' AND responsable_principal = true";
		$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->find($criteria);

		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		if(isset($_POST['VaSolicitudesVisitasAcademicas']))
        {

			$modelVaSolicitudesVisitasAcademicas->attributes=$_POST['VaSolicitudesVisitasAcademicas'];
			//Mensaje cuando se rachaza una solicitud
			$nombre_mensajero = $this->getNombreRechazaSolicitudVisitaAcademica($rfc_empleado);
			$header_mensaje = "El Empleado ".$nombre_mensajero.' con el RFC ( '.$rfc_empleado.' ) rechazó la Solicitud el dia y hora '.date('Y-m-d H:i:s').' '.'<br>';
			$motivo_rech = "<br>El Motivo: <br>";
			$body_mensaje = $header_mensaje.$motivo_rech.$modelVaSolicitudesVisitasAcademicas->observaciones_solicitud.'<br><br>';

			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Error!!! no se guardaron los datos de la Solicitud.");
				$this->redirect(array('listaJVSolicitudesVisitasAcademicas'));


			}else{

				Yii::app()->user->setFlash('success', "Error!!! no se guardaron los datos de la Solicitud.");

			}
        }

		$this->render('rechazarJVSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'fecha_creac_sol' => $fecha_creac_sol,
					  'fecha_sal' => $fecha_sal,
					  'fecha_reg' => $fecha_reg,
					  'tipo_visita_acad' => $tipo_visita_acad,
					  'empresa' => $empresa,
					  'rfcEmpleado' => $modelVaResponsablesVisitasAcademicas->rfcEmpleado
		));
	}

	public function getNombreRechazaSolicitudVisitaAcademica($rfcEmpleado)
	{
		$modelHEmpleados = HEmpleados::model()->findByPk($rfcEmpleado);

		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No hay datos del Empleado.');

		return $modelHEmpleados->nmbEmpleado.' '.$modelHEmpleados->apellPaterno.' '.$modelHEmpleados->apellMaterno;
	}

	public function actionDetalleSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Roxana
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);

		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'periodo' => $periodo,
					  'fecha_creac_sol' => $fecha_creac_sol,
					  'fecha_sal' => $fecha_sal,
					  'fecha_reg' => $fecha_reg,
					  'tipo_visita_acad' => $tipo_visita_acad,
					  'empresa' => $empresa,
					  'id' => $id,
					  'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					  'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica
		));
	}

	public function actionDetalleVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleVSolicitudVisitaAcademica',array(
					'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					'periodo' => $periodo,
					'fecha_creac_sol' => $fecha_creac_sol,
					'fecha_sal' => $fecha_sal,
					'fecha_reg' => $fecha_reg,
					'tipo_visita_acad' => $tipo_visita_acad,
					'empresa' => $empresa,
					'id' => $id,
					'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica
		));
	}

	public function actionDetalleSSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		/*$VaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
												'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas//,
												//'rfcEmpleado' => $rfcEmpleado
											));

		if($VaResponsablesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No hay datos del Responsable de esa Solicitud.');

		$VaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
												'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_visitas_academicas//,
												//'rfcEmpleado' => $rfcEmpleado
											));

		if($VaMateriasImparteResponsableVisitaAcademica === NULL)
			throw new CHttpException(404,'No hay datos de la Materia que cubre la Solicitud.');*/

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$lista_responsables = $this->getListaResponsablesVisitasAcademicas();

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleSSolicitudVisitaAcademica',array(
					'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					'periodo' => $periodo,
					'fecha_creac_sol' => $fecha_creac_sol,
					'fecha_sal' => $fecha_sal,
					'fecha_reg' => $fecha_reg,
					'tipo_visita_acad' => $tipo_visita_acad,
					'empresa' => $empresa,
					'id' => $id,
					'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					/*'VaResponsablesVisitasAcademicas' => $VaResponsablesVisitasAcademicas,
					'lista_responsables' => $lista_responsables,
					'VaMateriasImparteResponsableVisitaAcademica' => $VaMateriasImparteResponsableVisitaAcademica,
					'materias_asignadas_responsable' => $this->getMateriasDepartamentoResponsableSolicitudVisitaAcademica($info_empleado->deptoCatedratico),*/
					'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica
		));
	}

	public function actionDetalleJVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		//formato a la informacion
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$fecha_creac_sol = InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);

		$id = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;

		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('detalleJVSolicitudVisitaAcademica',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'periodo' => $periodo,
						'fecha_creac_sol' => $fecha_creac_sol,
						'fecha_sal' => $fecha_sal,
						'fecha_reg' => $fecha_reg,
						'tipo_visita_acad' => $tipo_visita_acad,
						'empresa' => $empresa,
						'id' => $id,
						'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica
		));
	}

	//Jefe de Proyectos de Vinculación
	public function actionImprimirSolicitudVisitaAcademica()
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		//Comprobar que es Jefe de Proyectos de Vinculacion
		$is_jefe_proyvinc = $this->esJefeProyectosVinculacion($rfcEmpleado);
		//Obtener Departamento Catedratico
		$depto_catedratico = $this->getDeptoCatedratico($rfcEmpleado);

		//Periodo y Año
		$per = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
		$an = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual
		$periodo = trim($per[0]['periodoactual']); //periodo actual
		$anio = trim($an[0]['periodoactual']); //Año actual

		//Obtener el Departamento Academico correspondiente
		$criteria = new CDbCriteria;
		$criteria->condition = " \"cveDeptoAcad\" = '$depto_catedratico' ";
		$modelHDepartamentos = HDepartamentos::model()->find($criteria);

		//Solicitudes validadas por el jefe de proyectos de vinculacion y el subdirector academico
		$qry_val_solicitudes = "select
								sva.no_solicitud,
								( select nombre from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita) as empresa,
								( select desc_estado from pe_vinculacion.g_rpempresas emp
									join public.\"X_estados\" est
									on est.id_estado = emp.idestado
									where emp.idempresa = sva.id_empresa_visita) as estado,
								sva.area_a_visitar,
								sva.no_alumnos,
								to_char(sva.fecha_hora_salida_visita::date, 'DD-MM-YYYY') as fec_salida,
								to_char(sva.fecha_hora_regreso_visita::date, 'DD-MM-YYYY') as fec_regreso,
								(
								select esp.\"dscEspecialidad\"
								from pe_vinculacion.va_solicitudes_visitas_academicas s
								join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
								on aava.id_solicitud_visitas_academicas = s.id_solicitud_visitas_academicas
								join public.\"E_datosAlumno\" eda
								on eda.\"nctrAlumno\" = aava.\"nctrAlumno\"
								join public.\"E_especialidad\" esp
								on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
								where s.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								limit 1
								) as carrera,
								(select hemp.\"nmbCompletoEmp\" from pe_vinculacion.va_responsables_visitas_academicas r
									join public.\"H_empleados\" hemp
									on hemp.\"rfcEmpleado\" = r.\"rfcEmpleado\"
									where r.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas AND
									r.responsable_principal = true
								) as docente_responsable,
								(
								select '( '|| ecm.\"cveMateria\" || ' )' || ' ' || ecm.\"dscMateria\"
								from pe_vinculacion.va_materias_imparte_responsable_visita_academica mir
								join public.\"E_catalogoMaterias\" ecm
								on ecm.\"cveMateria\" = mir.\"cveMateria\"
								where mir.id_solicitud_materias_responsable_visitas_academicas = sva.id_solicitud_visitas_academicas
								limit 1
								) as asignaturas
								from pe_vinculacion.va_solicitudes_visitas_academicas sva
								join pe_vinculacion.va_responsables_visitas_academicas rva
								on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								where rva.cve_depto_acad = '$depto_catedratico' AND sva.periodo = '$periodo' AND sva.anio = '$anio' AND
								sva.valida_jefe_depto_academico is not null
							";

		$lista_sol_validadas = Yii::app()->db->createCommand($qry_val_solicitudes)->queryAll();

		//Validacion del Subdirector Academico
		$subd_academico = $this->getSubdirectorAcademico();

		//Prueba envio de correos
		//$this->enviarCorreoConfirmacionEliminacion();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-L',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'L'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Marca de Agua
		//$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		//$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$qry_cod_cal = "select * from pe_vinculacion.va_codigos_calidad_mod_visitas_academicas where id_codigo_calidad_mod_va = 1 ";
		$rs = Yii::app()->db->createCommand($qry_cod_cal)->queryAll();
		$c_calidad = $rs[0]['codigo_calidad'];
		$revision = "Revisión ".$rs[0]['revision'];

		$footer =
		"<table class='bold' style='border-top: 5px solid #1B5E20; border-left: 1px solid #FFFFFF; border-collapse: collapse;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 12px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 12px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Solicitud Visita Académica');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirSolicitudVisitaAcademica', array(
						   'modelHDepartamentos' => $modelHDepartamentos,
						   'fec_actual' => $this->getFormatoFechaReportePDF(),
						   'periodo' => $periodo,
						   'lista_sol_validadas' => $lista_sol_validadas,
						   'anio' => $anio,
						   'subd_academico' => $subd_academico,
						   //'val_jefe_proy_vinc' => $this->getJefeProyectosVinculacion($rfcEmpleado),
						   'val_jefe_depto_academico' => $this->jefeDepartamentoAcademico($depto_catedratico),
						   'val_subd_academico' => $subd_academico

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Solicitud Visita Académica.pdf', 'I');
	}

	public function jefeDepartamentoAcademico($depto_academico)
	{
		$qry_jefe_depto_acad = " select hemp.\"nmbCompletoEmp\" as name_jefe
									from public.\"H_empleados\" hemp
									join public.\"H_departamentos\" dep
									on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
									where dep.\"cveDeptoAcad\" = '$depto_academico' AND hemp.\"cvePuestoGeneral\" = '03'
								";

		$jefe_depto = Yii::app()->db->createCommand($qry_jefe_depto_acad)->queryAll();

		return ($jefe_depto == NULL OR empty($jefe_depto)) ? "--" : $jefe_depto[0]['name_jefe'];
	}

	//Vista de las Solicitudes por parte del Subdirector Academico
	public function actionImprimirSSolicitudVisitaAcademica()
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Periodo y Año
		$per = Yii::app()->db->createCommand("select public.periodoactual(0)")->queryAll(); //periodo actual
		$an = Yii::app()->db->createCommand("select public.periodoactual(1)")->queryAll(); //Año actual
		$periodo = trim($per[0]['periodoactual']); //periodo actual
		$anio = trim($an[0]['periodoactual']); //Año actual

		//lista de las solicitudes validadas por el subdirector academico
		$qry_sol_validadas = "select
								sva.no_solicitud,
								( select nombre from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita) as empresa,
								( select desc_estado from pe_vinculacion.g_rpempresas emp
									join public.\"X_estados\" est
								    on est.id_estado = emp.idestado
									where emp.idempresa = sva.id_empresa_visita) as estado,
								sva.area_a_visitar,
								sva.no_alumnos,
								to_char(sva.fecha_hora_salida_visita::date, 'DD-MM-YYYY') as fec_salida,
								to_char(sva.fecha_hora_regreso_visita::date, 'DD-MM-YYYY') as fec_regreso,
								(
								  select esp.\"dscEspecialidad\"
								  from pe_vinculacion.va_solicitudes_visitas_academicas s
								  join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
								  on aava.id_solicitud_visitas_academicas = s.id_solicitud_visitas_academicas
								  join public.\"E_datosAlumno\" eda
								  on eda.\"nctrAlumno\" = aava.\"nctrAlumno\"
								  join public.\"E_especialidad\" esp
								  on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
								  where s.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								  limit 1
								) as carrera,
								(select hemp.\"nmbCompletoEmp\" from pe_vinculacion.va_responsables_visitas_academicas r
									join public.\"H_empleados\" hemp
								    on hemp.\"rfcEmpleado\" = r.\"rfcEmpleado\"
								    where r.id_solicitud_visitas_academicas = rva.id_solicitud_visitas_academicas AND
								    r.responsable_principal = true
								) as docente_responsable,
								(
								  select '( '|| ecm.\"cveMateria\" || ' )' || ' ' || ecm.\"dscMateria\"
								  from pe_vinculacion.va_materias_imparte_responsable_visita_academica mir
								  join public.\"E_catalogoMaterias\" ecm
								  on ecm.\"cveMateria\" = mir.\"cveMateria\"
								  where mir.id_solicitud_materias_responsable_visitas_academicas = sva.id_solicitud_visitas_academicas
								  limit 1
								) as asignaturas
								from pe_vinculacion.va_solicitudes_visitas_academicas sva
								join pe_vinculacion.va_responsables_visitas_academicas rva
								on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								where rva.cve_depto_acad = 8 AND sva.periodo = '2' AND sva.anio = 2019 AND
								sva.valida_subdirector_academico is not null
							";

		$lista_sol_validadas = Yii::app()->db->createCommand($qry_sol_validadas)->queryAll();

		$subd_academico = $this->getSubdirectorAcademico();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-L',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'L'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Marca de Agua
		//$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		//$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$qry_cod_cal = "select * from pe_vinculacion.va_codigos_calidad_mod_visitas_academicas where id_codigo_calidad_mod_va = 1 ";
		$rs = Yii::app()->db->createCommand($qry_cod_cal)->queryAll();
		$c_calidad = $rs[0]['codigo_calidad'];
		$revision = "Revisión ".$rs[0]['revision'];

		$footer =
		"<table class='bold' style='border-top: 5px solid #1B5E20; border-left: 1px solid #FFFFFF; border-collapse: collapse;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 12px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 12px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Solicitud Visita Académica');

        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirSSolicitudVisitaAcademica', array(
						   'fec_actual' => $this->getFormatoFechaReportePDF(),
						   'periodo' => $periodo,
						   'anio' => $anio,
						   'lista_sol_validadas' => $lista_sol_validadas,
						   'val_subd_academico' => $subd_academico
						   /*'subd_academico' => $subd_academico,
						   'val_jefe_proy_vinc' => $this->getJefeProyectosVinculacion($rfcEmpleado),
						   'val_subd_academico' => $subd_academico*/

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Solicitud Visita Académica.pdf', 'I');
	}

	public function getJefeProyectosVinculacion($rfcEmpleado)
	{
		$qry_jefe_proy_vinc = "select * from public.h_ocupacionpuesto
							where \"rfcEmpleado\" = '$rfcEmpleado' ";

		$jefe_proy_vinc = Yii::app()->db->createCommand($qry_jefe_proy_vinc)->queryAll();

		return (sizeof($jefe_proy_vinc) > 0) ? $jefe_proy_vinc[0]['nmbCompletoEmp'] : null;
	}

	public function getSubdirectorAcademico()
	{
		$qry_subd_acad = "select * from public.h_ocupacionpuesto
							where \"cvePuestoGeneral\" = '02' AND \"cvePuestoParticular\" = '02' ";

		$subd_acad = Yii::app()->db->createCommand($qry_subd_acad)->queryAll();

		return (sizeof($subd_acad) > 0) ? $subd_acad[0]['nmbCompletoEmp'] : null;
	}

	//Correo de informativo de cuando se cancela la Solicitud de Visita Academica
	public function enviarCorreoConfirmacionEliminacion()
	{
		/*$modelHEmpleados = HEmpleados::model()->findByPk();

		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No existe Empleado con ese RFC.');*/

		Yii::import('ext.phpmailer.JPhpMailer');
		Yii::import('ext.phpmailer.JPhpMailer');
		//require_once Yii::app()->basePath . '/extensions/phpmailer/JPhpMailer.php';

		$mail = new JPhpMailer(); 
		$mail->IsSMTP();
	    $mail->SMTPSecure = "tls"; //ssl  
	    //$mail->Host = 'smtp.gmail.com'; //Envio de correo
	    //$mail->Host = 'smtp.googlemail.com:465'; //Envio de correo
	    $mail->Host = "ssl://smtp.gmail.com"; // specify main and backup server
	    $mail->SMTPAuth = true; 
	    $mail->SMTPSecure = true; 
	    $mail->Username = 'javiramirez2014@gmail.com';
		$mail->Port = '587'; //465
	    $mail->Password = 't1mamameMima#'; 
	    $mail->Priority = 1;
	    $mail->SMTPKeepAlive = true;  
	    $mail->Mailer = "smtp"; 
	    //$mail->IsSMTP(); // telling the class to use SMTP  
	    $mail->SMTPAuth   = true;  
	    $mail->CharSet = 'utf-8';  
	    $mail->SMTPDebug  = 1; // debugging: 1 = errors and messages, 2 = messages only
	    $mail->SetFrom('javiramirez2014@gmail.com', 'Javier Ramirez');  //Quien envía
	    $mail->Subject = 'PHPMailer Test Subject via GMail, basic with authentication'; 
	    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; 
	    $mail->MsgHTML('<h1>JUST A TEST!</h1>'); 
	    $mail->AddAddress('javiramirez2014@gmail.com', 'Otro Javi Ramirez'); 
	    $mail->SetLanguage( 'en', 'phpmailer/language/' );
	    //$mail->Send();
	    if(!$mail->send()) 
	    {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   die;
		}

	    Yii::app()->user->setFlash('contact','Thank you for... as possible.');
	    $this->refresh();

	}

	public function actionImprimirListaEstudiantesAsistenAVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Consulta de datos para llenar el PDF
		$qry_1 = "select
					(select \"dscDepartamento\" from public.\"H_departamentos\" where \"cveDepartamento\" = rva.cve_depto_acad ) as depto_doc_responsable,
					sva.nombre_visita_academica,
					(select domicilio from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita) as domicilio,
					(select \"nmbCompletoEmp\" from public.\"H_empleados\" where \"rfcEmpleado\" = rva.\"rfcEmpleado\" ) docente_responsable,
					(select sva.fecha_hora_salida_visita::timestamp::date) as fecha_desde_visita,
					(select sva.fecha_hora_regreso_visita::timestamp::date) as fecha_hasta_visita,
					(
						(case when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
						(extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
						(case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
						(extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
						(case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)

					) as hora_desde,
					(
						(case when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
						(extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
						(case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
						(extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
						(case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)

					) as hora_hasta,
					(
						(case when sva.fecha_creacion_solicitud is null then ' ' ELSE(
							extract('day' from sva.fecha_creacion_solicitud) ||' de '|| ( case
						   when extract('month' from sva.fecha_creacion_solicitud)=1 then 'Enero'
						   when extract('month' from sva.fecha_creacion_solicitud)=2 then 'Febrero'
						   when extract('month' from sva.fecha_creacion_solicitud)=3 then 'Marzo'
						   when extract('month' from sva.fecha_creacion_solicitud)=4 then 'Abril'
						   when extract('month' from sva.fecha_creacion_solicitud)=5 then 'Mayo'
						   when extract('month' from sva.fecha_creacion_solicitud)=6 then 'Junio'
						   when extract('month' from sva.fecha_creacion_solicitud)=7 then 'Julio'
						   when extract('month' from sva.fecha_creacion_solicitud)=8 then 'Agosto'
						   when extract('month' from sva.fecha_creacion_solicitud)=9 then 'Septiembre'
						   when extract('month' from sva.fecha_creacion_solicitud)=10 then 'Octubre'
						   when extract('month' from sva.fecha_creacion_solicitud)=11 then 'Noviembre'
						   when extract('month' from sva.fecha_creacion_solicitud)=12 then 'Diciembre'
						   END)||' de '|| extract('year' from sva.fecha_creacion_solicitud) || ' a las ' || ( case
						   when extract('hour' from sva.fecha_creacion_solicitud) < 10 then '0' else '' end) ||
						   (extract('hour' from sva.fecha_creacion_solicitud)) || ':' ||
						   (case when extract('minute' from sva.fecha_creacion_solicitud) < 10 then '0' else '' end) ||
						   (extract('minute' from sva.fecha_creacion_solicitud)) || ' ' ||
						   (case when extract('hour' from sva.fecha_creacion_solicitud) >= 12 then 'PM' else 'AM' end)
				   		)end)
					) as valida_docente_responsable
					from pe_vinculacion.va_solicitudes_visitas_academicas sva
					join pe_vinculacion.va_responsables_visitas_academicas rva
					on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
					where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND rva.responsable_principal = true
				";

		$datos_visita = Yii::app()->db->createCommand($qry_1)->queryAll();

		//Lista de Alumnos que estan inscritos en la Solicitud con el ID especificado
		$qry_2 = "select
					(eda.\"nmbAlumno\") as nombre_alumno,
					(eda.\"nctrAlumno\") as no_control,
					(select \"dscEspecialidad\" from public.\"E_especialidad\" where \"cveEspecialidad\" = eda.\"cveEspecialidadAlu\" ) as carrera_alumno,
					(eda.\"semAlumno\") as semestre_alumno
					from public.\"E_datosAlumno\" eda
					join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
					on aava.\"nctrAlumno\" = eda.\"nctrAlumno\"
					where aava.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
				";

		$lista_alumnos = Yii::app()->db->createCommand($qry_2)->queryAll();

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '5',
			'margin_right' =>  '5',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-P',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Marca de Agua
		//$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		//$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$qry_cod_cal = "select * from pe_vinculacion.va_codigos_calidad_mod_visitas_academicas where id_codigo_calidad_mod_va = 2 ";
		$rs = Yii::app()->db->createCommand($qry_cod_cal)->queryAll();
		$c_calidad = $rs[0]['codigo_calidad'];
		$revision = "Revisión ".$rs[0]['revision'];

		$footer =
		"<table class='bold' style='border-top: 5px solid #1B5E20; border-left: 1px solid #FFFFFF; border-collapse: collapse;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='bold border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 14px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='bold border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 14px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Lista Estudiantes Asisten Visita Académica');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirListaEstudiantesAsistenAVisitaAcademica', array(
						  'datos_visita' => $datos_visita,
						  'lista_alumnos' => $lista_alumnos

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Lista Estudiantes Asisten Visita Académica.pdf', 'I');
	}

	public function actionImprimirFormatoSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//Consulta de datos para llenar el PDF
		$qry_sol_aprob = "select
							emp.nombre_presenta as encargado,
							emp.cargo_presenta as cargo,
							emp.nombre as empresa,
							( (select desc_mun from public.\"X_municipios\" where id_municipio = emp.idmunicipio) || ', ' ||
							(select desc_estado from public.\"X_estados\" where id_estado = emp.idestado ) ) as lugar_empresa,
							sva.fecha_creacion_solicitud,
							sva.no_alumnos as total_alumnos,
							sva.no_solicitud,
							sva.area_a_visitar as area_visitar,
							sva.objetivo_visitar_area as objetivo_visita,
							(
								(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
										extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
										when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
										when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
										when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
										when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
										when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
										when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
										when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
										when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
										when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
										when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
										when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
										when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
										END)||' de '||extract('year' from sva.fecha_hora_salida_visita) || ' a las ' || ( case
										when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)
								)end)

							) as fecha_hora_salida_visita,
							(
								(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
										extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
										when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
										when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
										when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
										when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
										when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
										when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
										when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
										when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
										when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
										when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
										when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
										when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
										END)||' de '||extract('year' from sva.fecha_hora_regreso_visita) || ' a las ' || ( case
										when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)
								)end)

							) as fecha_hora_regreso_visita,
							(select \"nmbCompletoEmp\" from public.h_ocupacionpuesto where \"cvePuestoGeneral\" = '04'
							AND \"cvePuestoParticular\" = '03' AND \"cveDepartamentoEmp\" = 6 ) as jefa_of_servicios_externos
							from pe_vinculacion.va_solicitudes_visitas_academicas sva
							join pe_vinculacion.g_rpempresas emp
							on emp.idempresa = sva.id_empresa_visita
							where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
						";

		$datos_solicitud_visita = Yii::app()->db->createCommand($qry_sol_aprob)->queryAll();

		//Obtenemos el año para el PDf de la Solicitud de Visita Academica
		$anio_solicitud = $this->getAnioSolicitud($datos_solicitud_visita[0]['fecha_creacion_solicitud']);

		//Formato Fecha de validacion Jefe de Vinculacion
		$fecha_val_sol = InfoSolicitudVisitasAcademicas::getFechaValidacionJefeOficinaExternosVinculacionVisitasAcademicas($id_solicitud_visitas_academicas);

		//Lista de especialidades de los alumnos que asisten a la visita
		$qry_lista_carreras = "select DISTINCT(esp.\"cveEspecialidad\"), esp.\"dscEspecialidad\" from pe_vinculacion.va_solicitudes_visitas_academicas sva
									join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
									on aava.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
									join public.\"E_datosAlumno\" eda
									on eda.\"nctrAlumno\" = aava.\"nctrAlumno\"
									join public.\"E_especialidad\" esp
									on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
									where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
								";

		$lista_carreras_alum = Yii::app()->db->createCommand($qry_lista_carreras)->queryAll();

		//Lista de docentes responsables de la visita academica
		$qry_docentes_responsables = "select rva.\"rfcEmpleado\", emp.\"nmbCompletoEmp\"
										from pe_vinculacion.va_responsables_visitas_academicas rva
										join public.\"H_empleados\" emp
										on emp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
										where rva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
									";

		$lista_docentes_responsables = Yii::app()->db->createCommand($qry_docentes_responsables)->queryAll();

		//legenda si es mes especial
		$modelVaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk(1);
		if($modelVaConfiguracionModVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existe informacion de esa configuración.');

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '20',
			'margin_right' =>  '20',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-P',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Formato Solicitud Visita Académica');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirFormatoSolicitudVisitaAcademica', array(
						  'fec_actual' => $this->getFormatoFechaReportePDF(),
						  'datos_solicitud_visita' => $datos_solicitud_visita,
						  'lista_docentes_responsables' => $this->getFormatoResponsablesVisita($lista_docentes_responsables),
						  'lista_carreras_alumnos' => $this->getFormatoCarrerasAlumnos($lista_carreras_alum),
						  'val_jefe_vinculacion' => $this->getJefeGestionTecnologicaYVinculacion(),
						  'anio_solicitud' => $anio_solicitud,
						  'legenda_mes' => $modelVaConfiguracionModVisitasAcademicas->texto_legenda_inicio_reportes_pdf,
						  'fecha_val_sol' => $fecha_val_sol

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Formato Solicitud Visita Académica.pdf', 'I');
	}

	public function getAnioSolicitud($fec)
	{
		//echo $fec;
		$fechaComoEntero = strtotime($fec);
		//echo "<br>";
		$anio = date("Y", $fechaComoEntero);
		//echo $anio;

		return $anio;
	}

	public function getFormatoCarrerasAlumnos($lista_carreras_alum)
	{
		$tam = sizeof($lista_carreras_alum);
		$names_carreras = null;

		for($i = 0; $i < $tam; $i++)
		{
			$name_carrera = $lista_carreras_alum[$i]['dscEspecialidad'];

			if(($i + 1) == $tam)
				$names_carreras = $names_carreras.$name_carrera;
			else
				$names_carreras = $names_carreras.$name_carrera.', ';

		}

		return $names_carreras;
	}

	public function getFormatoResponsablesVisita($lista_docentes_responsables)
	{
		$tam = sizeof($lista_docentes_responsables);
		$names_resp = null;

		for($i = 0; $i < $tam; $i++)
		{
			$name_resp = $lista_docentes_responsables[$i]['nmbCompletoEmp'];

			if(($i + 1) == $tam)
				$names_resp = $names_resp.$name_resp;
			else
				$names_resp = $names_resp.$name_resp.'/';

		}

		return $names_resp;
	}

	public function actionImprimirReporteResultadosEIncidentesVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Consulta de datos para llenar el PDF
		$qry_sol_aprob = "select
								(
									select hemp.\"nmbCompletoEmp\" from public.\"H_empleados\" hemp
									join pe_vinculacion.va_responsables_visitas_academicas rva
									on hemp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
									where rva.responsable_principal = true
									and rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas

								) as docente_responsable,
								sva.no_alumnos,
								(
									(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
											extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
											when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
											when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
											when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
											when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
											when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
											when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
											when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
											when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
											when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
											when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
											when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
											when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
											END)||' de '||extract('year' from sva.fecha_hora_salida_visita)
									)end)
								) as fecha_salida_visita,
								(
									(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
											extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
											when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
											when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
											when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
											when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
											when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
											when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
											when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
											when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
											when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
											when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
											when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
											when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
											END)||' de '||extract('year' from sva.fecha_hora_regreso_visita)
									)end)

								) as fecha_regreso_visita,
								(
									( case when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)

								) as horario_salida_visita,
								(
									( case when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)

								) as horario_regreso_visita,
								( select nombre from pe_vinculacion.g_rpempresas where idempresa = sva.id_empresa_visita ) as nombre_empresa,
								riva.unidades_materias_cubrieron,
								riva.cumplieron_objetivos,
								riva.descripcion_incidentes,
								riva.valida_docente_reponsable
								from pe_vinculacion.va_reporte_resultados_incidencias_visitas_academicas riva
								join pe_vinculacion.va_solicitudes_visitas_academicas sva
								on sva.id_solicitud_visitas_academicas = riva.id_reporte_resultados_incidencias
								where riva.id_reporte_resultados_incidencias = '$id_solicitud_visitas_academicas'
							";

		$datos_reporte_incidencias = Yii::app()->db->createCommand($qry_sol_aprob)->queryAll();

		//Lista de materias que cubre la Visita Academica
		$qry_matesem_cubre_sol = "select ecm.\"dscMateria\", ecm.\"semMateria\"
									from pe_vinculacion.va_solicitudes_visitas_academicas sva
									join pe_vinculacion.va_materias_imparte_responsable_visita_academica mirva
									on mirva.id_solicitud_materias_responsable_visitas_academicas = sva.id_solicitud_visitas_academicas
									join public.\"E_catalogoMaterias\" ecm
									on ecm.\"cveMateria\" = mirva.\"cveMateria\"
									where mirva.id_solicitud_materias_responsable_visitas_academicas = '$id_solicitud_visitas_academicas'
								";

		$lista_matsem_cubre = Yii::app()->db->createCommand($qry_matesem_cubre_sol)->queryAll();

		//lista de carreras que cubre por materia
		$qry_carreras = "select DISTINCT(esp.\"cveEspecialidad\"), esp.\"dscEspecialidad\"
						from pe_vinculacion.va_solicitudes_visitas_academicas sva
							join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
							on aava.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
							join public.\"E_datosAlumno\" eda
							on eda.\"nctrAlumno\" = aava.\"nctrAlumno\"
							join public.\"E_especialidad\" esp
							on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
							where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
						";

		$lista_carreras = Yii::app()->db->createCommand($qry_carreras)->queryAll();

		//Fecha de validacion del reporte de incidencias por parte del docente responsable
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
		$fec_val_docente = InfoSolicitudVisitasAcademicas::getFormatoFechaValidaDocenteReponsableReporteResultados($id_solicitud_visitas_academicas);

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '15',
			'margin_right' =>  '15',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-P',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$qry_cod_cal = "select * from pe_vinculacion.va_codigos_calidad_mod_visitas_academicas where id_codigo_calidad_mod_va = 3 ";
		$rs = Yii::app()->db->createCommand($qry_cod_cal)->queryAll();
		$c_calidad = $rs[0]['codigo_calidad'];
		$revision = "Revisión ".$rs[0]['revision'];

		$footer =
		"<table class='bold' style='border-top: 5px solid #1B5E20; border-left: 1px solid #FFFFFF; border-collapse: collapse;' name='footer' width=\"1000\">
           <tr colspan='2'>
			 <td style='bold border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 14px;' align=\"left\">
			 	{$c_calidad}
			 </td>
			 <td style='bold border-left: 1px solid #FFFFFF; border-right: 1px solid #FFFFFF; border-bottom: 1px solid #FFFFFF; font-size: 14px;' align=\"right\">
			 	{$revision}
			 </td>
           </tr>
        </table>";
		$mPDF1->SetHTMLFooter($footer);
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Reporte Resultados Visita Académica');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirReporteResultadosEIncidentesVisitaAcademica', array(
						  'fec_actual' => $this->getFormatoFechaReportePDF(),
						  'datos_reporte_incidencias' => $datos_reporte_incidencias,
						  'lista_mat_cubre' => $this->getFormatoMaterias($lista_matsem_cubre),
						  'lista_semestre_cubre' => $this->getFormatoSemestres($lista_matsem_cubre),
						  'lista_carreras' => $this->getFormatoCarrerasAlumnos($lista_carreras),
						  'fec_val_docente' => $fec_val_docente

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Reporte Resultados Visita Académica.pdf', 'I');
	}

	public function getFormatoMaterias($lista_matsem_cubre)
	{
		$tam = sizeof($lista_matsem_cubre);
		$names_materias = null;

		for($i = 0; $i < $tam; $i++)
		{
			$name_carrera = $lista_matsem_cubre[$i]['dscMateria'];

			if(($i + 1) == $tam)
				$names_materias = $names_materias.$name_carrera;
			else
				$names_materias = $names_materias.$name_carrera.', ';

		}

		return $names_materias;
	}

	public function getFormatoSemestres($lista_matsem_cubre)
	{
		$tam = sizeof($lista_matsem_cubre);
		$names_semestres = null;

		for($i = 0; $i < $tam; $i++)
		{
			$name_carrera = $lista_matsem_cubre[$i]['semMateria'];

			if(($i + 1) == $tam)
				$names_semestres = $names_semestres.$name_carrera;
			else
				$names_semestres = $names_semestres.$name_carrera.', ';

		}

		return $names_semestres;
	}

	public function actionImprimirFormatoSolicitudPresentacionYAgradecimiento($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';
		require_once Yii::app()->basePath . '/vendors/mpdfv2/autoload.php';

		//Consulta de datos para llenar el PDF
		$qry_sol_aprob = "select sva.no_solicitud,
							(emp.nombre_presenta) as encargado,
							(emp.cargo_presenta) as cargo,
							(emp.nombre) as empresa,
							sva.no_alumnos,
							( (select desc_mun from public.\"X_municipios\" where id_municipio = emp.idmunicipio) || ', ' ||
							(select desc_estado from public.\"X_estados\" where id_estado = emp.idestado ) ) as lugar_empresa,
							(
								(case when sva.fecha_hora_salida_visita is null then 'SV' ELSE(
										extract('day' from sva.fecha_hora_salida_visita) ||' de '|| ( case
										when extract('month' from sva.fecha_hora_salida_visita)=1 then 'Enero'
										when extract('month' from sva.fecha_hora_salida_visita)=2 then 'Febrero'
										when extract('month' from sva.fecha_hora_salida_visita)=3 then 'Marzo'
										when extract('month' from sva.fecha_hora_salida_visita)=4 then 'Abril'
										when extract('month' from sva.fecha_hora_salida_visita)=5 then 'Mayo'
										when extract('month' from sva.fecha_hora_salida_visita)=6 then 'Junio'
										when extract('month' from sva.fecha_hora_salida_visita)=7 then 'Julio'
										when extract('month' from sva.fecha_hora_salida_visita)=8 then 'Agosto'
										when extract('month' from sva.fecha_hora_salida_visita)=9 then 'Septiembre'
										when extract('month' from sva.fecha_hora_salida_visita)=10 then 'Octubre'
										when extract('month' from sva.fecha_hora_salida_visita)=11 then 'Noviembre'
										when extract('month' from sva.fecha_hora_salida_visita)=12 then 'Diciembre'
										END)||' de '||extract('year' from sva.fecha_hora_salida_visita) || ' a las ' || ( case
										when extract('hour' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_salida_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_salida_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_salida_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_salida_visita) >= 12 then 'PM' else 'AM' end)
								)end)
							) as hora_salida_visita,
							(
								(case when sva.fecha_hora_regreso_visita is null then 'SV' ELSE(
										extract('day' from sva.fecha_hora_regreso_visita) ||' de '|| ( case
										when extract('month' from sva.fecha_hora_regreso_visita)=1 then 'Enero'
										when extract('month' from sva.fecha_hora_regreso_visita)=2 then 'Febrero'
										when extract('month' from sva.fecha_hora_regreso_visita)=3 then 'Marzo'
										when extract('month' from sva.fecha_hora_regreso_visita)=4 then 'Abril'
										when extract('month' from sva.fecha_hora_regreso_visita)=5 then 'Mayo'
										when extract('month' from sva.fecha_hora_regreso_visita)=6 then 'Junio'
										when extract('month' from sva.fecha_hora_regreso_visita)=7 then 'Julio'
										when extract('month' from sva.fecha_hora_regreso_visita)=8 then 'Agosto'
										when extract('month' from sva.fecha_hora_regreso_visita)=9 then 'Septiembre'
										when extract('month' from sva.fecha_hora_regreso_visita)=10 then 'Octubre'
										when extract('month' from sva.fecha_hora_regreso_visita)=11 then 'Noviembre'
										when extract('month' from sva.fecha_hora_regreso_visita)=12 then 'Diciembre'
										END)||' de '||extract('year' from sva.fecha_hora_regreso_visita) || ' a las ' || ( case
										when extract('hour' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('hour' from sva.fecha_hora_regreso_visita)) || ':' ||
										(case when extract('minute' from sva.fecha_hora_regreso_visita) < 10 then '0' else '' end) ||
										(extract('minute' from sva.fecha_hora_regreso_visita)) || ' ' ||
										(case when extract('hour' from sva.fecha_hora_regreso_visita) >= 12 then 'PM' else 'AM' end)
								)end)

							) as fecha_hora_regreso_visita
							from pe_vinculacion.va_solicitudes_visitas_academicas sva
							join pe_vinculacion.g_rpempresas emp
							on emp.idempresa = sva.id_empresa_visita
							where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
						";

		$datos_empresa_sol = Yii::app()->db->createCommand($qry_sol_aprob)->queryAll();

		//Formato Fecha de validacion Jefe de Vinculacion
		$fecha_val_sol = InfoSolicitudVisitasAcademicas::getFechaValidacionJefeOficinaExternosVinculacionVisitasAcademicas($id_solicitud_visitas_academicas);

		//Datos de los docentes responsables de la Solicitud
		$qry_doc_resp = "select
							rva.\"rfcEmpleado\", emp.\"nmbCompletoEmp\"
							from pe_vinculacion.va_responsables_visitas_academicas rva
							join public.\"H_empleados\" emp
							on emp.\"rfcEmpleado\" = rva.\"rfcEmpleado\"
							where rva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
						";

		$lista_doc_respon = Yii::app()->db->createCommand($qry_doc_resp)->queryAll();

		//Datos de las carreras de los alumnos que asisten a la Visita Academica
		$qry_carreras_alum = "select DISTINCT(esp.\"cveEspecialidad\"), esp.\"dscEspecialidad\"
								from pe_vinculacion.va_solicitudes_visitas_academicas sva
								join pe_vinculacion.va_alumnos_asisten_visitas_academicas aava
								on aava.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
								join public.\"E_datosAlumno\" eda
								on eda.\"nctrAlumno\" = aava.\"nctrAlumno\"
								join public.\"E_especialidad\" esp
								on esp.\"cveEspecialidad\" = eda.\"cveEspecialidadAlu\"
								where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas'
							";

		$lista_carreras = Yii::app()->db->createCommand($qry_carreras_alum)->queryAll();

		//legenda del mes
		$modelVaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk(1);
		if($modelVaConfiguracionModVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existen datos de la configuración.');

		//layout para PDFs
		$this->layout='//layouts/mainVacio';
		# You can easily override default constructor's params
		//$mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4-L', 0,'',5, 5, 0, 0, 5, 5, 'L');
		$mPDF1 = new \Mpdf\Mpdf(array(
			'margin_left' => '20',
			'margin_right' =>  '20',
			'margin_top' => '5',
			'margin_bottom' => '5',
			'margin_header' => '5',
			'margin_footer' => '5',
			'mode' => 'utf-8',     //Codepage Values OR Codepage Values
			'format' => 'A4-P',        //A4, Letter, Legal, Executive, Folio, Demy, Royal, etc
			'orientation' => 'P'          //"L" for Landscape orientation, "P" for Portrait orientation
		));

		//Marca de Agua
		$mPDF1->SetWatermarkImage('images/servicio_social/alas.jpg', 0.5, '', array(0,20));
		$mPDF1->showWatermarkImage = true;
		//Tamaño de pdf
		$mPDF1->SetDisplayMode('fullpage');
		/************************************Footer************************************/
		$mPDF1->SetHTMLFooter('<img align="center" src="'. Yii::app()->request->baseUrl.'/images/servicio_social/banner_inf.jpg"/>');
		/************************************Footer************************************/
		//Titulo
		$mPDF1->SetTitle('Formato Presentacion y Agradecimiento Visita Académica');
        # render (full page)
        $mPDF1->WriteHTML(
			$this->render('imprimirFormatoSolicitudPresentacionYAgradecimiento', array(
						  'datos_empresa_sol' => $datos_empresa_sol,
						  'lista_doc_respon' => $this->getFormatoResponsablesVisita($lista_doc_respon),
						  'fec_actual' => $this->getFormatoFechaReportePDF(),
						  'lista_carreras' => $this->getFormatoCarrerasAlumnos($lista_carreras),
						  'val_jefe_vinculacion' => $this->getJefeGestionTecnologicaYVinculacion(),
						  'legenda_mes' => $modelVaConfiguracionModVisitasAcademicas->texto_legenda_inicio_reportes_pdf,
						  'fecha_val_sol' => $fecha_val_sol

			), true)
		);

		//Envia la salida en linea al navegador
		//D -> Descargar
		//F -> Save File
		$mPDF1->Output('Formato Presentacion y Agradecimiento Visita Académica.pdf', 'I');

	}

	public function getJefeGestionTecnologicaYVinculacion()
	{
		$qry_jefe_vinc = "select *
							from public.h_ocupacionpuesto
							where \"cvePuestoGeneral\" = '03' AND \"cvePuestoParticular\" = '02' AND \"cveDepartamentoEmp\" = '06' ";

		$datos_jefe_vinc = Yii::app()->db->createCommand($qry_jefe_vinc)->queryAll();

		return $datos_jefe_vinc;
	}

	//Damos formato a la fecha de los reportes de PDF
	public function getFormatoFechaReportePDF()
	{
		$ahora = time();
		$anio = date("Y",$ahora);
		$mes = date("m",$ahora);
		$dia = date("d",$ahora);

		switch($mes)
		{
			case '01':
				$mes_format = 'Enero';
			break;
			case '02':
				$mes_format = 'Febrero';
			break;
			case '03':
				$mes_format = 'Marzo';
			break;
			case '04':
				$mes_format = 'Abril';
			break;
			case '05':
				$mes_format = 'Mayo';
			break;
			case '06':
				$mes_format = 'Junio';
			break;
			case '07':
				$mes_format = 'Julio';
			break;
			case '08':
				$mes_format = 'Agosto';
			break;
			case '09':
				$mes_format = 'Septiembre';
			break;
			case '10':
				$mes_format = 'Octubre';
			break;
			case '11':
				$mes_format = 'Noviembre';
			break;
			case '12':
				$mes_format = 'Diciembre';
			break;
		}

		return $dia.'/'.$mes_format.'/'.$anio;
	}

	public function getTipoVisita($id_tipo_visita_academica)
	{
		$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($id_tipo_visita_academica);

		if($modelVaTiposVisitasAcademicas === NULL)
			throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

		return $modelVaTiposVisitasAcademicas->tipo_visita_academica;
	}

	public function getEmpresa($id_empresa_visita)
	{
		$modelGRpempresas = GRpempresas::model()->findByPk($id_empresa_visita);

		if($modelGRpempresas === NULL)
			throw new CHttpException(404,'No hay datos de las Empresas a Visitar.');

		return $modelGRpempresas->nombre;
	}

	public function getPeriodo($per)
	{
		$qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$per' ";
		$rs = Yii::app()->db->createCommand($qry_per)->queryAll();

		return ($rs[0]['dscPeriodo'] === null) ? '-' : $rs[0]['dscPeriodo'];
	}

	//Se editara la Solicitud solo por el responsable principal
	public function actionEditarVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		$info_empleado = $this->infoEmpleado($rfcEmpleado);

		//Datos del formulario
		$tipos_visitas = $this->getTipoVisitas();

		//Datos del formulario
		$lista_empresas_visitas = $this->getEmpresasVisitas();

		//Obtenmos el RFC del Empleado principal
		if($this->getResponsablePrincipal($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas, $rfcEmpleado))
		{

			$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
																			'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
																			'rfcEmpleado' => $rfcEmpleado
																		));
			$modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
																			'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_visitas_academicas//,
																			//'rfcEmpleado' => $rfcEmpleado
																		));

			if(isset($_POST['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->ultima_fecha_modificacion = date('Y-m-d H:i:s');

				//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
				$transaction = Yii::app()->db->beginTransaction();

				try
				{
					if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
					{

						if($modelVaSolicitudesVisitasAcademicas->save())
						{
							Yii::app()->user->setFlash('success', "Solicitud actualizada correctamente!!!");
							//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
							$transaction->commit();
							$this->redirect(array('listaSolicitudesVisitasAcademicas'));

						}else{

							//die('3');
							Yii::app()->user->setFlash('danger', "Error!!! no se pudo actualizar la Solicitud.");
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
						}

					}else{

						//die('2');
						Yii::app()->user->setFlash('danger', "Error!!! la Fecha de Salida de la Solicitud no puede ser mayor a la Fecha de Regreso de la Solicitud.");
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
					}

				}catch(Exception $e)
				{

					//die('1');
					Yii::app()->user->setFlash('danger', "Error!!! no se actualizaron los datos de la Solicitud.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
				}

			}

			$this->render('editarSolicitudVisitaAcademica',array(
						'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
						'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
						'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
						//'lista_responsables' => $lista_responsables,
						'tipos_visitas' => $tipos_visitas,
						'lista_empresas_visitas' => $lista_empresas_visitas,
						'info_empleado' => $info_empleado,
						'materias_asignadas_responsable' => $this->getMateriasDepartamentoResponsableSolicitudVisitaAcademica($info_empleado->deptoCatedratico),
						'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
						'cargo' => $this->getCargo($info_empleado->rfcEmpleado),
						'fec_creacion' => InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas)
			));

		}else{

			//Vista cuando el Empleado logeado no es el responsable
			$this->render('responsableNoPrincipalSolicitudVisitaAcademica',array(
						  'rfcEmpleado' => $rfcEmpleado,
						  'info_empleado' => $info_empleado,
						  'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
						  'cargo' => $this->getCargo($info_empleado->rfcEmpleado)
			));
		}
	}

	public function actionEditarSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		$info_empleado = $this->infoEmpleado($rfcEmpleado);

		//Datos del formulario
		$tipos_visitas = $this->getTipoVisitas();

		//Datos del formulario
		$lista_empresas_visitas = $this->getEmpresasVisitas();

		//Lista de responsables
		$lista_responsables = $this->getListaResponsablesVisitasAcademicas();

		//Obtenmos el RFC del Empleado principal
		if($this->getResponsablePrincipal($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas, $rfcEmpleado))
		{

			$VaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
																	'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
																	'rfcEmpleado' => $rfcEmpleado
			));

			$criteria = new CDbCriteria;
			$criteria->condition = " id_solicitud_materias_responsable_visitas_academicas = '$id_solicitud_visitas_academicas' ";
			$modelMaterias = VaMateriasImparteResponsableVisitaAcademica::model()->findAll($criteria);

			$tamanio = sizeof($modelMaterias);
			$unica_materia = ($tamanio == 1) ? true : false;

			if($unica_materia === true)
			{
				$criteria2 = new CDbCriteria;
				$criteria2->condition = " id_solicitud_materias_responsable_visitas_academicas = '$id_solicitud_visitas_academicas' ";
				$modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->find($criteria2);

			}

			//die;
			$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
			$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

			if(isset($_GET['VaResponsablesVisitasAcademicas']))
				$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

			$VaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
			$VaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

			if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
				$VaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

			//Solo se permitira editar campos del modelo VaSolicitudesVisitasAcademicas
			if(isset($_POST['VaSolicitudesVisitasAcademicas']))
			{
				$modelVaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];
				$modelVaSolicitudesVisitasAcademicas->ultima_fecha_modificacion = date('Y-m-d H:i:s');

				//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
				$transaction = Yii::app()->db->beginTransaction();

				try
				{

					if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
					{

						if($modelVaSolicitudesVisitasAcademicas->save())
						{
							Yii::app()->user->setFlash('success', "Solicitud actualizada correctamente!!!");
							//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
							$transaction->commit();
							$this->redirect(array('listaSolicitudesVisitasAcademicas'));

						}else{

							//die('3');
							Yii::app()->user->setFlash('danger', "Error!!! no se pudo actualizar la Solicitud.");
							//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
							$transaction->rollback();
						}
					}else{

						//die('21');
						Yii::app()->user->setFlash('danger', "Error!!! la Fecha de Salida de la Solicitud no puede ser mayor a la Fecha de Regreso de la Solicitud.");
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
					}

				}catch(Exception $e)
				{
					//die('1');
					Yii::app()->user->setFlash('danger', "Error!!! no se actualizaron los datos de la Solicitud.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
				}

			}

			$this->render('editarSolicitudVisitaAcademica',array(
							'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
							'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
							'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
							'id_visita' => $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas,
							'tipos_visitas' => $tipos_visitas,
							'lista_empresas_visitas' => $lista_empresas_visitas,
							'info_empleado' => $info_empleado,
							'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
							'cargo' => $this->getCargo($info_empleado->rfcEmpleado),
							'lista_responsables' => $lista_responsables,
							'unica_materia' => $unica_materia,
							'VaResponsablesVisitasAcademicas' => $VaResponsablesVisitasAcademicas,
							'VaMateriasImparteResponsableVisitaAcademica' => $VaMateriasImparteResponsableVisitaAcademica,
							//'materias_asignadas_responsable' => $materias_asignadas_responsable,
							'materias_asignadas_responsable' => $this->getMateriasDepartamentoResponsableSolicitudVisitaAcademica($info_empleado->deptoCatedratico),
							'fec_creacion' => InfoSolicitudVisitasAcademicas::getFechaCreacionSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas)
  			));

		}else{

			//Vista cuando el Empleado logeado no es el responsable
			$this->render('responsableNoPrincipalSolicitudVisitaAcademica',array(
						'rfcEmpleado' => $rfcEmpleado,
						'info_empleado' => $info_empleado,
						'departamento' => $this->getDepartamentoEmpleado($info_empleado->cveDepartamentoEmp),
						'cargo' => $this->getCargo($info_empleado->rfcEmpleado)
			));
		}

	}

	//Si devuelve la materia asignada a la solicitud de visita academica
	public function getMateriaSolicitudVisitaAcademica($depto_catedratico, $cveMateria)
	{

		$deptoCatedratico = trim($depto_catedratico);

		$qry_materias = "select hemp.\"rfcEmpleado\", ecm.\"cveMateria\", ecm.\"dscMateria\", ecm.\"deptoMateria\"
						from public.\"H_empleados\" hemp
						join public.\"E_gruposMaterias_new\" egm
						on egm.\"rfcEmpleadoMat\" = hemp.\"rfcEmpleado\"
						join public.\"E_catalogoMaterias\" ecm
						on ecm.\"cveMateria\" = egm.\"cveMateriaGpo\"
						where hemp.\"deptoCatedratico\" = '$deptoCatedratico' AND ecm.\"statMateria\" = true ";

		$listas_materias_responsable = Yii::app()->db->createCommand($qry_materias)->queryAll();
		$tam = sizeof($listas_materias_responsable);

		for($a=0; $a < $tam; $a++)
		{
			$lista_materias[$listas_materias_responsable[$a]['cveMateria']] = $listas_materias_responsable[$a]['cveMateria'] .'  -  '. $listas_materias_responsable[$a]['dscMateria'];
		}

		return $lista_materias;
	}

	//Agregar Responssables a la Solicitud
	public function actionAgregarNuevoResponsableVisitaAcademica()
	{

		if(isset($_POST['id_solicitud_visitas_academicas']) && $_POST['rfcEmpleado'])
		{
			$rfcEmpleado = $_POST['rfcEmpleado'];
			$id_solicitud_vis_acad = $_POST['id_solicitud_visitas_academicas'];

			//Validar Responsables NO REPETIDOS
			if($this->responsableNoRepetidoVisitaAcademica($id_solicitud_vis_acad, $rfcEmpleado))
			{
				//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
				$transaction = Yii::app()->db->beginTransaction();

				try
				{
					$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas;
					$modelVaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas = $id_solicitud_vis_acad;
					$modelVaResponsablesVisitasAcademicas->rfcEmpleado = $rfcEmpleado;
					$modelVaResponsablesVisitasAcademicas->fecha_registro_responsable = date('Y-m-d H:i:s');
					$modelVaResponsablesVisitasAcademicas->responsable_principal = false;

					if(!$modelVaResponsablesVisitasAcademicas->save())
					{
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						//die('3');
						Yii::app()->user->setFlash('danger', "Error!!! no se pudo agregar el Responsable a la Visita Académica.");
						$transaction->rollback();

					}else{

						//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
						Yii::app()->user->setFlash('success', "Responsable agregado a la Visita Académica correctamente!!!");
						$transaction->commit();
					}

				}catch(Exception $e)
				{
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					//die('2');
					Yii::app()->user->setFlash('danger', "Error!!! no se pudo agregar el Responsable a la Visita Académica.");
					$transaction->rollback();
				}

			}else{

				//die('1');
				Yii::app()->user->setFlash('danger', "Error!!! el Responsable ya ha sido agregado a la Visita Académica.");
			}

		}
	}

	//Agregar Nuevas Materias que cubre la Solicitud
	public function actionAgregarNuevaMateriaVisitaAcademica()
	{
		if(isset($_POST['id_solicitud_materias_responsable_visitas_academicas']) && $_POST['cveMateria'])
		{
			$id_sol = $_POST['id_solicitud_materias_responsable_visitas_academicas'];
			$cveMateria = $_POST['cveMateria'];

			//Validar Responsables NO REPETIDOS
			if($this->materiaNoRepetidaVisitaAcademica($id_sol, $cveMateria))
			{
				//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
				$transaction = Yii::app()->db->beginTransaction();

				try
				{
					$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica;
					$modelVaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas = $id_sol;
					$modelVaMateriasImparteResponsableVisitaAcademica->cveMateria = $cveMateria;
					$modelVaMateriasImparteResponsableVisitaAcademica->fecha_registro_materia_visita_academica = date('Y-m-d H:i:s');

					if(!$modelVaMateriasImparteResponsableVisitaAcademica->save())
					{
						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						//die('3');
						Yii::app()->user->setFlash('danger', "Error!!! no se pudo agregar la Materia a la Visita Académica.");
						$transaction->rollback();

					}else{

						//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
						Yii::app()->user->setFlash('success', "Materia agregada a la Visita Académica correctamente!!!");
						$transaction->commit();
					}


				}catch(Exception $e)
				{
					//die('2');
					Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al agregar la Materia a la Visita Académica.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
				}

			}else{

				//die('1');
				Yii::app()->user->setFlash('danger', "Error!!! La Materia ya ha sido agregada a la Visita Académica.");
			}
		}
	}

	public function actionEditarJVSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		//models
		require_once Yii::app()->basePath.'/modules/autorizacionSalida/models/RMAutSalidasDetalle.php';
		require_once Yii::app()->basePath.'/modules/autorizacionSalida/models/RMAutSalidas.php';

		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcOficinaVisitasIndustriales'];
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$modelVaSolicitudesVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);
		$periodo = $this->getPeriodo($modelVaSolicitudesVisitasAcademicas->periodo);
		$empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);
		$fecha_sal = InfoSolicitudVisitasAcademicas::getFechaSalidaSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$fecha_reg = InfoSolicitudVisitasAcademicas::getFechaRegresoSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
		$tipo_visita_acad = $this->getTipoVisita($modelVaSolicitudesVisitasAcademicas->id_tipo_visita_academica);

		/*Tipos de FORMATO permitidos */
		$extensiones = array("jpeg", "jpg", "pdf");

		$VaSolicitudesVisitasAcademicas = new VaSolicitudesVisitasAcademicas;
		$modelVaBitacoraCambiosSolicitudVinculacion = new VaBitacoraCambiosSolicitudVinculacion; //Bitacora

		//Responsables
		$modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
		$modelVaResponsablesVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaResponsablesVisitasAcademicas']))
			$modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

		//Materias
		$modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		//Bitacora de cambios de la Solicitud
		$modelVaBitacoraCambiosSolicitudVinculacion = new VaBitacoraCambiosSolicitudVinculacion('search');
		$modelVaBitacoraCambiosSolicitudVinculacion->unsetAttributes();  // clear any default values

		if(isset($_GET['VaBitacoraCambiosSolicitudVinculacion']))
			$modelVaBitacoraCambiosSolicitudVinculacion->attributes=$_GET['VaBitacoraCambiosSolicitudVinculacion'];

		//Verificar que todos hayan validado
		$valido_val_jefes = $this->comprobarValidaciones($id_solicitud_visitas_academicas);

		if(isset($_POST['VaSolicitudesVisitasAcademicas']))
        {

			$VaSolicitudesVisitasAcademicas->attributes = $_POST['VaSolicitudesVisitasAcademicas'];

			//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
			$transaction = Yii::app()->db->beginTransaction();

			//Devuelve una Instancia del archivo subido
			$VaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa = CUploadedFile::getInstance($VaSolicitudesVisitasAcademicas, 'doc_oficio_confirmacion_empresa');

			//Verificamos que se haya subido alguna imagen o archivo pdf
			if($VaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa != NULL)
			{
				//1MB = 1048576
				// .5 MB = 524288
				if($VaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa->getSize() > '524288')
					$errors[] = 'El archivo debe pesar menos de 512 kB.';

				if(in_array($VaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa->getExtensionName(), $extensiones) === false)
					$errors[] = 'La extensión del archivo debe ser .jpeg, .pdf ó .jpg';

				$name = $_FILES['VaSolicitudesVisitasAcademicas']['name']['doc_oficio_confirmacion_empresa'];
				$filename = pathinfo($name, PATHINFO_FILENAME); //Obtenemos el nombre del archivo
				$ext = pathinfo($name, PATHINFO_EXTENSION); //Obtenemos el formato del documento subido .png, .jpg, .jpeg

				if(empty($errors))
				{
					//Le damos Nuevo Nombre del archivo de acuerdo al formato ejemplo oficio_20190704130124_1.jpg
					$new_name_ofic_sup = 'oficio_'.trim($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas).'_'.date('YmdHis').'.'.$ext;

					//Ejemplo de nombre de la carpeta seria Solicitud_1
					$new_name_carp = 'Solicitud_'.trim($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);

					//Eliminamos el oficio actual para subir el nuevo
					///images/Solicitudes/
					//file_exists
					if(is_file(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp.'/'.$modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa))
					{
						//удаляем файл unlink
						unlink(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp.'/'.$modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa);
						$modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa = null;
						$modelVaSolicitudesVisitasAcademicas->save();
					}

					//Ruta donde se guardara el oficio de confirmacion de la Empresa, ejemplo Solicitud_1
					if(!is_dir(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp))
					{
						mkdir(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp, 0, true);
						chmod(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp, 0777);
					}

					//Guardamos la imagen localmente Yii::getPathOfAlias("webroot").
					$VaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa->saveAs(Yii::app()->getModule('visitasacademicas')->basePath.'/Solicitudes/'.$new_name_carp.'/'.$new_name_ofic_sup);

					if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($VaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $VaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
					{
						//Para saber si el numero de alumnos es positivo
						if($this->isEnteroPositivo(trim($VaSolicitudesVisitasAcademicas->no_alumnos)))
						{
							//Si ocurre un error de insercion entonces se hace el rollback a los inserts
							try
							{

								$modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita = $VaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
								$modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita = $VaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
								$modelVaSolicitudesVisitasAcademicas->path_carpeta_oficio_confirmacion_empresa = trim($new_name_carp);
								$modelVaSolicitudesVisitasAcademicas->ultima_mod_oficio_conf_empresa = date('Y-m-d H:i:s');
								$modelVaSolicitudesVisitasAcademicas->doc_oficio_confirmacion_empresa = trim($new_name_ofic_sup);
								//Se pone la validacion de Roxana de vinculacion como la jefa de oficina de Visitas Industriales
								//cuando suba la imagen de validacion de la Empresa para la visita
								$modelVaSolicitudesVisitasAcademicas->valida_jefe_oficina_externos_vinculacion = date('Y-m-d H:i:s');

								if($modelVaSolicitudesVisitasAcademicas->save())
								{
									$VaBitacoraCambiosSolicitudVinculacion = new VaBitacoraCambiosSolicitudVinculacion;
									$VaBitacoraCambiosSolicitudVinculacion->id_solicitud_visitas_academicas = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;
									$VaBitacoraCambiosSolicitudVinculacion->fecha_hora_salida = $modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
									$VaBitacoraCambiosSolicitudVinculacion->fecha_hora_regreso = $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
									$VaBitacoraCambiosSolicitudVinculacion->no_alumnos_visita = trim($modelVaSolicitudesVisitasAcademicas->no_alumnos);
									$VaBitacoraCambiosSolicitudVinculacion->fecha_ultima_modificacion_solicitud = date('Y-m-d H:i:s');
									$VaBitacoraCambiosSolicitudVinculacion->usuario_modifica = trim($rfcEmpleado);
									$VaBitacoraCambiosSolicitudVinculacion->modifico_doc_digital = true;

									//Se inserta el registro en la bitacora
									if($VaBitacoraCambiosSolicitudVinculacion->save())
									{
										//Crear la solicitud de salida de automovil (Modulo de Materiales)
										$modelRMAutSalidas = new RMAutSalidas;
										$modelRMAutSalidas->fecha_aut_solicitada = date('Y-m-d H:i:s');
										$modelRMAutSalidas->id_aut_salida_estatu = 1; //Nueva Peticion
										$modelRMAutSalidas->cve_departamento = $this->getDeptoResponsablePrincipalSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);//Clave del Departamento del Empleado que hace la Solicitud
										$modelRMAutSalidas->id_aut_salida_tipo_conductor = 1; //Conductor institucional
										$modelRMAutSalidas->id_aut_salida_tipo_vehiculo = 3; //Autobus Oficial
										$modelRMAutSalidas->vale_gasolina = 0;

										//Se inserta el registro en la bitacora
										if($modelRMAutSalidas->save())
										{
											//Insertamos registro en detalle
											$modelRMAutSalidasDetalle = new RMAutSalidasDetalle;
											$modelRMAutSalidasDetalle->id_aut_salida = $modelRMAutSalidas->id_aut_salida;
											$modelRMAutSalidasDetalle->fecha_salida = $modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
											$modelRMAutSalidasDetalle->fecha_entrada = $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
											$modelRMAutSalidasDetalle->motivo_comision = "Visita Académica en la Empresa ".$this->getLocalizacionEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);//
											//Se obtiene el estado y municipio donde se encuentra la empresa
											$modelRMAutSalidasDetalle->destino = "Empresa ".$this->getLocalizacionEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);//Estado y municipio
											$modelRMAutSalidasDetalle->no_personas = trim($VaBitacoraCambiosSolicitudVinculacion->no_alumnos_visita);

											if($modelRMAutSalidasDetalle->save())
											{
												//Se pone la validacion de Roxana como jefa de servicios externos de vinculacion y Se liga con la solicitud de vehiculo
												$modelVaSolicitudesVisitasAcademicas2 = VaSolicitudesVisitasAcademicas::model()->findByPk($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
												$modelVaSolicitudesVisitasAcademicas2->valida_jefe_oficina_externos_vinculacion = date('Y-m-d H:i:s'); //Fecha de validacion por Roxana
												$modelVaSolicitudesVisitasAcademicas2->id_aut_salida_vehiculo = $modelRMAutSalidas->id_aut_salida; //Se liga con la solicitud de vehiculo

												if($modelVaSolicitudesVisitasAcademicas2->save())
												{
													Yii::app()->user->setFlash('success', "Solicitud Actualizada correctamente!!!");
													$transaction->commit();
													$this->redirect(array('listaJVSolicitudesVisitasAcademicas'));

												}else{

													Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
													$transaction->rollBack();
												}

											}else{

												Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
												$transaction->rollBack();
											}

										}else{

											Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
											$transaction->rollBack();
										}

									}else{

										Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica");
										$transaction->rollBack();
									}

								}else{

									//die("3");
									Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al realizar la actualización.");
									$transaction->rollBack();
								}

							}catch(Exception $e){

								Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al actualizar la información.");
								$transaction->rollBack();
							}

						}else{

							//die("No es entero positivo");
							Yii::app()->user->setFlash('danger', "Error!!! el numero de alumnos de la Solicitud debe ser un entero positivo.");
						}

					}else{

						//die("2");
						Yii::app()->user->setFlash('danger', "Error!!! La fecha y hora de salida y regreso no debe ser igual.");
					}

				}else{

					//Mostrar todos los errores que surgieron al tratar de subir la imagen
					//die("El tipo de formato o el tamaño de la imagen deben ser los correctos.");
					$var = "";
					$esp="<br>";
					$contador = 0;

					foreach($errors as $key=>$value)
					{
						$contador++;
						if($contador == 1)
						{
							$var = $value;
						}else{
							$var .= $esp.$value;
						}
					}

					Yii::app()->user->setFlash('danger', trim($var));
					//Resfrescamos pantalla
					$this->refresh();
				}

			}else{

				$modelSV2 = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
				if($modelSV2->doc_oficio_confirmacion_empresa != NULL)
				{
					//Cuando no se sube ninguna imagen
					if($this->validarFechasInicioRegresoSolicitudVisitaAcademica($VaSolicitudesVisitasAcademicas->fecha_hora_salida_visita, $VaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita))
					{
						//Para saber si el numero de alumnos es positivo
						if($this->isEnteroPositivo(trim($VaSolicitudesVisitasAcademicas->no_alumnos)))
						{
							//Si los inserts no se ejecutan correctamente entonces hacemos el rollback
							try
							{
								$modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita = $VaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
								$modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita = $VaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
								$modelVaSolicitudesVisitasAcademicas->no_alumnos = $VaSolicitudesVisitasAcademicas->no_alumnos;

								//Se agrega el cambio en el registro de la solicitud
								if($modelVaSolicitudesVisitasAcademicas->save())
								{
									$VaBitacoraCambiosSolicitudVinculacion = new VaBitacoraCambiosSolicitudVinculacion;
									$VaBitacoraCambiosSolicitudVinculacion->id_solicitud_visitas_academicas = $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas;
									$VaBitacoraCambiosSolicitudVinculacion->fecha_hora_salida = $modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
									$VaBitacoraCambiosSolicitudVinculacion->fecha_hora_regreso = $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
									$VaBitacoraCambiosSolicitudVinculacion->no_alumnos_visita = trim($modelVaSolicitudesVisitasAcademicas->no_alumnos);
									$VaBitacoraCambiosSolicitudVinculacion->fecha_ultima_modificacion_solicitud = date('Y-m-d H:i:s'); //Tiempo en que se modifico el registro
									$VaBitacoraCambiosSolicitudVinculacion->usuario_modifica = trim($rfcEmpleado);
									$VaBitacoraCambiosSolicitudVinculacion->modifico_doc_digital = false;

									//Se inserta el registro en la bitacora
									if($VaBitacoraCambiosSolicitudVinculacion->save())
									{

										//Crear la solicitud de salida de automovil (Modulo de Materiales)
										$modelRMAutSalidas = new RMAutSalidas;
										$modelRMAutSalidas->fecha_aut_solicitada = date('Y-m-d H:i:s');
										$modelRMAutSalidas->id_aut_salida_estatu = 1; //Nueva Peticion
										$modelRMAutSalidas->cve_departamento = $this->getDeptoResponsablePrincipalSolicitudVisitaAcademica($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);//Clave del Departamento del Empleado que hace la Solicitud
										$modelRMAutSalidas->id_aut_salida_tipo_conductor = 1; //Conductor institucional
										$modelRMAutSalidas->id_aut_salida_tipo_vehiculo = 3; //Autobus Oficial
										$modelRMAutSalidas->vale_gasolina = 0;

										if($modelRMAutSalidas->save())
										{

											//Insertamos registro en detalle
											$modelRMAutSalidasDetalle = new RMAutSalidasDetalle;
											$modelRMAutSalidasDetalle->id_aut_salida = $modelRMAutSalidas->id_aut_salida;
											$modelRMAutSalidasDetalle->fecha_salida = $modelVaSolicitudesVisitasAcademicas->fecha_hora_salida_visita;
											$modelRMAutSalidasDetalle->fecha_entrada = $modelVaSolicitudesVisitasAcademicas->fecha_hora_regreso_visita;
											$modelRMAutSalidasDetalle->motivo_comision = "Visita Académica en la Empresa ".$this->getLocalizacionEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);//
											//Se obtiene el estado y municipio donde se encuentra la empresa
											$modelRMAutSalidasDetalle->destino = "Empresa ".$this->getLocalizacionEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);//Estado y municipio
											$modelRMAutSalidasDetalle->no_personas = trim($VaBitacoraCambiosSolicitudVinculacion->no_alumnos_visita);

											if($modelRMAutSalidasDetalle->save())
											{

												//Se pone la validacion de Roxana como jefa de servicios externos de vinculacion y Se liga con la solicitud de vehiculo
												$modelVaSolicitudesVisitasAcademicas2 = VaSolicitudesVisitasAcademicas::model()->findByPk($modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas);
												$modelVaSolicitudesVisitasAcademicas2->valida_jefe_oficina_externos_vinculacion = date('Y-m-d H:i:s'); //Fecha de validacion por Roxana
												$modelVaSolicitudesVisitasAcademicas2->id_aut_salida_vehiculo = $modelRMAutSalidas->id_aut_salida; //Se liga con la solicitud de vehiculo

												if($modelVaSolicitudesVisitasAcademicas2->save())
												{
													//die;
													Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
													$transaction->commit();
													$this->redirect(array('listaJVSolicitudesVisitasAcademicas'));

												}else{

													$transaction->rollBack();
													Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
												}

											}else{

												$transaction->rollBack();
												Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
											}

										}else{

											$transaction->rollBack();
											Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica.");
										}

									}else{

										$transaction->rollBack();
										Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica");
									}

								}else{

									$transaction->rollBack();
									Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud de Visita Académica");
								}

							}catch(Exception $e){

								Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al actualizar la información.");
								$transaction->rollBack();
								//throw $e;
							}

						}else{

							//die("No es entero positivo");
							Yii::app()->user->setFlash('danger', "Error!!! el numero de alumnos de la Solicitud debe ser un entero positivo.");
						}

					}else{

						//die("Error!!! La fecha y hora de salida y regreso no debe ser igual.");
						Yii::app()->user->setFlash('danger', "Error!!! La fecha y hora de salida y regreso no debe ser igual.");
					}//

				}else{

					//die("Error!!! Tienes que subir el documento digital de confirmación de la Empresa.");
					Yii::app()->user->setFlash('danger', "Error!!! Tienes que subir el documento digital de confirmación de la Empresa.");
				}
			}
        }

		$this->render('editarJVSolicitudVisitaAcademica',array(
					  'modelVaSolicitudesVisitasAcademicas'=>$modelVaSolicitudesVisitasAcademicas,
					  'tipo_visita_acad' => $tipo_visita_acad,
					  'periodo' => $periodo,
					  'empresa' => $empresa,
					  'fecha_sal' => $fecha_sal,
					  'fecha_reg' => $fecha_reg,
					  'modelVaResponsablesVisitasAcademicas' => $modelVaResponsablesVisitasAcademicas,
					  'id_solicitud_visitas_academicas' => $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas,
					  'modelVaMateriasImparteResponsableVisitaAcademica' => $modelVaMateriasImparteResponsableVisitaAcademica,
					  'modelVaBitacoraCambiosSolicitudVinculacion' => $modelVaBitacoraCambiosSolicitudVinculacion, //Bitacora cambios
					  'valido_val_jefes' => $valido_val_jefes

		));
	}

	public function comprobarValidaciones($id_solicitud_visitas_academicas)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " (fecha_creacion_solicitud is not null AND valida_jefe_depto_academico is not null AND
								valida_subdirector_academico is not null AND valida_jefe_oficina_externos_vinculacion is not null) AND
								id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);

		return ($modelVaSolicitudesVisitasAcademicas === NULL) ? false : true;
	}

	public function getLocalizacionEmpresa($id_empresa)
	{

		$modelGRpempresas = GRpempresas::model()->findByPk($id_empresa);
		if($modelGRpempresas === NULL)
			throw new CHttpException(404,'No existe información de la Empresa.');

		//Nombre de la Empresa a visitar, municipio y estado
		$nombre_empresa = $modelGRpempresas->nombre;
		$id_estado = $modelGRpempresas->idestado;
		$id_municipio = $modelGRpempresas->idmunicipio;

		//Obtenemos estado y municipio
		$modelXEstados = XEstados::model()->findByPk($id_estado);
		if($modelXEstados === NULL)
			throw new CHttpException(404,'No existe información del Estado.');

		$estado = $modelXEstados->desc_estado;

		$modelXMunicipios = XMunicipios::model()->findByPk($id_municipio);
		if($modelXMunicipios === NULL)
			throw new CHttpException(404,'No existe información del Estado.');

		$municipio = $modelXMunicipios->desc_mun;

		return $nombre_empresa.' en '.$municipio.', '.$estado;
	}

	public function getDeptoResponsablePrincipalSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{

		$criteria = new CDbCriteria;
		$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND responsable_principal = true ";
		$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->find($criteria);
		if($modelVaResponsablesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existe información del Responsable Principal de la Solicitud.');

		return trim($modelVaResponsablesVisitasAcademicas->cve_depto);
	}

	public function responsableNoRepetidoVisitaAcademica($id_solicitud, $rfcEmpleado)
	{
		$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
																	'id_solicitud_visitas_academicas' => $id_solicitud,
																	'rfcEmpleado' => $rfcEmpleado
												));

		return ($modelVaResponsablesVisitasAcademicas === NULL) ? true : false;

	}

	public function materiaNoRepetidaVisitaAcademica($id_solicitud, $cveMateria)
	{
		$modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
																'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud,
																'cveMateria' => $cveMateria
													));

		return ($modelVaMateriasImparteResponsableVisitaAcademica === NULL) ? true : false;
	}

	public function getListaResponsablesVisitasAcademicas()
	{
		$criteria = new CDbCriteria;
        //$criteria->condition = " \"rfcEmpleado\" is not null AND \"nmbCompletoEmp\" is not null";
        $criteria->condition = " \"statEmpleado\" in ('01','02', '06', '07')";
        $modelHEmpleados = HEmpleados::model()->findAll($criteria);

        foreach ($modelHEmpleados as $model)
            $list_emp[$model->rfcEmpleado] = $model->rfcEmpleado .'  -  '. $model->nmbCompletoEmp;

		return $list_emp;
	}

	public function getEncargadoVisitasAcademicas()
	{
		$qry_of_visit_acad = "select * from public.h_ocupacionpuesto
								where \"cvePuestoGeneral\" = '04' AND \"cvePuestoParticular\" = '03' ";

		$rfc = Yii::app()->createCommand($qry_of_visit_acad)->queryAll();

		return $rfc[0]['rfcEmpleado'];
	}

	public function getCargo($rfc_empleado)
	{
		$rfcEmpleado = trim($rfc_empleado);
		$qry_cargo = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfcEmpleado' ";

		$rs = Yii::app()->db->createCommand($qry_cargo)->queryAll();

		return ($rs[0]['nmbPuesto'] === NULL) ? '-' : $rs[0]['nmbPuesto'];
	}

	public function getResponsablePrincipal($id_solicitud_visitas_academicas, $rfcEmpleado)
	{
		$modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
																	'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
																	'rfcEmpleado' => $rfcEmpleado
																));

		if($modelVaResponsablesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existe información del Empleado con esa Solicitud.');

		return $modelVaResponsablesVisitasAcademicas->responsable_principal;
	}

	public function getDepartamentoEmpleado($cve_depto)
	{
		$cveDepto = trim($cve_depto);
		$qry_depto = "select * from public.\"H_departamentos\" where \"cveDepartamento\" = '$cveDepto' ";
		$rs = Yii::app()->db->createCommand($qry_depto)->queryAll();

		return ($rs[0]['dscDepartamento'] === Null ) ? '-' : $rs[0]['dscDepartamento'];
	}

	public function validarFechasInicioRegresoSolicitudVisitaAcademica($fec_salida, $fec_regreso)
	{
		
		//$originalDate = "2010-03-21";
		$sal = date("Y-m-d H:i:s", strtotime($fec_salida));
		$reg = date("Y-m-d H:i:s", strtotime($fec_regreso));

		return ($sal < $reg) ? true : false;
	}

	public function actionEliminarSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
	{
		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);

		if($modelVaSolicitudesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No hay datos de la Solicitud.');

		//Instanciamos el objeto del meotodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		try
		{
			if($modelVaSolicitudesVisitasAcademicas->delete())
			{
				//Si todas las consultas, funciones e inserts de BD se ejecutaron correctamente entonces hacemos el commit
				$transaction->commit();
				Yii::app()->user->setFlash('success', "Solicitud eliminada correctamente!!!");
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Error!!! No se pudo eliminar la Solicitud.");
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e){

			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			Yii::app()->user->setFlash('danger', "Error!!! No se pudo eliminar la Solicitud.");
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}

	}

	public function getTipoVisitas()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "tipo_valido is true";
		$criteria->order = "id_tipo_visita_academica ASC";

		$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findAll($criteria);

		if($modelVaTiposVisitasAcademicas === NULL)
			throw new CHttpException(404,'No hay datos de los Tipos de Visitas.');

		$lista_tipos = CHtml::listData($modelVaTiposVisitasAcademicas, "id_tipo_visita_academica", "tipo_visita_academica");

		return $lista_tipos;

	}

	public function getEmpresasVisitas()
	{
		$criteria = new CDbCriteria;
		//$criteria->condition = "";
		$criteria->order = "idempresa ASC";
		$modelGRpempresas = GRpempresas::model()->findAll($criteria);

		if($modelGRpempresas === NULL)
			throw new CHttpException(404,'No hay datos de las Empresas a Visitar.');

		$lista_empresas_visitas = CHtml::listData($modelGRpempresas, "idempresa", "nombre");

		return $lista_empresas_visitas;
	}

	public function loadModel($id)
	{
		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id);

		if($modelVaSolicitudesVisitasAcademicas === null)
			throw new CHttpException(404,'No existen datos de la Solicitud.');

		return $modelVaSolicitudesVisitasAcademicas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-solicitudes-visitas-academicas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	//Verificar la informacion que se envia por metodo GET
	public function validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado)
	{
		if($this->isEnteroPositivo($id_solicitud_visitas_academicas))
		{
			$qry_val = " select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND
						rva.\"rfcEmpleado\" = '$rfcEmpleado'
					";

			$datos_validos = Yii::app()->db->createCommand($qry_val)->queryAll();

			if(sizeof($datos_validos) == 0)
				throw new CHttpException(404,'Información no disponible.');

		}else{

			throw new CHttpException(404,'El Valor introducido no es valido.');
		}

		return null;
	}

	public function getEstatusSolicitudes()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " id_estatus_solicitud_visita_academica > 0 ";
		$model = VaEstatusSolicitudVisitaAcademica::model()->findAll($criteria);
		if($model === NULL)
			throw new CHttpException(404,'No hay datos de los estados de las Solicitudes.');

		$lista_estatus = CHtml::listData($model, 'id_estatus_solicitud_visita_academica', 'estatus');

		return $lista_estatus;
	}

	public function Periodos()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " \"numPeriodo\" > '0' ";
		$model = EPeriodos::model()->findAll($criteria);
		if($model === NULL)
			throw new CHttpException(404,'No existe ese Periodo.');

		$lista_periodos = CHtml::listData($model, 'numPeriodo', 'dscPeriodo');

		return $lista_periodos;
	}
}
