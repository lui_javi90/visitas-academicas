<?php 
class VaAlumnosAsistenVisitasAcademicasController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
    }
    
    public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(//'listaAlumnosAsistiraVisitaAcademica', //Lista de Alumnos Agregado a la List
                                //'agregarAlumnoAVisitaAcademica', //Vista Jefe Vinculacion
                                //'miMenuVisitaAcademica', //Se muestra menu cuando lleva mas de una visita academica el docente
                                //'historicoVisitasAcademicas', //Se muestra el hisotorico de las visitas academicas que ha tenido el docente
                                //'eliminarAlumnoVisitaAcademica',
                                //'infoAlumnoSeleccionado',
                                //'validarAlumnosVisitaAcademica' //Valida que la lista de alumnos para evitar que agregen alumnos despues
										 
				),
				'roles' => array('jefe_vinculacion')
				//'users'=>array('@'),
            ),*/
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('agregarAlumnoAVisitaAcademica', //Lista de Alumnos Agregado a la Lista (Docente)
                                'validarAlumnosVisitaAcademica', //Valida que la lista de alumnos para evitar que agregen alumnos despues
                                'infoAlumnoSeleccionado', //Info del Alumno seleccionado para agregar a la lista
                                'eliminarAlumnoVisitaAcademica' //Eliminar Alumnos
                                		 
				),
				'roles' => array('docente')
				//'users'=>array('@'),
            ),
            /*array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('listaAlumnosAsistiraVisitaAcademica', //Lista de Alumnos Agregado a la List
                                
										 
				),
				'roles' => array('subdirector_academico')
				//'users'=>array('@'),
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('listaAlumnosAsistiraVisitaAcademica', //Lista de Alumnos Agregado a la List
                                
										 
				),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			),*/
		);
    }

    //para evitar sigan agregando alumnos
	public function actionValidarAlumnosVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
        $modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 8; //en Curso para evitar sigan agregando alumnos
        $modelVaSolicitudesVisitasAcademicas->val_lista_alumnos_visita_academica = date('Y-m-d H:i:s');

		try
		{
			if($modelVaSolicitudesVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
				//Si se realizo correctamente el cambio
                $transaction->commit();
                echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al validar la Solicitud.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
			}

		}catch(Exception $e)
		{
			Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al tratar de Validar la solicitud.");
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			echo CJSON::encode( $modelVaSolicitudesVisitasAcademicas->getErrors() );
		}
	}

    public function actionInfoAlumnoSeleccionado()
    {
        if (Yii::app()->request->isAjaxRequest)
		{
            $no_ctrl = trim(Yii::app()->request->getParam('nctrAlumno'));

            if($no_ctrl != NULL)
            {
                //$modelEDatosAlumno = EDatosAlumno::model()->findByPK($no_ctrl);
                $criteria = new CDbCriteria;
                $criteria->condition = " \"statAlumno\" = 'VI' AND \"insAlumno\" = 'S' AND \"nctrAlumno\" = '$no_ctrl' ";
                $modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

                if($modelEDatosAlumno != NULL)
                {
                    $nombre_alumno = $modelEDatosAlumno->nmbAlumno;
                    $id_carrera = $modelEDatosAlumno->cveEspecialidadAlu;
                    $modelEEspecialidad = EEspecialidad::model()->findByPk($id_carrera);
                    if($modelEEspecialidad === NULL)
                        $carrera = "DESCONOCIDA";

                    $carrera = $modelEEspecialidad->dscEspecialidad;
                    $semestre = $modelEDatosAlumno->semAlumno;
                    $status_campo = 'NV'; //No. control No Vacio


                }else{

                    $nombre_alumno = "";
                    $carrera = "";
                    $semestre = "";
                    $status_campo = 'NEA'; //No. control Vacio
                }

            }else{

                $no_ctrl = "";
                $nombre_alumno = "";
                $carrera = "";
                $semestre = "";
                $status_campo = 'V'; //No Existe Alumno en la BD con ese no. control
            }

             //datos enviados al form por AJAX
			echo CJSON::encode(array(

                'no_ctrl' => $no_ctrl,
                'nombre_alumno' => $nombre_alumno,
                'carrera' => $carrera,
                'semestre' => $semestre,
                'status_campo' => $status_campo
			));

			Yii::app()->end();
        }
    }

    public function actionListaAlumnosAsistiraVisitaAcademica()
    {
        //Tomar del inicio de sesion del Empleado en el SII
        //$rfc_empleado = Yii::app()->params['rfcSupervisor'];
        $rfc_empleado = Yii::app()->user->name;
        $rfcEmpleado = trim($rfc_empleado);

        //Agregar los Solicitudes que tenga aprobadas el 
        $modelVaAlumnosAsistenVisitasAcademicas = new VaAlumnosAsistenVisitasAcademicas('search');
        $modelVaAlumnosAsistenVisitasAcademicas->unsetAttributes();  // clear any default values

        if(isset($_GET['VaAlumnosAsistenVisitasAcademicas']))
        {
            $modelVaAlumnosAsistenVisitasAcademicas->attributes=$_GET['VaAlumnosAsistenVisitasAcademicas'];
        }

        $this->render('listaAlumnosAsistiraVisitaAcademica',array(
                      'modelVaAlumnosAsistenVisitasAcademicas'=>$modelVaAlumnosAsistenVisitasAcademicas,
                      'rfcEmpleado' => $rfcEmpleado
        ));
    }
    
    public function actionAgregarAlumnoAVisitaAcademica($id_solicitud_visitas_academicas)
    {
        //$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

        $this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);
        
        $criteria = new CDbCriteria;
        $criteria->condition = "id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
        $modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);

        if($modelVaSolicitudesVisitasAcademicas === NULL)
            throw new CHttpException(404,'No hay datos de la Solicitud.');

        //Obtener Empresa
        $empresa = $this->getEmpresa($modelVaSolicitudesVisitasAcademicas->id_empresa_visita);
        $lista_alumnos = $this->getAlumnosValidos(); //Se quito el select

        $modelVaAlumnosAsistenVisitasAcademicas = new VaAlumnosAsistenVisitasAcademicas;

        if(isset($_POST['VaAlumnosAsistenVisitasAcademicas']))
        {
            
            $modelVaAlumnosAsistenVisitasAcademicas->attributes = $_POST['VaAlumnosAsistenVisitasAcademicas'];
            $modelVaAlumnosAsistenVisitasAcademicas->id_solicitud_visitas_academicas = $id_solicitud_visitas_academicas;
            $modelVaAlumnosAsistenVisitasAcademicas->fecha_registro_alumno = date('Y-m-d');

            if($this->existeAlumnoYEstaVigente(trim($modelVaAlumnosAsistenVisitasAcademicas->nctrAlumno)))
            {
                //Validar que el mismo alumno NO sea agregado mas de una vez
                if($this->getAlumnoNoRepetidoEnVisitaAcademica($modelVaAlumnosAsistenVisitasAcademicas->nctrAlumno, $id_solicitud_visitas_academicas))
                {
                    //Validar que la insercion de datos del alumno
                    if($modelVaAlumnosAsistenVisitasAcademicas->save())
                    {
                        Yii::app()->user->setFlash('success', "Solicitud Validada correctamente!!!");
                        //$this->redirect(array('listaAlumnosAsistiraVisitaAcademica'));

                    }else{

                        Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al agregar al Alumno a la Visita Académica.");
                        //die("Error!!! ocurrio un error al agregar al Alumno a la Visita Académica.");
                    }

                }else{

                    Yii::app()->user->setFlash('danger', "Error!!! El Alumno ya ha sido agregado a esta Visita Académica.");
                    //die("Error!!! El Alumno ya ha sido agregado a esta Visita Académica.");

                }
            }
        }

        //Lista de Alumnos inscritos en la Visita Academica
        $modelListaVaAlumnosAsistenVisitasAcademicas = new VaAlumnosAsistenVisitasAcademicas('search');
        $modelListaVaAlumnosAsistenVisitasAcademicas->unsetAttributes();  // clear any default values

        if(isset($_GET['VaAlumnosAsistenVisitasAcademicas']))
        {
            $modelVaAlumnosAsistenVisitasAcademicas->attributes=$_GET['VaAlumnosAsistenVisitasAcademicas'];
        }

        $this->render('agregarAlumnoAVisitaAcademica',array(
                      'modelVaAlumnosAsistenVisitasAcademicas' => $modelVaAlumnosAsistenVisitasAcademicas,
                      'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
                      'modelListaVaAlumnosAsistenVisitasAcademicas' => $modelListaVaAlumnosAsistenVisitasAcademicas,
                      'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
                      'empresa' => $empresa,
                      'lista_alumnos' => $lista_alumnos
                      
        ));
    }

    public function existeAlumnoYEstaVigente($nctrAlumno)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " \"statAlumno\" = 'VI' AND \"insAlumno\" = 'S' AND \"nctrAlumno\" = '$nctrAlumno' ";
        $modelEDatosAlumno = EDatosAlumno::model()->find($criteria);

        return ($modelEDatosAlumno === NULL) ? false : true;
    }

    public function getAlumnosValidos()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = " \"statAlumno\" = 'VI' AND \"insAlumno\" = 'S' ";
        $modelEDatosAlumno = EDatosAlumno::model()->findAll($criteria);

        if($modelEDatosAlumno === NULL)
            throw new CHttpException(404,'No hay Alumnos que cumplan esas condiciones.');

        foreach ($modelEDatosAlumno as $model)
            $list_alumno[$model->nctrAlumno] = $model->nctrAlumno.'  -  '.$model->nmbAlumno;

        return $list_alumno;
    }

    //Verificar que el alunmo este vigente o inscrito
    public function getAlumnoNoRepetidoEnVisitaAcademica($_no_ctrl, $_id_solicitud_visitas_academicas)
    {
        $bandera = false;

        //Validamos parametros no nulos
        if($_no_ctrl != NULL AND $_id_solicitud_visitas_academicas != NULL)
        {
            $modelVaAlumnosAsistenVisitasAcademicas = VaAlumnosAsistenVisitasAcademicas::model()->findAllByAttributes(array(
                                                                        'id_solicitud_visitas_academicas' => $_id_solicitud_visitas_academicas,
                                                                        'nctrAlumno' => $_no_ctrl
                                                                ));

            //Validar que este alumno no este duplicado en la lista de la visita industrial en la que se quiere inscribir
            if($modelVaAlumnosAsistenVisitasAcademicas != NULL)
                $bandera = false;
            else
                $bandera = true;


        }else{

            $bandera = false;
        }
           
        return $bandera;
    }

    //Cuando se quiere eliminar a un alumno que no va ir a la Visita Academica
    public function actionEliminarAlumnoVisitaAcademica($id_solicitud_visitas_academicas, $nctrAlumno)
    {
        //Instanciamos objeto de tipo transaccion
        $transaction = Yii::app()->db->beginTransaction();

        try
        {

            if(VaAlumnosAsistenVisitasAcademicas::model()->deleteAll(" id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND \"nctrAlumno\" = '$nctrAlumno' "))
            {
                echo CJSON::encode( [ 'code' => 200 ] );
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Solicitud Eliminada correctamente!!!");

            }else{

                echo CJSON::encode( $modelVaAlumnosAsistenVisitasAcademicas->getErrors() );
                $transaction->rollBack();
                Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al eliminar la solicitud.");

            }

        }catch(Exception $e){

            echo CJSON::encode( $modelVaAlumnosAsistenVisitasAcademicas->getErrors() );
            $transaction->rollBack();
            Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al eliminar la solicitud.");
        }

    }

    public function getEmpresa($id_empresa_visita)
	{
		$modelGRpempresas = GRpempresas::model()->findByPk($id_empresa_visita);

		if($modelGRpempresas === NULL)
            throw new CHttpException(404,'No hay datos de las Empresas a Visitar.');

		return $modelGRpempresas->nombre;
	}

	public function getPeriodo($per)
	{
		$qry_per = "select * from public.\"E_periodos\" where \"numPeriodo\" = '$per' ";
		$rs = Yii::app()->db->createCommand($qry_per)->queryAll();

		return ($rs[0]['dscPeriodo'] === null) ? '-' : $rs[0]['dscPeriodo'];
    }
    
    //Verificar la informacion que se envia por metodo GET
	public function validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado)
	{
		if($this->isEnteroPositivo($id_solicitud_visitas_academicas))
		{
			$qry_val = " select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND 
						rva.\"rfcEmpleado\" = '$rfcEmpleado'
					";

			$datos_validos = Yii::app()->db->createCommand($qry_val)->queryAll();

			if(sizeof($datos_validos) == 0)
				throw new CHttpException(404,'Información no disponible.');

		}else{

			throw new CHttpException(404,'El Valor introducido no es valido.');
		}

		return null;
    }
    
    //Valida que el dato introducido sea un numero entero
    public function isEnteroPositivo($valor)
	{
  		$bandera = true;

  		if(!preg_match('/^[0-9]+$/', $valor)) //Si entra entonces es entero
  			$bandera = false;

  		return $bandera;
	}
}


?>