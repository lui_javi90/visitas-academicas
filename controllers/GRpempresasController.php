<?php

class GRpempresasController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaEmpresasVisitasAcademicas', //Vista Jefe de Vinculacion
								 'nuevaEmpresaVisitaAcademica', //Nueva Empresa
								 'detalleEmpresaVisitaAcademica', //Vista Jefe de Vinculacion
								 'editarEmpresaVisitaAcademica', //Vista Jefe de Vinculacion
								 'getMunicipiosEstado', //Mostrar los municipios de acuerdo al estado seleccionado
								 'eliminarEmpresaVisitaAcademica', //Eliminar Empresa
								 'agregarManifiestoRequisitosSeguridadIngresoEmpresa' //Agregar Manifiesto para los requisitos de seguridad
								 
				),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			),
		);
	}

	public function actionAgregarManifiestoRequisitosSeguridadIngresoEmpresa($idempresa)
	{
		$modelGRpempresas = $this->loadModel($idempresa);

		$this->render('agregarManifiestoRequisitosSeguridadIngresoEmpresa',array(
					  'modelGRpempresas'=>$modelGRpempresas,
		));
		
	}

	public function actionListaEmpresasVisitasAcademicas()
	{
		$modelGRpempresas = new GRpempresas('search');
		$modelGRpempresas->unsetAttributes();  // clear any default values

		//Agregar filtros de sector de la empresa
		$lista_sectores_empresa = $this->getSectoresEmpresas();
		//Estados
		$lista_estados = $this->getEstados();
		//Municipios
		$lista_municipios = $this->getMunicipios();

		if(isset($_GET['GRpempresas']))
			$modelGRpempresas->attributes=$_GET['GRpempresas'];

		$this->render('listaEmpresasVisitasAcademicas',array(
					  'modelGRpempresas'=>$modelGRpempresas,
					  'lista_sectores_empresa' => $lista_sectores_empresa,
					  'lista_estados' => $lista_estados,
					  'lista_municipios' => $lista_municipios
		));
	}

	public function actionNuevaEmpresaVisitaAcademica()
	{
		//require_once Yii::app()->basePath.'/models/CMN/XEstados.php';
		$modelGRpempresas = new GRpempresas;

		//Lista Sectores
		$lista_sectores = $this->getSectoresEmpresas();

		//Lista de estados
		$lista_estados = $this->getEstados();
		//$lista_municipios = $this->getMunicipios();
		
        if(isset($_POST['GRpempresas']))
        {
			$modelGRpempresas->attributes = $_POST['GRpempresas'];
			$modelGRpempresas->rfc = strtoupper($modelGRpempresas->rfc);

			if($this->is_valid_email($modelGRpempresas->email))
			{
			
				if($modelGRpempresas->save())
				{
					Yii::app()->user->setFlash('success', "Empresa registrada correctamente!!!");
					$this->redirect(array('listaEmpresasVisitasAcademicas'));

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos de la Empresa.");
				}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! el correo que introdujo no es valido.");
			}
		}
		
        $this->render('nuevaEmpresaVisitaAcademica',array(
					  'modelGRpempresas'=>$modelGRpempresas,
					  'lista_sectores' => $lista_sectores,
					  'lista_estados' => $lista_estados,
					  //'lista_municipios' => $lista_municipios
        ));
	}

	public function actionEditarEmpresaVisitaAcademica($idempresa)
	{
		$modelGRpempresas = $this->loadModel($idempresa);

		//Lista Sectores
		$lista_sectores = $this->getSectoresEmpresas();

		//Lista de estados
		$lista_estados = $this->getEstados();
		//$lista_municipios = $this->getMunicipios();

		if(isset($_POST['GRpempresas']))
		{
			$modelGRpempresas->attributes = $_POST['GRpempresas'];
			$modelGRpempresas->rfc = strtoupper($modelGRpempresas->rfc);

			if($this->is_valid_email($modelGRpempresas->email))
			{

				if($modelGRpempresas->save())
				{
					Yii::app()->user->setFlash('success', "Empresa registrada correctamente!!!");
					$this->redirect(array('listaEmpresasVisitasAcademicas'));

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! no se actualizaron los datos de la Empresa.");
				}

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! el correo que introdujo no es valido.");
			}
		}

		$this->render('editarEmpresaVisitaAcademica',array(
					  'modelGRpempresas'=>$modelGRpempresas,
					  'lista_sectores' => $lista_sectores,
					  'lista_estados' => $lista_estados
		));
	}

	//Eliminar Empresa (Duda)
	public function actionEliminarEmpresaVisitaAcademica($idempresa)
	{

		$modelGRpempresas = $this->loadModel($idempresa);

		if(!$this->haSidoAsignadaASoliictud($modelGRpempresas->idempresa))
		{
			//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
			$transaction = Yii::app()->db->beginTransaction();

			try
			{
				if($modelGRpempresas->delete())
				{
					Yii::app()->user->setFlash('success', "Empresa Eliminada correctamente!!!");
					//Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
					$transaction->commit();
					echo CJSON::encode( [ 'code' => 200 ] );

				}else{

					Yii::app()->user->setFlash('danger', "Error!!! no se pudo eliminar la Empresa.");
					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					echo CJSON::encode( $modelGRpempresas->getErrors() );
				}

			}catch(Exception $e)
			{
				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al eliminar la Empresa.");
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				echo CJSON::encode( [ 'code' => 200 ] );
			}

		}else{

			Yii::app()->user->setFlash('danger', "Error!!! La Empresa ya ha sido asignada a una Solicitud.");
			echo CJSON::encode( [ 'code' => 200 ] );
		}
	}

	public function haSidoAsignadaASoliictud($idempresa)
	{
		$qry_emp = "select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
					join pe_vinculacion.g_rpempresas remp
					on remp.idempresa = sva.id_empresa_visita
					where sva.id_empresa_visita = '$idempresa' ";

		$rs = Yii::app()->db->createCommand($qry_emp)->queryAll();

		return (sizeof($rs) > 0) ? true : false;
	}

	/*
	* Valida un email usando expresiones regulares. 
	* Devuelve true si es correcto o false en caso contrario
	*/
	function is_valid_email($str)
	{
 		$matches = null;
  		return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
	}	

	public function getEstados()
	{

		$criteria = new CDbCriteria;
		$criteria->condition = " id_estado > '00' AND id_estado < '33' ";
		$criteria->order = "id_estado ASC";
		$modelXEstados = XEstados::model()->findAll($criteria);

		$lista_estados = CHtml::listData($modelXEstados, 'id_estado', 'desc_estado');

		return $lista_estados;
	}

	public function getMunicipios()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = " id_estado > '00' AND id_estado < '33' ";
		$criteria->order = "id_estado ASC";
		$modelXMunicipios = XMunicipios::model()->findAll();

		$lista_municipios = CHtml::listData($modelXMunicipios, 'id_municipio', 'desc_mun');

		return $lista_municipios;
	}

	public function getSectoresEmpresas()
	{
		$modelGRpsectorempresa = GRpsectorempresa::model()->findAll();

		$lista_sectores = CHtml::listData($modelGRpsectorempresa, 'idsector', 'descripcion');

		return $lista_sectores;
	}

	public function actionGetMunicipiosEstado()
	{
		$id_estado = trim($_POST['GRpempresas']['idestado']);

		$criteria = new CDbCriteria();
		$criteria->condition = " id_estado = '$id_estado' ";
		$criteria->order = "id_municipio ASC";
		$modelXMunicipios = XMunicipios::model()->findAll($criteria);
		foreach($modelXMunicipios as $data)
			echo "<option value=\"{$data->id_municipio}\">{$data->desc_mun}</option>";

	}

	public function actionDetalleEmpresaVisitaAcademica($idempresa)
	{
		$this->layout='//layouts/mainVacio';

		$modelGRpempresas = $this->loadModel($idempresa);

		//Info de la Empresa
		$sector = $this->getSector($modelGRpempresas->idsector);
		$estado = $this->getEstado($modelGRpempresas->idestado);
		$municipio = $this->getMunicipio($modelGRpempresas->idmunicipio);

		$this->render('detalleEmpresaVisitaAcademica',array(
					  'modelGRpempresas' => $modelGRpempresas,
					  'sector' => $sector,
					  'estado' => $estado,
					  'municipio' => $municipio
		));
	}

	public function getSector($idsector)
	{
		$id = $idsector;
		$qry_sec = "select * from pe_vinculacion.g_rpsectorempresa where idsector = '$id' ";
		$rs = Yii::app()->db->createCommand($qry_sec)->queryAll();

		return $rs[0]['descripcion'];
	}

	public function getEstado($id_estado)
	{
		$id_edo = $id_estado;
		$qry_estado = "select * from public.\"X_estados\" where id_estado = '$id_edo' ";
		$rs = Yii::app()->db->createCommand($qry_estado)->queryAll();

		return $rs[0]['desc_estado'];
	}

	public function getMunicipio($id_municipio)
	{
		$id_mun = $id_municipio;
		//$id_edo = $data->idestado;
		$qry_mun = "select * from public.\"X_municipios\" where id_municipio = '$id_mun' ";
		$rs = Yii::app()->db->createCommand($qry_mun)->queryAll();

		return $rs[0]['desc_mun'];
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GRpempresas']))
		{
			$model->attributes=$_POST['GRpempresas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idempresa));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GRpempresas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelGRpempresas = GRpempresas::model()->findByPk($id);

		if($modelGRpempresas === null)
			throw new CHttpException(404,'No existen datos de esa Empresa.');

		return $modelGRpempresas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='grpempresas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
