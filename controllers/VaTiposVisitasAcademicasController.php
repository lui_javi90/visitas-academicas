<?php

class VaTiposVisitasAcademicasController extends Controller
{
	
	//public $layout='//layouts/column2';

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaTiposVisitasAcademicas', //Vista Jefe Vinculacion
								 'listaDTiposVisitasAcademicas', //VIsta Docente, Subdirector y Academico y Jefe Proyecto Planeacion
								 'nuevoTipoVisitaAcademica',
								 'editarTipoVisitaAcademica',
								 'deshabilitarTipoVisitaAcademica'
								 //'editarSolicitudVisitaAcademica',
								 //'eliminarSolicitudVisitaAcademica'
								 
				),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			),
		);
	}

	public function actionListaTiposVisitasAcademicas()
	{
		$modelVaTiposVisitasAcademicas = new VaTiposVisitasAcademicas('search');
		$modelVaTiposVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaTiposVisitasAcademicas']))
			$modelVaTiposVisitasAcademicas->attributes=$_GET['VaTiposVisitasAcademicas'];

		$this->render('listaTiposVisitasAcademicas',array(
					  'modelVaTiposVisitasAcademicas'=>$modelVaTiposVisitasAcademicas,
		));
	}

	public function actionListaDTiposVisitasAcademicas()
	{
		$modelVaTiposVisitasAcademicas = new VaTiposVisitasAcademicas('search');
		$modelVaTiposVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaTiposVisitasAcademicas']))
			$modelVaTiposVisitasAcademicas->attributes=$_GET['VaTiposVisitasAcademicas'];

		$this->render('listaDTiposVisitasAcademicas',array(
					  'modelVaTiposVisitasAcademicas'=>$modelVaTiposVisitasAcademicas,
		));
	}

	public function actionNuevoTipoVisitaAcademica()
	{
		$modelVaTiposVisitasAcademicas = new VaTiposVisitasAcademicas;

		if(isset($_POST['VaTiposVisitasAcademicas']))
		{
			$modelVaTiposVisitasAcademicas->attributes=$_POST['VaTiposVisitasAcademicas'];

			if($modelVaTiposVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Registro guardado correctamente!!!");
				$this->redirect(array('listaTiposVisitasAcademicas'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos.");
			}
		}

		$this->render('nuevoTipoVisitaAcademica',array(
					  'modelVaTiposVisitasAcademicas'=>$modelVaTiposVisitasAcademicas,
		));
	}

	public function actionDeshabilitarTipoVisitaAcademica($id_tipo_visita_academica, $val = null)
	{
		
		$modelVaTiposVisitasAcademicas = $this->loadModel($id_tipo_visita_academica);
		$val = ($val === null) ? 1 : 0;
		$modelVaTiposVisitasAcademicas->tipo_valido = ($val == 0) ? false : true;

		//Instanciamos el objeto del meotodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();
		try
		{

			if($modelVaTiposVisitasAcademicas->save())
			{
				//Si todas las consultas, funciones e inserts de BD se ejecutaron correctamente entonces hacemos el commit
				$transaction->commit();

				//Si todo salio bien se redirige a la vista 
				echo CJSON::encode( [ 'code' => 200 ] );

			}else{

				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al desactivar el Tipo de Visita Académica.");
				echo CJSON::encode( $modelRhHPlazasEmpleadoRHumanos->getErrors() );
			}

		}catch(Exception $e){
			
			//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
			$transaction->rollback();
			Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al realizar la acción desactivar el Tipo de Visita Académica.");
			echo CJSON::encode( $modelRhHPlazasEmpleadoRHumanos->getErrors() );
		}
	}

	public function actionEditarTipoVisitaAcademica($id_tipo_visita_academica)
	{
		$modelVaTiposVisitasAcademicas = $this->loadModel($id_tipo_visita_academica);

		if(isset($_POST['VaTiposVisitasAcademicas']))
		{
			$modelVaTiposVisitasAcademicas->attributes=$_POST['VaTiposVisitasAcademicas'];

			if($modelVaTiposVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Registro guardado correctamente!!!");
				$this->redirect(array('listaTiposVisitasAcademicas'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se guardaron los datos.");
			}
		}

		$this->render('editarTipoVisitaAcademica',array(
					  'modelVaTiposVisitasAcademicas'=>$modelVaTiposVisitasAcademicas,
		));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaTiposVisitasAcademicas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelVaTiposVisitasAcademicas = VaTiposVisitasAcademicas::model()->findByPk($id);

		if($modelVaTiposVisitasAcademicas === null)
			throw new CHttpException(404,'No existe ningun registro de Tipo de visita académica.');

		return $modelVaTiposVisitasAcademicas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-tipos-visitas-academicas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
