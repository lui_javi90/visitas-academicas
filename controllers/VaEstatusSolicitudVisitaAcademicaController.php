<?php

class VaEstatusSolicitudVisitaAcademicaController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaVEstatusSolicitudVisitaAcademica', //Vista de Roxana Vinculacion
								'editarEstatusSolicitudVisitaAcademica'
							),
				'roles'=>array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaEstatusSolicitudVisitaAcademica'

							),
				'roles'=>array('docente', 'jefe_vinculacion', 'subdirector_academico', 'jefe_departamento_academico')
				//'users'=>array('*'),
			),
		);
	}

	public function actionListaEstatusSolicitudVisitaAcademica()
	{
		$modelVaEstatusSolicitudVisitaAcademica = new VaEstatusSolicitudVisitaAcademica('search');
		$modelVaEstatusSolicitudVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaEstatusSolicitudVisitaAcademica']))
		{
			$modelVaEstatusSolicitudVisitaAcademica->attributes=$_GET['VaEstatusSolicitudVisitaAcademica'];
		}

		$this->render('listaEstatusSolicitudVisitaAcademica',array(
					'modelVaEstatusSolicitudVisitaAcademica'=>$modelVaEstatusSolicitudVisitaAcademica,
		));
	}

	public function actionListaVEstatusSolicitudVisitaAcademica()
	{
		$modelVaEstatusSolicitudVisitaAcademica = new VaEstatusSolicitudVisitaAcademica('search');
		$modelVaEstatusSolicitudVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaEstatusSolicitudVisitaAcademica']))
		{
			$modelVaEstatusSolicitudVisitaAcademica->attributes=$_GET['VaEstatusSolicitudVisitaAcademica'];
		}

		$this->render('listaVEstatusSolicitudVisitaAcademica',array(
					'modelVaEstatusSolicitudVisitaAcademica'=>$modelVaEstatusSolicitudVisitaAcademica,
		));
	}

	public function actionEditarEstatusSolicitudVisitaAcademica($id_estatus_solicitud_visita_academica)
	{
		$modelVaEstatusSolicitudVisitaAcademica = $this->loadModel($id_estatus_solicitud_visita_academica);

		if(isset($_POST['VaEstatusSolicitudVisitaAcademica']))
		{
			$modelVaEstatusSolicitudVisitaAcademica->attributes = $_POST['VaEstatusSolicitudVisitaAcademica'];
			if($modelVaEstatusSolicitudVisitaAcademica->save())
			{
				Yii::app()->user->setFlash('success', "Estatus actualizado correctamente!!!");
				$this->redirect(array('listaVEstatusSolicitudVisitaAcademica'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! No se pudo actualizar el Estatus.");
			}
		}

		$this->render('editarEstatusSolicitudVisitaAcademica',array(
					  'modelVaEstatusSolicitudVisitaAcademica'=>$modelVaEstatusSolicitudVisitaAcademica,
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new VaEstatusSolicitudVisitaAcademica;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VaEstatusSolicitudVisitaAcademica']))
		{
			$model->attributes=$_POST['VaEstatusSolicitudVisitaAcademica'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_estatus_solicitud_visita_academica));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaEstatusSolicitudVisitaAcademica');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelVaEstatusSolicitudVisitaAcademica = VaEstatusSolicitudVisitaAcademica::model()->findByPk($id);

		if($modelVaEstatusSolicitudVisitaAcademica === null)
			throw new CHttpException(404,'No existe Estatus con ese ID.');

		return $modelVaEstatusSolicitudVisitaAcademica;
	}

	/**
	 * Performs the AJAX validation.
	 * @param VaEstatusSolicitudVisitaAcademica $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-estatus-solicitud-visita-academica-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
