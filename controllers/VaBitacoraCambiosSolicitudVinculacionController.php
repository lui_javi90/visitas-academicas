<?php

class VaBitacoraCambiosSolicitudVinculacionController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaBitacoraCambiosVisitaAcademica',
								//'view'
						),
				'roles' => array('docente')
				//'users'=>array('*'),
			),
		);
	}

	//Lista de cambios Por Solicitud Visita Academica 
	public function actionListaBitacoraCambiosVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//Tomar del inicio de sesion del Empleado en el SII
		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Roxana
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);

		$modelVaBitacoraCambiosSolicitudVinculacion = new VaBitacoraCambiosSolicitudVinculacion('search');
		$modelVaBitacoraCambiosSolicitudVinculacion->unsetAttributes();  // clear any default values

		//Por simple inspeccion verificamos si la Solicitud Existe
		$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
		if($modelVaSolicitudesVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existe la Solicitud de Visita Académica.');

		if(isset($_GET['VaBitacoraCambiosSolicitudVinculacion']))
			$modelVaBitacoraCambiosSolicitudVinculacion->attributes=$_GET['VaBitacoraCambiosSolicitudVinculacion'];

		$this->render('listaBitacoraCambiosVisitaAcademica',array(
					  'modelVaBitacoraCambiosSolicitudVinculacion'=>$modelVaBitacoraCambiosSolicitudVinculacion,
					  'id_solicitud_visitas_academicas' => $modelVaSolicitudesVisitasAcademicas->id_solicitud_visitas_academicas,
					  //'id' => $VaBitacoraCambiosSolicitudVinculacion->id_solicitud_visitas_academicas
		));
	}

	//Verificar la informacion que se envia por metodo GET
	public function validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado)
	{
		if($this->isEnteroPositivo($id_solicitud_visitas_academicas))
		{
			$qry_val = " select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND 
						rva.\"rfcEmpleado\" = '$rfcEmpleado'
					";

			$datos_validos = Yii::app()->db->createCommand($qry_val)->queryAll();

			if(sizeof($datos_validos) == 0)
				throw new CHttpException(404,'Información no disponible.');

		}else{

			throw new CHttpException(404,'El Valor introducido no es valido.');
		}

		return null;
	}

	//Valida que el dato introducido sea un numero entero
    public function isEnteroPositivo($valor)
	{
  		$bandera = true;

  		if(!preg_match('/^[0-9]+$/', $valor)) //Si entra entonces es entero
  			$bandera = false;

  		return $bandera;
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new VaBitacoraCambiosSolicitudVinculacion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VaBitacoraCambiosSolicitudVinculacion']))
		{
			$model->attributes=$_POST['VaBitacoraCambiosSolicitudVinculacion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_bitacora_solicitud_visita_academica));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VaBitacoraCambiosSolicitudVinculacion']))
		{
			$model->attributes=$_POST['VaBitacoraCambiosSolicitudVinculacion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_bitacora_solicitud_visita_academica));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaBitacoraCambiosSolicitudVinculacion');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new VaBitacoraCambiosSolicitudVinculacion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VaBitacoraCambiosSolicitudVinculacion']))
			$model->attributes=$_GET['VaBitacoraCambiosSolicitudVinculacion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$modelVaBitacoraCambiosSolicitudVinculacion = VaBitacoraCambiosSolicitudVinculacion::model()->findByPk($id);

		if($modelVaBitacoraCambiosSolicitudVinculacion === null)
			throw new CHttpException(404,'No se encontraron datos de Bitacora con ese ID.');

		return $modelVaBitacoraCambiosSolicitudVinculacion;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-bitacora-cambios-solicitud-vinculacion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
