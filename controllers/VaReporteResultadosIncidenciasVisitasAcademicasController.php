<?php

class VaReporteResultadosIncidenciasVisitasAcademicasController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaReporteResultadosIncidenciasVisitaAcademica',
								'nuevoReporteResultadosIncidenciasVisitaAcademica',
								'editarReporteResultadosIncidenciasVisitaAcademica',
								'validarReporteResultadosEIncidencias' //Validar el reporte de resultados e incidencias
							),
				'roles' => array('docente')
				//'users'=>array('@'),
			),
		);
	}

	public function actionNuevoReporteResultadosIncidenciasVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);

		//Instanciamos el objeto del metodo CDbConnection::beginTransaction.
		$transaction = Yii::app()->db->beginTransaction();

		$modelVaReporteResultadosIncidenciasVisitasAcademicas = new VaReporteResultadosIncidenciasVisitasAcademicas;

		if(isset($_POST['VaReporteResultadosIncidenciasVisitasAcademicas']))
		{
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->attributes = $_POST['VaReporteResultadosIncidenciasVisitasAcademicas'];
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->id_reporte_resultados_incidencias = $id_solicitud_visitas_academicas;
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->fecha_creacion_reporte = date('Y-m-d H:i:s');
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->valida_docente_reponsable = date('Y-m-d H:i:s');

			try
			{
				$criteria = new CDbCriteria;
				$criteria->condition = " id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";
				$modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->find($criteria);
				if($modelVaSolicitudesVisitasAcademicas === NULL)
					throw new CHttpException(404,'No se encontró Reporte de Resultados e incidencias de la Visita Académica.');

				$modelVaSolicitudesVisitasAcademicas->id_estatus_solicitud_visita_academica = 5; //COMPLETADA

				if($modelVaSolicitudesVisitasAcademicas->save())
				{
					if($modelVaReporteResultadosIncidenciasVisitasAcademicas->save())
					{
						Yii::app()->user->setFlash('succcess', "Información guardada correctamente!!!");
						//Si se realizo correctamente el cambio
						$transaction->commit();
						$this->redirect(array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'));

					}else{

						//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
						$transaction->rollback();
						Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar la información.");
					}

				}else{

					//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
					$transaction->rollback();
					Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar la información.");
				}

			}catch(Exception $e)
			{
				//Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar la información.");
			}
		}

		$this->render('nuevoReporteResultadosIncidenciasVisitaAcademica',array(
					  'modelVaReporteResultadosIncidenciasVisitasAcademicas'=>$modelVaReporteResultadosIncidenciasVisitasAcademicas,
					  'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas
		));
	}

	//Si se puede editar el reporte
	public function actionEditarReporteResultadosIncidenciasVisitaAcademica($id_solicitud_visitas_academicas)
	{
		//$rfc_empleado = Yii::app()->params['rfcSupervisor']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
		$rfc_empleado = Yii::app()->user->name;
		$rfcEmpleado = trim($rfc_empleado);

		$this->validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado);
		
		$modelVaReporteResultadosIncidenciasVisitasAcademicas = $this->loadModel($id_solicitud_visitas_academicas);

		if(isset($_POST['VaReporteResultadosIncidenciasVisitasAcademicas']))
		{
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->attributes = $_POST['VaReporteResultadosIncidenciasVisitasAcademicas'];
			$modelVaReporteResultadosIncidenciasVisitasAcademicas->id_estatus_solicitud_visita_academica = 5; //COMPLETADA

			if($modelVaReporteResultadosIncidenciasVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('succcess', "Información guardada correctamente!!!");
				$this->redirect(array('vaSolicitudesVisitasAcademicas/listaSolicitudesVisitasAcademicas'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! no se pudo guardar la información.");
			}
		}

		$this->render('editarReporteResultadosIncidenciasVisitaAcademica',array(
					 'modelVaReporteResultadosIncidenciasVisitasAcademicas'=>$modelVaReporteResultadosIncidenciasVisitasAcademicas
		));
	}

	//Verificar la informacion que se envia por metodo GET
	public function validaInfo($id_solicitud_visitas_academicas, $rfcEmpleado)
	{
		if($this->isEnteroPositivo($id_solicitud_visitas_academicas))
		{
			$qry_val = " select * from pe_vinculacion.va_solicitudes_visitas_academicas sva
						join pe_vinculacion.va_responsables_visitas_academicas rva
						on rva.id_solicitud_visitas_academicas = sva.id_solicitud_visitas_academicas
						where sva.id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' AND 
						rva.\"rfcEmpleado\" = '$rfcEmpleado'
					";

			$datos_validos = Yii::app()->db->createCommand($qry_val)->queryAll();

			if(sizeof($datos_validos) == 0)
				throw new CHttpException(404,'Información no disponible.');

		}else{

			throw new CHttpException(404,'El Valor introducido no es valido.');
		}

		return null;
	}

	//Valida que el dato introducido sea un numero entero
    public function isEnteroPositivo($valor)
	{
  		$bandera = true;

  		if(!preg_match('/^[0-9]+$/', $valor)) //Si entra entonces es entero
  			$bandera = false;

  		return $bandera;
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaReporteResultadosIncidenciasVisitasAcademicas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new VaReporteResultadosIncidenciasVisitasAcademicas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VaReporteResultadosIncidenciasVisitasAcademicas']))
			$model->attributes=$_GET['VaReporteResultadosIncidenciasVisitasAcademicas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$modelVaReporteResultadosIncidenciasVisitasAcademicas = VaReporteResultadosIncidenciasVisitasAcademicas::model()->findByPk($id);

		if($modelVaReporteResultadosIncidenciasVisitasAcademicas===null)
			throw new CHttpException(404,'No se encontró Reporte de Resultados e incidencias de la Visita Académica.');

		return $modelVaReporteResultadosIncidenciasVisitasAcademicas;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-reporte-resultados-incidencias-visitas-academicas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
