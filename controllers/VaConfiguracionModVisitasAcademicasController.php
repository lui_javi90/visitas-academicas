<?php

class VaConfiguracionModVisitasAcademicasController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('configuracionModuloVisitaAcademica',
								'nuevoConfiguracionModuloVisitaAcademica',
								'editarConfiguracionModuloVisitaAcademica'
						),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			),
		);
	}

	public function actionConfiguracionModuloVisitaAcademica()
	{
		require_once Yii::app()->basePath.'/modules/visitasacademicas/staticClasses/InfoSolicitudVisitasAcademicas.php';

		$VaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk(1);
		if($VaConfiguracionModVisitasAcademicas === NULL)
			throw new CHttpException(404,'No existen información de la configuración.');


		$id = $VaConfiguracionModVisitasAcademicas->id_configuracion_mod_visitas_academicas;
		$fec = InfoSolicitudVisitasAcademicas::getFechaUltimaActualizacionOtrasConfiguraciones($id);

		$fec_ult_act = ($fec[0]['fecha_act'] == 'SV') ? "Desconocida" : $fec[0]['fecha_act'].' a las '.$fec[0]['hora'];

		$modelVaConfiguracionModVisitasAcademicas = new VaConfiguracionModVisitasAcademicas('search');
		$modelVaConfiguracionModVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaConfiguracionModVisitasAcademicas']))
			$modelVaConfiguracionModVisitasAcademicas->attributes=$_GET['VaConfiguracionModVisitasAcademicas'];

		$this->render('configuracionModuloVisitaAcademica',array(
					'modelVaConfiguracionModVisitasAcademicas'=>$modelVaConfiguracionModVisitasAcademicas,
					'fec_ult_act' => $fec_ult_act
		));
	}

	public function actionNuevoConfiguracionModuloVisitaAcademica()
	{
		$modelVaConfiguracionModVisitasAcademicas = new VaConfiguracionModVisitasAcademicas;

		if(isset($_POST['VaConfiguracionModVisitasAcademicas']))
		{
			$modelVaConfiguracionModVisitasAcademicas->attributes=$_POST['VaConfiguracionModVisitasAcademicas'];

			if($modelVaConfiguracionModVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Información actualizada correctamente!!!");
				$this->redirect(array('configuracionModuloVisitaAcademica'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al actualizar la información.");
			}
		}

		$this->render('nuevoConfiguracionModuloVisitaAcademica',array(
					  'modelVaConfiguracionModVisitasAcademicas'=>$modelVaConfiguracionModVisitasAcademicas,
		));
	}

	public function actionEditarConfiguracionModuloVisitaAcademica($id_configuracion_mod_visitas_academicas)
	{
		$modelVaConfiguracionModVisitasAcademicas = $this->loadModel($id_configuracion_mod_visitas_academicas);

		if(isset($_POST['VaConfiguracionModVisitasAcademicas']))
		{
			$modelVaConfiguracionModVisitasAcademicas->attributes = $_POST['VaConfiguracionModVisitasAcademicas'];

			if($modelVaConfiguracionModVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Codigo de Calidad registrado correctamente!!!");
				$this->redirect(array('configuracionModuloVisitaAcademica'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al actualizar la información.");
			}
		}

		$this->render('editarConfiguracionModuloVisitaAcademica',array(
					  'modelVaConfiguracionModVisitasAcademicas'=>$modelVaConfiguracionModVisitasAcademicas,
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaConfiguracionModVisitasAcademicas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelVaConfiguracionModVisitasAcademicas = VaConfiguracionModVisitasAcademicas::model()->findByPk($id);

		if($modelVaConfiguracionModVisitasAcademicas===null)
			throw new CHttpException(404,'No existe registro de Configuración');

		return $modelVaConfiguracionModVisitasAcademicas;
	}

	/**
	 * Performs the AJAX validation.
	 * @param VaConfiguracionModVisitasAcademicas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-configuracion-mod-visitas-academicas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
