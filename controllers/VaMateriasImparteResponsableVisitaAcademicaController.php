<?php
class VaMateriasImparteResponsableVisitaAcademicaController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
    }
    
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaMateriaResponsableVisitaAcademica',
								 //'agregarMateriaSolicitudVisitaAcademica',
                                 'quitarMateriaResponsableVisitaAcademica',
                                 'eliminarMateriaVisitaAcademica',
                                 'agregarMateriaCubreSolicitudVisitaAcademica', //Vista Subdirector
                                 'infoMateriaCubreSeleccionada'
				),
				'roles' => array('jefe_vinculacion', 'jefe_departamento_academico')
				//'users'=>array('@'),
			),
		);
    }

    public function actionEliminarMateriaVisitaAcademica($id_solicitud_materias_responsable_visitas_academicas, $cveMateria)
    {
        $modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
                            'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_materias_responsable_visitas_academicas,
                            'cveMateria' => $cveMateria     
                        ));
        
        if($modelVaMateriasImparteResponsableVisitaAcademica != NULL)
        {
            
            //Instanciamos el objeto del metodo CDbConnection::beginTransaction.
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                if($modelVaMateriasImparteResponsableVisitaAcademica->delete())
                {
                    Yii::app()->user->setFlash('success', "Materia eliminada correctamente!!!");
                    //Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
                    $transaction->commit();
                    //Si todo salio bien se redirige a la vista 
                    echo CJSON::encode( [ 'code' => 200 ] );
                }else{

                    Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al eliminar la Materia.");
                    //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                    $transaction->rollback();
                    //Si marco un error
                    echo CJSON::encode( $modelVaMateriasImparteResponsableVisitaAcademica->getErrors() );
                }
            }catch(Exception $e)
            {
                Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al realizar la Acción.");
                //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                $transaction->rollback();
                //Si marco un error
                echo CJSON::encode( $modelVaMateriasImparteResponsableVisitaAcademica->getErrors() );
            }
        }
    }
    
    public function actionlistaMateriaResponsableVisitaAcademica()
    {
        $modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$modelVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaSolicitudesVisitasAcademicas']))
			$VaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

		$this->render('listaMateriaResponsableVisitaAcademica',array(
					  'modelVaMateriasImparteResponsableVisitaAcademica'=>$modelVaMateriasImparteResponsableVisitaAcademica,
		));
    }

    //Vista Subdirector
    public function actionAgregarMateriaCubreSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $modelVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica;

        $VaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
                                                'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_visitas_academicas  
                                        ));
        
        if($VaMateriasImparteResponsableVisitaAcademica === NULL)
            throw new CHttpException(404,'No se encontraron datos de la Materia con la Solicitud de Visita Académica.');

        //Obtener el Depto Catedratico del Responsable
        //$infoEmpleado = $this->infoEmpleado($rfc);
        $lista_materias = $this->getMateriasDepartamentoResponsableSolicitudVisitaAcademica(); //Todas las materias

        if(isset($_POST['VaMateriasImparteResponsableVisitaAcademica']))
        {
            //Instanciamos el objeto del metodo CDbConnection::beginTransaction.
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                $modelVaMateriasImparteResponsableVisitaAcademica->attributes = $_POST['VaMateriasImparteResponsableVisitaAcademica'];
                $cve_mat = trim($modelVaMateriasImparteResponsableVisitaAcademica->cveMateria);

                //Permitir registrar como maximo 5 materias en la misma Solicitud
                if($this->numeroMateriasRegistradasEnSolicitud($VaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas))
                {

                    //Validar que la misma materia no sea registradas en la misma Solicitud de Visita Academica
                    if($this->materiaNoRepetidaSolicitudVisitaAcademica($id_solicitud_visitas_academicas, $cve_mat))
                    {
                        
                        $modelVaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas = $id_solicitud_visitas_academicas;
                        $modelVaMateriasImparteResponsableVisitaAcademica->fecha_registro_materia_visita_academica = date('Y-m-d H:i:s');

                        if($modelVaMateriasImparteResponsableVisitaAcademica->save())
                        {
                            Yii::app()->user->setFlash('succcess', "Materia agregada correctamente!!!");
                            //Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
                            $transaction->commit();
                            $this->redirect(array('agregarMateriaCubreSolicitudVisitaAcademica', 'id_solicitud_visitas_academicas'=>$id_solicitud_visitas_academicas));

                        }else{

                            //die('Error!!! ocurrio un error al agregar la Materia a la Solicitud de Visita Académica.');
                            Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al agregar la Materia a la Solicitud de Visita Académica.");
                            //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                            $transaction->rollback();
                        }

                    }else{

                        //die('Error!!! La Materia ya ha sido asignada a la Solicitud de Visita Académica.');
                        Yii::app()->user->setFlash('danger', "Error!!! La Materia ya ha sido asignada a la Solicitud de Visita Académica.");
                        //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                        $transaction->rollback();
                    }

                }else{

                    //die('Error!!! Has excedido el numero de Materias que cubre una Solicitud de Visita Académica.');
                    Yii::app()->user->setFlash('danger', "Error!!! Has excedido el numero de Materias que puede cubrir una Solicitud de Visita Académica.");
                    //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                    $transaction->rollback();
                }

            }catch(Exception $e)
            {
                //die('Error!!! ocurrio un error al realizar la Acción.');
                Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al realizar la Acción.");
                //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                $transaction->rollback();
            }
        }

        $listaVaMateriasImparteResponsableVisitaAcademica = new VaMateriasImparteResponsableVisitaAcademica('search');
		$listaVaMateriasImparteResponsableVisitaAcademica->unsetAttributes();  // clear any default values

		if(isset($_GET['VaMateriasImparteResponsableVisitaAcademica']))
			$listaVaMateriasImparteResponsableVisitaAcademica->attributes = $_GET['VaMateriasImparteResponsableVisitaAcademica'];

        $this->render('agregarMateriaCubreSolicitudVisitaAcademica',array(
            'modelVaMateriasImparteResponsableVisitaAcademica'=>$modelVaMateriasImparteResponsableVisitaAcademica,
            'listaVaMateriasImparteResponsableVisitaAcademica' => $listaVaMateriasImparteResponsableVisitaAcademica,
            'lista_materias' => $lista_materias,
            'id_solicitud_materias_responsable_visitas_academicas' => $VaMateriasImparteResponsableVisitaAcademica->id_solicitud_materias_responsable_visitas_academicas

        ));
    }

    //Se permite agregar materias de otros departamentos
    public function getMateriasDepartamentoResponsableSolicitudVisitaAcademica()
    {
        //$rfcEmpleado = trim($rfc_empleado);
		//$deptoCatedratico = trim($depto_catedratico);

		$qry_materias = "select hemp.\"rfcEmpleado\", ecm.\"cveMateria\", ecm.\"dscMateria\", ecm.\"deptoMateria\"
						from public.\"H_empleados\" hemp
						join public.\"E_gruposMaterias_new\" egm
						on egm.\"rfcEmpleadoMat\" = hemp.\"rfcEmpleado\"
						join public.\"E_catalogoMaterias\" ecm
						on ecm.\"cveMateria\" = egm.\"cveMateriaGpo\"
						where ecm.\"statMateria\" = true ";
		
		$listas_materias_responsable = Yii::app()->db->createCommand($qry_materias)->queryAll();
		$tam = sizeof($listas_materias_responsable);

		for($a=0; $a < $tam; $a++)
		{
			$lista_materias[$listas_materias_responsable[$a]['cveMateria']] = $listas_materias_responsable[$a]['cveMateria'] .'  -  '. $listas_materias_responsable[$a]['dscMateria'];
		}

		return $lista_materias;
    }

    //Para validar que una misma materia no este registrada en la misma solicitud de visita academica
    public function materiaNoRepetidaSolicitudVisitaAcademica($id_solicitud_materias_responsable_visitas_academicas, $cveMateria)
    {
        $modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
                                                'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_materias_responsable_visitas_academicas,
                                                'cveMateria' => $cveMateria
                                        ));

        return ($modelVaMateriasImparteResponsableVisitaAcademica === NULL) ? true : false;
    }

    //Para limitar el numero de materias que cubre una visita academica
    public function numeroMateriasRegistradasEnSolicitud($id_solicitud_materias_responsable_visitas_academicas)
    {
        /*$modelVaMateriasImparteResponsableVisitaAcademica = VaMateriasImparteResponsableVisitaAcademica::model()->findByAttributes(array(
                                'id_solicitud_materias_responsable_visitas_academicas' => $id_solicitud_materias_responsable_visitas_academicas
                        ));*/
        
        $id = trim($id_solicitud_materias_responsable_visitas_academicas);
        $qry_nu_mat_sol = "select * from pe_vinculacion.va_materias_imparte_responsable_visita_academica 
                            where id_solicitud_materias_responsable_visitas_academicas = '$id' ";

        $max_mat = Yii::app()->db->createCommand($qry_nu_mat_sol)->queryAll();

        return (sizeof($max_mat) >= 5 ) ? false : true;
    }

    public function actionInfoMateriaCubreSeleccionada()
    {
        if (Yii::app()->request->isAjaxRequest)
		{
            $cve_materia = trim(Yii::app()->request->getParam('cveMateria'));

            if($cve_materia != NULL)
            {
                $modelECatalogoMaterias = ECatalogoMaterias::model()->findByPk($cve_materia);
                if($modelECatalogoMaterias != NULL)
                {
                    $nombre_materia = $modelECatalogoMaterias->dscMateria;
                    $depto = $modelECatalogoMaterias->deptoMateria; //Departamento que la tiene asignada
                    //Obtenemos el departamento
                    $qry_depto_acad = "select * from public.\"H_departamentos\" where \"cveDeptoAcad\" = '$depto' ";
                    $rs = Yii::app()->db->createCommand($qry_depto_acad)->queryAll();
                    $departamento = $rs[0]['dscDepartamento'];
                    $cve_of_materia = (trim($modelECatalogoMaterias->cveOficialMat) === NULL or empty($modelECatalogoMaterias->cveOficialMat)) ? "--" : $modelECatalogoMaterias->cveOficialMat;
                    $status_campo = 'NV'; //No vacio
                }else{

                    $nombre_materia = "";
                    $departamento = "";
                    $cve_of_materia = "";
                    $status_campo = 'V'; // Vacio
                }

            }else{

                $cve_materia = "";
                $nombre_materia = "";
                $departamento = "";
                $cve_of_materia = "";
                $status_campo = "V"; // Vacio
            }

            //datos enviados al form por AJAX
			echo CJSON::encode(array(
                'cve_materia' => $cve_materia,
                'nombre_materia' => $nombre_materia,
                'departamento' => $departamento,
                'cve_of_materia' => $cve_of_materia,
                'status_campo' => $status_campo
			));

			Yii::app()->end();
        }

    }
    
}
?>