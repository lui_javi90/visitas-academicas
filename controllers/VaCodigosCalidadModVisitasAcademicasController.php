<?php

class VaCodigosCalidadModVisitasAcademicasController extends Controller
{
	
	//public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('listaCodigosCalidadVisitasAcademicas', //Vista Jefe de Vinculacion
								 'nuevoCodigoCalidadModVisitaAcademica', //Nuevo Codigo Calidad
								 'editarCodigoCalidadModVisitaAcademica', //Editar Codigo Calidad
								 'eliminarCodigoCalidadModVisitaAcademica'
								
				),
				'roles' => array('jefe_oficina_servicios_externos_vinculacion')
				//'users'=>array('@'),
			),
		);
		
	}

	public function actionListaCodigosCalidadVisitasAcademicas()
	{
		$modelVaCodigosCalidadModVisitasAcademicas = new VaCodigosCalidadModVisitasAcademicas('search');
		$modelVaCodigosCalidadModVisitasAcademicas->unsetAttributes();  // clear any default values

		if(isset($_GET['VaCodigosCalidadModVisitasAcademicas']))
		{
			$modelVaCodigosCalidadModVisitasAcademicas->attributes=$_GET['VaCodigosCalidadModVisitasAcademicas'];
		}

		$this->render('listaCodigosCalidadVisitasAcademicas',array(
					  'modelVaCodigosCalidadModVisitasAcademicas'=>$modelVaCodigosCalidadModVisitasAcademicas,
		));
	}

	public function actionNuevoCodigoCalidadModVisitaAcademica()
	{
		$modelVaCodigosCalidadModVisitasAcademicas = new VaCodigosCalidadModVisitasAcademicas;

		if(isset($_POST['VaCodigosCalidadModVisitasAcademicas']))
		{
			$modelVaCodigosCalidadModVisitasAcademicas->attributes = $_POST['VaCodigosCalidadModVisitasAcademicas'];

			if($modelVaCodigosCalidadModVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Codigo de Calidad registrado correctamente!!!");
				$this->redirect(array('listaCodigosCalidadVisitasAcademicas'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al registrar el codigo de calidad.");
			}
		}

		$this->render('nuevoCodigoCalidadModVisitaAcademica',array(
					  'modelVaCodigosCalidadModVisitasAcademicas'=>$modelVaCodigosCalidadModVisitasAcademicas,
		));
	}

	public function actionEditarCodigoCalidadModVisitaAcademica($id_codigo_calidad_mod_va)
	{
		$modelVaCodigosCalidadModVisitasAcademicas = $this->loadModel($id_codigo_calidad_mod_va);

		if(isset($_POST['VaCodigosCalidadModVisitasAcademicas']))
		{
			$modelVaCodigosCalidadModVisitasAcademicas->attributes=$_POST['VaCodigosCalidadModVisitasAcademicas'];
			if($modelVaCodigosCalidadModVisitasAcademicas->save())
			{
				Yii::app()->user->setFlash('success', "Codigo de Calidad registrado correctamente!!!");
				$this->redirect(array('listaCodigosCalidadVisitasAcademicas'));

			}else{

				Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al actualizar el codigo de calidad.");
			}
				
		}

		$this->render('editarCodigoCalidadModVisitaAcademica',array(
					  'modelVaCodigosCalidadModVisitasAcademicas'=>$modelVaCodigosCalidadModVisitasAcademicas,
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionEliminarCodigoCalidadModVisitaAcademica($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('VaCodigosCalidadModVisitasAcademicas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$modelVaCodigosCalidadModVisitasAcademicas = VaCodigosCalidadModVisitasAcademicas::model()->findByPk($id);

		if($modelVaCodigosCalidadModVisitasAcademicas === null)
			throw new CHttpException(404,'No existe información sobre el codigo de calidad.');

		return $modelVaCodigosCalidadModVisitasAcademicas;
	}

	/**
	 * Performs the AJAX validation.
	 * @param VaCodigosCalidadModVisitasAcademicas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='va-codigos-calidad-mod-visitas-academicas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
