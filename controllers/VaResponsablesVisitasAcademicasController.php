<?php
class VaResponsablesVisitasAcademicasController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
    }
    
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('eliminarResponsableVisitaAcademica', //Vista Docente
                                 'listaSolicitudesValidadasAgregarResponsables',
                                 'agregarResponsableSolicitudVisitaAcademica', //Vista para agregar responsables
                                 'infoResponsableSeleccionado', //MOstrar Informacion del Responsable seleccionado
								 
				),
				'roles' => array('jefe_vinculacion', 'jefe_departamento_academico')
				//'users'=>array('@'),
			),
		);
    }
    
    //Eliminar Responsable de la Visita Academica
    public function actionEliminarResponsableVisitaAcademica($id_solicitud_visitas_academicas, $rfcEmpleado)
    {
        $modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
                                                                    'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
                                                                    'rfcEmpleado' => $rfcEmpleado     
                                                    ));

        if($modelVaResponsablesVisitasAcademicas != NULL)
        {
            //Instanciamos el objeto del metodo CDbConnection::beginTransaction.
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                if($modelVaResponsablesVisitasAcademicas->delete())
                {
                    Yii::app()->user->setFlash('success', "Responsable eliminado correctamente!!!");
                    //Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
                    $transaction->commit();
                    //Si todo salio bien se redirige a la vista 
                    echo CJSON::encode( [ 'code' => 200 ] );

                }else{

                    Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al eliminar al Responsable.");
                    //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                    $transaction->rollback();
                    //Si marco un error
                    echo CJSON::encode( $modelVaResponsablesVisitasAcademicas->getErrors() );
                }

            }catch(Exception $e)
            {
                Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al realizar la Acción Solicitada.");
                //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                $transaction->rollback();
                echo CJSON::encode( $modelVaResponsablesVisitasAcademicas->getErrors() );
            }
        }
    }

    //Lista de las Solicitudes que fueron Aprobadas y que se les puede agregar un Responsable o materia
    public function actionListaSolicitudesValidadasAgregarResponsables()
    {
        //Tomar del inicio de sesion del Empleado en el SII
        //$rfc_empleado = Yii::app()->params['rfcJefeProyectosVinculacion']; // RFC del Empleado que tenga el rol de Jefe de Proyectos de Vinculacion
        $rfc_empleado = Yii::app()->user->name;
        $rfcEmpleado = trim($rfc_empleado);

        //Comprobar que es Jefe de Proyectos de Vinculacion
        $is_jefe_proyvinc = $this->esJefeProyectosVinculacion($rfcEmpleado);

        //Obtener Departamento Catedratico
        $depto_catedratico = $this->getDeptoCatedratico($rfcEmpleado);

        //Comprobra si es jefe de departamento academico
        $is_jefe_depto_academico = $this->esJefeDepartamentoAcademico($depto_catedratico, $rfcEmpleado);

        if($is_jefe_proyvinc == true || $is_jefe_depto_academico == true)
        {
            $modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
            $modelVaResponsablesVisitasAcademicas->unsetAttributes();

            if(isset($_GET['VaResponsablesVisitasAcademicas']))
                $modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];
                
            $this->render('listaSolicitudesValidadasAgregarResponsables',array(
                          'modelVaResponsablesVisitasAcademicas'=>$modelVaResponsablesVisitasAcademicas,
                          'is_jefe_proyvinc' => $is_jefe_proyvinc,
                          'is_jefe_depto_academico' => $is_jefe_depto_academico,
                          'depto_catedratico' => $depto_catedratico,
                          'rfcEmpleado' => $rfcEmpleado
            ));

        }else{

            //No es jefe de proyecto vinculacion y jefe departamento academico
            $modelVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
            $modelVaResponsablesVisitasAcademicas->unsetAttributes();

            if(isset($_GET['VaResponsablesVisitasAcademicas']))
                $modelVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];
                
            $this->render('listaSolicitudesValidadasAgregarResponsables',array(
                          'modelVaResponsablesVisitasAcademicas'=>$modelVaResponsablesVisitasAcademicas,
                          'is_jefe_proyvinc' => $is_jefe_proyvinc,
                          'is_jefe_depto_academico' => $is_jefe_depto_academico,
                          'depto_catedratico' => $depto_catedratico,
                          'rfcEmpleado' => $rfcEmpleado
            ));

        }
    }

    //Si es jefe de proyectos de vinculacion de algun departamento academico
    public function esJefeProyectosVinculacion($rfcEmpleado)
    {
        //Jefe Proyectos Vinculacion
        $qry_jef_proyvinc = "select * from public.h_ocupacionpuesto
                                where \"rfcEmpleado\" = '$rfcEmpleado' AND \"cvePuestoGeneral\" = '04' AND
                                \"cvePuestoParticular\" = '33'
                            ";

        $jefe_proy_vinc = Yii::app()->db->createCommand($qry_jef_proyvinc)->queryAll();

        return (sizeof($jefe_proy_vinc) > 0) ? true : false;
    }

    public function esJefeDepartamentoAcademico($depto_acad, $rfcEmpleado)
    {
        /*echo $depto_acad."<br>";
        echo $rfcEmpleado;
        die;*/

        $qry_jefe_depto_acad = "select * from public.\"H_empleados\" hemp
                                join public.\"H_departamentos\" dep
                                on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
                                where dep.\"cveDeptoAcad\" = '$depto_acad' AND hemp.\"cvePuestoGeneral\" = '03' AND 
                                hemp.\"rfcEmpleado\" = '$rfcEmpleado'
                            ";

        $jefe_depto_acad = Yii::app()->db->createCommand($qry_jefe_depto_acad)->queryAll();

        return (sizeof($jefe_depto_acad) != NULL or !empty($jefe_depto_acad)) ? true : false;
    }

    public function getDeptoCatedratico($rfcEmpleado)
    {
        //Paras saber si es jefe de proyecto de su departamento
        $qry_puest = "select hemp.\"rfcEmpleado\", dep.\"cveDeptoAcad\", dep.\"cveDepartamento\"
                        from public.\"H_empleados\" hemp
                        join public.\"H_departamentos\" dep
                        on dep.\"cveDepartamento\" = hemp.\"cveDepartamentoEmp\"
                        where hemp.\"cvePuestoGeneral\" = '04' AND hemp.\"cvePuestoParticular\" = '33' AND
                            hemp.\"rfcEmpleado\" = '$rfcEmpleado' ";

        $rs = Yii::app()->db->createCommand($qry_puest)->queryAll();

        if(sizeof($rs) > 0)
        {
            if($rs[0]['cveDeptoAcad'] > 0)
                return trim($rs[0]['cveDeptoAcad']); //1
            else
                return 0;

        }else{

            return 0;
        }

    }

    public function actionAgregarResponsableSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $VaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas;

        $modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
                                                                    'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas
                                                                ));
        if($modelVaResponsablesVisitasAcademicas === NULL)
            throw new CHttpException(404,'No se encontraron datos del Responsable de esa Solicitud.');

        $modelVaSolicitudesVisitasAcademicas = VaSolicitudesVisitasAcademicas::model()->findByPk($id_solicitud_visitas_academicas);
        if($modelVaSolicitudesVisitasAcademicas === NULL)
            throw new CHttpException(404,'No se encontraron datos de la Solicitud.');

        //Lista de responsables para agregar
        $lista_responsables = $this->getListaResponsablesVisitasAcademicas();
        
        if(isset($_POST['VaResponsablesVisitasAcademicas']))
        {
            //Instanciamos el objeto del metodo CDbConnection::beginTransaction.
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                $VaResponsablesVisitasAcademicas->attributes = $_POST['VaResponsablesVisitasAcademicas'];
                $rfc = trim($VaResponsablesVisitasAcademicas->rfcEmpleado);

                //El macimo numero de responsables asignados a una misma Solicitud es de 5
                if($this->getMaximoNumeroResponsablesSolicitudVisitaAcademica($modelVaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas))
                {

                    //Si no ha sido agregado el Responsable a esa Visita Academica entonces se puede registrar
                    if($this->responsableNoRepetido($rfc, $modelVaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas))
                    {
                        //Obtener el Depto Catedratico del Responsable
                        $infoEmpleado = $this->infoEmpleado($rfc);
                        $VaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas = $id_solicitud_visitas_academicas;
                        $VaResponsablesVisitasAcademicas->fecha_registro_responsable = date('Y-m-d H:i:s');
                        $VaResponsablesVisitasAcademicas->responsable_principal = false;
                        $VaResponsablesVisitasAcademicas->cve_depto_acad = $infoEmpleado->deptoCatedratico;
                        $VaResponsablesVisitasAcademicas->cve_depto = $infoEmpleado->cveDepartamentoEmp;

                        if($VaResponsablesVisitasAcademicas->save())
                        {
                            Yii::app()->user->setFlash('succcess', "Responsable agregado correctamente!!!");
                            //Si todas los Inserts se ejecutaron correctamente entonces hacemos el commit
                            $transaction->commit();
                            $this->redirect(array('agregarResponsableSolicitudVisitaAcademica', 'id_solicitud_visitas_academicas'=>$id_solicitud_visitas_academicas));

                        }else{

                            //die('4');
                            Yii::app()->user->setFlash('danger', "Error!!! ocurrio un error al agregar al Responsable.");
                            //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                            $transaction->rollback();
                        }

                    }else{

                        //die('3');
                        Yii::app()->user->setFlash('danger', "Error!!! Este Responsable ya ha sido agregado a la Solicitud de Visita Académica.");
                        //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                        $transaction->rollback();
                    }

                }else{

                    //die('2');
                    Yii::app()->user->setFlash('danger', "Error!!! Solo se puede agregar como maximo 5 responsables a una misma Solictud de Visita Académica.");
                    //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                    $transaction->rollback();
                }

            }catch(Exception $e)
            {
                //die('1');
                Yii::app()->user->setFlash('danger', "Error!!! Ocurrio un error al realizar la Acción Solicitada.");
                //Si Ocurrio algun error en una consulta se deshace todas las consultas que se habian realizado
                $transaction->rollback();
            }
        }

        $listaVaResponsablesVisitasAcademicas = new VaResponsablesVisitasAcademicas('search');
        $listaVaResponsablesVisitasAcademicas->unsetAttributes();

        if(isset($_GET['VaResponsablesVisitasAcademicas']))
            $listaVaResponsablesVisitasAcademicas->attributes = $_GET['VaResponsablesVisitasAcademicas'];

        $this->render('agregarResponsableSolicitudVisitaAcademica', array(
                      'modelVaResponsablesVisitasAcademicas'=>$modelVaResponsablesVisitasAcademicas,
                      'modelVaSolicitudesVisitasAcademicas' => $modelVaSolicitudesVisitasAcademicas,
                      'VaResponsablesVisitasAcademicas' => $VaResponsablesVisitasAcademicas, //Agregar Nuevo Registro
                      'listaVaResponsablesVisitasAcademicas' => $listaVaResponsablesVisitasAcademicas, //Listar Responsables
                      'lista_responsables' => $lista_responsables,
                      'id_solicitud_visitas_academicas' => $modelVaResponsablesVisitasAcademicas->id_solicitud_visitas_academicas
                    
        ));
    }

    //controlar el maximo numero de Responsables por Solicitud
    public function getMaximoNumeroResponsablesSolicitudVisitaAcademica($id_solicitud_visitas_academicas)
    {
        $qry_num_resp = "select * from pe_vinculacion.va_responsables_visitas_academicas 
                        where id_solicitud_visitas_academicas = '$id_solicitud_visitas_academicas' ";

        $rs = Yii::app()->db->createCommand($qry_num_resp)->queryAll();

        return (sizeof($rs) < 5) ? true : false;
    }

    //Validar que no se registre un mismo responsable mas de una vez en la visita academica
    public function responsableNoRepetido($rfc, $id_solicitud_visitas_academicas)
    {
        $modelVaResponsablesVisitasAcademicas = VaResponsablesVisitasAcademicas::model()->findByAttributes(array(
                                                'id_solicitud_visitas_academicas' => $id_solicitud_visitas_academicas,
                                                'rfcEmpleado' => $rfc
        ));

        return ($modelVaResponsablesVisitasAcademicas === NULL) ? true : false;
    }
    
    public function infoEmpleado($rfc_empleado)
	{
		$rfcEmpleado = trim($rfc_empleado);

        $modelHEmpleados = HEmpleados::model()->findByPk($rfcEmpleado);
        
		if($modelHEmpleados === NULL)
			throw new CHttpException(404,'No hay datos del Empleado.');

		return $modelHEmpleados;
	}

    public function getListaResponsablesVisitasAcademicas()
	{
		$criteria = new CDbCriteria();
        //$criteria->condition = " \"rfcEmpleado\" is not null AND \"nmbCompletoEmp\" is not null";
        $criteria->condition = " \"statEmpleado\" in ('01','02', '06', '07')";
        $modelHEmpleados = HEmpleados::model()->findAll($criteria);

        foreach ($modelHEmpleados as $model)
            $list_emp[$model->rfcEmpleado] = $model->rfcEmpleado .'  -  '. $model->nmbCompletoEmp;  

		return $list_emp;
    }
    
    public function actionInfoResponsableSeleccionado()
    {
        if (Yii::app()->request->isAjaxRequest)
		{
            $rfc_empleado = trim(Yii::app()->request->getParam('rfcEmpleado'));

            if($rfc_empleado != NULL)
            {
                $modelHEmpleados = HEmpleados::model()->findByPk($rfc_empleado);

                if($modelHEmpleados != NULL)
                {
                    $nombre_completo = trim($modelHEmpleados->nmbEmpleado).' '.trim($modelHEmpleados->apellPaterno).' '.trim($modelHEmpleados->apellMaterno);

                    //Obtenemos el Departamento al que pertenece el Empleado o Responsable
                    //$modelHDepartamentos = HDepartamentos::model()->findByPk($modelHEmpleados->cveDepartamentoEmp);
                    $qry_depto = "select * from public.\"H_departamentos\" where \"cveDepartamento\" = '$modelHEmpleados->cveDepartamentoEmp' ";
                    $rs_depto = Yii::app()->db->createCommand($qry_depto)->queryAll();

                    if(sizeof($rs_depto) > 0)
                        $departamento = $rs_depto[0]['dscDepartamento'];
                    else
                        $departamento = "DESCONOCIDO";

                    //Obtenemos el Puesto del Empleado o Responsable
                    $qry_puesto = "select * from public.h_ocupacionpuesto where \"rfcEmpleado\" = '$rfc_empleado' ";
                    $rs_puesto = Yii::app()->db->createCommand($qry_puesto)->queryAll();

                    if(sizeof($rs_puesto) > 0)
                        $puesto = $rs_puesto[0]['nmbPuesto']; //Puesto del Empleado
                    else
                        $puesto = "NO DEFINIDO";

                    
                    $status_campo = 'NV'; //RFC No Vacio

                }else{

                    $rfc_empleado = "";
                    $nombre_completo = "";
                    $departamento = "";
                    $puesto = "";
                    $status_campo = "V"; //RFC Vacio

                }

            }else{

                $rfc_empleado = "";
                $nombre_completo = "";
                $departamento = "";
                $puesto = "";
                $status_campo = 'NER'; //No Existe Responsable en la BD con ese RFC
            }

            //datos enviados al form por AJAX
			echo CJSON::encode(array(
                'rfc_empleado' => $rfc_empleado,
                'nombre_completo' => $nombre_completo,
                'departamento' => $departamento,
                'puesto' => $puesto,
                'status_campo' => $status_campo
			));

			Yii::app()->end();
        }
    }

}


?>